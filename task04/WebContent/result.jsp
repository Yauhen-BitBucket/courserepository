<?xml version = "1.0" encoding = "UTF-8" ?>
<jsp:root xmlns:jsp = "http://java.sun.com/JSP/Page" version = "2.0"
          xmlns:c = "http://java.sun.com/jsp/jstl/core"
          xmlns:fmt = "http://java.sun.com/jsp/jstl/fmt"
          xmlns:func = "http://www.task04.training.by/FunctionLibrary">
    <jsp:directive.page contentType = "text/html; UTF-8" 
                        pageEncoding = "UTF-8" isErrorPage = "true"/>
    <jsp:output doctype-root-element = "html"
            doctype-public = "-//W3C//DTD XHTML 1.0 Transitional//EN"
            doctype-system = "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
            omit-xml-declaration = "true" />
    <fmt:setLocale value = "${(not empty locale) ? locale : defaultLocale}"/>
    <fmt:setBundle basename = "by.training.task04.bundle.PageLabels"/>
    <fmt:message key = "table.cell.absence.value" var = "absenceValue"/>
    <c:url var = "localeChangingURL" 
           value = "${controllerPath}">
        <c:param name = "command" 
                 value = "locale-changing-command" />
        <c:param name = "pathPropertyKey" 
                 value = "${resultPagePathKey}" />
    </c:url>
    <c:url var = "welcomePageURL" 
           value = "${controllerPath}">
        <c:param name = "command" 
                 value = "redirection-command" />
        <c:param name = "pathPropertyKey" 
                 value = "${welcomePagePathKey}" />
    </c:url>
    <c:choose>
       <c:when test = "${(empty pageContext.errorData.servletName)
                         and (empty errorCode)}">
           <c:set var = "pageTitle">
               <fmt:message key = "result.page.title"/>
           </c:set>
           <c:if test = "${empty technology}">
               <c:set var = "technology" scope = "session"
                      value = "${param['technology']}"/>
           </c:if>
           <c:choose>
               <c:when test = "${technology == 'SAX'}">
                   <c:set var = "pageHeader">
                       <fmt:message key = "result.page.sax.header"/>
                   </c:set>
               </c:when>
               <c:when test = "${technology == 'DOM'}">
                   <c:set var = "pageHeader">
                       <fmt:message key = "result.page.dom.header"/>
                   </c:set>
               </c:when>
               <c:otherwise>
                   <c:set var = "pageHeader">
                       <fmt:message key = "result.page.stax.header"/>
                   </c:set>
               </c:otherwise>
           </c:choose>
       </c:when>
       <c:otherwise>
           <c:set var = "pageTitle">
               <fmt:message key = "error.page.title"/>
           </c:set>
           <c:set var = "pageHeader">
               <fmt:message key = "error.page.header"/>
           </c:set>
           <c:if test = "${not empty pageContext.errorData.servletName}">
               <c:set var = "errorCode"
                      value = "${pageContext.errorData.statusCode}"
                      scope = "session"/>
           </c:if>
       </c:otherwise>
    </c:choose>
    <html xmlns = "http://www.w3.org/1999/xhtml">
        <head>
            <title>${pageTitle}</title>
            <link href = "css/style.css" rel = "stylesheet" 
                  type = "text/css" />
        </head>
        <body>
            <h1>${pageHeader}</h1>
            <div id = "result" class = "basic-navigation">
                <a href = "${localeChangingURL}">
                    <fmt:message key = "locale.changig.reference.label"/>
                </a>
                <a href = "${welcomePageURL}">
                    <fmt:message key = "welcome.page.reference.label"/>
                </a>
            </div>
            <c:choose>
                <c:when test = "${(not empty cards)
                                  and (empty pageContext.errorData.servletName)
                                  and (empty errorCode)}">
	               <table>
                       <thead>
                           <tr>
                               <th rowspan = "2">
                                   <fmt:message key = "identifier.table.header"/>
                               </th>
                               <th rowspan = "2">
                                   <fmt:message key = "image.theme.table.header"/>
                               </th>
                               <th rowspan = "2">
                                   <fmt:message key = "producing.country.table.header"/>
                               </th>
                               <th rowspan = "2">
                                   <fmt:message key = "year.table.header"/>
                               </th>
                               <th rowspan = "2">
                                   <fmt:message key = "author.table.header"/>
                               </th>
                               <th rowspan = "2">
                                   <fmt:message key = "value.table.header"/>
                               </th>
                               <th colspan = "4">
                                   <fmt:message key = "destination.address.table.header"/>
                               </th>
                               <th rowspan = "2">
                                   <fmt:message key = "sent.state.table.header"/>
                               </th>
                               <th rowspan = "2">
                                   <fmt:message key = "card.type.table.header"/>
                               </th>
                               <th rowspan = "2">
                                   <fmt:message key = "issue.stamp.table.header"/>
                               </th>
                           </tr>
                           <tr>
                                <th>
                                   <fmt:message key = "destination.city.table.header"/>
                               </th>
                               <th>
                                   <fmt:message key = "destination.country.table.header"/>
                               </th>
                               <th>
                                   <fmt:message key = "destination.street.table.header"/>
                               </th>
                               <th>
                                   <fmt:message key = "destination.code.table.header"/>
                               </th>
                           </tr>
                       </thead>
                       <tbody>
                           <c:forEach var = "card" items = "${cards}">
                               <tr>
                                   <td>
                                       ${card.number}
                                   </td>
                                   <td>
                                       ${card.theme}
                                   </td>
                                   <td>
                                       ${card.country}
                                   </td>
                                   <td>
                                       ${card.year}
                                   </td>
                                   <td>
                                       <c:choose>
                                           <c:when test="${not empty card.authors}">
                                               <c:forEach var = "author"
	                                                      varStatus = "iterationStatus"
			                                              items = "${card.authors}">
			                                       ${author}${(func:testLastIteration(iterationStatus))
			                                                  ? "" : ", "}
		                                       </c:forEach>
                                           </c:when>
                                           <c:otherwise>
                                               ${absenceValue}
                                           </c:otherwise>
                                       </c:choose>
                                   </td>
                                   <td>
                                       ${card.value}
                                   </td>
                                   <td>
                                       ${card.destination.city}
                                   </td>
                                   <td>
                                       ${card.destination.country}
                                   </td>
                                   <td>
                                       ${card.destination.street}
                                   </td>
                                   <td>
                                       ${(func:testPropertyExistence(card.destination,
                                               "postalCode"))
                                         ? card.destination.postalCode
                                         : absenceValue}
                                   </td>
                                   <td>
                                       ${card.sentState}
                                   </td>
                                   <td>
                                       ${card.type}
                                   </td>
                                   <td>
                                       ${card.issueStamp}
                                   </td>
                               </tr>
                           </c:forEach>
                       </tbody>
                   </table>
	           </c:when>
	           <c:when test = "${(empty cards)
                                  and (empty pageContext.errorData.servletName)
                                  and (empty errorCode)}">
	               <div class = "error-message-container">
	                   <fmt:message key = "data.absence.message"/>
	               </div>
	           </c:when>
	           <c:otherwise>
	               <div class = "error-message-container">
	                   <c:choose>
	                       <c:when test = "${(pageContext.errorData.statusCode == 404)
	                                         or (errorCode == 404)}">
	                           <fmt:message key = "resource.absence.error.message"/>
	                       </c:when>
	                       <c:otherwise>
	                           <fmt:message key = "general.error.message"/>
	                       </c:otherwise>
	                   </c:choose>
	               </div>
	           </c:otherwise>
            </c:choose>
        </body>
    </html>
</jsp:root>
