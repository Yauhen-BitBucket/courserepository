<?xml version = "1.0" encoding = "UTF-8"?>
<jsp:root xmlns:jsp = "http://java.sun.com/JSP/Page" version = "2.0"
          xmlns:c = "http://java.sun.com/jsp/jstl/core"
          xmlns:fmt = "http://java.sun.com/jsp/jstl/fmt">
    <jsp:directive.page contentType = "text/html; charset=UTF-8"
                        pageEncoding = "UTF-8"/>
    <jsp:output doctype-root-element = "html"
            doctype-public = "-//W3C//DTD XHTML 1.0 Transitional//EN"
            doctype-system = "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
            omit-xml-declaration = "true" />
    <fmt:setLocale value = "${(not empty locale) ? locale : defaultLocale}"/>
    <fmt:setBundle basename = "by.training.task04.bundle.PageLabels"/>
    <c:url var = "localeChangingURL" 
           value = "${controllerPath}">
        <c:param name = "command" 
                 value = "locale-changing-command" />
        <c:param name = "pathPropertyKey" 
                 value = "${welcomePagePathKey}" />
    </c:url>
    <c:url var = "parsingURL" 
           value = "${controllerPath}">
    </c:url>
    <c:remove var ="errorCode" scope = "session"/>
    <c:remove var ="technology" scope = "session"/>
    <html xmlns = "http://www.w3.org/1999/xhtml">
	    <head>
	        <title><fmt:message key = "welcome.page.title"/></title>
	        <link href = "css/style.css" rel = "stylesheet" 
                  type = "text/css" />
	    </head>
	    <body>
	       <h1><fmt:message key = "welcome.page.header"/></h1>
	        <div id = "welcome" class = "basic-navigation">
                <a href = "${localeChangingURL}">
                    <fmt:message key = "locale.changig.reference.label"/>
                </a>
            </div>
	        <div>
	           <form method = "post" action = "${parsingURL}"
	                 enctype = "multipart/form-data">
	               <div>
	                   <label for = "file-input" >
	                       <fmt:message key = "welcome.page.input.file.label"/>
	                   </label>
	                   <input id = "file-input" type = "file" name = "file"
	                          accept = ".xml"/>
	               </div>
	               <div>
	                   <label for = "technology-input">
	                       <fmt:message key = "welcome.page.selection.label"/>
	                   </label>
	                   <select id = "technology-input" name = "technology">
	                       <option>SAX</option>
	                       <option>DOM</option>
	                       <option value = "STAX">StAX</option>
	                   </select>
	               </div>
	               <div>
	                   <button type = "submit">
	                       <fmt:message key = "welcome.page.submit.button.label"/>
	                   </button>
	               </div>
	               <div>
	                   <input type = "hidden" name = "command"
	                          value = "parsing-command"/>
	               </div>
	           </form>
	        </div>
	    </body>
    </html>
</jsp:root>
