XML\XSD & Web-Parsing

There is an XML and XML schema documents. It's necessary to parse the XML
document applying the schema. Parsers used: SAX, DOM, StAX. When on web: you
should upload the XML document, it should be available to choose a parser.
Parsing results must be output in a table.

An application has the following structure (packages):

 - by.training.task04.builder               there are parsers here
 - by.training.task04.controller            for a controller
 - by.training.task04.controller.command    commands performed by the
                                            controller
 - by.training.task04.entity                for entities
 - by.training.task04.dao                   DAO objects
 - by.training.task04.exception             exceptions used by the application
 - by.training.task04.main                  contains a class which is an entry
                                            point of this application
 - by.training.task04.reader                contains a reader for reading
                                            application data
 - by.training.task04.service               services of this application
 - test.task04.service                      for testing

Used technologies: Log4j2, TestNG, Servlet/JSP.

Needed libraries: log4j-api-2.11.2.jar, log4j-core-2.11.2.jar,
commons-beanutils-1.9.2.jar, commons-collections-3.2.2.jar,
commons-logging-1.2.jar, jcommander-1.72.jar, jstl.jar, standard.jar,
testng-6.9.9.jar,
!!! sheme.jar (it's manually created and contains a schema
definition; this library resides in WebContent/WEB-INF/lib) !!!.

|-------------------------------------------------------------------|
|                                                                   |
|Important note: allowCasualMultipartParsing="true" must be added to|
|                the 'Context' element in Server.xml file.          |
|                                                                   |
|-------------------------------------------------------------------|
