/**
 * Provides classes for an interaction with a data source.
 * @author Yauhen Sazonau
 * @since 1.0
 */
package by.training.task04.dao;
