/* FileCardDocumentDAO.java
 * A card document DAO implementation which is based on a file data source.
 */
package by.training.task04.dao;


import by.training.task04.exception.ApplicationException;

import by.training.task04.reader.DataReader;

import java.io.Reader;
import java.io.StringReader;


/**
 * A card document DAO implementation which is based on a file data source.
 * @version 1.0, 06/06/19
 * @since 1.0
 */
public class FileCardDocumentDAO implements CardDocumentDAO {
    /**
     * Specifies a relative path to the file which information will be read
     * from. The identifier holds the value of "data/input.txt".
     */
    public static final String FILE_PATH = "data/OldCards.xml";
    /**
     * Retrieves a card document from a data source.
     * @return a card document from a data source
     * @throws DAOException if there are problems with retrieving a card
     *                      document from a data source
     */
    @Override
    public Reader retrieveCardDocument() throws DAOException {
        String text;
        /* The main part of the method. */
        try {
            text = new DataReader().readData(FILE_PATH);
        } catch (ApplicationException e) {
            throw new DAOException("Can't retrieve matrix data.", e);
        }
        return new StringReader(text);
    }
}
