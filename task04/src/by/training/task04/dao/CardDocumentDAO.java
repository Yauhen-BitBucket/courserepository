/* CardDocumentDAO.java
 * A contract which card document DAO implementations should comply with.
 */
package by.training.task04.dao;


import java.io.Reader;


/**
 * A contract which card document DAO implementations should comply with.
 * @version 1.0, 06/06/19
 * @since 1.0
 */
public interface CardDocumentDAO {
    /**
     * Retrieves a card document from a data source.
     * @return a card document from a data source
     * @throws DAOException if there are problems with retrieving a card
     *                      document from a data source
     */
    Reader retrieveCardDocument() throws DAOException;
}
