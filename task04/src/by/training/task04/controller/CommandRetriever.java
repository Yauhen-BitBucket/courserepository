/* CommandRetriever.java
 * A command retriever.
 */
package by.training.task04.controller;


import java.util.EnumMap;
import java.util.Map;

import by.training.task04.controller.command.Command;
import by.training.task04.controller.command.CommandName;
import by.training.task04.controller.command.DOMParsingCommand;
import by.training.task04.controller.command.ExitCommand;
import by.training.task04.controller.command.ReportPrintingCommand;
import by.training.task04.controller.command.SAXParsingCommand;
import by.training.task04.controller.command.StAXParsingCommand;


/**
 * A command retriever.
 * @version 1.0, 06/04/19
 * @since 1.0
 */
class CommandRetriever {
    /**
     * A mapping of command names to command implementations. <code>null</code>
     * value is prohibited.
     */
    private final Map<CommandName, Command> commandMapping
            = new EnumMap<>(CommandName.class);
    /**
     * Creates an instance of this class.
     */
    CommandRetriever() {
        commandMapping.put(CommandName.DOM_PARSING_COMMAND,
                new DOMParsingCommand());
        commandMapping.put(CommandName.SAX_PARSING_COMMAND,
                new SAXParsingCommand());
        commandMapping.put(CommandName.STAX_PARSING_COMMAND,
                new StAXParsingCommand());
        commandMapping.put(CommandName.EXIT_COMMAND, new ExitCommand());
        commandMapping.put(CommandName.REPORT_PRINTING_COMMAND,
                new ReportPrintingCommand());
    }
    /**
     * Retrieves a command using a command name.
     * @param name the <code>String</code> value which is the name of command
     *             to retrieve. It should be a hyphen-joined string, a word
     *             case daoen't matter. Accepted values are
     *             "sax-parsing-command", "dom-parsing-command",
     *             "stax-parsing-command", "report-printing-command",
     *             "exit-command".
     * @return a command using a command name. <code>null</code> value is
     *         prohibited. If a command name isn't known, the exit command is
     *         returned.
     */
    Command retrieveCommand(final String name) {
        Command command = null;
        CommandName commandName = null;
        String processedName;
        /* The main part of the method. */
        try {
            processedName = name.toUpperCase();
            processedName = processedName.replace("-", "_");
            commandName = CommandName.valueOf(processedName);
            command = commandMapping.get(commandName);
        } catch (NullPointerException | IllegalArgumentException e) {
            command = commandMapping.get(CommandName.EXIT_COMMAND);
        }
        return command;
    }
}
