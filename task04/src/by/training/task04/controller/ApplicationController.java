/* ApplicationController.java
 * A controller of this application.
 */
package by.training.task04.controller;


import by.training.task04.controller.command.Command;
import by.training.task04.controller.command.ExitCommand;

import by.training.task04.exception.ApplicationException;


/**
 * A controller of this application.
 * @version 1.0, 06/10/19
 * @since 1.0
 */
public class ApplicationController {
    /**
     * A command retriever. <code>null</code> value is prohibited.
     */
    private final CommandRetriever commandRetriever = new CommandRetriever();
    /**
     * Performs an incoming request.
     * @param commandName the <code>String</code> value which is the name of
     *                    command to execute. It should be a hyphen-joined
     *                    string, a word case daoen't matter. Accepted values
     *                    are "element-setting-command",
     *                    "report-printing-command".
     * @param exitStateKeeper the <code>ExitStateKeeper</code> value which is
     *                        an object used to control the exit state
     * @throws ApplicationException if the <code>exitStateKeeper</code>
     *                              argument is a <code>null</code> value
     */
    public void executeTask(final String commandName,
            final ExitStateKeeper exitStateKeeper) {
        Command command;
        if (exitStateKeeper == null) {
            throw new ApplicationException("The 'exitStateKeeper' argument is "
                    + "a 'null' value.");
        }
        /* The main part of the method. */
        command = commandRetriever.retrieveCommand(commandName);
        command.execute();
        if (command instanceof ExitCommand) {
            exitStateKeeper.setExitState(true);
        }
    }
}
