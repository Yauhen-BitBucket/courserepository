/* Command.java
 * A contract which a particular command should comply with.
 */
package by.training.task04.controller.command;


/**
 * A contract which a particular command should comply with.
 * @version 1.0, 06/04/19
 * @since 1.0
 */
public interface Command {
    /**
     * Performs actions represented by a particular command.
     * @throws ApplicationException if there are problems with performing
     *                              actions represented by a particular command
     */
    void execute();
}
