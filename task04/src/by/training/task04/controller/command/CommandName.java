/* CommandName.java
 * A command name. The following commands are considered: a command of
 * retrieving postal cards using a DOM parser, a command of retrieving postal
 * cards using a StAX parser, a command of retrieving postal cards using a StAX
 * parser, a command of exiting an application, a command of printing postal
 * card information.
 */
package by.training.task04.controller.command;


/**
 * A command name. The following commands are considered: a command of
 * retrieving postal cards using a DOM parser, a command of retrieving postal
 * cards using a StAX parser, a command of retrieving postal cards using a StAX
 * parser, a command of exiting an application, a command of printing postal
 * card information.
 * @version 1.0, 06/10/19
 * @since 1.0
 */
public enum CommandName {
    /**
     * Represents a command of retrieving postal cards using a DOM parser.
     */
    DOM_PARSING_COMMAND,
    /**
     * Represents a command of retrieving postal cards using a SAX parser.
     */
    SAX_PARSING_COMMAND,
    /**
     * Represents a command of retrieving postal cards using a StAX parser.
     */
    STAX_PARSING_COMMAND,
    /**
     * Represents a command of exiting an application.
     */
    EXIT_COMMAND,
    /**
     * Represents a command of printing postal card information.
     */
    REPORT_PRINTING_COMMAND
}
