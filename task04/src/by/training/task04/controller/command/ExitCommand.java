/* ExitCommand.java
 * A command used to exit an application.
 */
package by.training.task04.controller.command;


/**
 * A command used to exit an application.
 * @version 1.0, 06/04/19
 * @since 1.0
 */
public class ExitCommand implements Command {
    /**
     * Performs actions needed to exit an application.
     */
    @Override
    public void execute() {
        /* Does nothing. */
    }
}
