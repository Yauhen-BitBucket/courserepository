/* StAXParsingCommand.java
 * A command used to retrieve postal cards using a StAX parser.
 */
package by.training.task04.controller.command;


import by.training.task04.exception.ApplicationException;

import by.training.task04.service.CardService;
import by.training.task04.service.ServiceException;
import by.training.task04.service.ServiceFactory;

import by.training.task04.service.CardService.XMLParserName;


/**
 * A command used to retrieve postal cards using a StAX parser.
 * @version 1.0, 06/10/19
 * @since 1.0
 */
public class StAXParsingCommand implements Command {
    /**
     * Performs actions needed to retrieve postal cards using a StAX parser.
     * @throws ApplicationException if there are problems with performing
     *                              actions represented by this command
     */
    @Override
    public void execute() {
        ServiceFactory factory = ServiceFactory.getInstance();
        CardService service = factory.retrieveCardService();
        /* The main part of the method. */
        try {
            ReportPrintingCommand.setPostalCards(
                    service.retrieveCards(XMLParserName.STAX));
        } catch (ServiceException e) {
            throw new ApplicationException("There are problems with executing "
                    + "the command of retrieving postal cards using a "
                    + "StAX parser.", e);
        }
    }
}
