/* ReportPrintingCommand.java
 * A command used to print postal card information.
 */
package by.training.task04.controller.command;


import by.training.task04.entity.Card;

import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * A command used to print postal card information.
 * @version 1.0, 06/10/19
 * @since 1.0
 */
public class ReportPrintingCommand implements Command {
    /**
     * A logger for this application. <code>null</code> value is prohibited.
     */
    private static final Logger LOGGER
            = LogManager.getLogger("by.training.task04.controller.command");
    /**
     * Postal cards to be printed. <code>null</code> value is allowed.
     */
    private static Set<Card> postalCards;
    /**
     * Performs actions needed to print postal card information.
     * @throws ApplicationException if there are problems with performing
     *                              actions represented by this command
     */
    @Override
    public void execute() {
        if ((postalCards != null) && (!postalCards.isEmpty())) {
            LOGGER.info(String.format("%-10s %-14s %-14s %-7s %-50s %-10s "
                    + "%-100s %-7s %-14s %-19s", "Identifier", "Image theme",
                    "Country", "Year", "Authors", "Value",
                    "Destination address", "Is sent", "Card type",
                    "Issue stamp"));
            for (Card card : postalCards) {
                LOGGER.info(String.format(
                        "%-10s %-14s %-14s %-7d %-50s %-10s %-100s %-7s "
                        + "%-14s %-19s", card.getNumber(), card.getTheme(),
                        card.getCountry(), card.getYear(), card.getAuthors(),
                        card.getValue(), card.getDestination(),
                        card.getSentState(), card.getType(),
                        card.getIssueStamp()));
            }
        } else {
            LOGGER.info("There is no information available.");
        }
    }
    /**
     * Sets postal cards to be printed.
     * @param cards the <code>Set</code> value which is a set of postal cards
     *              to be printed
     */
    public static void setPostalCards(final Set<Card> cards) {
        postalCards = cards;
    }
}
