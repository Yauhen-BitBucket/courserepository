/*
 * DataReader.java
 * A reader. It's used to read the whole text stored in a text file.
 */
package by.training.task04.reader;


import java.io.IOException;

import java.nio.charset.StandardCharsets;

import java.nio.file.Files;
import java.nio.file.Paths;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import by.training.task04.exception.ApplicationException;


/**
 * A reader. It's used to read the whole text stored in a text file.
 * @author Yauhen Sazonau
 * @version 1.0, 06/06/19
 * @since 1.0
 */
public class DataReader {
    /**
     * Constructs an instance of this class.
     */
    public DataReader() {
        /* The default initialization is sufficient. */
    }
    /**
     * Reads the whole text stored in a text file.
     * @param filePath the <code>String</code> value which is a relative path
     *                 to the file which a text will be read from
     * @return the whole text stored in a text file
     * @throws ApplicationException if there are problems with reading
     *                              a text or a target file is empty
     */
    public String readData(final String filePath) {
        String result;
        Stream<String> stream = null;
        /* The main part of the method. */
        try {
            stream = Files.lines(Paths.get(filePath), StandardCharsets.UTF_8);
            result = stream.collect(Collectors.joining(String.format("%n")));
        } catch (IOException e) {
            throw new ApplicationException("There are problems with reading a "
                    + "text.", e);
        } finally {
            if (stream != null) {
                stream.close();
            }
        }
        if (result.isEmpty()) {
            throw new ApplicationException("There is no data to read.");
        }
        return result;
    }
}
