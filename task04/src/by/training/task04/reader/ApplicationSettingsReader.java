/*
 * ApplicationSettingsReader.java
 * A reader for deriving text strings which represent application settings.
 * These settings include various paths to resources.
 */
package by.training.task04.reader;


import by.training.task04.exception.ApplicationException;

import java.util.MissingResourceException;
import java.util.ResourceBundle;


/**
 * A reader for deriving text strings which represent application settings.
 * These settings include various paths to resources.
 * @author Yauhen Sazonau
 * @version 06/16/19
 * @since 1.0
 */
public final class ApplicationSettingsReader {
    /* This class follows the Singleton pattern. */
    /**
     * The name of a property which retains the path to an application
     * controller that is relative to the current context root. The
     * identifier holds the value "controllerPath".
     */
    public static final String CONTROLLER_PATH_KEY = "controllerPath";
    /**
     * The name of a property which retains the path to the result page that
     * is relative to the current context root. The identifier holds the
     * value "resultPagePath".
     */
    public static final String RESULT_PAGE_PATH_KEY = "resultPagePath";
    /**
     * The name of a property which retains the path to the welcome page that
     * is relative to the current context root. The identifier holds the
     * value "welcomePagePath".
     */
    public static final String WELCOME_PAGE_PATH_KEY = "welcomePagePath";
    /**
     * The name of the resource bundle that contains all the necessary
     * information. The identifier holds the value
     * by.training.task04.bundle.ApplicationSettings".
     */
    private static final String BUNDLE_NAME
            = "by.training.task04.bundle.ApplicationSettings";
    /**
     * The single instance of this class. <code>null</code> value is
     * permitted and indicates that the single instance hasn't yet been
     * created.
     */
    private static ApplicationSettingsReader instance;
    /**
     * An instance of the <code>ResourceBundle</code> class that will help in
     * reading application settings. Can't be a <code>null</code> value.
     */
    private ResourceBundle bundle;
    /**
     * Creates an instance of the application settings reader. We use the
     * <code>private</code> key word to prevent creating an instance of this
     * class outside of it.
     */
    private ApplicationSettingsReader() {
        /* The default initialization is sufficient. */
    }
    /**
     * Gets the value of a property by the key.
     * @param key the <code>String</code> value which is the key of a property
     * @return the value of a property. Can't be a <code>null</code> value.
     * @throws ApplicationException if no resource has been found by the
     *                              <code>key</code> or if the <code>key</code>
     *                              argument is a <code>null</code> value
     */
    public String getProperty(final String key) {
        String result;
        /* The main part of the method. */
        if (key == null) {
            throw new ApplicationException("The 'key' argument is 'null'.");
        }
        try {
            result = bundle.getString(key);
        } catch (MissingResourceException e) {
            throw new ApplicationException("There is no value which "
                    + "corresponds to the specified key: " + key + ".", e);
        }
        return result;
    }
    /**
     * Creates an instance of the application settings reader if it is needed
     * or returns an existing. You must invoke this method because of no
     * having an ability to invoke the constructor.
     * @return a newly created instance of the
     *         <code>ApplicationSettingsReader</code> class or an existing
     *         if it has been created before. Can't be a <code>null</code>
     *         value.
     * @throws ApplicationException if an underlying source of data is
     *                              missed
     */
    public static ApplicationSettingsReader getInstance() {
        synchronized (ApplicationSettingsReader.class) {
            if (instance == null) {
                instance = new ApplicationSettingsReader();
                try {
                    instance.bundle = ResourceBundle.getBundle(BUNDLE_NAME);
                } catch (MissingResourceException e) {
                    throw new ApplicationException("An underlying source of "
                            + "data is missed: " + BUNDLE_NAME + ".", e);
                }
            }
        }
        return instance;
    }
}
