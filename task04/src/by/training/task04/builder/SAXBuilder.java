/* SAXBuilder.java
 * A builder which uses the SAX technology to create a collection of postal
 * cards.
 */
package by.training.task04.builder;


import by.training.task04.exception.ApplicationException;

import java.io.IOException;
import java.io.Reader;

import javax.xml.XMLConstants;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


/**
 * A builder which uses the SAX technology to create a collection of postal
 * cards.
 * @version 1.0, 06/07/19
 * @since 1.0
 */
public class SAXBuilder extends AbstractBuilder {
    /**
     * A SAX parser. <code>null</code> value is prohibited.
     */
    private SAXParser parser;
    /**
     * Constructs an instance of this class.
     * @throws ApplicationException if an instance of this class can't be
     *                              created
     */
    public SAXBuilder() {
        Class<SAXBuilder> classInstance = SAXBuilder.class;
        ClassLoader classLoader = classInstance.getClassLoader();
        SAXParserFactory factory;
        Schema schema;
        SchemaFactory schemaFactory
                = SchemaFactory.newInstance(
                        XMLConstants.W3C_XML_SCHEMA_NS_URI);
        /* The main part of the method. */
        try {
            schema = schemaFactory.newSchema(
                    classLoader.getResource(SCHEMA_PATH));
            factory = SAXParserFactory.newInstance();
            factory.setSchema(schema);
            factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            factory.setNamespaceAware(true);
            parser = factory.newSAXParser();
        } catch (SAXException | ParserConfigurationException  e) {
            throw new ApplicationException("A SAX parser can't be created.",
                    e);
        }
    }
    /**
     * Creates a collection of postal cards using the SAX technology.
     * @param reader the <code>Reader</code> value which is an input stream
     *               from which to read an XML document containing a collection
     *               of postal cards
     * @throws ApplicationException if there are problems with parsing an XML
     *                              document using the SAX technology
     */
    @Override
    public void buildPostalCards(final Reader reader) {
        try {
            parser.parse(new InputSource(reader),
                    new CardHandler(getPostalCards()));
        } catch (SAXException | IOException e) {
            throw new ApplicationException("There are problems with parsing "
                    + "an XML document using the SAX technology.", e);
        }
    }
}
