/* StAXBuilder.java
 * A builder which uses the StAX technology to create a collection of postal
 * cards.
 */
package by.training.task04.builder;


import static by.training.task04.builder.AttributeNames.ATTRIBUTE_NAME_1;
import static by.training.task04.builder.AttributeNames.ATTRIBUTE_NAME_2;
import static by.training.task04.builder.AttributeNames.ATTRIBUTE_NAME_3;

import by.training.task04.entity.Address;
import by.training.task04.entity.BLRAddress;
import by.training.task04.entity.Card;
import by.training.task04.entity.CardType;
import by.training.task04.entity.CardValue;
import by.training.task04.entity.ImageTheme;

import by.training.task04.exception.ApplicationException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.xml.XMLConstants;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import javax.xml.transform.stax.StAXSource;

import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;


/**
 * A builder which uses the StAX technology to create a collection of postal
 * cards.
 * @version 1.0, 06/09/19
 * @since 1.0
 */
public class StAXBuilder extends AbstractBuilder {
    /**
     * A logger for this application. <code>null</code> value is prohibited.
     */
    private static final Logger LOGGER
            = LogManager.getLogger(StAXBuilder.class);
    /**
     * An empty string. The identifier holds the value of "".
     */
    private static final String EMPTY_STRING = "";
    /**
     * Constructs an instance of this class.
     */
    public StAXBuilder() {
        /* The default initialization is sufficient. */
    }
    /**
     * Creates a collection of postal cards using the StAX technology.
     * @param reader the <code>Reader</code> value which is an input stream
     *               from which to read an XML document containing a collection
     *               of postal cards
     * @throws ApplicationException if there are problems with parsing an XML
     *                              document using the StAX technology
     */
    @Override
    public void buildPostalCards(final Reader reader) {
        Class<SAXBuilder> classInstance = SAXBuilder.class;
        ClassLoader classLoader = classInstance.getClassLoader();
        Schema schema;
        SchemaFactory schemaFactory
                = SchemaFactory.newInstance(
                        XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Set<Card> cards = getPostalCards();
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader parser;
        Validator validator;
        String document;
        int type;
        /* The main part of the method. */
        factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, Boolean.TRUE);
        factory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES,
                Boolean.FALSE);
        factory.setProperty(XMLInputFactory.SUPPORT_DTD, Boolean.FALSE);
        try {
            document = retrieveDocument(reader);
            parser = factory.createXMLStreamReader(new StringReader(document));
            schema = schemaFactory.newSchema(
                    classLoader.getResource(SCHEMA_PATH));
            validator = schema.newValidator();
            validator.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD,
                    EMPTY_STRING);
            validator.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA,
                    EMPTY_STRING);
            validator.setErrorHandler(new CardHandler(getPostalCards()));
            validator.validate(new StAXSource(parser));
            parser = factory.createXMLStreamReader(new StringReader(document));
            while (parser.hasNext()) {
                type = parser.next();
                if ((type == XMLStreamConstants.START_ELEMENT)
                        && (Element.valueOf(
                                convertToEnumName(parser.getLocalName()))
                                == Element.CARD)) {
                    cards.add(createPostalCard(parser));
                }
            }
        } catch (XMLStreamException | SAXException | IOException
                 | ApplicationException e) {
            throw new ApplicationException("There are problems with parsing "
                    + "an XML document using the StAX technology.", e);
        }
    }
    /**
     * Creates a postal card.
     * @param parser the <code>XMLStreamReader</code> value which is a StAX
     *               parser
     * @return a postal card
     * @throws XMLStreamException if there are problems with reading data
     */
    private Card createPostalCard(final XMLStreamReader parser)
            throws XMLStreamException {
        Address address = new Address();
        Card card = new Card();
        Element element;
        List<String> authors = new ArrayList<>();
        int type;
        /* The main part of the method. */
        card.setAuthors(authors);
        card.setDestination(address);
        card.setSentState(Boolean.parseBoolean(parser.getAttributeValue(null,
                ATTRIBUTE_NAME_1)));
        card.setType(CardType.valueOf(convertToEnumName(
                parser.getAttributeValue(null, ATTRIBUTE_NAME_2))));
        card.setIssueStamp(parser.getAttributeValue(null, ATTRIBUTE_NAME_3));
        while (parser.hasNext()) {
            type = parser.next();
            switch (type) {
            case XMLStreamConstants.START_ELEMENT:
                element = Element.valueOf(convertToEnumName(
                        parser.getLocalName()));
                setCardProperies(element, card, authors, address, parser);
                break;
            case XMLStreamConstants.END_ELEMENT:
                element = Element.valueOf(convertToEnumName(
                        parser.getLocalName()));
                if (element == Element.CARD) {
                    return card;
                }
                break;
            default:
                break;
            }
        }
        return card;
    }
    /**
     * Sets properties of a postal card.
     * @param element the <code>Element</code> value which is a current element
     * @param card the <code>Card</code> value which is a target for setting
     *             properties
     * @param authors the <code>List</code> value which is a list of authors
     * @param address the <code>Address</code> value which is a destination
     *                address
     * @param parser the <code>XMLStreamReader</code> value which is a StAX
     *               parser
     * @throws XMLStreamException if there are problems with reading data
     */
    private void setCardProperies(final Element element, final Card card,
            final List<String> authors, final Address address,
            final XMLStreamReader parser) throws XMLStreamException {
        Address belarusAdderess;
        switch (element) {
        case IDENTIFIER:
            card.setNumber(getText(parser));
            break;
        case THEME:
            card.setTheme(ImageTheme.valueOf(convertToEnumName(getText(
                    parser))));
            break;
        case COUNTRY:
            card.setCountry(getText(parser));
            break;
        case YEAR:
            card.setYear(Integer.parseInt(getText(parser)));
            break;
        case AUTHOR:
            authors.add(getText(parser));
            break;
        case VALUE:
            card.setValue(CardValue.valueOf(convertToEnumName(
                    getText(parser))));
            break;
        case CITY:
            address.setCity(getText(parser));
            break;
        case COUNTRY_NAME:
            address.setCountry(getText(parser));
            break;
        case STREET:
            address.setStreet(getText(parser));
            break;
        case POSTAL_CODE:
            belarusAdderess = new BLRAddress();
            belarusAdderess.setCity(address.getCity());
            belarusAdderess.setCountry(address.getCountry());
            belarusAdderess.setStreet(address.getStreet());
            ((BLRAddress) belarusAdderess).setPostalCode(getText(parser));
            card.setDestination(belarusAdderess);
            break;
        default:
            break;
        }
    }
    /**
     * Retrieves the text contents of a current element.
     * @param parser the <code>XMLStreamReader</code> value which is a StAX
     *               parser
     * @return the text contents of a current element
     * @throws XMLStreamException if there are problems with reading data
     */
    private String getText(final XMLStreamReader parser)
            throws XMLStreamException {
        parser.next();
        return parser.getText();
    }
    /**
     * Reads an XML document from the specified reader.
     * @param reader the <code>Reader</code> value from which to read an XML
     *               document
     * @return a read XML document as an instance of the <code>String</code>
     *         class.
     * @throws ApplicationException if there are problems with reading a
     *                              provided reader
     */
    private String retrieveDocument(final Reader reader) {
        BufferedReader bufferedReader = new BufferedReader(reader);
        StringBuilder stringBuilder = new StringBuilder();
        String tempString;
        /* The main part of the method. */
        try {
            tempString = bufferedReader.readLine();
            while (tempString != null) {
                stringBuilder.append(tempString);
                tempString = bufferedReader.readLine();
            }
        } catch (IOException e) {
            throw new ApplicationException("There are problems with reading a "
                    + "provided reader.", e);
        } finally {
            try {
                bufferedReader.close();
            } catch (IOException e2) {
                LOGGER.error("There are problems with closing a reader.", e2);
            }
        }
        return (stringBuilder.toString());
    }
}
