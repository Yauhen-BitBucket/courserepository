/**
 * Provides various builders used to create a collection of postal cards.
 * @author Yauhen Sazonau
 * @since 1.0
 */
package by.training.task04.builder;
