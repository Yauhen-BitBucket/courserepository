/* CardHandler.java
 * A handler used by a SAX parser to parse an XML postal card document.
 */
package by.training.task04.builder;


import static by.training.task04.builder.AbstractBuilder.convertToEnumName;

import by.training.task04.entity.Address;
import by.training.task04.entity.BLRAddress;
import by.training.task04.entity.Card;
import by.training.task04.entity.CardType;
import by.training.task04.entity.CardValue;
import by.training.task04.entity.ImageTheme;

import by.training.task04.exception.ApplicationException;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;


/**
 * A handler used by a SAX parser to parse an XML postal card document.
 * @version 1.0, 06/07/19
 * @since 1.0
 */
public class CardHandler extends DefaultHandler {
    /**
     * An empty string. The identifier holds the value of "".
     */
    public static final String EMPTY_STRING = "";
    /**
     * A postal card which is currently being processed.
     */
    private Card currentCard;
    /**
     * An element which is currently being processed.
     */
    private Element currentElement;
    /**
     * A collection of postal cards. <code>null</code> value is prohibited.
     */
    private Set<Card> postalCards;
    /**
     * Constructs an instance of this class using the specified argument.
     * @param cards the <code>Set</code> value which is an empty collection of
     *              postal cards
     * @throws ApplicationException if the argument is a <code>null</code>
     *                              value
     */
    public CardHandler(final Set<Card> cards) {
        if (cards == null) {
            throw new ApplicationException("The 'cards' argument is a 'null' "
                    + "value.");
        }
        postalCards = cards;
    }
    /**
     * Retrieves a collection of postal cards.
     * @return a collection of postal cards
     */
    public Set<Card> getPostalCards() {
        return postalCards;
    }
    /**
     * Receives notification of the start of an element.
     * @param uri the <code>String</code> value which is the Namespace URI, or
     *            the empty string if the element has no Namespace URI or if
     *            Namespace processing is not being performed
     * @param localName the <code>String</code> value which is the local name
     *                  (without prefix), or the empty string if Namespace
     *                  processing is not being performed
     * @param qName the <code>String</code> value which is the qualified name
     *              (with prefix), or the empty string if qualified names are
     *              not available
     * @param attributes the <code>Attributes</code> value which is the
     *                   attributes attached to the element. If there are no
     *                   attributes, it shall be an empty Attributes
     */
    @Override
    public void startElement(final String uri, final String localName,
            final String qName, final Attributes attributes) {
        Element element;
        String processedName;
        /* The main part of the method. */
        processedName = convertToEnumName(localName);
        element = Element.valueOf(processedName);
        if (element == Element.CARD) {
            currentCard = new Card();
            currentCard.setDestination(new Address());
            currentCard.setAuthors(new ArrayList<>());
            currentCard.setSentState(Boolean.parseBoolean(
                    attributes.getValue(uri, "isSent")));
            currentCard.setType(CardType.valueOf(convertToEnumName(
                    attributes.getValue(uri, "type"))));
            currentCard.setIssueStamp(attributes.getValue(uri, "issueDate"));
        } else {
            currentElement = element;
        }
    }
    /**
     * Receives notification of the end of an element.
     * @param uri the <code>String</code> value which is the Namespace URI, or
     *            the empty string if the element has no Namespace URI or if
     *            Namespace processing is not being performed
     * @param localName the <code>String</code> value which is the local name
     *                  (without prefix), or the empty string if Namespace
     *                  processing is not being performed
     * @param qName the <code>String</code> value which is the qualified name
     *              (with prefix), or the empty string if qualified names are
     *              not available
     */
    @Override
    public void endElement(final String uri, final String localName,
            final String qName) {
        Element element;
        String processedName;
        /* The main part of the method. */
        processedName = convertToEnumName(localName);
        element = Element.valueOf(processedName);
        if (element == Element.CARD) {
            postalCards.add(currentCard);
        }
    }
    /**
     * Receives notification of character data inside an element.
     * @param ch the <code>char[]</code> value which is the characters
     * @param start the <code>int</code> value which is the start position in
     *              the character array
     * @param length the <code>int</code> value which is the number of
     *               characters to use from the character array
     */
    @Override
    public void characters(final char[] ch, final int start,
            final int length) {
        Address address;
        BLRAddress belarussianAddress;
        List<String> authors;
        String value = new String(ch, start, length);
        /* The main part of the method. */
        if (currentElement == null) {
            return;
        }
        switch (currentElement) {
        case IDENTIFIER:
            currentCard.setNumber(value);
            break;
        case THEME:
            currentCard.setTheme(ImageTheme.valueOf(convertToEnumName(value)));
            break;
        case COUNTRY:
            currentCard.setCountry(value);
            break;
        case YEAR:
            currentCard.setYear(Integer.parseInt(value));
            break;
        case AUTHOR:
            authors = currentCard.getAuthors();
            authors.add(value);
            break;
        case VALUE:
            currentCard.setValue(CardValue.valueOf(convertToEnumName(value)));
            break;
        case COUNTRY_NAME:
            address = currentCard.getDestination();
            address.setCountry(value);
            break;
        case CITY:
            address = currentCard.getDestination();
            address.setCity(value);
            break;
        case STREET:
            address = currentCard.getDestination();
            address.setStreet(value);
            break;
        case POSTAL_CODE:
            address = currentCard.getDestination();
            belarussianAddress = new BLRAddress();
            belarussianAddress.setCountry(address.getCountry());
            belarussianAddress.setCity(address.getCity());
            belarussianAddress.setStreet(address.getStreet());
            belarussianAddress.setPostalCode(value);
            currentCard.setDestination(belarussianAddress);
            break;
        default:
        }
        currentElement = null;
    }
    /**
     * Receives a notification of a recoverable error.
     * @param exception the <code>SAXParseException</code> value which is the
     *                  error information encapsulated in a SAX parse exception
     */
    @Override
    public void error(final SAXParseException exception) throws SAXException {
        throw new SAXException("An XML document doesn't correspond to an XSD "
                + "scheme.", exception);
    }
    /**
     * Receives a notification of a non-recoverable error.
     * @param exception the <code>SAXParseException</code> value which is the
     *                  error information encapsulated in a SAX parse exception
     */
    @Override
    public void fatalError(final SAXParseException exception)
            throws SAXException {
        throw new SAXException("An XML document isn't well-formed.",
                exception);
    }
}
