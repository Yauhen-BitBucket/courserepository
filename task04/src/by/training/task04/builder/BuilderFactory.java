/* BuilderFactory.java
 * A factory for various builders.
 */
package by.training.task04.builder;


import by.training.task04.exception.ApplicationException;


/**
 * A factory for various builders.
 * @version 1.0, 06/16/19
 * @since 1.0
 */
public final class BuilderFactory {
    /**
     * Constructs an instance of this class.
     */
    private BuilderFactory() {
        /* The default initialization is sufficient. */
    }
    /**
     * Creates a builder using the specified type.
     * @param parserType the <code>String</code> value which is a parser type.
     *                   Accepted values are "SAX", "DOM", "STAX".
     * @return a builder which corresponds to the specified type
     * @throws ApplicationException if the argument is a 'null' value or isn't
     *                              one of the predefined values
     */
    public static AbstractBuilder createBuilder(final String parserType) {
        if (parserType == null) {
            throw new ApplicationException("The specified parser type is a "
                    + "'null' value.");
        }
        switch (parserType) {
        case "SAX":
            return (new SAXBuilder());
        case "DOM":
            return (new DOMBuilder());
        case "STAX":
            return (new StAXBuilder());
        default:
            throw new ApplicationException("Wrong parser type is specified: "
                    + parserType + ".");
        }
    }
}
