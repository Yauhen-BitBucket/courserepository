/* DOMBuilder.java
 * A builder which uses the DOM technology to create a collection of postal
 * cards.
 */
package by.training.task04.builder;


import static by.training.task04.builder.AttributeNames.ATTRIBUTE_NAME_1;
import static by.training.task04.builder.AttributeNames.ATTRIBUTE_NAME_2;
import static by.training.task04.builder.AttributeNames.ATTRIBUTE_NAME_3;

import static by.training.task04.builder.ElementNames.ELEMENT_NAME_1;
import static by.training.task04.builder.ElementNames.ELEMENT_NAME_2;
import static by.training.task04.builder.ElementNames.ELEMENT_NAME_3;
import static by.training.task04.builder.ElementNames.ELEMENT_NAME_4;
import static by.training.task04.builder.ElementNames.ELEMENT_NAME_5;
import static by.training.task04.builder.ElementNames.ELEMENT_NAME_6;
import static by.training.task04.builder.ElementNames.ELEMENT_NAME_7;
import static by.training.task04.builder.ElementNames.ELEMENT_NAME_8;
import static by.training.task04.builder.ElementNames.ELEMENT_NAME_9;
import static by.training.task04.builder.ElementNames.ELEMENT_NAME_10;
import static by.training.task04.builder.ElementNames.ELEMENT_NAME_11;

import by.training.task04.entity.Address;
import by.training.task04.entity.BLRAddress;
import by.training.task04.entity.Card;
import by.training.task04.entity.CardType;
import by.training.task04.entity.CardValue;
import by.training.task04.entity.ImageTheme;

import by.training.task04.exception.ApplicationException;

import java.io.IOException;
import java.io.Reader;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.xml.XMLConstants;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * A builder which uses the DOM technology to create a collection of postal
 * cards.
 * @version 1.0, 06/08/19
 * @since 1.0
 */
public class DOMBuilder extends AbstractBuilder {
    /**
     * A DOM parser. <code>null</code> value is prohibited.
     */
    private DocumentBuilder documentBuilder;
    /**
     * Constructs an instance of this class.
     * @throws ApplicationException if an instance of this class can't be
     *                              created
     */
    public DOMBuilder() {
        Class<SAXBuilder> classInstance = SAXBuilder.class;
        ClassLoader classLoader = classInstance.getClassLoader();
        DocumentBuilderFactory factory;
        Schema schema;
        SchemaFactory schemaFactory
                = SchemaFactory.newInstance(
                        XMLConstants.W3C_XML_SCHEMA_NS_URI);
        /* The main part of the method. */
        try {
            schema = schemaFactory.newSchema(
                    classLoader.getResource(SCHEMA_PATH));
            factory = DocumentBuilderFactory.newInstance();
            factory.setSchema(schema);
            factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            factory.setNamespaceAware(true);
            documentBuilder = factory.newDocumentBuilder();
            documentBuilder.setErrorHandler(new CardHandler(getPostalCards()));
        } catch (SAXException | ParserConfigurationException e) {
            throw new ApplicationException("A DOM parser can't be created.",
                    e);
        }
    }
    /**
     * Creates a collection of postal cards using the DOM technology.
     * @param reader the <code>Reader</code> value which is an input stream
     *               from which to read an XML document containing a collection
     *               of postal cards
     * @throws ApplicationException if there are problems with parsing an XML
     *                              document using the DOM technology
     */
    @Override
    public void buildPostalCards(final Reader reader) {
        Document document;
        Element root;
        NodeList cards;
        Set<Card> postalCards = getPostalCards();
        int length;
        /* The main part of the method. */
        try {
            document = documentBuilder.parse(new InputSource(reader));
        } catch (IOException | SAXException e) {
            throw new ApplicationException("There are problems with parsing "
                    + "an XML document using the DOM technology.", e);
        }
        root = document.getDocumentElement();
        cards = root.getElementsByTagName(ELEMENT_NAME_1);
        length = cards.getLength();
        for (int i = 0; i < length; i++) {
            postalCards.add(createPostalCard((Element) cards.item(i)));
        }
    }
    /**
     * Creates a postal card from an XML element.
     * @param cardElement the <code>Element</code> value which is an XML
     *                    element from that a postal card is created
     * @return a postal card
     */
    private Card createPostalCard(final Element cardElement) {
        Address address;
        Card card = new Card();
        List<String> authors = new ArrayList<>();
        Node node;
        NodeList authorList;
        NodeList elements;
        int length;
        /* The main part of the method. */
        card.setSentState(Boolean.parseBoolean(cardElement.getAttribute(
                ATTRIBUTE_NAME_1)));
        card.setType(CardType.valueOf(convertToEnumName(
                cardElement.getAttribute(ATTRIBUTE_NAME_2))));
        card.setIssueStamp(cardElement.getAttribute(ATTRIBUTE_NAME_3));
        card.setNumber(getElementTextContents(cardElement, ELEMENT_NAME_2));
        card.setTheme(ImageTheme.valueOf(convertToEnumName(
                getElementTextContents(cardElement, ELEMENT_NAME_3))));
        card.setCountry(getElementTextContents(cardElement, ELEMENT_NAME_4));
        card.setYear(Integer.parseInt(getElementTextContents(cardElement,
                ELEMENT_NAME_5)));
        authorList = cardElement.getElementsByTagName(ELEMENT_NAME_6);
        length = authorList.getLength();
        for (int i = 0; i < length; i++) {
            node = authorList.item(i);
            authors.add(node.getTextContent());
        }
        card.setAuthors(authors);
        card.setValue(CardValue.valueOf(convertToEnumName(
                getElementTextContents(cardElement, ELEMENT_NAME_7))));
        elements = cardElement.getElementsByTagName(ELEMENT_NAME_11);
        if (elements.getLength() != 0) {
            address = new BLRAddress();
            ((BLRAddress) address).setPostalCode(getElementTextContents(
                    cardElement, ELEMENT_NAME_11));
        } else {
            address = new Address();
        }
        address.setCity(getElementTextContents(cardElement, ELEMENT_NAME_9));
        address.setCountry(getElementTextContents(cardElement,
                ELEMENT_NAME_8));
        address.setStreet(getElementTextContents(cardElement,
                ELEMENT_NAME_10));
        card.setDestination(address);
        return card;
    }
    /**
     * Retrieves the text contents of an element specified by the name of
     * 'elementName' which is a first descendant of the 'element' element.
     * @param element the <code>Element</code> value which is a parent element
     * @param elementName the <code>String</code> value which is the name of a
     *                    descendant
     * @return the text contents of an element specified by the name of
     *         'elementName' which is a first descendant of the 'element'
     *         element
     */
    private String getElementTextContents(final Element element,
            final String elementName) {
        NodeList nList = element.getElementsByTagName(elementName);
        Node node = nList.item(0);
        return (node.getTextContent());
    }
}
