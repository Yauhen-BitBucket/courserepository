/*
 * ElementNames.java
 * A class for storing element names.
 */
package by.training.task04.builder;


/**
 * A class for storing element names.
 * @author Yauhen Sazonau
 * @version 1.0, 06/09/19
 * @since 1.0
 */
public final class ElementNames {
    /**
     * An element name. The identifier holds the value of "card".
     */
    public static final String ELEMENT_NAME_1 = "card";
    /**
     * An element name. The identifier holds the value of "identifier".
     */
    public static final String ELEMENT_NAME_2 = "identifier";
    /**
     * An element name. The identifier holds the value of "theme".
     */
    public static final String ELEMENT_NAME_3 = "theme";
    /**
     * An element name. The identifier holds the value of "country".
     */
    public static final String ELEMENT_NAME_4 = "country";
    /**
     * An element name. The identifier holds the value of "year".
     */
    public static final String ELEMENT_NAME_5 = "year";
    /**
     * An element name. The identifier holds the value of "author".
     */
    public static final String ELEMENT_NAME_6 = "author";
    /**
     * An element name. The identifier holds the value of "value".
     */
    public static final String ELEMENT_NAME_7 = "value";
    /**
     * An element name. The identifier holds the value of "country-name".
     */
    public static final String ELEMENT_NAME_8 = "country-name";
    /**
     * An element name. The identifier holds the value of "city".
     */
    public static final String ELEMENT_NAME_9 = "city";
    /**
     * An element name. The identifier holds the value of "street".
     */
    public static final String ELEMENT_NAME_10 = "street";
    /**
     * An element name. The identifier holds the value of "postal-code".
     */
    public static final String ELEMENT_NAME_11 = "postal-code";
    /**
     * Constructs an instance of this class.
     */
    private ElementNames() {
        /* The default initialization is sufficient. */
    }
}
