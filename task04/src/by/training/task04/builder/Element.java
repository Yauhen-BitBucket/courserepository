/*
 * Elements.java
 * An element of an XML postal card document. There are the following elements:
 * old-cards, card, card-properties, identifier, theme, country, year, author,
 * value, destination, country-name, city, street.
 */
package by.training.task04.builder;


/**
 * An element of an XML postal card document. There are the following elements:
 * old-cards, card, card-properties, identifier, theme, country, year, author,
 * value, destination, country-name, city, street.
 * @author Yauhen Sazonau
 * @version 1.0, 06/07/19
 * @since 1.0
 */
enum Element {
    /**
     * Represents an 'old-cards' element.
     */
    OLD_CARDS,
    /**
     * Represents a 'card' element.
     */
    CARD,
    /**
     * Represents a 'card-properties' element.
     */
    CARD_PROPERTIES,
    /**
     * Represents an 'identifier' element.
     */
    IDENTIFIER,
    /**
     * Represents a 'theme' element.
     */
    THEME,
    /**
     * Represents a 'country' element.
     */
    COUNTRY,
    /**
     * Represents a 'year' element.
     */
    YEAR,
    /**
     * Represents an 'author' element.
     */
    AUTHOR,
    /**
     * Represents a 'value' element.
     */
    VALUE,
    /**
     * Represents a 'destination' element.
     */
    DESTINATION,
    /**
     * Represents a 'country-name' element.
     */
    COUNTRY_NAME,
    /**
     * Represents a 'city' element.
     */
    CITY,
    /**
     * Represents a 'street' element.
     */
    STREET,
    /**
     * Represents a 'postal-code' element.
     */
    POSTAL_CODE
}
