/* AbstractBuilder.java
 * A builder which card builders should be extended from.
 */
package by.training.task04.builder;


import by.training.task04.entity.Card;

import by.training.task04.exception.ApplicationException;

import java.io.Reader;

import java.util.HashSet;
import java.util.Set;


/**
 * A builder which card builders should be extended from.
 * @version 1.0, 06/07/19
 * @since 1.0
 */
public abstract class AbstractBuilder {
    /**
     * Specifies a relative path to the XSD file. The identifier holds the
     * value of "data/OldCardsScheme.xsd".
     */
    protected static final String SCHEMA_PATH = "data/OldCardsScheme.xsd";
    /**
     * A collection of postal cards. <code>null</code> value is prohibited.
     */
    private Set<Card> postalCards;
    /**
     * Constructs an instance of this class.
     */
    public AbstractBuilder() {
        postalCards = new HashSet<>();
    }
    /**
     * Retrieves a collection of postal cards.
     * @return a collection of postal cards
     */
    public Set<Card> getPostalCards() {
        return postalCards;
    }
    /**
     * Sets a collection of postal cards.
     * @param cards the <code>Set</code> value which is a collection of
     *              postal cards to be set
     */
    public void setPostalCards(final Set<Card> cards) {
        this.postalCards = cards;
    }
    /**
     * Creates a collection of postal cards.
     * @param reader the <code>Reader</code> value which is an input stream
     *               from which to read an XML document containing a collection
     *               of postal cards
     * ApplicationException if there are problems with parsing an XML
     *                      document
     */
    public abstract void buildPostalCards(Reader reader);
    /**
     * Converts the specified value to an upper case and replaces dashes and
     * spaces to the underscore character.
     * @param value the <code>String</code> value which is a value to convert
     * @return the result of conversion of the specified value
     * @throws ApplicationException if the argument is a <code>null</code>
     *                              value
     */
    public static String convertToEnumName(final String value) {
        String processedName;
        /* The main part of the method. */
        if (value == null) {
            throw new ApplicationException("The 'value' argument is a 'null' "
                    + "value.");
        }
        processedName = value.toUpperCase();
        processedName = processedName.replaceAll(" |-", "_");
        return processedName;
    }
}
