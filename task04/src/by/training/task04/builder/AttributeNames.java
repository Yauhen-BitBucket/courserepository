/*
 * AttributeNames.java
 * A class for storing attribute names.
 */
package by.training.task04.builder;


/**
 * A class for storing attribute names.
 * @author Yauhen Sazonau
 * @version 1.0, 06/09/19
 * @since 1.0
 */
public final class AttributeNames {
    /**
     * An attribute name. The identifier holds the value of "isSent".
     */
    public static final String ATTRIBUTE_NAME_1 = "isSent";
    /**
     * An attribute name. The identifier holds the value of "type".
     */
    public static final String ATTRIBUTE_NAME_2 = "type";
    /**
     * An attribute name. The identifier holds the value of "issueDate".
     */
    public static final String ATTRIBUTE_NAME_3 = "issueDate";
    /**
     * Constructs an instance of this class.
     */
    private AttributeNames() {
        /* The default initialization is sufficient. */
    }
}
