/*
 * CardValue.java
 * The value of a card. The value of a card can be: historic, collection,
 * thematic.
 */
package by.training.task04.entity;


/**
 * The value of a card. The value of a card can be: historic, collection,
 * thematic.
 * @author Yauhen Sazonau
 * @version 1.0, 06/06/19
 * @since 1.0
 */
public enum CardValue {
    /**
     * Represents a historic card value.
     */
    HISTORIC,
    /**
     * Represents a collection card value.
     */
    COLLECTION,
    /**
     * Represents a thematic card value.
     */
    THEMATIC
}
