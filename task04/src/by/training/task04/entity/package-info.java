/**
 * Provides classes each of which encapsulates the information about some
 * entity. These entities are: an address, a postal card, a postal card type,
 * a postal card value, an image theme. The main purpose of this classes is
 * that they are used as data transfer objects.
 * @author Yauhen Sazonau
 * @since 1.0
 */
package by.training.task04.entity;
