/*
 * Card.java
 * A postal card.
 */
package by.training.task04.entity;


import java.util.List;
import java.util.Objects;


/**
 * A postal card.
 * @author Yauhen Sazonau
 * @version 1.0, 06/06/19
 * @since 1.0
 */
public class Card {
    /**
     * A target destination of this card.
     */
    private Address destination;
    /**
     * The type of a card.
     */
    private CardType type;
    /**
     * A card value.
     */
    private CardValue value;
    /**
     * A sent state.
     */
    private boolean sentState;
    /**
     * The theme of an image.
     */
    private ImageTheme theme;
    /**
     * An issue year.
     */
    private int year;
    /**
     * A list of authors.
     */
    private List<String> authors;
    /**
     * The name of a producing country.
     */
    private String country;
    /**
     * An issue date and time of this card.
     */
    private String issueStamp;
    /**
     * A unique card identifier.
     */
    private String number;
    /**
     * Constructs an instance of this class.
     */
    public Card() {
        /* The default initialization is sufficient. */
    }
    /**
     * Retrieves an address destination of this card.
     * @return an address destination of this card
     */
    public Address getDestination() {
        return destination;
    }
    /**
     * Sets an address destination of this card.
     * @param address the <code>Address</code> value which is an address
     *                destination to be set
     */
    public void setDestination(final Address address) {
        this.destination = address;
    }
    /**
     * Retrieves a type of this card.
     * @return a type of this card
     */
    public CardType getType() {
        return type;
    }
    /**
     * Sets a type of this card.
     * @param cardType the <code>CardType</code> value which is a type of this
     *                 card to be set
     */
    public void setType(final CardType cardType) {
        this.type = cardType;
    }
    /**
     * Retrieves a value of this card.
     * @return a value of this card
     */
    public CardValue getValue() {
        return value;
    }
    /**
     * Sets a value of this card.
     * @param cardValue the <code>CardValue</code> value which is a value of
     *                  this card to be set
     */
    public void setValue(final CardValue cardValue) {
        this.value = cardValue;
    }
    /**
     * Retrieves a sent state of this card.
     * @return a sent state of this card
     */
    public boolean getSentState() {
        return sentState;
    }
    /**
     * Sets a sent state of this card.
     * @param state the <code>boolean</code> value which is a sent state to be
     *              set
     */
    public void setSentState(final boolean state) {
        this.sentState = state;
    }
    /**
     * Retrieves the theme of an image.
     * @return the theme of an image
     */
    public ImageTheme getTheme() {
        return theme;
    }
    /**
     * Sets the theme of an image.
     * @param cardTheme the <code>ImageTheme</code> value which is the theme of
     *                  an image to be set
     */
    public void setTheme(final ImageTheme cardTheme) {
        this.theme = cardTheme;
    }
    /**
     * Retrieves an issue year.
     * @return an issue year
     */
    public int getYear() {
        return year;
    }
    /**
     * Sets an issue year.
     * @param cardYear the <code>int</code> value which is an issue year to be
     *                 set
     */
    public void setYear(final int cardYear) {
        this.year = cardYear;
    }
    /**
     * Retrieves a list of authors.
     * @return a list of authors
     */
    public List<String> getAuthors() {
        return authors;
    }
    /**
     * Sets a list of authors.
     * @param cardAuthors the <code>List</code> value which is a list of
     *                    authors to be set
     */
    public void setAuthors(final List<String> cardAuthors) {
        this.authors = cardAuthors;
    }
    /**
     * Retrieves the name of a producing country.
     * @return the name of a producing country
     */
    public String getCountry() {
        return country;
    }
    /**
     * Sets the name of a producing country.
     * @param cardCountry the <code>String</code> value which is the name of a
     *                    producing country to be set
     */
    public void setCountry(final String cardCountry) {
        this.country = cardCountry;
    }
    /**
     * Retrieves an issue date and time of this card.
     * @return an issue date and time of this card
     */
    public String getIssueStamp() {
        return issueStamp;
    }
    /**
     * Sets an issue date and time of this card.
     * @param stamp the <code>String</code> value which is an issue date and
     *              time to be set
     */
    public void setIssueStamp(final String stamp) {
        this.issueStamp = stamp;
    }
    /**
     * Retrieves a unique card identifier.
     * @return a unique card identifier
     */
    public String getNumber() {
        return number;
    }
    /**
     * Sets a unique card identifier.
     * @param cardNumber the <code>String</code> value which is a unique card
     *                   identifier to be set
     */
    public void setNumber(final String cardNumber) {
        this.number = cardNumber;
    }
    /**
     * Indicates whether some other object is "equal to" this one.
     * @param object the <code>Object</code> value that is an object with
     *               which to compare. <code>null</code> value is possible.
     * @return <code>true</code> if this object is the same as the object
     *         supplied through the <code>object</code> argument;
     *         <code>false</code> otherwise
     */
    @Override
    public boolean equals(final Object object) {
        Card card;
        boolean result = true;
        /* The main part of the method. */
        if (this == object) {
            return true;
        } else if (object == null) {
            return false;
        }
        /* The supplied object should be an instance of the same class. */
        result = getClass() == object.getClass();
        if (result) {
            card = (Card) object;
            result = ((Objects.equals(destination, card.destination))
                      && (Objects.equals(type, card.type))
                      && (Objects.equals(value, card.value))
                      && (sentState == card.sentState)
                      && (Objects.equals(theme, card.theme))
                      && (year == card.year)
                      && (Objects.equals(authors, card.authors))
                      && (Objects.equals(country, card.country))
                      && (Objects.equals(issueStamp, card.issueStamp))
                      && (Objects.equals(number, card.number)));
        }
        return result;
    }
    /**
     * Retrieves a hash code value for this object.
     * @return a hash code value for this object
     */
    @Override
    public int hashCode() {
        return (Objects.hash(destination, type, value, sentState, theme, year,
                             authors, country, issueStamp, number));
    }
    /**
     * Returns a string representation of this object.
     * @return a string representation of the object. <code>null</code> value
     *         is prohibited.
     */
    @Override
    public String toString() {
        String string;
        /* The main part of the method. */
        string = "Card properties:" + "\nDestination - " + destination
                 + "\nType: " + type + "\nValue: " + value + "\nisSent: "
                 + sentState + "\nImage theme: " + theme + "\nIssue year: "
                 + year + "\nAuthors: " + authors + "\nProdicing country: "
                 + country + "\nIssue stamp: " + issueStamp + "\nId: "
                 + number;
        return string;
    }
}
