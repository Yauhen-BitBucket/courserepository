/*
 * HashCodeCoefficients.java
 * A class for storing hash code coefficients.
 */
package by.training.task04.entity;


/**
 * A class for storing hash code coefficients.
 * @author Yauhen Sazonau
 * @version 1.0, 06/06/19
 * @since 1.0
 */
public final class HashCodeCoefficients {
    /**
     * A coefficient used to calculate a hash code value of an object. The
     * identifier holds the value of "31".
     */
    public static final byte HASH_CODE_COEFFICIENT = 31;
    /**
     * A power coefficient used to calculate a hash code value of an object.
     * The identifier holds the value of "4".
     */
    public static final byte POWER_COEFFICIENT_4 = 4;
    /**
     * A power coefficient used to calculate a hash code value of an object.
     * The identifier holds the value of "3".
     */
    public static final byte POWER_COEFFICIENT_3 = 3;
    /**
     * A power coefficient used to calculate a hash code value of an object.
     * The identifier holds the value of "2".
     */
    public static final byte POWER_COEFFICIENT_2 = 2;
    /**
     * Constructs an instance of this class.
     */
    private HashCodeCoefficients() {
        /* The default initialization is sufficient. */
    }
}
