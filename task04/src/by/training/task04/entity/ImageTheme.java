/*
 * ImageTheme.java
 * The theme of an image. An image can have one of the following themes: city
 * landscape, nature, people, religion, sport, architecture.
 */
package by.training.task04.entity;


/**
 * The theme of an image. An image can have one of the following themes: city
 * landscape, nature, people, religion, sport, architecture.
 * @author Yauhen Sazonau
 * @version 1.0, 06/06/19
 * @since 1.0
 */
public enum ImageTheme {
    /**
     * Represents a city landscape theme.
     */
    CITY_LANDSCAPE,
    /**
     * Represents a nature theme.
     */
    NATURE,
    /**
     * Represents a people theme.
     */
    PEOPLE,
    /**
     * Represents a religion theme.
     */
    RELIGION,
    /**
     * Represents a sport theme.
     */
    SPORT,
    /**
     * Represents an architecture theme.
     */
    ARCHITECTURE
}
