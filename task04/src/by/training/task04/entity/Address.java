/*
 * Address.java
 * An address. The base class in the address hierarchy. Contains fields for
 * storing information which is common for all addresses.
 */
package by.training.task04.entity;


import static by.training.task04.entity.HashCodeCoefficients
        .HASH_CODE_COEFFICIENT;
import static by.training.task04.entity.HashCodeCoefficients
        .POWER_COEFFICIENT_2;

import java.util.Objects;


/**
 * An address. The base class in the address hierarchy. Contains fields for
 * storing information which is common for all addresses.
 * @author Yauhen Sazonau
 * @version 1.0, 06/06/19
 * @since 1.0
 */
public class Address {
    /**
     * The name of a city.
     */
    private String city;
    /**
     * The name of a country.
     */
    private String country;
    /**
     * The name of a street.
     */
    private String street;
    /**
     * Constructs an instance of this class.
     */
    public Address() {
        /* The default initialization is sufficient. */
    }
    /**
     * Retrieves the name of a city.
     * @return the name of a city
     */
    public String getCity() {
        return city;
    }
    /**
     * Sets the name of a city.
     * @param cityName the <code>String</code> value which is the name of a
     *                 city to be set
     */
    public void setCity(final String cityName) {
        this.city = cityName;
    }
    /**
     * Retrieves the name of a country.
     * @return the name of a country
     */
    public String getCountry() {
        return country;
    }
    /**
     * Sets the name of a country.
     * @param countryName the <code>String</code> value which is the name of a
     *                    country to be set
     */
    public void setCountry(final String countryName) {
        this.country = countryName;
    }
    /**
     * Retrieves the name of a street.
     * @return the name of a street
     */
    public String getStreet() {
        return street;
    }
    /**
     * Sets the name of a street.
     * @param streetName the <code>String</code> value which is the name of a
     *                   street to be set
     */
    public void setStreet(final String streetName) {
        this.street = streetName;
    }
    /**
     * Indicates whether some other object is "equal to" this one.
     * @param object the <code>Object</code> value that is an object with
     *               which to compare. <code>null</code> value is possible.
     * @return <code>true</code> if this object is the same as the object
     *         supplied through the <code>object</code> argument;
     *         <code>false</code> otherwise
     */
    @Override
    public boolean equals(final Object object) {
        Address address;
        boolean result = true;
        /* The main part of the method. */
        if (this == object) {
            return true;
        } else if (object == null) {
            return false;
        }
        /* The supplied object should belong to the address hierarchy. */
        result = object instanceof Address;
        if (result) {
            address = (Address) object;
            result = (Objects.equals(city, address.getCity()))
                     && (Objects.equals(country, address.getCountry()))
                     && (Objects.equals(street, address.getStreet()));
        }
        return result;
    }
    /**
     * Retrieves a hash code value for this object.
     * @return a hash code value for this object
     */
    @Override
    public int hashCode() {
        int cityHashCode;
        int countryHashCode;
        int streetHashCode;
        /* The main part of the method. */
        if (city == null) {
            cityHashCode = 0;
        } else {
            cityHashCode = city.hashCode();
        }
        if (country == null) {
            countryHashCode = 0;
        } else {
            countryHashCode = country.hashCode();
        }
        if (street == null) {
            streetHashCode = 0;
        } else {
            streetHashCode = street.hashCode();
        }
        return (((cityHashCode) * (HASH_CODE_COEFFICIENT
                                   ^ POWER_COEFFICIENT_2))
                + ((countryHashCode) * (HASH_CODE_COEFFICIENT))
                + (streetHashCode));
    }
    /**
     * Returns a string representation of this object.
     * @return a string representation of the object. <code>null</code> value
     *         is prohibited.
     */
    @Override
    public String toString() {
        String string;
        /* The main part of the method. */
        string = "city: " + city + ", country: " + country + ", street: "
                 + street;
        return string;
    }
}
