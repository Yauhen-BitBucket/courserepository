/*
 * CardType.java
 * The type of a card. A card can be one of the following types:
 * congratulation, advertisement, ordinary.
 */
package by.training.task04.entity;


/**
 * The type of a card. A card can be one of the following types:
 * congratulation, advertisement, ordinary.
 * @author Yauhen Sazonau
 * @version 1.0, 06/06/19
 * @since 1.0
 */
public enum CardType {
    /**
     * Represents a congratulation card type.
     */
    CONGRATULATION,
    /**
     * Represents a advertisement card type.
     */
    ADVERTISEMENT,
    /**
     * Represents a ordinary card type.
     */
    ORDINARY
}
