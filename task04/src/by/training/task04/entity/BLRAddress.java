/*
 * BLRAddress.java
 * A belarussian address. The main difference between it and a general address
 * is that belarussian addresses have a postal code.
 */
package by.training.task04.entity;


import static by.training.task04.entity.HashCodeCoefficients
        .HASH_CODE_COEFFICIENT;
import static by.training.task04.entity.HashCodeCoefficients
        .POWER_COEFFICIENT_3;

import java.util.Objects;


/**
 * A belarussian address. The main difference between it and a general address
 * is that belarussian addresses have a postal code.
 * @author Yauhen Sazonau
 * @version 1.0, 06/06/19
 * @since 1.0
 */
public class BLRAddress extends Address {
    /**
     * A postal code.
     */
    private String postalCode;
    /**
     * Constructs an instance of this class.
     */
    public BLRAddress() {
        /* The default initialization is sufficient. */
    }
    /**
     * Retrieves the postal code of this address.
     * @return the postal code of this address
     */
    public String getPostalCode() {
        return postalCode;
    }
    /**
     * Sets the postal code of this address.
     * @param code the <code>String</code> value which is the postal code to be
     *             set
     */
    public void setPostalCode(final String code) {
        this.postalCode = code;
    }
    /**
     * Indicates whether some other object is "equal to" this one.
     * @param object the <code>Object</code> value that is an object with
     *               which to compare. <code>null</code> value is possible.
     * @return <code>true</code> if this object is the same as the object
     *         supplied through the <code>object</code> argument;
     *         <code>false</code> otherwise
     */
    @Override
    public boolean equals(final Object object) {
        BLRAddress address;
        boolean result = true;
        /* The main part of the method. */
        if (!super.equals(object)) {
            return false;
        }
        /* The class of the supplied object should be the same or extend this
         * class.
         */
        result = object instanceof BLRAddress;
        if (result) {
            address = (BLRAddress) object;
            result = Objects.equals(postalCode, address.postalCode);
        }
        return result;
    }
    /**
     * Retrieves a hash code value for this object.
     * @return a hash code value for this object
     */
    @Override
    public int hashCode() {
        int postalCodeHashCode;
        /* The main part of the method. */
        if (postalCode == null) {
            postalCodeHashCode = 0;
        } else {
            postalCodeHashCode = postalCode.hashCode();
        }
        return (((postalCodeHashCode) * (HASH_CODE_COEFFICIENT
                                         ^ POWER_COEFFICIENT_3))
                + (super.hashCode()));
    }
    /**
     * Returns a string representation of this object.
     * @return a string representation of the object. <code>null</code> value
     *         is prohibited.
     */
    @Override
    public String toString() {
        String string;
        /* The main part of the method. */
        string = super.toString() + ", postal code: " + postalCode;
        return string;
    }
}
