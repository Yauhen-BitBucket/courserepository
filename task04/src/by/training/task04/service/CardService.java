/* CardService.java
 * A contract which card service should comply with.
 */
package by.training.task04.service;


import by.training.task04.entity.Card;

import java.util.Set;


/**
 * A contract which card service should comply with.
 * @version 1.0, 06/07/19
 * @since 1.0
 */
public interface CardService {
    /**
     * An XML parser name.
     * @version 1.0, 06/10/19
     * @since 1.0
     */
    enum XMLParserName {
        /**
         * Represents a SAX parser.
         */
        SAX,
        /**
         * Represents a DOM parser.
         */
        DOM,
        /**
         * Represents a StAX parser.
         */
        STAX,
        /**
         * Represents a dummy <code>enum</code> constant.
         */
        DUMMY
    }
    /**
     * Retrieves postal cards using the specified technology.
     * @param parserName the <code>XMLParserName</code> value which is a name
     *                   of a XML parser used to retrieve postal cards
     * @return postal cards
     * @throws ServiceException if there are problems with retrieving postal
     *                          cards using the specified technology
     */
    Set<Card> retrieveCards(XMLParserName parserName)
            throws ServiceException;
}
