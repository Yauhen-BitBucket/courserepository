/* CardServiceImpl.java
 * An implementation of card service.
 */
package by.training.task04.service;


import by.training.task04.builder.AbstractBuilder;
import by.training.task04.builder.BuilderFactory;

import by.training.task04.dao.CardDocumentDAO;
import by.training.task04.dao.DAOException;
import by.training.task04.dao.FileDAOFactory;

import by.training.task04.entity.Card;

import by.training.task04.exception.ApplicationException;

import java.io.Reader;

import java.util.Set;


/**
 * An implementation of card service.
 * @version 1.0, 06/07/19
 * @since 1.0
 */
public class CardServiceImpl implements CardService {
    /**
     * Retrieves postal cards using the specified technology.
     * @param parserName the <code>XMLParserName</code> value which is a name
     *                   of a XML parser used to retrieve postal cards
     * @return postal cards
     * @throws ServiceException if there are problems with retrieving postal
     *                          cards using the specified technology
     */
    @Override
    public Set<Card> retrieveCards(final XMLParserName parserName)
            throws ServiceException {
        AbstractBuilder builder;
        FileDAOFactory factory = FileDAOFactory.getInstance();
        CardDocumentDAO cardDocumentDAO = factory.retrieveCardDocumentDAO();
        Reader reader;
        /* The main part of the method. */
        if (parserName == null) {
            throw new ServiceException("The specified XML parser is a 'null' "
                    + "value.");
        }
        try {
            reader = cardDocumentDAO.retrieveCardDocument();
            switch (parserName) {
            case SAX:
                builder = BuilderFactory.createBuilder(
                        XMLParserName.SAX.name());
                break;
            case DOM:
                builder = BuilderFactory.createBuilder(
                        XMLParserName.DOM.name());
                break;
            case STAX:
                builder = BuilderFactory.createBuilder(
                        XMLParserName.STAX.name());
                break;
            default:
                throw new ServiceException("The specified XML parser isn't "
                        + "known: " + parserName + ".");
            }
            builder.buildPostalCards(reader);
        } catch (DAOException | ApplicationException e) {
            throw new ServiceException("Can't retrieve postal cards.", e);
        }
        return builder.getPostalCards();
    }
}
