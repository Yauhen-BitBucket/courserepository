/* ServiceFactory.java
 * A factory used to create various services.
 */
package by.training.task04.service;


/**
 * A factory used to create various services.
 * @version 1.0, 06/07/19
 * @since 1.0
 */
public final class ServiceFactory {
    /**
     * Represents a single instance of this class. <code>null</code> value is
     * prohibited.
     */
    private static ServiceFactory instance = new ServiceFactory();
    /**
     * An instance of the <code>CardServiceImpl</code> class.
     * <code>null</code> value is prohibited.
     */
    private final CardService cardService = new CardServiceImpl();
    /**
     * Creates an instance of this class. You have no ability to create
     * an instance of this class using this constructor. You should use the
     * <code>getInstance</code> method.
     */
    private ServiceFactory() {
        /* The default initialization is sufficient. */
    }
    /**
     * Retrieves an instance of the <code>CardService</code> class.
     * @return an instance of the <code>CardService</code> class
     */
    public CardService retrieveCardService() {
        return cardService;
    }
    /**
     * Retrieves an instance if this class.
     * @return an instance if this class
     */
    public static ServiceFactory getInstance() {
        return instance;
    }
}
