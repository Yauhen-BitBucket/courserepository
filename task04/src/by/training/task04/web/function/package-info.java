/**
 * Contains classes which hold static functions.
 * @author Yauhen Sazonau
 * @since 1.0
 */
package by.training.task04.web.function;
