/*
 * FunctionHolder.java
 * A class for storing static functions.
 */
package by.training.task04.web.function;


import static org.apache.commons.beanutils.PropertyUtils.isReadable;

import by.training.task04.exception.ApplicationException;

import javax.servlet.jsp.jstl.core.LoopTagStatus;


/**
 * A class for storing static functions.
 * @author Yauhen Sazonau
 * @version 1.0, 06/18/19
 * @since 1.0
 */
public final class FunctionHolder {
    /**
     * Constructs an instance of this class.
     */
    private FunctionHolder() {
        /* The default initialization is sufficient. */
    }
    /**
     * Verifies whether the specified property exists in the specified bean.
     * @param bean the <code>Object</code> value which is a target bean
     * @param property the <code>String</code> value which is a property name
     * @return <code>true</code> if the specified property exists in the
     *         specified bean, <code>false</code> otherwise
     * @throws ApplicationException if one of the arguments is a
     *                              <code>null</code> value
     */
    public static boolean testPropertyExistence(final Object bean,
            final String property) {
        if (bean == null) {
            throw new ApplicationException("The 'bean' argument is a 'null' "
                    + "value.");
        }
        if (property == null) {
            throw new ApplicationException("The 'property' argument is a "
                    + "'null' value.");
        }
        return (isReadable(bean, property));
    }
    /**
     * Verifies whether the current round of the iteration is the last one.
     * @param status the <code>LoopTagStatus</code> value which is an iteration
     *               status
     * @return <code>true</code> if the current round of the iteration is the
     *         last one, <code>false</code> otherwise
     * @throws ApplicationException if the argument is a <code>null</code>
     *                              value
     */
    public static boolean testLastIteration(final LoopTagStatus status) {
        if (status == null) {
            throw new ApplicationException("The 'status' argument is a 'null' "
                    + "value.");
        }
        return (status.isLast());
    }
}
