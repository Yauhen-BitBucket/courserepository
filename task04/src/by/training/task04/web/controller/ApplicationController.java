/*
 * ApplicationController.java
 * An application controller.
 */
package by.training.task04.web.controller;


import static by.training.task04.web.command.ParameterNames.PARAMETER_NAME_1;

import by.training.task04.web.command.Command;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * An application controller.
 * @author Yauhen Sazonau
 * @version 1.0, 06/16/19
 * @since 1.0
 */
public class ApplicationController extends HttpServlet {
    /**
     * A command retriever. <code>null</code> value is prohibited.
     */
    private final CommandRetriever commandRetriever = new CommandRetriever();
    /**
     * Processes a GET request received from the <code>protected</code>
     * <code>service</code> method.
     * @param request the <code>HttpServletRequest</code> value which is a
     *                GET request a user has made to the servlet
     * @param resopnse the <code>HttpServletResponse</code> value which is
     *                 a response the servlet sends to a user
     * @throws IOException if an input or output error occurs while this
     *                     controller handles a GET request
     * @throws ServletException if a GET request could not be handled
     */
    @Override
    protected void doGet(final HttpServletRequest request,
            final HttpServletResponse resopnse) throws ServletException,
                                                       IOException {
        processRequest(request, resopnse);
    }
    /**
     * Processes a POST request received from the <code>protected</code>
     * <code>service</code> method.
     * @param request the <code>HttpServletRequest</code> value which is a
     *                POST request a user has made to the servlet
     * @param resopnse the <code>HttpServletResponse</code> value which is
     *                 a response the servlet sends to a user
     * @throws IOException if an input or output error occurs while this
     *                     controller handles a POST request
     * @throws ServletException if a POST request could not be handled
     */
    @Override
    protected void doPost(final HttpServletRequest request,
            final HttpServletResponse resopnse) throws ServletException,
                                                       IOException {
        processRequest(request, resopnse);
    }
    /**
     * Responses to a user request.
     * @param request the <code>HttpServletRequest</code> value which is a
     *                GET or POST request a user has made to the servlet
     * @param resopnse the <code>HttpServletResponse</code> value which is
     *                 a response the servlet sends to a user
     * @throws IOException if an input or output error occurs while this
     *                     controller handles a GET or POST request
     * @throws ServletException if a GET or POST request could not be handled
     */
    private void processRequest(final HttpServletRequest request,
            final HttpServletResponse resopnse) throws ServletException,
                                                       IOException {
        Command command;
        RequestDispatcher dispatcher;
        String commandName = request.getParameter(PARAMETER_NAME_1);
        String path;
        /* The main part of the method. */
        command = commandRetriever.retrieveCommand(commandName);
        path = command.execute(request, getServletContext());
        dispatcher = getServletContext().getRequestDispatcher(
                resopnse.encodeURL(path));
        dispatcher.forward(request, resopnse);
    }
}
