/* CommandRetriever.java
 * A command retriever.
 */
package by.training.task04.web.controller;


import by.training.task04.exception.ApplicationException;

import by.training.task04.web.command.Command;
import by.training.task04.web.command.CommandName;
import by.training.task04.web.command.LocaleChangingCommand;
import by.training.task04.web.command.ParsingCommand;
import by.training.task04.web.command.RedirectionCommand;

import java.util.EnumMap;
import java.util.Map;


/**
 * A command retriever.
 * @version 1.0, 06/16/19
 * @since 1.0
 */
class CommandRetriever {
    /**
     * A mapping of command names to command implementations. <code>null</code>
     * value is prohibited.
     */
    private final Map<CommandName, Command> commandMapping
            = new EnumMap<>(CommandName.class);
    /**
     * Creates an instance of this class.
     */
    CommandRetriever() {
        commandMapping.put(CommandName.PARSING_COMMAND, new ParsingCommand());
        commandMapping.put(CommandName.REDIRECTION_COMMAND,
                new RedirectionCommand());
        commandMapping.put(CommandName.LOCALE_CHANGING_COMMAND,
                new LocaleChangingCommand());
    }
    /**
     * Retrieves a command using a command name.
     * @param name the <code>String</code> value which is the name of command
     *             to retrieve. It should be a hyphen-joined string, a word
     *             case daoen't matter. Accepted values are "parsing-command",
     *             "redirection-command", "locale-changing-command".
     * @return a command using a command name
     * @throws ApplicationException if the argument is a <code>null</code>
     *                              value or a command name isn't known
     */
    Command retrieveCommand(final String name) {
        Command command = null;
        CommandName commandName = null;
        String processedName;
        /* The main part of the method. */
        try {
            processedName = name.toUpperCase();
            processedName = processedName.replace("-", "_");
            commandName = CommandName.valueOf(processedName);
            command = commandMapping.get(commandName);
        } catch (NullPointerException | IllegalArgumentException e) {
            throw new ApplicationException("The specified command name isn't "
                    + "known: " + name + ".", e);
        }
        return command;
    }
}
