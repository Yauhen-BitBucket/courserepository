/*
 * LocaleChangingCommand.java
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to change the locale in use.
 */
package by.training.task04.web.command;


import static by.training.task04.web.command.ParameterNames.PARAMETER_NAME_4;

import by.training.task04.exception.ApplicationException;

import by.training.task04.reader.ApplicationSettingsReader;

import java.util.Locale;

import javax.servlet.ServletContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


/**
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to change the locale in use.
 * @author Yauhen Sazonau
 * @version 1.0, 06/16/19
 * @since 1.0
 */
public class LocaleChangingCommand implements Command {
    /* This class implements the ICommand interface. */
    /**
     * The name of a context attribute which is used to retain an instance of
     * the <code>java.util.Locale</code> class. The instance will be used if
     * a user doesn't change the default setting. The identifier holds the
     * value of "defaultLocale".
     */
    public static final String DEFAULT_LOCALE_ATTRIBUTE_NAME
            = "defaultLocale";
    /**
     * The locale which represents English language. Can't be a
     * <code>null</code> value.
     */
    public static final Locale ENGLISH_LANGUAGE_LOCALE = Locale.ENGLISH;
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>java.util.Locale</code> class. The identifier holds the
     * value of "locale".
     */
    public static final String LOCALE_ATTRIBUTE_NAME = "locale";
    /**
     * The Russian language code. The identifier holds the value "ru".
     */
    public static final String RUSSIAN_LANGUAGE_CODE = "ru";
    /**
     * The locale which represents Russian language. Can't be a
     * <code>null</code> value.
     */
    public static final Locale RUSSIAN_LANGUAGE_LOCALE
            = new Locale(RUSSIAN_LANGUAGE_CODE);
    /**
     * Constructs an instance of this class.
     */
    public LocaleChangingCommand() {
        /* The default initialization is sufficient. */
    }
    /**
     * Fulfills actions needed to change the locale in use.
     * @param request the <code>HttpServletRequest</code> value that is a
     *                   request to change the locale in use
     * @param servletContext the <code>ServletContext</code> value that is
     *                       the servlet context of the web application in
     *                       which a caller is executing
     * @return a <code>String</code> specifying the path to a resource which
     *            is relative to the current context root. The path points to
     *            the value specified in the request. It can't be a
     *            <code>null</code> value.
     * @throws ApplicationException if one of the method arguments is a
     *                                 <code>null</code> value
     */
    public String execute(final HttpServletRequest request,
                          final ServletContext servletContext) {
        ApplicationSettingsReader        applicationSettingsReader;
        HttpSession                        session;
        Locale                            defaultLocale;
        Locale                            localeInUse;
        Locale                            sessionLocale;
        String                            pathKey;
        String                             resourcePath;
        /* The main part of the method. */
        if (request == null) {
            throw new ApplicationException("The 'request' argument is "
                    + "'null'.");
        }
        if (servletContext == null) {
            throw new ApplicationException("The 'servletContext' argument "
                    + "is 'null'.");
        }
        applicationSettingsReader = ApplicationSettingsReader.getInstance();
        session = request.getSession();
        /*
         * Retrieving the value of a parameter which holds the key of a
         * property of a path.
         */
        pathKey = request.getParameter(PARAMETER_NAME_4);
        /*
         * Retrieving the value of the locale in use.
         */
        defaultLocale
                = (Locale) servletContext
                           .getAttribute(DEFAULT_LOCALE_ATTRIBUTE_NAME);
        sessionLocale = (Locale) session.getAttribute(LOCALE_ATTRIBUTE_NAME);
        if (sessionLocale != null) {
            localeInUse = sessionLocale;
        } else {
            localeInUse = defaultLocale;
        }
        if (ENGLISH_LANGUAGE_LOCALE.equals(localeInUse)) {
            session.setAttribute(LOCALE_ATTRIBUTE_NAME,
                                 RUSSIAN_LANGUAGE_LOCALE);
        } else if (RUSSIAN_LANGUAGE_LOCALE.equals(localeInUse)) {
            session.setAttribute(LOCALE_ATTRIBUTE_NAME,
                                 ENGLISH_LANGUAGE_LOCALE);
        }
        resourcePath = applicationSettingsReader.getProperty(pathKey);
        return resourcePath;
    }
}
