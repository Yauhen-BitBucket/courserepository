/* CommandName.java
 * A command name. The following commands are considered: a command of
 * retrieving postal cards using a parser, a redirection command, a command of
 * changing a locale in use.
 */
package by.training.task04.web.command;


/**
 * A command name. The following commands are considered: a command of
 * retrieving postal cards using a parser, a redirection command, a command of
 * changing a locale in use.
 * @version 1.0, 06/16/19
 * @since 1.0
 */
public enum CommandName {
    /**
     * Represents a command of retrieving postal cards using a parser.
     */
    PARSING_COMMAND,
    /**
     * Represents a redirection command.
     */
    REDIRECTION_COMMAND,
    /**
     * Represents a command of changing a locale in use.
     */
    LOCALE_CHANGING_COMMAND
}
