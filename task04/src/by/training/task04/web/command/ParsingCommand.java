/* ParsingCommand.java
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to retrieve postal cards using a
 * parser.
 */
package by.training.task04.web.command;


import static by.training.task04.reader.ApplicationSettingsReader
        .RESULT_PAGE_PATH_KEY;

import static by.training.task04.web.command.ParameterNames.PARAMETER_NAME_2;
import static by.training.task04.web.command.ParameterNames.PARAMETER_NAME_3;

import static java.nio.charset.StandardCharsets.UTF_8;

import by.training.task04.builder.AbstractBuilder;
import by.training.task04.builder.BuilderFactory;

import by.training.task04.entity.Card;

import by.training.task04.exception.ApplicationException;

import by.training.task04.reader.ApplicationSettingsReader;

import java.io.IOException;
import java.io.InputStreamReader;

import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;


/**
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to retrieve postal cards using a
 * parser.
 * @version 1.0, 06/16/19
 * @since 1.0
 */
public class ParsingCommand implements Command {
    /**
     * An attribute name used to store postal cards. The identifier holds the
     * value of "cards".
     */
    private static final String CARD_ATTRIBUTE_NAME = "cards";
    /**
     * Performs actions needed to retrieve postal cards using a parser.
     * @param request the <code>HttpServletRequest</code> value which is a
     *                request a user has made
     * @param servletContext the <code>ServletContext</code> value that is
     *                       the servlet context of the web application in
     *                       which a caller is executing
     * @return a <code>String</code> specifying the path to a resource
     *         which is relative to the current context root. The path points
     *         to the result page. It can't be a <code>null</code> value.
     * @throws ApplicationException if there are problems with performing
     *                              actions represented by this command
     */
    @Override
    public String execute(final HttpServletRequest request,
            final ServletContext servletContext) {
        AbstractBuilder builder;
        ApplicationSettingsReader settingsReader
                = ApplicationSettingsReader.getInstance();
        HttpSession session = request.getSession();
        Part part;
        InputStreamReader reader;
        Set<Card> cards;
        String technology = request.getParameter(PARAMETER_NAME_3);
        /* The main part of the method. */
        try {
            part = request.getPart(PARAMETER_NAME_2);
            reader = new InputStreamReader(part.getInputStream(), UTF_8);
            builder = BuilderFactory.createBuilder(technology);
            builder.buildPostalCards(reader);
            cards = builder.getPostalCards();
            session.setAttribute(CARD_ATTRIBUTE_NAME, cards);
        } catch (ApplicationException | IOException | ServletException e) {
            throw new ApplicationException("There are problems with executing "
                    + "the command of retrieving postal cards using a "
                    + "parser.", e);
        }
        return settingsReader.getProperty(RESULT_PAGE_PATH_KEY);
    }
}
