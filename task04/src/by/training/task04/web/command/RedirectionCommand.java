/*
 * RedirectionCommand.java
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to redirect it to the specified
 * path.
 */
package by.training.task04.web.command;


import static by.training.task04.web.command.ParameterNames.PARAMETER_NAME_4;

import by.training.task04.exception.ApplicationException;

import by.training.task04.reader.ApplicationSettingsReader;

import javax.servlet.ServletContext;

import javax.servlet.http.HttpServletRequest;


/**
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to redirect it to the specified
 * path.
 * @author Yauhen Sazonau
 * @version 1.0, 06/16/19
 * @since 1.0
 */
public class RedirectionCommand implements Command {
    /* This class implements the ICommand interface. */
    /**
     * Constructs an instance of this class.
     */
    public RedirectionCommand() {
        /* The default initialization is sufficient. */
    }
    /**
     * Fulfills actions needed to redirect an incoming request to the
     * specified path.
     * @param request the <code>HttpServletRequest</code> value that is a
     *                request to redirect it to the specified path
     * @param servletContext the <code>ServletContext</code> value that is
     *                       the servlet context of the web application in
     *                       which a caller is executing
     * @return a <code>String</code> specifying the path to a resource which
     *         is relative to the current context root. It can't be a
     *         <code>null</code> value.
     * @throws ApplicationException if one of the method arguments is a
     *                              <code>null</code> value
     */
    public String execute(final HttpServletRequest request,
                          final ServletContext servletContext) {
        ApplicationSettingsReader applicationSettingsReader;
        String pathKey;
        String resourcePath;
        /* The main part of the method. */
        if (request == null) {
            throw new ApplicationException("The 'request' argument is "
                    + "'null'.");
        }
        if (servletContext == null) {
            throw new ApplicationException("The 'servletContext' argument "
                    + "is 'null'.");
        }
        applicationSettingsReader = ApplicationSettingsReader.getInstance();
        /*
         * Retrieving the value of a parameter which holds the key of a
         * property of a path.
         */
        pathKey = request.getParameter(PARAMETER_NAME_4);
        resourcePath = applicationSettingsReader.getProperty(pathKey);
        return resourcePath;
    }
}
