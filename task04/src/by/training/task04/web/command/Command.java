/* Command.java
 * A contract which a particular command should comply with.
 */
package by.training.task04.web.command;


import javax.servlet.ServletContext;

import javax.servlet.http.HttpServletRequest;


/**
 * A contract which a particular command should comply with.
 * @version 1.0, 06/16/19
 * @since 1.0
 */
public interface Command {
    /**
     * Performs actions represented by a particular command.
     * @param request the <code>HttpServletRequest</code> value which is a
     *                request a user has made
     * @param servletContext the <code>ServletContext</code> value that is
     *                       the servlet context of a web application in
     *                       which a caller is executing
     * @return a <code>String</code> specifying the path to a resource
     *         which is relative to the current context root. It can't be a
     *         <code>null</code> value.

     * @throws ApplicationException if there are problems with performing
     *                              actions represented by a particular command
     */
    String execute(HttpServletRequest request, ServletContext servletContext);
}
