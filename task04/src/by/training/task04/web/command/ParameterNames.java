/*
 * ParameterNames.java
 * A class for storing request parameter names.
 */
package by.training.task04.web.command;


/**
 * A class for storing request parameter names.
 * @author Yauhen Sazonau
 * @version 1.0, 06/16/19
 * @since 1.0
 */
public final class ParameterNames {
    /**
     * A request parameter name. The identifier holds the value of "command".
     */
    public static final String PARAMETER_NAME_1 = "command";
    /**
     * A request parameter name. The identifier holds the value of "file".
     */
    public static final String PARAMETER_NAME_2 = "file";
    /**
     * A request parameter name. The identifier holds the value of
     * "technology".
     */
    public static final String PARAMETER_NAME_3 = "technology";
    /**
     * A request parameter name. The identifier holds the value of
     * "pathPropertyKey".
     */
    public static final String PARAMETER_NAME_4 = "pathPropertyKey";
    /**
     * Constructs an instance of this class.
     */
    private ParameterNames() {
        /* The default initialization is sufficient. */
    }
}
