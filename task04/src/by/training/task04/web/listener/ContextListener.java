/*
 * ContextListener.java
 * The listener which fulfills an initialization of a web application and
 * releases resources used during its life cycle.
 */
package by.training.task04.web.listener;


import static by.training.task04.reader.ApplicationSettingsReader
        .CONTROLLER_PATH_KEY;
import static by.training.task04.reader.ApplicationSettingsReader
        .RESULT_PAGE_PATH_KEY;
import static by.training.task04.reader.ApplicationSettingsReader
        .WELCOME_PAGE_PATH_KEY;

import static by.training.task04.web.command.LocaleChangingCommand
        .RUSSIAN_LANGUAGE_LOCALE;

import by.training.task04.reader.ApplicationSettingsReader;

import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;


/**
 * The listener which fulfills an initialization of a web application and
 * releases resources used during its life cycle.
 * @author Yauhen Sazonau
 * @version 1.0, 06/17/19
 * @since 1.0
 */
public class ContextListener implements ServletContextListener {
    /**
     * The name of a context attribute which is used to retain an instance of
     * the <code>String</code> class that is the path to an application
     * controller which is relative to the current context root. The
     * identifier holds the value of "controllerPath".
     */
    public static final String CONTROLLER_PATH_ATTRIBUTE_NAME
            = "controllerPath";
    /**
     * The name of a context attribute which is used to retain an instance of
     * the <code>java.util.Locale</code> class. The instance will be used if
     * a user doesn't change the default setting. The identifier holds the
     * value of "defaultLocale".
     */
    public static final String DEFAULT_LOCALE_ATTRIBUTE_NAME
            = "defaultLocale";
    /**
     * The name of a context attribute which is used to retain an instance of
     * the <code>String</code> class that is the key of the result page path.
     * The identifier holds the value of "resultPagePathKey".
     */
    public static final String RESULT_PAGE_PATH_KEY_ATTRIBUTE_NAME
            = "resultPagePathKey";
    /**
     * The name of a context attribute which is used to retain an instance of
     * the <code>String</code> class that is the key of the welcome page path.
     * The identifier holds the value of "welcomePagePathKey".
     */
    public static final String WELCOME_PAGE_PATH_KEY_ATTRIBUTE_NAME
            = "welcomePagePathKey";
    /**
     * Releases resources used during the life cycle of an application.
     * @param servletContextEvent the <code>ServletContextEvent</code> value
     *                            which is the event notifying that the
     *                            servlet context is about to be shut down
     */
    @Override
    public void contextDestroyed(
            final ServletContextEvent servletContextEvent) {
        /* Does nothing. */
    }
    /**
     * Fulfills an initialization of a web application. It includes
     * instantiating the Singleton classes. Also context attributes used by
     * the application are stored.
     * @param servletContextEvent the <code>ServletContextEvent</code> value
     *                            which is the event of starting a web
     *                            application initialization process
     */
    @Override
    public void contextInitialized(
            final ServletContextEvent servletContextEvent) {
        ApplicationSettingsReader reader
                = ApplicationSettingsReader.getInstance();
        Locale defaultLocale = RUSSIAN_LANGUAGE_LOCALE;
        ServletContext context = servletContextEvent.getServletContext();
        /* The main part of the method. */
        context.setAttribute(CONTROLLER_PATH_ATTRIBUTE_NAME,
                reader.getProperty(CONTROLLER_PATH_KEY));
        context.setAttribute(RESULT_PAGE_PATH_KEY_ATTRIBUTE_NAME,
                RESULT_PAGE_PATH_KEY);
        context.setAttribute(WELCOME_PAGE_PATH_KEY_ATTRIBUTE_NAME,
                WELCOME_PAGE_PATH_KEY);
        context.setAttribute(DEFAULT_LOCALE_ATTRIBUTE_NAME, defaultLocale);
    }
}
