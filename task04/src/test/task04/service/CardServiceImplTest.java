/*
 * CardServiceImplTest.java
 * A TestNG class used to test functionality of the CardServiceImpl class.
 */
package test.task04.service;


import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertThrows;

import static test.task04.service.YearValues.YEAR_VALUE_1;
import static test.task04.service.YearValues.YEAR_VALUE_2;
import static test.task04.service.YearValues.YEAR_VALUE_3;
import static test.task04.service.YearValues.YEAR_VALUE_4;
import static test.task04.service.YearValues.YEAR_VALUE_5;
import static test.task04.service.YearValues.YEAR_VALUE_6;
import static test.task04.service.YearValues.YEAR_VALUE_7;
import static test.task04.service.YearValues.YEAR_VALUE_8;
import static test.task04.service.YearValues.YEAR_VALUE_9;
import static test.task04.service.YearValues.YEAR_VALUE_10;
import static test.task04.service.YearValues.YEAR_VALUE_11;
import static test.task04.service.YearValues.YEAR_VALUE_12;
import static test.task04.service.YearValues.YEAR_VALUE_13;
import static test.task04.service.YearValues.YEAR_VALUE_14;
import static test.task04.service.YearValues.YEAR_VALUE_15;
import static test.task04.service.YearValues.YEAR_VALUE_16;
import static test.task04.service.YearValues.YEAR_VALUE_17;

import by.training.task04.entity.Address;
import by.training.task04.entity.BLRAddress;
import by.training.task04.entity.Card;
import by.training.task04.entity.CardType;
import by.training.task04.entity.CardValue;
import by.training.task04.entity.ImageTheme;

import by.training.task04.exception.ApplicationException;

import by.training.task04.service.CardService;

import by.training.task04.service.CardService.XMLParserName;

import by.training.task04.service.ServiceException;
import by.training.task04.service.ServiceFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


/**
 * A TestNG class used to test functionality of the
 * <code>CardServiceImpl</code> class.
 * @author Yauhen Sazonau
 * @version 1.0, 06/12/19
 * @since 1.0
 */
public class CardServiceImplTest {
    /**
     * A set of cards which should be created after parsing. The parsing
     * technology doesn't matter.
     */
    private Set<Card> cards;
    /**
     * Verifies an XML document parsing using the specified technology.
     * @param technologyName the <code>XMLParserName</code> value which is a
     *                       technology name. Allowed values are
     *                       <code>XMLParserName.SAX</code>,
     *                       <code>XMLParserName.DOM</code>,
     *                       <code>XMLParserName.STAX</code>.
     * @throws ApplicationException if there are problems with parsing testing
     */
    @Test(description = "Verifies an XML document parsing using the specified "
                        + "technology.",
          dataProvider = "technologyNameProvider")
    public void testPositiveParsing(final XMLParserName technologyName) {
        ServiceFactory factory = ServiceFactory.getInstance();
        CardService service = factory.retrieveCardService();
        Set<Card> actual;
        /* The main part of the method. */
        try {
            actual = service.retrieveCards(technologyName);
        } catch (ServiceException e) {
            throw new ApplicationException("There are problems with parsing "
                    + "testing.", e);
        }
        assertEquals(actual, cards);
    }
    /**
     * Verifies throwing the <code>ServiceException</code> exception.
     * @param technologyName the <code>XMLParserName</code> value which is a
     *                       technology name. Allowed values are
     *                       <code>XMLParserName.DUMMY</code>, a
     *                       <code>null</code> value.
     */
    @Test(description = "Verifies throwing the ServiceException.",
          dataProvider = "pseudoTechnologyNameProvider")
    public void testNegativeParsing(final XMLParserName technologyName) {
        ServiceFactory factory = ServiceFactory.getInstance();
        CardService service = factory.retrieveCardService();
        /* The main part of the method. */
        assertThrows(ServiceException.class, () -> {
            service.retrieveCards(technologyName);
        });
    }
    /**
     * Supplies a parsing technology name.
     * @return a parsing technology name. One of
     *         <code>XMLParserName.SAX</code>, <code>XMLParserName.DOM</code>,
     *         <code>XMLParserName.STAX</code>.
     */
    @DataProvider(name = "technologyNameProvider")
    public Object[][] provideTechnologyName() {
        return new Object[][]{{XMLParserName.SAX}, {XMLParserName.DOM},
                              {XMLParserName.STAX}};
    }
    /**
     * Supplies a pseudo-technology name.
     * @return a pseudo-technology name.
     */
    @DataProvider(name = "pseudoTechnologyNameProvider")
    public Object[][] providePseudoTechnologyName() {
        return new Object[][]{{XMLParserName.DUMMY}, {null}};
    }
    /**
     * Creates a set of cards used for testing.
     */
    @BeforeTest
    public void createCards() {
        Address tempAddress;
        Card tempCard;
        List<String> tempAuthors;
        /* The main part of the method. */
        cards = new HashSet<>();
        /* Filling the set. */
        tempCard = new Card();
        tempCard.setSentState(false);
        tempCard.setType(CardType.CONGRATULATION);
        tempCard.setIssueStamp("1800-02-02T14:14:00");
        tempCard.setNumber("num777800");
        tempCard.setValue(CardValue.HISTORIC);
        tempCard.setTheme(ImageTheme.CITY_LANDSCAPE);
        tempCard.setCountry("Brazil");
        tempCard.setYear(YEAR_VALUE_1);
        tempAuthors = new ArrayList<>();
        tempAuthors.add("Lula da Silva");
        tempCard.setAuthors(tempAuthors);
        tempAddress = new Address();
        tempAddress.setCity("Buenos Aires");
        tempAddress.setCountry("Argentina");
        tempAddress.setStreet("St. Dominica, 25");
        tempCard.setDestination(tempAddress);
        cards.add(tempCard);
        tempCard = new Card();
        tempCard.setSentState(false);
        tempCard.setType(CardType.ORDINARY);
        tempCard.setIssueStamp("1831-12-02T12:12:18");
        tempCard.setNumber("num777801");
        tempCard.setValue(CardValue.THEMATIC);
        tempCard.setTheme(ImageTheme.NATURE);
        tempCard.setCountry("Germany");
        tempCard.setYear(YEAR_VALUE_2);
        tempAuthors = new ArrayList<>();
        tempAuthors.add("Thomas Muller");
        tempCard.setAuthors(tempAuthors);
        tempAddress = new Address();
        tempAddress.setCity("Munich");
        tempAddress.setCountry("Germany");
        tempAddress.setStreet("Blumenstraße, 37");
        tempCard.setDestination(tempAddress);
        cards.add(tempCard);
        tempCard = new Card();
        tempCard.setSentState(false);
        tempCard.setType(CardType.ORDINARY);
        tempCard.setIssueStamp("1900-03-03T01:01:01");
        tempCard.setNumber("num777802");
        tempCard.setValue(CardValue.THEMATIC);
        tempCard.setTheme(ImageTheme.NATURE);
        tempCard.setCountry("France");
        tempCard.setYear(YEAR_VALUE_3);
        tempAuthors = new ArrayList<>();
        tempAuthors.add("Gourge Petit");
        tempCard.setAuthors(tempAuthors);
        tempAddress = new Address();
        tempAddress.setCity("Paris");
        tempAddress.setCountry("France");
        tempAddress.setStreet("De fleur, 1");
        tempCard.setDestination(tempAddress);
        cards.add(tempCard);
        tempCard = new Card();
        tempCard.setSentState(false);
        tempCard.setType(CardType.ADVERTISEMENT);
        tempCard.setIssueStamp("2000-04-04T02:02:02");
        tempCard.setNumber("num777803");
        tempCard.setValue(CardValue.THEMATIC);
        tempCard.setTheme(ImageTheme.SPORT);
        tempCard.setCountry("France");
        tempCard.setYear(YEAR_VALUE_4);
        tempAuthors = new ArrayList<>();
        tempAuthors.add("Alex Muex");
        tempCard.setAuthors(tempAuthors);
        tempAddress = new Address();
        tempAddress.setCity("Lion");
        tempAddress.setCountry("France");
        tempAddress.setStreet("De fleur, 17");
        tempCard.setDestination(tempAddress);
        cards.add(tempCard);
        tempCard = new Card();
        tempCard.setSentState(false);
        tempCard.setType(CardType.CONGRATULATION);
        tempCard.setIssueStamp("1956-05-05T05:05:05");
        tempCard.setNumber("num777804");
        tempCard.setValue(CardValue.THEMATIC);
        tempCard.setTheme(ImageTheme.PEOPLE);
        tempCard.setCountry("СССР");
        tempCard.setYear(YEAR_VALUE_5);
        tempAuthors = new ArrayList<>();
        tempAuthors.add("Иван Петров");
        tempCard.setAuthors(tempAuthors);
        tempAddress = new Address();
        tempAddress.setCity("Одесса");
        tempAddress.setCountry("СССР");
        tempAddress.setStreet("Ленина, 2");
        tempCard.setDestination(tempAddress);
        cards.add(tempCard);
        tempCard = new Card();
        tempCard.setSentState(false);
        tempCard.setType(CardType.ADVERTISEMENT);
        tempCard.setIssueStamp("1929-06-06T06:06:06");
        tempCard.setNumber("num777805");
        tempCard.setValue(CardValue.THEMATIC);
        tempCard.setTheme(ImageTheme.PEOPLE);
        tempCard.setCountry("СССР");
        tempCard.setYear(YEAR_VALUE_6);
        tempAuthors = new ArrayList<>();
        tempAuthors.add("Пётр Иванов");
        tempCard.setAuthors(tempAuthors);
        tempAddress = new Address();
        tempAddress.setCity("Киев");
        tempAddress.setCountry("СССР");
        tempAddress.setStreet("Энгельса, 28");
        tempCard.setDestination(tempAddress);
        cards.add(tempCard);
        tempCard = new Card();
        tempCard.setSentState(false);
        tempCard.setType(CardType.ORDINARY);
        tempCard.setIssueStamp("1977-07-07T07:07:07");
        tempCard.setNumber("num777806");
        tempCard.setValue(CardValue.THEMATIC);
        tempCard.setTheme(ImageTheme.RELIGION);
        tempCard.setCountry("Italy");
        tempCard.setYear(YEAR_VALUE_7);
        tempAuthors = new ArrayList<>();
        tempAuthors.add("Gianluca Rossi");
        tempAuthors.add("Gabriela Toccata");
        tempCard.setAuthors(tempAuthors);
        tempAddress = new Address();
        tempAddress.setCity("Florezia");
        tempAddress.setCountry("Italy");
        tempAddress.setStreet("Colori, 89");
        tempCard.setDestination(tempAddress);
        cards.add(tempCard);
        tempCard = new Card();
        tempCard.setSentState(false);
        tempCard.setType(CardType.ORDINARY);
        tempCard.setIssueStamp("1970-08-08T08:08:08");
        tempCard.setNumber("num777807");
        tempCard.setValue(CardValue.THEMATIC);
        tempCard.setTheme(ImageTheme.RELIGION);
        tempCard.setCountry("Spain");
        tempCard.setYear(YEAR_VALUE_8);
        tempAuthors = new ArrayList<>();
        tempAuthors.add("Jose Tavares");
        tempCard.setAuthors(tempAuthors);
        tempAddress = new Address();
        tempAddress.setCity("Madrid");
        tempAddress.setCountry("Spain");
        tempAddress.setStreet("Rojo, 38");
        tempCard.setDestination(tempAddress);
        cards.add(tempCard);
        tempCard = new Card();
        tempCard.setSentState(false);
        tempCard.setType(CardType.ORDINARY);
        tempCard.setIssueStamp("1958-09-09T09:09:09");
        tempCard.setNumber("num777808");
        tempCard.setValue(CardValue.THEMATIC);
        tempCard.setTheme(ImageTheme.ARCHITECTURE);
        tempCard.setCountry("Spain");
        tempCard.setYear(YEAR_VALUE_9);
        tempAuthors = new ArrayList<>();
        tempAuthors.add("Julo Scarses");
        tempCard.setAuthors(tempAuthors);
        tempAddress = new Address();
        tempAddress.setCity("Sevilla");
        tempAddress.setCountry("Spain");
        tempAddress.setStreet("Uno, 19");
        tempCard.setDestination(tempAddress);
        cards.add(tempCard);
        tempCard = new Card();
        tempCard.setSentState(false);
        tempCard.setType(CardType.ORDINARY);
        tempCard.setIssueStamp("1858-10-10T10:10:10");
        tempCard.setNumber("num777809");
        tempCard.setValue(CardValue.HISTORIC);
        tempCard.setTheme(ImageTheme.ARCHITECTURE);
        tempCard.setCountry("Portugal");
        tempCard.setYear(YEAR_VALUE_10);
        tempAuthors = new ArrayList<>();
        tempAuthors.add("Juao Pinto");
        tempCard.setAuthors(tempAuthors);
        tempAddress = new Address();
        tempAddress.setCity("Lissabon");
        tempAddress.setCountry("Portugal");
        tempAddress.setStreet("Meireles, 91");
        tempCard.setDestination(tempAddress);
        cards.add(tempCard);
        tempCard = new Card();
        tempCard.setSentState(false);
        tempCard.setType(CardType.ORDINARY);
        tempCard.setIssueStamp("1998-11-11T11:11:11");
        tempCard.setNumber("num777810");
        tempCard.setValue(CardValue.HISTORIC);
        tempCard.setTheme(ImageTheme.CITY_LANDSCAPE);
        tempCard.setCountry("Latvia");
        tempCard.setYear(YEAR_VALUE_11);
        tempAuthors = new ArrayList<>();
        tempAuthors.add("Jannis Spruts");
        tempCard.setAuthors(tempAuthors);
        tempAddress = new Address();
        tempAddress.setCity("Riga");
        tempAddress.setCountry("Latvia");
        tempAddress.setStreet("Lenina, 6");
        tempCard.setDestination(tempAddress);
        cards.add(tempCard);
        tempCard = new Card();
        tempCard.setSentState(false);
        tempCard.setType(CardType.ORDINARY);
        tempCard.setIssueStamp("1993-12-12T12:12:12");
        tempCard.setNumber("num777811");
        tempCard.setValue(CardValue.COLLECTION);
        tempCard.setTheme(ImageTheme.NATURE);
        tempCard.setCountry("Lithuania");
        tempCard.setYear(YEAR_VALUE_12);
        tempAuthors = new ArrayList<>();
        tempAuthors.add("Lita Melitaite");
        tempCard.setAuthors(tempAuthors);
        tempAddress = new Address();
        tempAddress.setCity("Vilnus");
        tempAddress.setCountry("Lithuania");
        tempAddress.setStreet("Engelsa, 2");
        tempCard.setDestination(tempAddress);
        cards.add(tempCard);
        tempCard = new Card();
        tempCard.setSentState(false);
        tempCard.setType(CardType.ORDINARY);
        tempCard.setIssueStamp("1999-04-04T14:59:59");
        tempCard.setNumber("num777812");
        tempCard.setValue(CardValue.COLLECTION);
        tempCard.setTheme(ImageTheme.PEOPLE);
        tempCard.setCountry("Estonia");
        tempCard.setYear(YEAR_VALUE_13);
        tempAuthors = new ArrayList<>();
        tempAuthors.add("Otto Etto");
        tempCard.setAuthors(tempAuthors);
        tempAddress = new Address();
        tempAddress.setCity("Tallin");
        tempAddress.setCountry("Estonia");
        tempAddress.setStreet("Engelsa, 63");
        tempCard.setDestination(tempAddress);
        cards.add(tempCard);
        tempCard = new Card();
        tempCard.setSentState(false);
        tempCard.setType(CardType.ADVERTISEMENT);
        tempCard.setIssueStamp("1801-04-04T15:59:59");
        tempCard.setNumber("num777813");
        tempCard.setValue(CardValue.HISTORIC);
        tempCard.setTheme(ImageTheme.PEOPLE);
        tempCard.setCountry("China");
        tempCard.setYear(YEAR_VALUE_14);
        tempAuthors = new ArrayList<>();
        tempAuthors.add("Lui Shun");
        tempCard.setAuthors(tempAuthors);
        tempAddress = new Address();
        tempAddress.setCity("Beijin");
        tempAddress.setCountry("China");
        tempAddress.setStreet("Hun Dao, 7");
        tempCard.setDestination(tempAddress);
        cards.add(tempCard);
        tempCard = new Card();
        tempCard.setSentState(false);
        tempCard.setType(CardType.ADVERTISEMENT);
        tempCard.setIssueStamp("1819-04-04T16:59:59");
        tempCard.setNumber("num777814");
        tempCard.setValue(CardValue.HISTORIC);
        tempCard.setTheme(ImageTheme.CITY_LANDSCAPE);
        tempCard.setCountry("Russian empire");
        tempCard.setYear(YEAR_VALUE_15);
        tempAuthors = new ArrayList<>();
        tempAuthors.add("Andrej Vasilevsi");
        tempCard.setAuthors(tempAuthors);
        tempAddress = new Address();
        tempAddress.setCity("St. Petersburg");
        tempAddress.setCountry("Russian empire");
        tempAddress.setStreet("Liteinyj, 17");
        tempCard.setDestination(tempAddress);
        cards.add(tempCard);
        tempCard = new Card();
        tempCard.setSentState(true);
        tempCard.setType(CardType.ORDINARY);
        tempCard.setIssueStamp("2010-04-04T17:59:59");
        tempCard.setNumber("num777815");
        tempCard.setValue(CardValue.THEMATIC);
        tempCard.setTheme(ImageTheme.CITY_LANDSCAPE);
        tempCard.setCountry("Беларусь");
        tempCard.setYear(YEAR_VALUE_16);
        tempAuthors = new ArrayList<>();
        tempAuthors.add("Антон Семёнов");
        tempCard.setAuthors(tempAuthors);
        tempAddress = new BLRAddress();
        tempAddress.setCity("Минск");
        tempAddress.setCountry("Беларусь");
        tempAddress.setStreet("Интернациональная, 5");
        ((BLRAddress) tempAddress).setPostalCode("220007");
        tempCard.setDestination(tempAddress);
        cards.add(tempCard);
        tempCard = new Card();
        tempCard.setSentState(true);
        tempCard.setType(CardType.ORDINARY);
        tempCard.setIssueStamp("2017-04-04T18:59:59");
        tempCard.setNumber("num777816");
        tempCard.setValue(CardValue.THEMATIC);
        tempCard.setTheme(ImageTheme.SPORT);
        tempCard.setCountry("США");
        tempCard.setYear(YEAR_VALUE_17);
        tempAuthors = new ArrayList<>();
        tempCard.setAuthors(tempAuthors);
        tempAddress = new BLRAddress();
        tempAddress.setCity("Брест");
        tempAddress.setCountry("Беларусь");
        tempAddress.setStreet("Коммунистическая, 22");
        ((BLRAddress) tempAddress).setPostalCode("220001");
        tempCard.setDestination(tempAddress);
        cards.add(tempCard);
    }
}
