/**
 * Contains a TestNG class for verifying functionality of the
 * <code>CardServiceImpl</code> class.
 * @author Yauhen Sazonau
 * @since 1.0
 */
package test.task04.service;
