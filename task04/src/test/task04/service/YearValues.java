/*
 * YearValues.java
 * A class for storing year values.
 */
package test.task04.service;


/**
 * A class for storing year values.
 * @author Yauhen Sazonau
 * @version 1.0, 06/12/19
 * @since 1.0
 */
public final class YearValues {
    /**
     * A year value. The identifier holds the value of "1800".
     */
    public static final short YEAR_VALUE_1 = 1800;
    /**
     * A year value. The identifier holds the value of "1831".
     */
    public static final short YEAR_VALUE_2 = 1831;
    /**
     * A year value. The identifier holds the value of "1900".
     */
    public static final short YEAR_VALUE_3 = 1900;
    /**
     * A year value. The identifier holds the value of "2000".
     */
    public static final short YEAR_VALUE_4 = 2000;
    /**
     * A year value. The identifier holds the value of "1956".
     */
    public static final short YEAR_VALUE_5 = 1956;
    /**
     * A year value. The identifier holds the value of "1929".
     */
    public static final short YEAR_VALUE_6 = 1929;
    /**
     * A year value. The identifier holds the value of "1977".
     */
    public static final short YEAR_VALUE_7 = 1977;
    /**
     * A year value. The identifier holds the value of "1970".
     */
    public static final short YEAR_VALUE_8 = 1970;
    /**
     * A year value. The identifier holds the value of "1958".
     */
    public static final short YEAR_VALUE_9 = 1958;
    /**
     * A year value. The identifier holds the value of "1858".
     */
    public static final short YEAR_VALUE_10 = 1858;
    /**
     * A year value. The identifier holds the value of "1998".
     */
    public static final short YEAR_VALUE_11 = 1998;
    /**
     * A year value. The identifier holds the value of "1993".
     */
    public static final short YEAR_VALUE_12 = 1993;
    /**
     * A year value. The identifier holds the value of "1999".
     */
    public static final short YEAR_VALUE_13 = 1999;
    /**
     * A year value. The identifier holds the value of "1801".
     */
    public static final short YEAR_VALUE_14 = 1801;
    /**
     * A year value. The identifier holds the value of "1819".
     */
    public static final short YEAR_VALUE_15 = 1819;
    /**
     * A year value. The identifier holds the value of "2010".
     */
    public static final short YEAR_VALUE_16 = 2010;
    /**
     * A year value. The identifier holds the value of "2017".
     */
    public static final short YEAR_VALUE_17 = 2017;
    /**
     * Constructs an instance of this class.
     */
    private YearValues() {
        /* The default initialization is sufficient. */
    }
}
