package thread19executorservicecallable;

import java.util.concurrent.Callable;

import java.util.Random;
public class CalcCallable implements Callable<Number> {
@Override
public Number call() throws Exception {
Number res = new Random().nextGaussian(); // имитация вычислений
return res;
}
}