Multithreading

There is a matrix that is initialized with integer values. All diagonal
elements are set to zero. The size of the matrix is N on N, where 8<= N <= 12.
There are M threads, where 4 <= M <= 6. Each thread attempts to set a diagonal
element to its value (a positive integer). A cell is allowed to be changed only
ones. All diagonal elements must be set to a non zero value.

An application has the following structure (packages):

 - by.training.task03.controller            a controller of this application
 - by.training.task03.controller.command    commands performed by the
                                            controller
 - by.training.task03.dao                   DAO objects
 - by.training.task03.exception             exceptions used by the application
 - by.training.task03.main                  contains a class which is an entry
                                            point of this application
 - by.training.task03.matrix        contains a matrix representation
 - by.training.task03.reader        contains a reader for reading application
                                    data
 - by.training.task03.service       services of this application
 - by.training.task03.task          contains classes which implement the
                                    Runnable interface
                                    
Used technologies: Log4j2.

Needed libraries: log4j-api-2.11.2.jar, log4j-core-2.11.2.jar.