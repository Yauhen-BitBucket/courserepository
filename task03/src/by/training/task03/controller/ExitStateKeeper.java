/* ApplicationController.java
 * A class used to keep an exit state of an application.
 */
package by.training.task03.controller;


/**
 * A class used to keep an exit state of an application.
 * @version 1.0, 06/04/19
 * @since 1.0
 */
public class ExitStateKeeper {
    /**
     * The exit state of an application. <code>false</code> if an application
     * should continue functioning, <code>true</code> otherwise.
     */
    private boolean exitState = false;
    /**
     * Retrieves the exit state of an application.
     * @return the exit state of an application
     */
    public boolean getExitState() {
        return exitState;
    }
    /**
     * Sets the exit state of an application.
     * @param state the <code>boolean</code> value which is the exit state to
     *              be set
     */
    public void setExitState(final boolean state) {
        exitState = state;
    }
}
