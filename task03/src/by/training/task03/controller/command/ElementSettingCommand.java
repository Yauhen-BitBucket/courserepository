/* ElementSettingCommand.java
 * A command used to set diagonal elements of a matrix to a non zero value.
 */
package by.training.task03.controller.command;


import by.training.task03.exception.ApplicationException;
import by.training.task03.exception.ServiceException;

import by.training.task03.service.ElementSettingService;
import by.training.task03.service.ServiceFactory;


/**
 * A command used to set diagonal elements of a matrix to a non zero value.
 * @version 1.0, 06/04/19
 * @since 1.0
 */
public class ElementSettingCommand implements Command {
    /**
     * Performs actions needed to set diagonal elements of a matrix to a non
     * zero value.
     * @throws ApplicationException if there are problems with performing
     *                              actions represented by this command
     */
    @Override
    public void execute() {
        ServiceFactory factory = ServiceFactory.getInstance();
        ElementSettingService service
                = factory.retrieveElementSettingService();
        /* The main part of the method. */
        try {
            service.setDiagonalElements();
        } catch (ServiceException e) {
            throw new ApplicationException("There are problems with executing "
                    + "the command of setting diagonal elements to a non zero "
                    + "value.", e);
        }
    }
}
