/* ReportPrintingCommand.java
 * A command used to print a matrix representation.
 */
package by.training.task03.controller.command;


import by.training.task03.exception.ApplicationException;
import by.training.task03.exception.ServiceException;

import by.training.task03.service.ReportPrintingService;
import by.training.task03.service.ServiceFactory;


/**
 * A command used to print a matrix representation.
 * @version 1.0, 06/04/19
 * @since 1.0
 */
public class ReportPrintingCommand implements Command {
    /**
     * Performs actions needed to print a matrix representation.
     * @throws ApplicationException if there are problems with performing
     *                              actions represented by this command
     */
    @Override
    public void execute() {
        ServiceFactory factory = ServiceFactory.getInstance();
        ReportPrintingService service
                = factory.retrieveReportPrintingService();
        /* The main part of the method. */
        try {
            service.printReport();
        } catch (ServiceException e) {
            throw new ApplicationException("There are problems with executing "
                    + "the command of printing a report.", e);
        }
    }
}
