/**
 * Provides various commands performed by an application controller.
 * @author Yauhen Sazonau
 * @since 1.0
 */
package by.training.task03.controller.command;
