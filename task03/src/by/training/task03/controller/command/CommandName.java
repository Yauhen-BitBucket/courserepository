/* CommandName.java
 * A command name. The following commands are considered: a command of setting
 * diagonal elements of a matrix to a non zero value, a command of printing a
 * matrix representation.
 */
package by.training.task03.controller.command;


/**
 * A command name. The following commands are considered: a command of setting
 * diagonal elements of a matrix to a non zero value, a command of printing a
 * matrix representation, a command of setting diagonal elements of a matrix to
 * zero, an exit command.
 * @version 1.0, 06/04/19
 * @since 1.0
 */
public enum CommandName {
    /**
     * Represents a command of setting diagonal elements of a matrix to zero.
     */
    ELEMENT_RESETTING_COMMAND,
    /**
     * Represents a command of setting diagonal elements of a matrix to a non
     * zero value.
     */
    ELEMENT_SETTING_COMMAND,
    /**
     * Represents a command of exiting an application.
     */
    EXIT_COMMAND,
    /**
     * Represents a command of printing a matrix representation.
     */
    REPORT_PRINTING_COMMAND
}
