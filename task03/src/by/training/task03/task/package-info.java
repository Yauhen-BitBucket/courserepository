/**
 * Contains task classes used by this application.
 * @author Yauhen Sazonau
 * @since 1.0
 */
package by.training.task03.task;
