/*
 * DiagonalElementSetter.java
 * A task used to set a particular diagonal element of a matrix.
 */
package by.training.task03.task;


import by.training.task03.exception.ApplicationException;
import by.training.task03.exception.DAOException;

import by.training.task03.matrix.Matrix;

import java.util.Random;

import java.util.concurrent.Phaser;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;


/**
 * A task used to set a particular diagonal element of a matrix.
 * @author Yauhen Sazonau
 * @version 1.0, 05/29/19
 * @since 1.0
 */
public class DiagonalElementSetter implements Runnable {
    /**
     * A logger for this application. <code>null</code> value is prohibited.
     */
    private static final Logger LOGGER
            = LogManager.getLogger(DiagonalElementSetter.class);
    /**
     * A value which is set by performing a task represented by this class.
     */
    private int value;
    /**
     * A phaser. <code>null</code> value is prohibited.
     */
    private Phaser phaser;
    /**
     * A generator used to generate integer values. <code>null</code> value is
     * prohibited.
     */
    private Random generator;
    /**
     * Constructs an instance of this class.
     * @param currentPhaser the <code>Phaser</code> value used by this instance
     * @param currentValue the <code>int</code> value used by this instance to
     *                     set by performing a task represented by this class
     * @throws ApplicationException if the 'currentPhaser' argument is a
     *                              <code>null</code> value
     */
    public DiagonalElementSetter(final Phaser currentPhaser,
            final int currentValue) {
        if (currentPhaser == null) {
            throw new ApplicationException("The argument is a 'null' value.");
        }
        phaser = currentPhaser;
        value = currentValue;
        generator = new Random();
    }
    /**
     * Represents a task of setting a diagonal element of a matrix.
     */
    public void run() {
        boolean condition = true;
        boolean isSet;
        Matrix matrix = null;
        try {
            matrix = Matrix.getInstance();
        } catch (DAOException e) {
            LOGGER.error("A matrix is incorrectly initialized.");
            condition = false;
        }
        if (condition) {
            int diagonalSize = matrix.getRowCount();
            while (!matrix.isAllDiagonalElementsSet()) {
                int index = generator.nextInt(diagonalSize);
                isSet = matrix.setDiagonalElement((byte) index, value);
                if (isSet) {
                    LOGGER.info(Thread.currentThread().getName() + " set "
                            + value + " in " + index + " position.");
                }
                phaser.arriveAndAwaitAdvance();
            }
            phaser.arriveAndDeregister();
        }
    }
}
