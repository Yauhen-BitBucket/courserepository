/*
 * StateVerifier.java
 * A task used to verify a state of a matrix.
 */
package by.training.task03.task;


import by.training.task03.exception.ApplicationException;
import by.training.task03.exception.DAOException;

import by.training.task03.matrix.Matrix;
import by.training.task03.matrix.State;

import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;


/**
 * A task used to verify a state of a matrix.
 * @author Yauhen Sazonau
 * @version 1.0, 05/31/19
 * @since 1.0
 */
public class StateVerifier implements Runnable {
    /**
     * An interval which defines how often to query a matrix state. The
     * identifier holds the value of "1".
     */
    private static final int TIMEOUT = 1;
    /**
     * A logger for this application. <code>null</code> value is prohibited.
     */
    private static final Logger LOGGER
            = LogManager.getLogger(StateVerifier.class);
    /**
     * Verifies a state of a matrix.
     */
    public void run() {
        boolean condition = true;
        Matrix matrix = null;
        try {
            matrix = Matrix.getInstance();
        } catch (DAOException e) {
            LOGGER.error("A matrix is incorrectly initialized.");
            condition = false;
        }
        if (condition) {
            while (!matrix.isAllDiagonalElementsSet()) {
                LOGGER.info("Matrix state is '"
                            + retrieveStateAsString(matrix.getState()) + "'.");
                try {
                    TimeUnit.MILLISECONDS.sleep(TIMEOUT);
                } catch (InterruptedException e) {
                    LOGGER.error("The process of verifying a matrix state was "
                                 + "interrupted.");
                    throw new ApplicationException(e);
                }
            }
            LOGGER.info("Matrix state is '"
                        + retrieveStateAsString(matrix.getState()) + "'.");
        }
    }
    /**
     * Retrieves a string representation of a state.
     * @param state the <code>State</code> value
     * @return a string representation of a state
     */
    private String retrieveStateAsString(final State state) {
        Class<?> stateClass = state.getClass();
        return stateClass.getSimpleName();
    }
}
