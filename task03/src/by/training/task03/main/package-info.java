/**
 * Contains a class which is an entry point of this application.
 * @author Yauhen Sazonau
 * @since 1.0
 */
package by.training.task03.main;
