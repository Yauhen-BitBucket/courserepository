/*
 * Runner.java
 * An entry point of this application.
 */
package by.training.task03.main;


import by.training.task03.controller.ApplicationController;
import by.training.task03.controller.ExitStateKeeper;

import by.training.task03.controller.command.CommandName;

import by.training.task03.exception.ApplicationException;

import java.util.Scanner;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;


/**
 * An entry point of this application.
 * @author Yauhen Sazonau
 * @version 1.0, 05/31/19
 * @since 1.0
 */
public final class Runner {
    /**
     * A choice value. The identifier holds the value of "1".
     */
    private static final int CHOICE_VALUE_1 = 1;
    /**
     * A choice value. The identifier holds the value of "2".
     */
    private static final int CHOICE_VALUE_2 = 2;
    /**
     * A choice value. The identifier holds the value of "1".
     */
    private static final int CHOICE_VALUE_3 = 3;
    /**
     * A logger for reporting errors. <code>null</code> value is prohibited.
     */
    private static final Logger ERROR_LOGGER
            = LogManager.getLogger("by.training.task03");
    /**
     * A printed message. The identifier holds the value of
     * "Please, enter a desired action:".
     */
    private static final String PRINTED_MESSAGE_1
            = "Please, enter a desired action:";
    /**
     * A printed message. The identifier holds the value of
     * "1 - set diagonal elements;".
     */
    private static final String PRINTED_MESSAGE_2
            = "1 - set diagonal elements;";
    /**
     * A printed message. The identifier holds the value of
     * "2 - print a report;".
     */
    private static final String PRINTED_MESSAGE_3 = "2 - print a report;";
    /**
     * A printed message. The identifier holds the value of
     * "3 - reset diagonal elements;".
     */
    private static final String PRINTED_MESSAGE_4
            = "3 - reset diagonal elements;";
    /**
     * A printed message. The identifier holds the value of
     * "4 - exit the application.".
     */
    private static final String PRINTED_MESSAGE_5
            = "4 - exit the application.";
    /**
     * Constructs an instance of this class.
     */
    private Runner() {
        /* The default initialization is sufficient. */
    }
    /**
     * Starts execution of a controller of this application.
     * @param args an array of input values
     */
    public static void main(final String[] args) {
        ApplicationController controller = new ApplicationController();
        CommandName name;
        ExitStateKeeper exitStateKeeper = new ExitStateKeeper();
        int choice;
        Scanner scanner = new Scanner(System.in);
        String commandName;
        String userInput;
        /* The main part of the method. */
        while (!exitStateKeeper.getExitState()) {
            System.out.println(PRINTED_MESSAGE_1);
            System.out.println(PRINTED_MESSAGE_2);
            System.out.println(PRINTED_MESSAGE_3);
            System.out.println(PRINTED_MESSAGE_4);
            System.out.println(PRINTED_MESSAGE_5);
            userInput = scanner.nextLine();
            try {
                choice = Byte.parseByte(userInput);
                if (choice == CHOICE_VALUE_1) {
                    name = CommandName.ELEMENT_SETTING_COMMAND;
                    commandName = name.name();
                } else if (choice == CHOICE_VALUE_2) {
                    name = CommandName.REPORT_PRINTING_COMMAND;
                    commandName = name.name();
                } else if (choice == CHOICE_VALUE_3) {
                    name = CommandName.ELEMENT_RESETTING_COMMAND;
                    commandName = name.name();
                } else {
                    name = CommandName.EXIT_COMMAND;
                    commandName = name.name();
                }
            } catch (NumberFormatException e) {
                name = CommandName.EXIT_COMMAND;
                commandName = name.name();
            }
            try {
                controller.executeTask(commandName, exitStateKeeper);
            } catch (ApplicationException e) {
                ERROR_LOGGER.error("Problems occurred during performing a "
                        + "user specified action.", e);
            }
        }
        scanner.close();
    }
}
