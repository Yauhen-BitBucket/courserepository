/**
 * Contains a reader used to read the whole text stored in a text file.
 * @author Yauhen Sazonau
 * @since 1.0
 */
package by.training.task03.reader;
