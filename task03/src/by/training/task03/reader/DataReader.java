/*
 * DataReader.java
 * A reader. It's used to read the whole text stored in a text file.
 */
package by.training.task03.reader;


import by.training.task03.exception.ApplicationException;

import java.nio.charset.StandardCharsets;

import java.nio.file.Files;
import java.nio.file.Paths;

import java.util.List;

import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * A reader. It's used to read the whole text stored in a text file.
 * @author Yauhen Sazonau
 * @version 1.0, 05/29/19
 * @since 1.0
 */
public class DataReader {
    /**
     * Constructs an instance of this class.
     */
    public DataReader() {
        /* The default initialization is sufficient. */
    }
    /**
     * Reads the whole text stored in a text file.
     * @param filePath the <code>String</code> value which is a relative path
     *                 to the file which a text will be read from
     * @return the whole text stored in a text file as a set of lines
     * @throws ApplicationException if there are problems with reading
     *                              a text or a target file is empty
     */
    public List<String> readData(final String filePath) {
        List<String> result;
        Stream<String> stream = null;
        /* The main part of the method. */
        try {
            stream = Files.lines(Paths.get(filePath), StandardCharsets.UTF_8);
            result = stream.collect(Collectors.toList());
        } catch (Exception e) {
            throw new ApplicationException("There are problems with reading a "
                    + "text.", e);
        } finally {
            if (stream != null) {
                stream.close();
            }
        }
        if (result.isEmpty()) {
            throw new ApplicationException("There is no data to read.");
        }
        return result;
    }
}
