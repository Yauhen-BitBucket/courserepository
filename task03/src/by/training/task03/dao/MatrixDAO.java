/* MatrixDAO.java
 * A contract which matrix DAO implementations should comply with.
 */
package by.training.task03.dao;


import by.training.task03.exception.DAOException;

import java.util.List;


/**
 * A contract which matrix DAO implementations should comply with.
 * @version 1.0, 06/04/19
 * @since 1.0
 */
public interface MatrixDAO {
    /**
     * Retrieves matrix data from a data source.
     * @return matrix data from a data source
     * @throws DAOException if there are problems with retrieving matrix data
     */
    List<String> retrieveMatrixData() throws DAOException;
}
