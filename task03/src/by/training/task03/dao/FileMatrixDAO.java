/* FileMatrixDAO.java
 * A matrix DAO implementation which is based on a file data source.
 */
package by.training.task03.dao;


import by.training.task03.exception.ApplicationException;
import by.training.task03.exception.DAOException;

import by.training.task03.reader.DataReader;

import java.util.List;


/**
 * A matrix DAO implementation which is based on a file data source.
 * @version 1.0, 06/04/19
 * @since 1.0
 */
public class FileMatrixDAO implements MatrixDAO {
    /**
     * Specifies a relative path to the file which information will be read
     * from. The identifier holds the value of "data/input.txt".
     */
    public static final String FILE_PATH = "data/input.txt";
    /**
     * Represents the maximum value a matrix size can have. The identifier
     * holds the value of "12".
     */
    private static final int MATRIX_SIZE_MAX_VALUE = 12;
    /**
     * Represents the minimum value a matrix size can have. The identifier
     * holds the value of "8".
     */
    private static final int MATRIX_SIZE_MIN_VALUE = 8;
    /**
     * Represents the maximum value a thread count can have. The identifier
     * holds the value of "6".
     */
    private static final int THREAD_COUNT_MAX_VALUE = 6;
    /**
     * Represents the minimum value a thread count can have. The identifier
     * holds the value of "4".
     */
    private static final int THREAD_COUNT_MIN_VALUE = 4;
    /**
     * Retrieves matrix data from a file data source.
     * @return matrix data from a file data source
     * @throws DAOException if there are problems with retrieving matrix data
     */
    @Override
    public List<String> retrieveMatrixData() throws DAOException {
        List<String> result;
        /* The main part of the method. */
        try {
            result = new DataReader().readData(FILE_PATH);
            if (!verifyData(result)) {
                throw new DAOException("Matrix data is incorrect.");
            }
        } catch (ApplicationException e) {
            throw new DAOException("Can't retrieve matrix data.", e);
        }
        return result;
    }
    /**
     * Verifies that the supplied matrix and thread data is correct. The size
     * of a matrix shouldn't be less than the minimum size and greater than the
     * maximum size. A thread count shouldn't be less than the minimum thread
     * count and greater than the maximum thread count. The matrix should be
     * initialized by integer values and diagonal elements should be
     * initialized by zeros. Thread values are unique positive integers.
     * @param data the <code>List</code> value which is data to verify
     * @return <code>true</code> if the supplied matrix and thread data is
     *         correct, <code>false</code> otherwise
     */
    private boolean verifyData(final List<String> data) {
        String row;
        String[] rowValues;
        int length = data.size();
        int size;
        int threadCount;
        /* The main part of the method. */
        if ((length < MATRIX_SIZE_MIN_VALUE + 1 + THREAD_COUNT_MIN_VALUE)
                || (length > MATRIX_SIZE_MAX_VALUE + 1
                             + THREAD_COUNT_MAX_VALUE)) {
            return false;
        }
        try {
            size = Integer.parseInt(data.get(0));
            if ((size < MATRIX_SIZE_MIN_VALUE)
                    || (size > MATRIX_SIZE_MAX_VALUE)) {
                return false;
            }
            for (int i = 1; i <= size; i++) {
                row = data.get(i);
                rowValues = row.split(" ");
                if (rowValues.length != size) {
                    return false;
                }
                for (int j = 0; j < size; j++) {
                    Integer.parseInt(rowValues[j]);
                }
            }
            threadCount = length - 1 - size;
            if ((threadCount < THREAD_COUNT_MIN_VALUE)
                    || (threadCount > THREAD_COUNT_MAX_VALUE)) {
                return false;
            }
            for (int i = size + 1; i < length; i++) {
                Integer.parseInt(data.get(i));
            }
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
}
