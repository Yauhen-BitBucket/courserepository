/* FileDAOFactory.java
 * A factory used to create various DAOs which are based on file data sources.
 */
package by.training.task03.dao;


/**
 * A factory used to create various DAOs which are based on file data sources.
 * @version 1.0, 06/04/19
 * @since 1.0
 */
public final class FileDAOFactory {
    /**
     * Represents a single instance of this class. <code>null</code> value is
     * prohibited.
     */
    private static FileDAOFactory instance = new FileDAOFactory();
    /**
     * An instance of the <code>FileMatrixDAO</code> class. <code>null</code>
     * value is prohibited.
     */
    private final MatrixDAO fileMatrixDAO = new FileMatrixDAO();
    /**
     * Creates an instance of this class. You have no ability to create
     * an instance of this class using this constructor. You should use the
     * <code>getInstance</code> method.
     */
    private FileDAOFactory() {
        /* The default initialization is sufficient. */
    }
    /**
     * Retrieves an instance of the <code>FileMatrixDAO</code> class.
     * @return an instance of the <code>FileMatrixDAO</code> class
     */
    public MatrixDAO retrieveMatrixDAO() {
        return fileMatrixDAO;
    }
    /**
     * Retrieves an instance if this class.
     * @return an instance if this class
     */
    public static FileDAOFactory getInstance() {
        return instance;
    }
}
