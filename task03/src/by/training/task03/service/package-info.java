/**
 * Represents a service layer of this application.
 * @author Yauhen Sazonau
 * @since 1.0
 */
package by.training.task03.service;
