/* ElementSettingServiceImpl.java
 * An implementation of element setting service.
 */
package by.training.task03.service;


import by.training.task03.dao.FileDAOFactory;
import by.training.task03.dao.MatrixDAO;

import by.training.task03.exception.DAOException;
import by.training.task03.exception.ServiceException;

import by.training.task03.matrix.Matrix;

import by.training.task03.task.DiagonalElementSetter;
import by.training.task03.task.StateVerifier;

import java.util.List;

import java.util.concurrent.Phaser;


/**
 * An implementation of element setting service.
 * @version 1.0, 06/04/19
 * @since 1.0
 */
public class ElementSettingServiceImpl implements ElementSettingService {
    /**
     * Sets all diagonal elements of the <code>Matrix</code> instance to a non
     * zero value.
     * @throws ServiceException if there are problems with working with the
     *                          <code>Matrix</code> instance
     * @see Matrix
     */
    @Override
    public void setDiagonalElements() throws ServiceException {
        FileDAOFactory factory = FileDAOFactory.getInstance();
        List<String> data;
        Matrix matrix;
        MatrixDAO matrixDAO;
        byte lineCount;
        byte matrixSize;
        Phaser phaser;
        /* The main part of the method. */
        try {
            matrix = Matrix.getInstance();
            matrixDAO = factory.retrieveMatrixDAO();
            data = matrixDAO.retrieveMatrixData();
        } catch (DAOException e) {
            throw new ServiceException("Can't set diagonal elements.", e);
        }
        lineCount = (byte) data.size();
        matrixSize = Byte.parseByte(data.get(0));
        phaser = new Phaser(lineCount - matrixSize);
        for (int i = matrixSize + 1; i < lineCount; i++) {
            Thread thread = new Thread(new DiagonalElementSetter(phaser,
                    Integer.parseInt(data.get(i))));
            thread.setName("Thread-" + (i - matrixSize));
            thread.start();
        }
        new Thread(new StateVerifier()).start();
        while (!matrix.isAllDiagonalElementsSet()) {
            phaser.arriveAndAwaitAdvance();
        }
        phaser.arriveAndDeregister();
    }
    /**
     * Sets all diagonal elements of the <code>Matrix</code> instance to zero.
     * @throws ServiceException if there are problems with working with the
     *                          <code>Matrix</code> instance
     * @see Matrix
     */
    @Override
    public void resetDiagonalElements() throws ServiceException {
        Matrix matrix;
        /* The main part of the method. */
        try {
            matrix = Matrix.getInstance();
        } catch (DAOException e) {
            throw new ServiceException("Can't reset diagonal elements.", e);
        }
        matrix.resetDiagonalElements();
    }
}
