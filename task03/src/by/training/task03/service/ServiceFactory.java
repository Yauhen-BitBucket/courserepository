/* ServiceFactory.java
 * A factory used to create various services.
 */
package by.training.task03.service;


/**
 * A factory used to create various services.
 * @version 1.0, 06/04/19
 * @since 1.0
 */
public final class ServiceFactory {
    /**
     * Represents a single instance of this class. <code>null</code> value is
     * prohibited.
     */
    private static ServiceFactory instance = new ServiceFactory();
    /**
     * An instance of the <code>ElementSettingServiceImpl</code> class.
     * <code>null</code> value is prohibited.
     */
    private final ElementSettingService elementSettingService
            = new ElementSettingServiceImpl();
    /**
     * An instance of the <code>ReportPrintingServiceImpl</code> class.
     * <code>null</code> value is prohibited.
     */
    private final ReportPrintingService reportPrintingService
            = new ReportPrintingServiceImpl();
    /**
     * Creates an instance of this class. You have no ability to create
     * an instance of this class using this constructor. You should use the
     * <code>getInstance</code> method.
     */
    private ServiceFactory() {
        /* The default initialization is sufficient. */
    }
    /**
     * Retrieves an instance of the <code>ElementSettingService</code> class.
     * @return an instance of the <code>ElementSettingService</code> class
     */
    public ElementSettingService retrieveElementSettingService() {
        return elementSettingService;
    }
    /**
     * Retrieves an instance of the <code>ReportPrintingService</code> class.
     * @return an instance of the <code>ReportPrintingService</code> class
     */
    public ReportPrintingService retrieveReportPrintingService() {
        return reportPrintingService;
    }
    /**
     * Retrieves an instance if this class.
     * @return an instance if this class
     */
    public static ServiceFactory getInstance() {
        return instance;
    }
}
