/* ReportPrintingServiceImpl.java
 * An implementation of report printing service.
 */
package by.training.task03.service;


import by.training.task03.exception.DAOException;
import by.training.task03.exception.ServiceException;

import by.training.task03.matrix.Matrix;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * An implementation of report printing service.
 * @version 1.0, 06/04/19
 * @since 1.0
 */
public class ReportPrintingServiceImpl implements ReportPrintingService {
    /**
     * A logger for this application. <code>null</code> value is prohibited.
     */
    private static final Logger LOGGER
            = LogManager.getLogger(ReportPrintingServiceImpl.class);
    /**
     * Prints a matrix representation.
     * @throws ServiceException if there are problems with printing a report
     * @see Matrix
     */
    @Override
    public void printReport() throws ServiceException {
        Matrix matrix;
        try {
            matrix = Matrix.getInstance();
        } catch (DAOException e) {
            throw new ServiceException("Can't print a report.", e);
        }
        LOGGER.info(matrix.toString());
    }
}
