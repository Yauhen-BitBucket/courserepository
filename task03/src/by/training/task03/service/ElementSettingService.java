/* ElementSettingService.java
 * A contract which element setting service should comply with.
 */
package by.training.task03.service;


import by.training.task03.exception.ServiceException;


/**
 * A contract which element setting service should comply with.
 * @version 1.0, 06/04/19
 * @since 1.0
 */
public interface ElementSettingService {
    /**
     * Sets all diagonal elements of the <code>Matrix</code> instance to a non
     * zero value.
     * @throws ServiceException if there are problems with working with the
     *                          <code>Matrix</code> instance
     * @see Matrix
     */
    void setDiagonalElements() throws ServiceException;
    /**
     * Sets all diagonal elements of the <code>Matrix</code> instance to zero.
     * @throws ServiceException if there are problems with working with the
     *                          <code>Matrix</code> instance
     * @see Matrix
     */
    void resetDiagonalElements() throws ServiceException;
}
