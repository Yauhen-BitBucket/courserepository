/* ReportPrintingService.java
 * A contract which report printing service should comply with.
 */
package by.training.task03.service;


import by.training.task03.exception.ServiceException;


/**
 * A contract which report printing service should comply with.
 * @version 1.0, 06/04/19
 * @since 1.0
 */
public interface ReportPrintingService {
    /**
     * Prints a matrix representation.
     * @throws ServiceException if there are problems with printing a report
     * @see Matrix
     */
    void printReport() throws ServiceException;
}
