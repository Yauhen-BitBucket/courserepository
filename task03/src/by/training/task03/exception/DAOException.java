/*
 * DAOException.java
 * The exception that provides the information of an interaction error with
 * a data source. It doesn't add any additional information to the basic
 * information that you can derive from a typical exception.
 */
package by.training.task03.exception;


/**
 * The exception that provides the information of an interaction error with
 * a data source. It doesn't add any additional information to the basic
 * information that you can derive from a typical exception.
 * @author Yauhen Sazonau
 * @version 1.0, 06/04/19
 * @since 1.0
 */
@SuppressWarnings("serial")
public class DAOException extends Exception {
    /**
     * Constructs a new <code>DAOException</code> exception with
     * <code>null</code> as its detail message. The cause is not initialized.
     */
    public DAOException() {
        /* The default initialization is sufficient. */
    }
    /**
     * Constructs a new <code>DAOException</code> exception
     * with the specified detail message. The cause is not initialized.
     * @param message the <code>String</code> value which is the information
     *                about an exception. <code>null</code> value is allowed
     *                and simply means that the information about the
     *                exception is absent.
     */
    public DAOException(final String message) {
        super(message);
    }
    /**
     * Constructs a new <code>DAOException</code> exception with the specified
     * cause.
     * @param exception the <code>Exception</code> value which is the cause of
     *                  an exception. <code>null</code> value is allowed and
     *                  simply means that the cause is nonexistent or unknown.
     */
    public DAOException(final Exception exception) {
        super(exception);
    }
    /**
     * Constructs a new <code>DAOException</code> exception
     * with the specified detail message and cause. Note that the detail
     * message associated with the cause is not automatically incorporated in
     * this exception's detail message.
     * @param message the <code>String</code> value which is the information
     *                about an exception. <code>null</code> value is allowed
     *                and simply means that the information about the
     *                exception is absent.
     * @param exception the <code>Exception</code> value which is the cause of
     *                  an exception. <code>null</code> value is allowed and
     *                  simply means that the cause is nonexistent or unknown.
     */
    public DAOException(final String message,
            final Exception exception) {
        super(message, exception);
    }
}
