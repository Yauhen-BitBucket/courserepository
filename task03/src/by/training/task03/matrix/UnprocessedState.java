/*
 * UnprocessedState.java
 * The unprocessed state.
 */
package by.training.task03.matrix;


/**
 * The unprocessed state.
 * @author Yauhen Sazonau
 * @version 1.0, 05/31/19
 * @since 1.0
 */
public class UnprocessedState implements State {

}
