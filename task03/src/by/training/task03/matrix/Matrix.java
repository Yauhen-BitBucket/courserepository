/*
 * Matrix.java
 * A matrix.
 */
package by.training.task03.matrix;


import by.training.task03.dao.FileDAOFactory;
import by.training.task03.dao.MatrixDAO;

import by.training.task03.exception.ApplicationException;
import by.training.task03.exception.DAOException;

import java.util.List;

import java.util.concurrent.locks.ReentrantLock;


/**
 * A matrix.
 * @author Yauhen Sazonau
 * @version 1.0, 05/29/19
 * @since 1.0
 */
public final class Matrix {
    /* Implements the Singleton pattern. */
    /**
     * Represents the maximum value the matrix size can have. The identifier
     * holds the value of "12".
     */
    public static final byte MAX_MATRIX_SIZE = 12;
    /**
     * Represents the minimum value the matrix size can have. The identifier
     * holds the value of "8".
     */
    public static final byte MIN_MATRIX_SIZE = 8;
    /**
     * Represents a single instance of this class. <code>null</code> value is
     * prohibited.
     */
    private static Matrix instance;
    /**
     * A lock which threads are synchronized on. <code>null</code> value is
     * prohibited.
     */
    private static final ReentrantLock LOCK = new ReentrantLock();
    /**
     * An internal representation of a matrix. <code>null</code> value is
     * prohibited.
     */
    private int[][] array;
    /**
     * Describes a state which a matrix can be in. <code>null</code> value is
     * prohibited.
     */
    private State state;
    /**
     * Creates a matrix using the specified size. You have no ability to create
     * an instance of this class using this constructor. You should use the
     * <code>getInstance</code> method.
     * @param size the <code>byte</code> value which is the size to be set. It
     *             shouldn't be less than the minimum value and greater than
     *             the maximum value.
     * @throws ApplicationException if the requested size isn't appropriate
     * @see Matrix#MAX_MATRIX_SIZE
     * @see Matrix#MIN_MATRIX_SIZE
     * @see #getInstance()
     */
    private Matrix(final byte size) {
        if ((size < MIN_MATRIX_SIZE) || (size > MAX_MATRIX_SIZE)) {
            throw new ApplicationException("The requested matrix size isn't "
                    + "allowed. The size shouldn't be less than "
                    + MIN_MATRIX_SIZE + " and greater than " + MAX_MATRIX_SIZE
                    + ". The supplied values is " + size + ".");
        }
        array = new int[size][size];
    }
    /**
     * Retrieves the vertical size of this matrix.
     * @return the vertical size of this matrix
     */
    public byte getRowCount() {
        return ((byte) array.length);
    }
    /**
     * Retrieves the horizontal size of this matrix.
     * @return the horizontal size of this matrix
     */
    public byte getColumnCount() {
        return ((byte) array[0].length);
    }
    /**
     * Retrieves a state of this matrix.
     * @return a state of this matrix. <code>null</code> value is prohibited.
     */
    public State getState() {
        return state;
    }
    /**
     * Verifies whether the specified cell is available for retrieving or
     * setting.
     * @param rowNumber the <code>int</code> value which is the row number
     * @param columnNumber the <code>int</code> value which is the column
     *                     number
     * @return <code>true</code> if the the specified cell is available for
     *         retrieving or setting; <code>false</code> otherwise
     */
    private boolean checkRange(final int rowNumber, final int columnNumber) {
        return ((rowNumber >= 0) && (rowNumber < array.length)
                && (columnNumber >= 0) && (columnNumber < array[0].length));
    }
    /**
     * Sets the specified value into the specified position.
     * @param rowNumber the <code>int</code> value which is the row number
     * @param columnNumber the <code>int</code> value which is the column
     *                     number
     * @param value the <code>int</code> value which is a value to be set
     * @throws ApplicationException if the row and column numbers are less
     *                              than zero or greater than the matrix size
     *                              minus one
     */
    public void setElement(final int rowNumber, final int columnNumber,
            final int value) {
        if (checkRange(rowNumber, columnNumber)) {
            array[rowNumber][columnNumber] = value;
            return;
        }
        throw new ApplicationException("The requested cell isn't available. "
                + "The row and column numbers shouldn't be less than zero and "
                + "greater than the matrix size minus one. The supplied row "
                + "number is " + rowNumber + ", the supplied column number is "
                + columnNumber
                + ".");
    }
    /**
     * Retrieves the string representation of this matrix.
     * @return the string representation of this matrix
     */
    @Override
    public String toString() {
        String newLine = String.format("%n");
        StringBuilder s = new StringBuilder("Matrix: " + array.length + "x"
                                            + array[0].length + newLine);
        for (int[] row : array) {
            for (int value : row) {
                s.append(String.format("%4d ", value));
            }
            s.append(newLine + newLine);
        }
        return s.toString();
    }
    /**
     * Retrieves a diagonal element of this matrix using the specified number.
     * @param number the <code>byte</code> value which is the number of a
     *               diagonal element that is to be retrieved
     * @return a diagonal element of this matrix
     * @throws ApplicationException if the 'number' argument is less than zero
     *                              or greater than the size of this matrix
     *                              minus one
     */
    public int retrieveDiagonalElement(final byte number) {
        int result;
        if ((number < 0) || (number > array.length - 1)) {
            throw new ApplicationException("The 'number' argument shouldn't be"
                    + " less than zero or greater than the size of this "
                    + "matrix minus one. The supplied value is " + number
                    + ".");
        }
        /* The main part of the method. */
        LOCK.lock();
        result = array[number][number];
        LOCK.unlock();
        return result;
    }
    /**
     * Sets the specified value into the diagonal of this matrix using the
     * specified position.
     * @param number the <code>byte</code> value which is the number of a
     *               diagonal element that is to be set
     * @param value the <code>int</code> value which is a value to be set
     * @return <code>true</code> if the specified value was set into the
     *         diagonal of this matrix using the specified position
     */
    public boolean setDiagonalElement(final byte number, final int value) {
        boolean result = false;
        if ((number < 0) || (number > array.length - 1)) {
            throw new ApplicationException("The 'number' argument shouldn't be"
                    + " less than zero or greater than the size of this "
                    + "matrix minus one. The supplied value is " + number
                    + ".");
        }
        LOCK.lock();
        if (retrieveDiagonalElement(number) == 0) {
            state = new ModifiedState();
            array[number][number] = value;
            result = true;
            if (isAllDiagonalElementsSet()) {
                state = new ProcessedState();
            }
        }
        LOCK.unlock();
        return result;
    }
    /**
     * Retrieves an instance if this class.
     * @return a newly created instance of this class or an existing if it has
     *         been created before
     * @throws DAOException if there are problems with retrieving an instance
     *                      if this class
     */
    public static Matrix getInstance() throws DAOException {
        byte matrixSize;
        FileDAOFactory factory = FileDAOFactory.getInstance();
        List<String> data;
        Matrix matrix;
        MatrixDAO matrixDAO;
        String row;
        String[] rowValues;
        /* The main part of the method. */
        LOCK.lock();
        if (instance == null) {
            matrixDAO = factory.retrieveMatrixDAO();
            data = matrixDAO.retrieveMatrixData();
            matrixSize = Byte.parseByte(data.get(0));
            matrix = new Matrix(matrixSize);
            for (int i = 1; i <= matrixSize; i++) {
                row = data.get(i);
                rowValues = row.split(" ");
                for (int j = 0; j < matrixSize; j++) {
                    matrix.setElement(i - 1, j,
                                      Integer.parseInt(rowValues[j]));
                }
            }
            matrix.state = new UnprocessedState();
            instance = matrix;
        }
        LOCK.unlock();
        return instance;
    }
    /**
     * Verifies whether all diagonal elements were set to a non zero value.
     * @return <code>true</code> if all diagonal elements were set to a non
     *         zero value, <code>false</code> otherwise
     */
    public boolean isAllDiagonalElementsSet() {
        boolean result = true;
        int size = array.length;
        /* The main part of the method. */
        LOCK.lock();
        for (int i = 0; i < size; i++) {
            if (array[i][i] == 0) {
                result = false;
            }
        }
        LOCK.unlock();
        return result;
    }
    /**
     * Sets all diagonal elements to zero.
     */
    public void resetDiagonalElements() {
        int length = array.length;
        for (int i = 0; i < length; i++) {
            array[i][i] = 0;
        }
    }
}
