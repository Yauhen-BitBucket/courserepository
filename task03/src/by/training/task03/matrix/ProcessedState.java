/*
 * ProcessedState.java
 * The processed state.
 */
package by.training.task03.matrix;


/**
 * The processed state.
 * @author Yauhen Sazonau
 * @version 1.0, 05/31/19
 * @since 1.0
 */
public class ProcessedState implements State {

}
