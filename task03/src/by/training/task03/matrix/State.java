/*
 * State.java
 * A state. Describes a state which a matrix can be in.
 */
package by.training.task03.matrix;


/**
 * A state. Describes a state which a matrix can be in.
 * @author Yauhen Sazonau
 * @version 1.0, 05/31/19
 * @since 1.0
 */
public interface State {

}
