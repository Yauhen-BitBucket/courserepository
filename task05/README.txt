Ticket assigning in a polyclinic

A user logs in the system and directed to the main page. Depending on a role
a particular interface is presented to him (there are three roles: a patient,
a physician, a receptionist).

A patient can view a list of free tickets to a particular specialist on a
particular date; a patient can cancel a ticket; a patient can view a list of
his own assigned tickets; a patient can view surgery hours of physicians of a
particular speciality.

A physician adds his new tickets in the system; a physician can view a list of
registered patients on a particular date; a physician commits a visit by fixing
diagnosis and treatment.

A receptionist can view a list of patients which were serviced during a
specified period; a receptionist can add a patient account to the system; a
receptionist can view statistics of patient service per a particular year and
month; a receptionist can block users.

An application has the following structure (packages):

 - by.training.task05.bundle                application settings and interface
                                            labels
 - by.training.task05.dao                   general DAO interfaces
 - by.training.task05.dao.implementation    implementations of DAO interfaces
 - by.training.task05.entity                application entities
 - by.training.task05.exception             a general purpose exception
 - by.training.task05.pool                  pool implementation
 - by.training.task05.reader                property readers
 - by.training.task05.web.command           implementations of commands used by
                                            a controller
 - by.training.task05.web.controller        a controller of the application
 - by.training.task05.web.filter            filters used by the application
 - by.training.task05.web.listener          a listener for application lifecycle
 - test.task05.dao.implementation           tests for DAO implementations
 
 Used technologies: Log4j2, TestNG, Servlet/JSP, pure JavaScript, jQuery, CSS.
 
 Needed libraries: jcommander-1.72.jar, jstl.jar, log4j-api-2.11.2.jar,
 log4j-core-2.11.2.jar, log4j-web-2.11.2.jar,
 mysql-connector-java-5.1.12-bin.jar, standard.jar, testng-6.9.9.jar.