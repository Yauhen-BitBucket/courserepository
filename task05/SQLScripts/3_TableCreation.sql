USE `tickets`;

DROP TABLE IF EXISTS `personaldata`;

CREATE TABLE IF NOT EXISTS `personaldata` (
  `PassportNumber` char(9) NOT NULL,
  `FirstName` varchar(15) NOT NULL,
  `LastName` varchar(15) NOT NULL,
  CONSTRAINT `PK_personaldata` PRIMARY KEY (`PassportNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='A table for storing personal data of application users.';

DROP TABLE IF EXISTS `user`;

CREATE TABLE IF NOT EXISTS `user` (
  `UserID` int(10) NOT NULL AUTO_INCREMENT,
  `Login` char(32) NOT NULL,
  `Password` char(32) NOT NULL,
  `Role` enum('PATIENT','PHYSICIAN','HEAD_PHYSICIAN') NOT NULL,
  `PassportNumber` char(9) NOT NULL,
  `Image` LONGBLOB NULL,
  `BlockedState` TINYINT(1) NOT NULL DEFAULT '0',
  CONSTRAINT `PK_user` PRIMARY KEY (`UserID`),
  CONSTRAINT `CU_login` UNIQUE KEY (`Login`),
  KEY `FK_user_personaldata` (`PassportNumber`),
  CONSTRAINT `FK_user_personaldata` FOREIGN KEY (`PassportNumber`) REFERENCES `personaldata` (`PassportNumber`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='A table for storing user data.';

DROP TABLE IF EXISTS `speciality`;

CREATE TABLE IF NOT EXISTS `speciality` (
  `SpecialityID` int(10) NOT NULL AUTO_INCREMENT,
  `Name` enum('THERAPEUTIST','SURGEON','OPHTHALMOLOGIST','UROLOGIST','OTOLARYNGOLOGIST') NOT NULL,
  CONSTRAINT `PK_speciality` PRIMARY KEY (`SpecialityID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='A table for a doctor speciality.';

DROP TABLE IF EXISTS `physician`;

CREATE TABLE IF NOT EXISTS `physician` (
  `PhysicianID` int(10) NOT NULL,
  `Speciality` int(10) NOT NULL,
  `RoomNumber` int(10) NOT NULL,
  `OddStartingTime` time NOT NULL,
  `OddEndingTime` time NOT NULL,
  `EvenStartingTime` time NOT NULL,
  `EvenEndingTime` time NOT NULL,
  KEY `FK_physician_user` (`PhysicianID`),
  KEY `FK_physician_speciality` (`Speciality`),
  CONSTRAINT `Odd_time_precedence` CHECK(OddStartingTime < OddEndingTime),
  CONSTRAINT `Even_time_precedence` CHECK(EvenStartingTime < EvenEndingTime),
  CONSTRAINT `FK_physician_speciality` FOREIGN KEY (`Speciality`) REFERENCES `speciality` (`SpecialityID`) ON UPDATE CASCADE,
  CONSTRAINT `FK_physician_user` FOREIGN KEY (`PhysicianID`) REFERENCES `user` (`UserID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='A table for storing doctor data.';

DROP TABLE IF EXISTS `ticket`;

CREATE TABLE IF NOT EXISTS `ticket` (
  `TicketID` int(10) NOT NULL AUTO_INCREMENT,
  `Physician` int(10) NOT NULL,
  `Patient` int(10) DEFAULT NULL,
  `Date` date NOT NULL,
  `StartingTime` time NOT NULL,
  `EndingTime` time NOT NULL,
  `Diagnosis` varchar(300) DEFAULT NULL,
  `Treatment` varchar(300) DEFAULT NULL,
  CONSTRAINT `time_precedence` CHECK(StartingTime < EndingTime),
  CONSTRAINT `PK_ticket` PRIMARY KEY (`TicketID`),
  KEY `FK_user_physician` (`Physician`),
  KEY `FK_user_patient` (`Patient`),
  CONSTRAINT `FK_user_patient` FOREIGN KEY (`Patient`) REFERENCES `user` (`UserID`) ON UPDATE CASCADE,
  CONSTRAINT `FK_user_physician` FOREIGN KEY (`Physician`) REFERENCES `user` (`UserID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='A table for storing tickets.';
