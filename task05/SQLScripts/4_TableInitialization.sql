INSERT INTO `personaldata` (`PassportNumber`, `FirstName`, `LastName`)
	VALUES
	('AA8882347', 'Алина', 'Григоренко'),
	('EE1113377', 'Андрей', 'Соболев');

INSERT INTO `user` (`UserID`, `Login`, `Password`, `Role`, `PassportNumber`, `Image`)
	VALUES
	(1000, '079323A49C300DCD8DFFE1460DEDE02C', '7C6A180B36896A0A8C02787EEAFB0E4C', 'HEAD_PHYSICIAN', 'AA8882347', NULL),
	(1001, '3B38C223CD0767C5E6F40A7FB86159B4', '6CB75F652A9B52798EB6CF2201057C73', 'PHYSICIAN', 'EE1113377', NULL);

INSERT INTO `speciality` (`SpecialityID`, `Name`)
	VALUES
	(2000, 'THERAPEUTIST'),
	(2001, 'SURGEON'),
	(2002, 'OPHTHALMOLOGIST'),
	(2003, 'UROLOGIST'),
	(2004, 'OTOLARYNGOLOGIST');

INSERT INTO `physician` (`PhysicianID`, `Speciality`, `RoomNumber`, `OddStartingTime`, `OddEndingTime`, `EvenStartingTime`, `EvenEndingTime`)
	VALUES (1001, 2000, 100, '08:00:00', '14:00:00', '14:00:00', '20:00:00');
