<?xml version = "1.0" encoding = "UTF-8" ?>
<jsp:root xmlns:jsp = "http://java.sun.com/JSP/Page" version = "2.0"
          xmlns:c = "http://java.sun.com/jsp/jstl/core"
          xmlns:fmt = "http://java.sun.com/jsp/jstl/fmt">
    <jsp:directive.page contentType = "text/html; charset=UTF-8"
                        pageEncoding = "UTF-8"/>
    <jsp:output doctype-root-element = "html"
        doctype-public = "-//W3C//DTD XHTML 1.0 Transitional//EN"
        doctype-system = "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
        omit-xml-declaration = "true" />
    <fmt:setLocale value = "${(not empty locale) ? locale : defaultLocale}"/>
    <fmt:setBundle basename = "by.training.task05.bundle.PageLabels"/>
    <c:set var = "userExistenceCondition"
           value = "${empty authenticatedUser}"
           scope = "page" />
    <c:url var = "localeChangingURL" 
           value = "/controller">
        <c:param name = "command" 
                 value = "locale-changing-command" />
        <c:param name = "pathPropertyKey" 
                 value = "error.page.path" />
    </c:url>
    <c:url var = "logoutURL" 
           value = "/controller">
        <c:param name = "command" 
                 value = "logout-command" />
    </c:url>
    <c:url var = "styleURL" value = "${stylePath}"/>
    <c:set var = "returnURL">
           <c:choose>
               <c:when test = "${userExistenceCondition}">
                   <c:url value = "/welcome.jsp"/>
               </c:when>
               <c:otherwise>
                   <c:url value = "/main.jsp"/>
               </c:otherwise>
           </c:choose>
    </c:set>
    <c:set var = "returnReferenceLabel" scope = "page">
        <fmt:message>
            <c:choose>
                <c:when test = "${userExistenceCondition}">
                    welcome.page.reference.label
                </c:when>
                <c:otherwise>
                    main.page.reference.label
                </c:otherwise>
            </c:choose>
        </fmt:message>
    </c:set>
    <c:if test = "${pageContext.errorData.statusCode != 0}">
        <c:set var = "code" value = "${pageContext.errorData.statusCode}"
                scope = "session" />
    </c:if>
    <html xmlns = "http://www.w3.org/1999/xhtml">
        <head>
            <meta charset = "UTF-8"></meta>
            <title><fmt:message key = "error.page.title"/></title>
            <link href = "${styleURL}" rel = "stylesheet" type = "text/css"/>
        </head>
        <body>
            <c:if test = "${not userExistenceCondition}">
                <c:url var = "imageURL" 
			           value = "/controller">
			        <c:param name = "command" 
			                 value = "image-fetching-command"/>
			        <c:param name = "login" 
			                 value = "${authenticatedUser.login}"/>
			    </c:url>
                <div class = "user-info">
	                <img src = "${imageURL}"/>
	                <span><fmt:message key = "login.label"/></span>
	                <span><c:out value = "${authenticatedUser.login}" escapeXml = "true"/></span>
	                <span><fmt:message key = "role.label"/></span>
	                <span><fmt:message key = "${roleMap[authenticatedUser.role]}"/></span>
	                <span><fmt:message key = "dot.label"/></span>
	            </div>
            </c:if>
            <h1><fmt:message key = "error.page.header"/></h1>
            <div id = "error"
                    class = 'basic-navigation ${(not userExistenceCondition)
                                                ? "extended" : ""}'>
                <a href = "${localeChangingURL}">
                    <fmt:message key = "locale.changing.reference.label"/>
                </a>
                <a href = "${returnURL}">
                    ${returnReferenceLabel}
                </a>
                <c:if test = "${not userExistenceCondition}">
                    <a href = "${logoutURL}">
                        <fmt:message key = "logout.reference.label"/>
                    </a>
                </c:if>
            </div>
            <div id = "errorView">
                <p>
                    <fmt:message key = "error.code.label"/>
                    <span>
                        ${code}
                    </span>
                </p>
                <p>
                    <fmt:message key = "${errorInformationKey}"/>
                </p>
            </div>
        </body>
    </html>
</jsp:root>
