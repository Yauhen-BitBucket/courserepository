var validator = new DataValidator();

validator.YEAR_MIN_VALUE = 2000;

validator.TICKET_ID_MIN_VALUE = 1;

validator.TICKET_ID_MAX_VALUE = 100000000;

DataValidator.prototype.retrieveSessionID = function() {
	var localPath = window.location.pathname;
	var index = localPath.indexOf("jsessionid");
	var result = "";
	if (index !== -1) {
		result = localPath.substring(index + 11);
	}
	return result;
}

DataValidator.prototype.retrieveDate = function(inputSequence) {
	var year = parseInt(inputSequence.substring(0, 4), 10);
	var month = parseInt(inputSequence.substring(5, 7), 10);
	var day = parseInt(inputSequence.substring(8, 10), 10);
	return (new Date(year, month - 1, day));
}

DataValidator.prototype.checkPassportNumber = function(inputSequence) {
	var expression = /^[A-Z]{2}\d{7}$/;
	return (expression.test(inputSequence));
}

DataValidator.prototype.checkWord = function(inputSequence) {
	var expression = /^(?:[^!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~ \t\n\x0B\f\r]|'){1,}(?: (?:[^!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~ \t\n\x0B\f\r]|'){1,})*$/;
	return (expression.test(inputSequence));
}

DataValidator.prototype.checkYearCorrectness = function(inputSequence) {
	var expression = /^\d{4}$/;
	var result = expression.test(inputSequence);
	if ((result) && (parseInt(inputSequence) < validator.YEAR_MIN_VALUE)) {
		result = false;
	}
	return result;
}

DataValidator.prototype.checkMonthCorrectness = function(inputSequence) {
	var expression = /^\d{1,2}$/;
	var result = expression.test(inputSequence);
	var value = parseInt(inputSequence);
	if ((result) && ((value < 1) || (value > 12))) {
		result = false;
	}
	return result;
}

DataValidator.prototype.issueRequest = function(path, key) {
	$.get(path + ((this.retrieveSessionID() !== "")
				  ? ";jsessionid=" + this.retrieveSessionID() : ""),
		{command: 'message-fetching-command', messageKey: key},
		function(response) {
			var $errorList = $('#errorList');
			if ($errorList.text().indexOf(response) === -1) {
				$errorList.prepend('<li><p>' + response + '</p><li>');
			}
		}, 'text');
}

DataValidator.prototype.setPlaceholder = function(path, key, $element) {
	$.get(path + ((this.retrieveSessionID() !== "")
			  	  ? ";jsessionid=" + this.retrieveSessionID() : ""),
		{command: 'message-fetching-command', messageKey: key},
		function(response) {
			$element.val(response);
		}, 'text');
}

DataValidator.prototype.checkMinLength = function(inputSequence) {
	var expression = /^(?:.|\s){6,}/;
	return expression.test(inputSequence);
}

DataValidator.prototype.checkTimeCorrectness = function(inputSequence) {
	var expression = /^[012][123456789](?::[012345][0123456789]){2}$/;
	var result = expression.test(inputSequence);
	if (result) {
        hour = parseInt(inputSequence.substring(0, 2));
        if (hour > 23) {
            result = false;
        }
    } else {
        result = false;
    }
    return result;
}

DataValidator.prototype.checkTicketIDCorrectness = function(inputSequence) {
	var result = true;
	var value = parseInt(inputSequence);
	if (isNaN(value)) {
		result = false;
	} 
	if ((result)
			&& ((value < validator.TICKET_ID_MIN_VALUE)
				|| (value > validator.TICKET_ID_MAX_VALUE))) {
		result = false;
	}
	return result;
}

DataValidator.prototype.retrieveMonthDayCount = function(month, year) {
	var firstCase;
	var isALeapYear;
	var secondCase;
	var amount = 0;
    if ((month == 0) || (month == 2) || (month == 4)
            || (month == 6) || (month == 7) || (month == 9)
            || (month == 11)) {
        amount = 31;
    } else if ((month == 3) || (month == 5)
            || (month == 8) || (month == 10)) {
        amount = 30;
    } else if (month == 1) {
        firstCase = (year % 400) == 0;
        secondCase = ((year % 4) == 0)
                     && ((year % 100) != 0);
        isALeapYear =  firstCase || secondCase;
        if (isALeapYear) {
            amount = 29;
        } else {
            amount = 28;
        }
    }
    return amount;
}

DataValidator.prototype.checkDateCorrectness = function(inputSequence) {
	var expression = /^\d{4}[-]\d{2}[-]\d{2}$/;
	var result = expression.test(inputSequence);
	var year;
	var month;
	var day;
	if (result) {
		year = parseInt(inputSequence.substring(0, 4), 10);
		month = parseInt(inputSequence.substring(5, 7), 10);
		day = parseInt(inputSequence.substring(8, 10), 10);
		if (year >= validator.YEAR_MIN_VALUE) {
            if ((month < 1)
                    || (month > 12)
                    || (day < 1)
                    || (day > (validator
                    		   .retrieveMonthDayCount(month - 1, year)))) {
                result = false;
            }
        } else {
            result = false;
        }
	} else {
		result = false;
	}
	return result;
}

$('#authenticationForm button').click(function(event) {
	var correctnessCondition = true;
	var $login = $('#authenticationForm #login');
	var $password = $('#authenticationForm #password');
	var login = $login.hasClass('placeholder') ? "" : $login.val();
	var message;
	var password = $password.hasClass('placeholder') ? "" : $password.val();
	$('#errorList').html('');
	if (!validator.checkMinLength(login)) {
		$.get('controller' + ((validator.retrieveSessionID() !== "")
				  			  ? ";jsessionid=" + validator.retrieveSessionID()
				  			  : ""),
				{command: 'message-fetching-command',
				messageKey: 'login.input.error.message'},
				function(response) {
					var $errorList = $('#errorList');
					if ($errorList.text().indexOf(response) === -1) {
						$errorList.prepend('<li><p>' + response + '</p><li>');
					}
				}, 'text');
		correctnessCondition = false;
	}
	if (!validator.checkMinLength(password)) {
		$.get('controller' + ((validator.retrieveSessionID() !== "")
				  			  ? ";jsessionid=" + validator.retrieveSessionID()
				  			  : ""),
				{command: 'message-fetching-command',
				messageKey: 'password.input.error.message'},
				function(response) {
					var $errorList = $('#errorList');
					if ($errorList.text().indexOf(response) === -1) {
						$errorList.append('<li><p>' + response + '</p><li>');
					}
				}, 'text');
		correctnessCondition = false;
	}
	if (!correctnessCondition) {
		event.preventDefault();
	} else {
		$('#errorList').html('');
	}
});

$('#freeTicketFetchingForm button, #assignedTicketFetchingForm button')
.click(function(event) {
	var correctnessCondition = true;
	var curentDate;
	var $date = $('#date');
	var date = $date.hasClass('placeholder') ? "" : $date.val();
	if (!validator.checkDateCorrectness(date)) {
		$.get('../controller' + ((validator.retrieveSessionID() !== "")
				  				 ? ";jsessionid="
				  				   + validator.retrieveSessionID()
				  				 : ""), {command: 'message-fetching-command',
			messageKey: 'date.input.error.message'},
			function(response) {
				var $errorList = $('#errorList');
				if ($errorList.text().indexOf(response) === -1) {
					$errorList.html('');
					$errorList.prepend('<li><p>' + response + '</p><li>');
				}
			}, 'text');
		correctnessCondition = false;
	} else {
		year = parseInt(date.substring(0, 4), 10);
		month = parseInt(date.substring(5, 7), 10);
		day = parseInt(date.substring(8, 10), 10);
		currentDate = new Date();
		currentYear = currentDate.getFullYear();
		currentMonth = currentDate.getMonth();
		currentDay = currentDate.getDate();
		if (new Date(year, month - 1, day) < new Date(currentYear,
				currentMonth, currentDay)) {
			$.get('../controller'
				  + ((validator.retrieveSessionID() !== "")
					 ? ";jsessionid=" + validator.retrieveSessionID()
					 : ""), {command: 'message-fetching-command',
				messageKey: 'current.date.precendence.error.message'},
				function(response) {
					var $errorList = $('#errorList');
					if ($errorList.text().indexOf(response) === -1) {
						$errorList.html('');
						$errorList.prepend('<li><p>' + response + '</p><li>');
					}
				}, 'text');
			correctnessCondition = false;
		}
	}
	if (!correctnessCondition) {
		event.preventDefault();
	} else {
		$('#errorList').html('');
	}
});

$('#ticketAssigningForm button').click(function(event) {
	var correctnessCondition = true;
	var $ticketID = $('#ticketAssigningForm #number');
	var ticketID = $ticketID.hasClass('placeholder') ? "" : $ticketID.val();
	if (!validator.checkTicketIDCorrectness(ticketID)) {
		$.get('../controller' + ((validator.retrieveSessionID() !== "")
				  				 ? ";jsessionid="
				  				   + validator.retrieveSessionID()
				  				 : ""), {command: 'message-fetching-command',
			messageKey: 'ticket.id.input.error.message'},
			function(response) {
				var $errorList = $('#errorList');
				if ($errorList.text().indexOf(response) === -1) {
					$errorList.html('');
					$errorList.prepend('<li><p>' + response + '</p><li>');
				}
			}, 'text');
		correctnessCondition = false;
	} else {
		$('#errorList').html('');
	}
	if (!correctnessCondition) {
		event.preventDefault();
	}
});

$('#ticketCancellingForm button').click(function(event) {
	var correctnessCondition = true;
	var $ticketID = $('#ticketCancellingForm #number');
	var ticketID = $ticketID.hasClass('placeholder') ? "" : $ticketID.val();
	if (!validator.checkTicketIDCorrectness(ticketID)) {
		$.get('../controller' + ((validator.retrieveSessionID() !== "")
				  				 ? ";jsessionid="
				  				   + validator.retrieveSessionID()
				  				 : ""), {command: 'message-fetching-command',
			messageKey: 'ticket.id.input.error.message'},
			function(response) {
				var $errorList = $('#errorList');
				if ($errorList.text().indexOf(response) === -1) {
					$errorList.html('');
					$errorList.prepend('<li><p>' + response + '</p><li>');
				}
			}, 'text');
		correctnessCondition = false;
	} else {
		$('#errorList').html('');
	}
	if (!correctnessCondition) {
		event.preventDefault();
	}
});

$('#ticketAddingForm button').click(function(event) {
	var correctnessCondition = true;
	var curentDate;
	var $date = $('#date');
	var date = $date.hasClass('placeholder') ? "" : $date.val();
	var firstCondition = true;
	var secondCondition = true;
	var $startingTime = $('#startingTime');
	var $endingTime = $('#endingTime');
	var startingTime = $startingTime.hasClass('placeholder')
					   ? "" : $startingTime.val();
	var endingTime = $endingTime.hasClass('placeholder')
	   				 ? "" : $endingTime.val();
	$('#errorList').html('');
	if (!validator.checkDateCorrectness(date)) {
		validator.issueRequest('../controller', 'date.input.error.message');
		correctnessCondition = false;
	} else {
		year = parseInt(date.substring(0, 4), 10);
		month = parseInt(date.substring(5, 7), 10);
		day = parseInt(date.substring(8, 10), 10);
		currentDate = new Date();
		currentYear = currentDate.getFullYear();
		currentMonth = currentDate.getMonth();
		currentDay = currentDate.getDate();
		if (new Date(year, month - 1, day) < new Date(currentYear,
				currentMonth, currentDay)) {
			validator.issueRequest('../controller',
					'current.date.precendence.error.message');
			correctnessCondition = false;
		}
	}
	if (!validator.checkTimeCorrectness(startingTime)) {
		validator.issueRequest('../controller',
				'starting.time.input.error.message');
		firstCondition = false;
		correctnessCondition = false;
	}
	if (!validator.checkTimeCorrectness(endingTime)) {
		validator.issueRequest('../controller',
				'ending.time.input.error.message');
		secondCondition = false;
		correctnessCondition = false;
	}
	if ((firstCondition) && (secondCondition)
			&& (!(startingTime < endingTime))) {
		validator.issueRequest('../controller',
				'time.precendence.error.message');
		correctnessCondition = false;
	}
	if (!correctnessCondition) {
		event.preventDefault();
	} else {
		$('#errorList').html('');
	}
});

$('#visitCommittingForm button').click(function(event) {
	var correctnessCondition = true;
	var $ticketID = $('#visitCommittingForm #number');
	var ticketID = $ticketID.hasClass('placeholder') ? "" : $ticketID.val();
	var $diagnosis = $('#diagnosis');
	var diagnosis = $diagnosis.hasClass('placeholder') ? "" : $diagnosis.val();
	var $treatment = $('#treatment');
	var treatment = $treatment.hasClass('placeholder') ? "" : $treatment.val();
	$('#errorList').html('');
	if (!validator.checkTicketIDCorrectness(ticketID)) {
		validator.issueRequest('../controller',
				'ticket.id.input.error.message');
		correctnessCondition = false;
	}
	if (!validator.checkMinLength(diagnosis)) {
		validator.issueRequest('../controller',
				'diagnosis.input.error.message');
		correctnessCondition = false;
	}
	if (!validator.checkMinLength(treatment)) {
		validator.issueRequest('../controller',
				'treatment.input.error.message');
		correctnessCondition = false;
	}
	if (!correctnessCondition) {
		event.preventDefault();
	} else {
		$('#errorList').html('');
	}
});

$('#serviceInfoFetchingForm button').click(function(event) {
	var correctnessCondition = true;
	var $year = $('#year');
	var year = $year.hasClass('placeholder') ? "" : $year.val();
	var $month = $('#month');
	var month = $month.hasClass('placeholder') ? "" : $month.val();
	$('#errorList').html('');
	if (!validator.checkYearCorrectness(year)) {
		validator.issueRequest('../controller', 'year.input.error.message');
		correctnessCondition = false;
	}
	if (!validator.checkMonthCorrectness(month)) {
		validator.issueRequest('../controller', 'month.input.error.message');
		correctnessCondition = false;
	}
	if (!correctnessCondition) {
		event.preventDefault();
	} else {
		$('#errorList').html('');
	}
});

$('#usedTicketFetchingForm button').click(function(event) {
	var correctnessCondition = true;
	var currentDate = new Date();
	var currentYear = currentDate.getFullYear();
	var currentMonth = currentDate.getMonth();
	var currentDay = currentDate.getDate();
	var $startingDate = $('#startingDate');
	var startingDate = $startingDate.hasClass('placeholder')
					   ? "" : $startingDate.val();
	var $endingDate = $('#endingDate');
	var endingDate = $endingDate.hasClass('placeholder')
					 ? "" : $endingDate.val();
	var firstCondition = true;
	var secondCondition = true;
	$('#errorList').html('');
	if (!validator.checkDateCorrectness(startingDate)) {
		validator.issueRequest('../controller',
				'starting.date.input.error.message');
		firstCondition = false;
		correctnessCondition = false;
	} else {
		if (validator.retrieveDate(startingDate) > new Date(currentYear,
				currentMonth, currentDay)) {
			validator.issueRequest('../controller',
					'starting.date.precedence.error.message');
			firstCondition = false;
			correctnessCondition = false;
		}
	}
	if (!validator.checkDateCorrectness(endingDate)) {
		validator.issueRequest('../controller',
				'ending.date.input.error.message');
		secondCondition = false;
		correctnessCondition = false;
	} else {
		if (validator.retrieveDate(endingDate) > new Date(currentYear,
				currentMonth, currentDay)) {
			validator.issueRequest('../controller',
					'ending.date.precedence.error.message');
			secondCondition = false;
			correctnessCondition = false;
		}
	}
	if ((firstCondition) && (secondCondition)
			&& (startingDate > endingDate)) {
		validator.issueRequest('../controller',
				'period.precedence.error.message');
		correctnessCondition = false;
	}
	if (!correctnessCondition) {
		event.preventDefault();
	} else {
		$('#errorList').html('');
	}
});

$('#patientAddingForm button').click(function(event) {
	var correctnessCondition = true;
	var $passportNumber = $('#passportNumber');
	var passportNumber = $passportNumber.hasClass('placeholder')
						 ? "" : $passportNumber.val();
	var $firstName = $('#firstName');
	var firstName = $firstName.hasClass('placeholder') ? "" : $firstName.val();
	var $lastName = $('#lastName');
	var lastName = $lastName.hasClass('placeholder') ? "" : $lastName.val();
	var $login = $('#login');
	var login = $login.hasClass('placeholder') ? "" : $login.val();
	var $password = $('#password');
	var password = $password.hasClass('placeholder') ? "" : $password.val();
	$('#errorList').html('');
	if (!validator.checkPassportNumber(passportNumber)) {
		validator.issueRequest('../controller',
				'passport.number.input.error.message');
		correctnessCondition = false;
	}
	if (!validator.checkWord(firstName)) {
		validator.issueRequest('../controller',
				'first.name.input.error.message');
		correctnessCondition = false;
	}
	if (!validator.checkWord(lastName)) {
		validator.issueRequest('../controller',
				'last.name.input.error.message');
		correctnessCondition = false;
	}
	if (!validator.checkMinLength(login)) {
		validator.issueRequest('../controller',
				'login.input.error.message');
		correctnessCondition = false;
	}
	if (!validator.checkMinLength(password)) {
		validator.issueRequest('../controller',
				'password.input.error.message');
		correctnessCondition = false;
	}
	if (!correctnessCondition) {
		event.preventDefault();
	} else {
		$('#errorList').html('');
	}
});

$('#authenticationForm #login').blur(function() {
	$element = $(this);
	var value = $element.val();
	if (value.length === 0) {
		$element.toggleClass('placeholder');
		validator.setPlaceholder('controller', 'login.placeholder.label',
				$element);
	}
}).focus(function() {
	$element = $(this);
	if ($element.hasClass('placeholder')) {
		$element.val('');
	}
	$element.toggleClass('placeholder', false);
}).triggerHandler('blur');

$('#authenticationForm #password').blur(function() {
	$element = $(this);
	var value = $element.val();
	if (value.length === 0) {
		$element.toggleClass('placeholder');
		$element.attr('type', 'text');
		validator.setPlaceholder('controller', 'password.placeholder.label',
				$element);
	}
}).focus(function() {
	$element = $(this);
	if ($element.hasClass('placeholder')) {
		$element.val('');
		$element.attr('type', 'password');
	}
	$element.toggleClass('placeholder', false);
}).triggerHandler('blur');

$('#date, input[id$=Date]').blur(function() {
	$element = $(this);
	var value = $element.val();
	if (value.length === 0) {
		$element.toggleClass('placeholder');
		validator.setPlaceholder('../controller', 'date.placeholder.label',
				$element);
	}
}).focus(function() {
	$element = $(this);
	if ($element.hasClass('placeholder')) {
		$element.val('');
	}
	$element.toggleClass('placeholder', false);
}).trigger('blur');

$('#number').blur(function() {
	$element = $(this);
	var value = $element.val();
	if (value.length === 0) {
		$element.toggleClass('placeholder');
		validator.setPlaceholder('../controller',
				'ticket.number.placeholder.label',
				$element);
	}
}).focus(function() {
	$element = $(this);
	if ($element.hasClass('placeholder')) {
		$element.val('');
	}
	$element.toggleClass('placeholder', false);
}).triggerHandler('blur');

$('input[id$=Time]').blur(function() {
	$element = $(this);
	var value = $element.val();
	if (value.length === 0) {
		$element.toggleClass('placeholder');
		validator.setPlaceholder('../controller',
				'time.placeholder.label',
				$element);
	}
}).focus(function() {
	$element = $(this);
	if ($element.hasClass('placeholder')) {
		$element.val('');
	}
	$element.toggleClass('placeholder', false);
}).trigger('blur');

$('#diagnosis').blur(function() {
	$element = $(this);
	var value = $element.val();
	if (value.length === 0) {
		$element.toggleClass('placeholder');
		validator.setPlaceholder('../controller',
				'diagnosis.placeholder.label',
				$element);
	}
}).focus(function() {
	$element = $(this);
	if ($element.hasClass('placeholder')) {
		$element.val('');
	}
	$element.toggleClass('placeholder', false);
}).triggerHandler('blur');

$('#treatment').blur(function() {
	$element = $(this);
	var value = $element.val();
	if (value.length === 0) {
		$element.toggleClass('placeholder');
		validator.setPlaceholder('../controller',
				'treatment.placeholder.label',
				$element);
	}
}).focus(function() {
	$element = $(this);
	if ($element.hasClass('placeholder')) {
		$element.val('');
	}
	$element.toggleClass('placeholder', false);
}).triggerHandler('blur');

$('#passportNumber').blur(function() {
	$element = $(this);
	var value = $element.val();
	if (value.length === 0) {
		$element.toggleClass('placeholder');
		validator.setPlaceholder('../controller',
				'passport.number.placeholder.label',
				$element);
	}
}).focus(function() {
	$element = $(this);
	if ($element.hasClass('placeholder')) {
		$element.val('');
	}
	$element.toggleClass('placeholder', false);
}).triggerHandler('blur');

$('#firstName').blur(function() {
	$element = $(this);
	var value = $element.val();
	if (value.length === 0) {
		$element.toggleClass('placeholder');
		validator.setPlaceholder('../controller',
				'first.name.placeholder.label',
				$element);
	}
}).focus(function() {
	$element = $(this);
	if ($element.hasClass('placeholder')) {
		$element.val('');
	}
	$element.toggleClass('placeholder', false);
}).triggerHandler('blur');

$('#lastName').blur(function() {
	$element = $(this);
	var value = $element.val();
	if (value.length === 0) {
		$element.toggleClass('placeholder');
		validator.setPlaceholder('../controller',
				'last.name.placeholder.label',
				$element);
	}
}).focus(function() {
	$element = $(this);
	if ($element.hasClass('placeholder')) {
		$element.val('');
	}
	$element.toggleClass('placeholder', false);
}).triggerHandler('blur');

$('#patientAddingForm #login').blur(function() {
	$element = $(this);
	var value = $element.val();
	if (value.length === 0) {
		$element.toggleClass('placeholder');
		validator.setPlaceholder('../controller',
				'login.placeholder.label',
				$element);
	}
}).focus(function() {
	$element = $(this);
	if ($element.hasClass('placeholder')) {
		$element.val('');
	}
	$element.toggleClass('placeholder', false);
}).triggerHandler('blur');

$('#patientAddingForm #password').blur(function() {
	$element = $(this);
	var value = $element.val();
	if (value.length === 0) {
		$element.toggleClass('placeholder');
		$element.attr('type', 'text');
		validator.setPlaceholder('../controller',
				'password.placeholder.label',
				$element);
	}
}).focus(function() {
	$element = $(this);
	if ($element.hasClass('placeholder')) {
		$element.val('');
		$element.attr('type', 'password');
	}
	$element.toggleClass('placeholder', false);
}).triggerHandler('blur');

$('#year').blur(function() {
	$element = $(this);
	var value = $element.val();
	if (value.length === 0) {
		$element.toggleClass('placeholder');
		validator.setPlaceholder('../controller',
				'year.placeholder.label',
				$element);
	}
}).focus(function() {
	$element = $(this);
	if ($element.hasClass('placeholder')) {
		$element.val('');
	}
	$element.toggleClass('placeholder', false);
}).triggerHandler('blur');

$('#month').blur(function() {
	$element = $(this);
	var value = $element.val();
	if (value.length === 0) {
		$element.toggleClass('placeholder');
		validator.setPlaceholder('../controller',
				'month.placeholder.label',
				$element);
	}
}).focus(function() {
	$element = $(this);
	if ($element.hasClass('placeholder')) {
		$element.val('');
	}
	$element.toggleClass('placeholder', false);
}).triggerHandler('blur');

$('table#ownTicketView').on('click', 'button', function(event) {
	var $parent = $(event.target).parent();
	var $container = $('#ticket-canceling-container');
	event.preventDefault();
	$container.show();
	$('#ticket-canceling-container .dialog-box').show();
	$container.data('ticketID', $parent.next().children().val());
});

$('#ticket-canceling-container button:nth-of-type(1)').click(function() {
	$('#ticket-canceling-container').hide();
});

$('#ticket-canceling-container button:nth-of-type(2)').click(function() {
	$('#ticket-canceling-container').hide();
	$.post('../controller' + ((validator.retrieveSessionID() !== "")
		  	  				  ? ";jsessionid=" + validator.retrieveSessionID()
		  	  				  : ""),
		   {ticketID: $('#ticket-canceling-container').data('ticketID'),
	 	    command: 'ticket-cancelling-command'},
	 	   function(response) {
	 	    	$('#ticket-canceling-container .dialog-box').hide();
	 	    	$('#ticket-canceling-container').show();
	 	    	$('.message-dialog-box').show()
	 	    	.children().first()
	 	    	.text(response
	 	    		  .substring(response.indexOf('<p>') + 3,
	 	    				  	 response.indexOf('</p>')));
	 	   }, 'text');
});

$('#ticket-canceling-container .message-dialog-box button').click(function() {
	$('#ticket-canceling-container').hide();
 	$('.message-dialog-box').hide();
});

$('table#freeTicketView').on('click', 'button', function(event) {
	var $parent = $(event.target).parent();
	var $container = $('#ticket-assigning-container');
	event.preventDefault();
	$container.show();
	$('#ticket-assigning-container .dialog-box').show();
	$container.data('ticketID', $parent.next().children().val());
});

$('#ticket-assigning-container button:nth-of-type(1)').click(function() {
	$('#ticket-assigning-container').hide();
});

$('#ticket-assigning-container button:nth-of-type(2)').click(function() {
	$('#ticket-assigning-container').hide();
	$.post('../controller' + ((validator.retrieveSessionID() !== "")
		  	  				  ? ";jsessionid=" + validator.retrieveSessionID()
		  	  				  : ""),
		   {ticketID: $('#ticket-assigning-container').data('ticketID'),
	 	    command: 'ticket-assigning-command'},
	 	   function(response) {
	 	    	$('#ticket-assigning-container .dialog-box').hide();
	 	    	$('#ticket-assigning-container').show();
	 	    	$('.message-dialog-box').show()
	 	    	.children().first()
	 	    	.text(response
	 	    		  .substring(response.indexOf('<p>') + 3,
	 	    				  	 response.indexOf('</p>')));
	 	   }, 'text');
});

$('#ticket-assigning-container .message-dialog-box button').click(function() {
	$('#ticket-assigning-container').hide();
 	$('.message-dialog-box').hide();
});

$('table#userView').on('click', 'button', function(event) {
	var $parent = $(event.target).parent();
	var passportNumber = $parent.next().children().val();
	var blockedState = $parent.next().children().last().val();
	var $container = (blockedState === 'false')
					 ? $('#user-unblocking-container')
					 : $('#user-blocking-container');
	event.preventDefault();
	$container.show();
	$container.find('.dialog-box').show();
	$container.data('passportNumber', passportNumber);
	$container.data('blockedState', blockedState);
});

$('#user-unblocking-container button:nth-of-type(1)').click(function() {
	$('#user-unblocking-container').hide();
});

$('#user-unblocking-container button:nth-of-type(2)').click(function() {
	$('#user-unblocking-container').hide();
	$.post('../controller' + ((validator.retrieveSessionID() !== "")
		  	  				  ? ";jsessionid=" + validator.retrieveSessionID()
		  	  				  : ""),
		   {passportNumber: $('#user-unblocking-container')
							.data('passportNumber'),
		    blockedState: $('#user-unblocking-container').data('blockedState'),
	 	    command: 'blocked-state-setting-command'},
	 	   function(response) {
	 	    	$('#user-unblocking-container .dialog-box').hide();
	 	    	$('#user-unblocking-container').show();
	 	    	$('#user-unblocking-container .message-dialog-box').show()
	 	    	.children().first()
	 	    	.text(response
	 	    		  .substring(response.indexOf('<p>') + 3,
	 	    				  	 response.indexOf('</p>')));
	 	   }, 'text');
});

$('#user-unblocking-container .message-dialog-box button').click(function() {
	$('#user-unblocking-container').hide();
 	$('.message-dialog-box').hide();
});

$('#user-blocking-container button:nth-of-type(1)').click(function() {
	$('#user-blocking-container').hide();
});

$('#user-blocking-container button:nth-of-type(2)').click(function() {
	$('#user-blocking-container').hide();
	$.post('../controller' + ((validator.retrieveSessionID() !== "")
		  	  				  ? ";jsessionid=" + validator.retrieveSessionID()
		  	  				  : ""),
		   {passportNumber: $('#user-blocking-container')
							.data('passportNumber'),
		    blockedState: $('#user-blocking-container').data('blockedState'),
	 	    command: 'blocked-state-setting-command'},
	 	   function(response) {
	 	    	$('#user-blocking-container .dialog-box').hide();
	 	    	$('#user-blocking-container').show();
	 	    	$('#user-blocking-container .message-dialog-box').show()
	 	    	.children().first()
	 	    	.text(response
	 	    		  .substring(response.indexOf('<p>') + 3,
	 	    				  	 response.indexOf('</p>')));
	 	   }, 'text');
});

$('#user-blocking-container .message-dialog-box button').click(function() {
	$('#user-blocking-container').hide();
 	$('.message-dialog-box').hide();
});

function DataValidator() {}