<?xml version = "1.0" encoding = "UTF-8" ?>
<jsp:root xmlns:jsp = "http://java.sun.com/JSP/Page" version = "2.0"
          xmlns:c = "http://java.sun.com/jsp/jstl/core"
          xmlns:fmt = "http://java.sun.com/jsp/jstl/fmt"
          xmlns:dspl = "http://www.task05.training.by/PresentationLibrary">
    <jsp:directive.page contentType = "text/html; charset=UTF-8"
                        pageEncoding = "UTF-8"/>
    <jsp:output doctype-root-element = "html"
        doctype-public = "-//W3C//DTD XHTML 1.0 Transitional//EN"
        doctype-system = "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
        omit-xml-declaration = "true" />
    <fmt:setLocale value = "${(not empty locale) ? locale : defaultLocale}"/>
    <fmt:setBundle basename = "by.training.task05.bundle.PageLabels"/>
    <c:url var = "localeChangingURL" 
           value = "/controller">
        <c:param name = "command" 
                 value = "locale-changing-command" />
        <c:param name = "pathPropertyKey" 
                 value = "free.ticket.viewing.page.path" />
        <c:param name = "pageNumber" 
                 value = "${param['pageNumber']}" />
    </c:url>
    <c:url var = "logoutURL" value = "/controller">
        <c:param name = "command" value = "logout-command" />
    </c:url>
    <c:url var = "mainPageURL" value = "/main.jsp" />
    <c:url var = "freeTicketFetchingPageURL"
           value = "/patient/free-ticket-fetching.jsp" />
    <c:url var = "ticketAssigningURL" value = "/controller"/>
    <c:url var = "imageURL" 
           value = "/controller">
        <c:param name = "command" 
                 value = "image-fetching-command"/>
        <c:param name = "login" 
                 value = "${authenticatedUser.login}"/>
    </c:url>
    <c:url var = "styleURL" value = "${stylePath}"/>
    <c:url var = "jqueryURL" value = "/js/jquery-1.11.3.min.js"/>
    <c:url var = "scriptURL" value = "/js/script.js"/>
    <html xmlns = "http://www.w3.org/1999/xhtml">
        <head>
            <meta charset = "UTF-8"></meta>
            <title><fmt:message key = "free.ticket.viewing.page.title"/></title>
            <link href = "${styleURL}" rel = "stylesheet" type = "text/css"/>
        </head>
        <body>
            <div class = "user-info">
                <img src = "${imageURL}"/>
                <span><fmt:message key = "login.label"/></span>
                <span><c:out value = "${authenticatedUser.login}" escapeXml = "true"/></span>
                <span><fmt:message key = "role.label"/></span>
                <span><fmt:message key = "${roleMap[authenticatedUser.role]}"/></span>
                <span><fmt:message key = "dot.label"/></span>
            </div>
            <h1><fmt:message key = "free.ticket.viewing.page.header"/></h1>
            <div id = "free-ticket-viewing" class = "basic-navigation">
                <a href = "${localeChangingURL}">
                    <fmt:message key = "locale.changing.reference.label"/>
                </a>
                <a href = "${mainPageURL}">
                    <fmt:message key = "main.page.reference.label"/>
                </a>
                <a href = "${freeTicketFetchingPageURL}">
                    <fmt:message key = "free.ticket.fetching.page.reference.label"/>
                </a>
                <a href = "${logoutURL}">
                    <fmt:message key = "logout.reference.label"/>
                </a>
            </div>
            <dspl:freeTicketView sentCommandParam = "command"
                                 freeTicketData = "${freeTickets}"
                                 rowQuantity = "${rowQuantity}"
                                 pageURL = "/patient/free-ticket-viewing.jsp"
                                 sentPageNumberParam = "pageNumber"
                                 tableId = "freeTicketView"
                                 containerId = "pageReferenceContainer"
                                 sentTicketIDParam = "ticketID"
                                 commandName = "ticket-assigning-command"
                                 targetURL = "${ticketAssigningURL}">
                <jsp:attribute name = "ticketIdTableLabel" 
                               trim = "true">
                    <fmt:message key = "ticket.id.table.label"/>
                </jsp:attribute>
                <jsp:attribute name = "firstNameTableLabel" 
                               trim = "true">
                    <fmt:message key = "first.name.table.label"/>
                </jsp:attribute>
                <jsp:attribute name = "lastNameTableLabel" 
                               trim = "true">
                    <fmt:message key = "last.name.table.label"/>
                </jsp:attribute>
                <jsp:attribute name = "roomTableLabel" 
                               trim = "true">
                    <fmt:message key = "room.table.label"/>
                </jsp:attribute>
                <jsp:attribute name = "dateTableLabel" 
                               trim = "true">
                    <fmt:message key = "date.table.label"/>
                </jsp:attribute>
                <jsp:attribute name = "startingTimeTableLabel" 
                               trim = "true">
                    <fmt:message key = "starting.time.table.label"/>
                </jsp:attribute>
                <jsp:attribute name = "endingTimeTableLabel" 
                               trim = "true">
                    <fmt:message key = "ending.time.table.label"/>
                </jsp:attribute>
                <jsp:attribute name = "actionTableLabel" 
                               trim = "true">
                    <fmt:message key = "action.table.label"/>
                </jsp:attribute>
                <jsp:attribute name = "informationLackLabel" 
                               trim = "true">
                    <fmt:message key = "information.lack.label"/>
                </jsp:attribute>
                <jsp:attribute name = "pageNumberLabel" 
                               trim = "true">
                    <fmt:message key = "page.number.label"/>
                </jsp:attribute>
                <jsp:attribute name = "assigningButtonLabel" 
                               trim = "true">
                    <fmt:message key = "assigning.button.label"/>
                </jsp:attribute>
            </dspl:freeTicketView>
            <div id = "ticket-assigning-container"
                    class = "dialog-box-container">
                <div class = "dialog-box">
                    <p>
                        <fmt:message
                            key = "ticket.assigning.dialog.box.title.label"/>
                    </p>
                    <button>
                        <fmt:message
                            key = "dialog.box.canceling.button.label"/>
                    </button>
                    <button>
                        <fmt:message
                            key = "dialog.box.confirmation.button.label"/>
                    </button>
                </div>
                <div class = "message-dialog-box">
                    <p>content</p>
                    <button>
                        <fmt:message
                            key = "dialog.box.closing.button.label"/>
                    </button>
                </div>
            </div>
            <script src = "${jqueryURL}" type = "text/javascript">content</script>
            <script src = "${scriptURL}" type = "text/javascript">content</script>
        </body>
    </html>
</jsp:root>
