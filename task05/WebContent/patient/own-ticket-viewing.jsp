<?xml version = "1.0" encoding = "UTF-8" ?>
<jsp:root xmlns:jsp = "http://java.sun.com/JSP/Page" version = "2.0"
          xmlns:c = "http://java.sun.com/jsp/jstl/core"
          xmlns:fmt = "http://java.sun.com/jsp/jstl/fmt"
          xmlns:dspl = "http://www.task05.training.by/PresentationLibrary">
	<jsp:directive.page contentType = "text/html; charset=UTF-8"
                        pageEncoding = "UTF-8"/>
    <jsp:output doctype-root-element = "html"
        doctype-public = "-//W3C//DTD XHTML 1.0 Transitional//EN"
        doctype-system = "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
        omit-xml-declaration = "true" />
    <fmt:setLocale value = "${(not empty locale) ? locale : defaultLocale}"/>
    <fmt:setBundle var = "bundle"
                   basename = "by.training.task05.bundle.PageLabels"/>
    <c:url var = "localeChangingURL" 
           value = "/controller">
        <c:param name = "command" 
                 value = "locale-changing-command" />
        <c:param name = "pathPropertyKey" 
                 value = "own.ticket.viewing.page.path" />
        <c:param name = "pageNumber" 
                 value = "${param['pageNumber']}" />
    </c:url>
    <c:url var = "logoutURL" 
           value = "/controller">
        <c:param name = "command" 
                 value = "logout-command" />
    </c:url>
    <c:url var = "mainPageURL" 
           value = "/main.jsp" />
    <c:url var = "ticketCancelingURL" value = "/controller"/>
    <c:url var = "imageURL" 
           value = "/controller">
        <c:param name = "command" 
                 value = "image-fetching-command"/>
        <c:param name = "login" 
                 value = "${authenticatedUser.login}"/>
    </c:url>
    <c:url var = "styleURL" value = "${stylePath}"/>
    <c:url var = "jqueryURL" value = "/js/jquery-1.11.3.min.js"/>
    <c:url var = "scriptURL" value = "/js/script.js"/>
	<html xmlns = "http://www.w3.org/1999/xhtml">
	    <head>
	        <meta charset = "UTF-8"></meta>
	        <title><fmt:message key = "own.ticket.viewing.page.title"
	                            bundle = "${bundle}"/></title>
	        <link href = "${styleURL}" rel = "stylesheet" type = "text/css"/>
	    </head>
		<body>
		    <div class = "user-info">
                <img src = "${imageURL}"/>
                <span><fmt:message key = "login.label" bundle = "${bundle}"/></span>
                <span><c:out value = "${authenticatedUser.login}" escapeXml = "true"/></span>
                <span><fmt:message key = "role.label" bundle = "${bundle}"/></span>
                <span><fmt:message key = "${roleMap[authenticatedUser.role]}" bundle = "${bundle}"/></span>
                <span><fmt:message key = "dot.label" bundle = "${bundle}"/></span>
            </div>
		    <h1><fmt:message key = "own.ticket.viewing.page.header"
		                     bundle = "${bundle}"/></h1>
		    <div id = "own-ticket-viewing" class = "basic-navigation">
                <a href = "${localeChangingURL}">
                    <fmt:message key = "locale.changing.reference.label"
                                 bundle = "${bundle}"/>
                </a>
                <a href = "${mainPageURL}">
                    <fmt:message key = "main.page.reference.label"
                                 bundle = "${bundle}"/>
                </a>
                <a href = "${logoutURL}">
                    <fmt:message key = "logout.reference.label"
                                 bundle = "${bundle}"/>
                </a>
            </div>
            <dspl:ownTicketView ownTicketData = "${ownTickets}"
                    rowQuantity = "${rowQuantity}"
                    pageURL = "/patient/own-ticket-viewing.jsp"
                    sentPageNumberParam = "pageNumber"
                    specialityMapping = "${specialityMap}"
                    resourceBundle = "${bundle}"
                    tableId = "ownTicketView"
                    containerId = "pageReferenceContainer"
                    sentTicketIDParam = "ticketID"
                    commandName = "ticket-cancelling-command"
                    targetURL = "${ticketCancelingURL}"
                    sentCommandParam = "command">
                <jsp:attribute name = "ticketIdTableLabel" 
                               trim = "true">
                    <fmt:message key = "ticket.id.table.label"
                                 bundle = "${bundle}"/>
                </jsp:attribute>
                <jsp:attribute name = "specialityTableLabel" 
                               trim = "true">
                    <fmt:message key = "speciality.table.label"
                                 bundle = "${bundle}"/>
                </jsp:attribute>
                <jsp:attribute name = "dateTableLabel" 
                               trim = "true">
                    <fmt:message key = "date.table.label"
                                 bundle = "${bundle}"/>
                </jsp:attribute>
                <jsp:attribute name = "startingTimeTableLabel" 
                               trim = "true">
                    <fmt:message key = "starting.time.table.label"
                                 bundle = "${bundle}"/>
                </jsp:attribute>
                <jsp:attribute name = "endingTimeTableLabel" 
                               trim = "true">
                    <fmt:message key = "ending.time.table.label"
                                 bundle = "${bundle}"/>
                </jsp:attribute>
                <jsp:attribute name = "diagnosisTableLabel" 
                               trim = "true">
                    <fmt:message key = "diagnosis.table.label"
                                 bundle = "${bundle}"/>
                </jsp:attribute>
                <jsp:attribute name = "treatmentTableLabel" 
                               trim = "true">
                    <fmt:message key = "treatment.table.label"
                                 bundle = "${bundle}"/>
                </jsp:attribute>
                <jsp:attribute name = "actionTableLabel" 
                               trim = "true">
                    <fmt:message key = "action.table.label"
                                 bundle = "${bundle}"/>
                </jsp:attribute>
                <jsp:attribute name = "valueLackTableCellLabel" 
                               trim = "true">
                    <fmt:message key = "table.cell.label"
                                 bundle = "${bundle}"/>
                </jsp:attribute>
                <jsp:attribute name = "informationLackLabel" 
                               trim = "true">
                    <fmt:message key = "information.lack.label"
                                 bundle = "${bundle}"/>
                </jsp:attribute>
                <jsp:attribute name = "pageNumberLabel" 
                               trim = "true">
                    <fmt:message key = "page.number.label"
                                 bundle = "${bundle}"/>
                </jsp:attribute>
                <jsp:attribute name = "cancelingButtonLabel" 
                               trim = "true">
                    <fmt:message key = "canceling.button.label"
                                 bundle = "${bundle}"/>
                </jsp:attribute>
            </dspl:ownTicketView>
            <div id = "ticket-canceling-container"
                    class = "dialog-box-container">
                <div class = "dialog-box">
                    <p>
                        <fmt:message
                            key = "ticket.canceling.dialog.box.title.label"
                            bundle = "${bundle}"/>
                    </p>
                    <button>
                        <fmt:message
                            key = "dialog.box.canceling.button.label"
                            bundle = "${bundle}"/>
                    </button>
                    <button>
                        <fmt:message
                            key = "dialog.box.confirmation.button.label"
                            bundle = "${bundle}"/>
                    </button>
                </div>
                <div class = "message-dialog-box">
                    <p>content</p>
                    <button>
                        <fmt:message
                            key = "dialog.box.closing.button.label"
                            bundle = "${bundle}"/>
                    </button>
                </div>
            </div>
            <script src = "${jqueryURL}" type = "text/javascript">content</script>
            <script src = "${scriptURL}" type = "text/javascript">content</script>
		</body>
	</html>
</jsp:root>
