<?xml version = "1.0" encoding = "UTF-8" ?>
<jsp:root xmlns:jsp = "http://java.sun.com/JSP/Page" version = "2.0"
          xmlns:c = "http://java.sun.com/jsp/jstl/core"
          xmlns:fmt = "http://java.sun.com/jsp/jstl/fmt"
          xmlns:dspl = "http://www.task05.training.by/PresentationLibrary">
    <jsp:directive.page contentType = "text/html; charset=UTF-8"
                        pageEncoding = "UTF-8"/>
    <jsp:output doctype-root-element = "html"
        doctype-public = "-//W3C//DTD XHTML 1.0 Transitional//EN"
        doctype-system = "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
        omit-xml-declaration = "true" />
    <fmt:setLocale value = "${(not empty locale) ? locale : defaultLocale}"/>
    <fmt:setBundle basename = "by.training.task05.bundle.PageLabels"/>
    <c:url var = "localeChangingURL" value = "/controller">
        <c:param name = "command"
                 value = "locale-changing-command" />
        <c:param name = "pathPropertyKey"
                 value = "ticket.cancelling.page.path" />
        <c:forEach var = "entry" items = "${param}">
            <c:set var = "parameterName" 
                   value = "${entry['key']}" />
            <c:if test = "${(parameterName != 'command') 
                            and (parameterName != 'pathPropertyKey')}">
                <c:param name = "${parameterName}" 
                         value = "${dspl:convertString(entry['value'])}" />
            </c:if>
        </c:forEach>
    </c:url>
    <c:url var = "logoutURL" value = "/controller">
        <c:param name = "command" value = "logout-command" />
    </c:url>
    <c:url var = "mainPageURL" value = "/main.jsp" />
    <c:url var = "ticketCancellingURL" value = "/controller"/>
    <c:url var = "imageURL" 
           value = "/controller">
        <c:param name = "command" 
                 value = "image-fetching-command"/>
        <c:param name = "login" 
                 value = "${authenticatedUser.login}"/>
    </c:url>
    <c:url var = "styleURL" value = "${stylePath}"/>
    <c:url var = "jqueryURL" value = "/js/jquery-1.11.3.min.js"/>
    <c:url var = "scriptURL" value = "/js/script.js"/>
    <html xmlns = "http://www.w3.org/1999/xhtml">
        <head>
            <meta charset = "UTF-8"></meta>
            <title><fmt:message key = "ticket.cancelling.page.title"/></title>
            <link href = "${styleURL}" rel = "stylesheet" type = "text/css"/>
        </head>
        <body>
            <div class = "user-info">
                <img src = "${imageURL}"/>
                <span><fmt:message key = "login.label"/></span>
                <span><c:out value = "${authenticatedUser.login}" escapeXml = "true"/></span>
                <span><fmt:message key = "role.label"/></span>
                <span><fmt:message key = "${roleMap[authenticatedUser.role]}"/></span>
                <span><fmt:message key = "dot.label"/></span>
            </div>
            <h1><fmt:message key = "ticket.cancelling.page.header"/></h1>
            <div id = "ticket-cancelling" class = "basic-navigation">
                <a href = "${localeChangingURL}">
                    <fmt:message key = "locale.changing.reference.label"/>
                </a>
                <a href = "${mainPageURL}">
                    <fmt:message key = "main.page.reference.label"/>
                </a>
                <a href = "${logoutURL}">
                    <fmt:message key = "logout.reference.label"/>
                </a>
            </div>
            <c:forEach var = "entry" items = "${errorMessages}">
                <c:set target = "${errorMessages}" 
                       property = "${entry['key']}">
                    <fmt:message key = "${entry['key']}"/>
                </c:set>
            </c:forEach>
            <dspl:inputErrorMessages 
                    inputErrorMessages = "${errorMessages}"
                    listId = "errorList">
            </dspl:inputErrorMessages>
            <dspl:ticketForm sentCommandParam = "command"
                    sentTicketIDParam = "ticketID"
                    formId = "ticketCancellingForm"
                    commandName = "ticket-cancelling-command"
                    targetURL = "${ticketCancellingURL}">
                <jsp:attribute name = "ticketIDInputLabel" 
                              trim = "true">
                    <fmt:message key = "ticket.id.input.label"/>
                </jsp:attribute>
                <jsp:attribute name = "submitButtonLabel" 
                               trim = "true">
                    <fmt:message key = "submit.button.label"/>
                </jsp:attribute>
            </dspl:ticketForm>
            <script src = "${jqueryURL}" type = "text/javascript">content</script>
            <script src = "${scriptURL}" type = "text/javascript">content</script>
        </body>
    </html>
</jsp:root>
