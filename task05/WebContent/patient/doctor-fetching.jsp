<?xml version = "1.0" encoding = "UTF-8" ?>
<jsp:root xmlns:jsp = "http://java.sun.com/JSP/Page" version = "2.0"
          xmlns:c = "http://java.sun.com/jsp/jstl/core"
          xmlns:fmt = "http://java.sun.com/jsp/jstl/fmt"
          xmlns:dspl = "http://www.task05.training.by/PresentationLibrary">
    <jsp:directive.page contentType = "text/html; charset=UTF-8"
                        pageEncoding = "UTF-8"/>
    <jsp:output doctype-root-element = "html"
        doctype-public = "-//W3C//DTD XHTML 1.0 Transitional//EN"
        doctype-system = "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
        omit-xml-declaration = "true" />
    <fmt:setLocale value = "${(not empty locale) ? locale : defaultLocale}"/>
    <fmt:setBundle var = "bundle"
                   basename = "by.training.task05.bundle.PageLabels"/>
    <c:url var = "localeChangingURL" value = "/controller">
        <c:param name = "command"
                 value = "locale-changing-command" />
        <c:param name = "pathPropertyKey"
                 value = "doctor.fetching.page.path" />
    </c:url>
    <c:url var = "logoutURL" value = "/controller">
        <c:param name = "command"
                 value = "logout-command" />
    </c:url>
    <c:url var = "mainPageURL" value = "/main.jsp" />
    <c:url var = "doctorFetchingURL" value = "/controller"/>
    <c:url var = "imageURL" 
           value = "/controller">
        <c:param name = "command" 
                 value = "image-fetching-command"/>
        <c:param name = "login" 
                 value = "${authenticatedUser.login}"/>
    </c:url>
    <c:url var = "styleURL" value = "${stylePath}"/>
    <html xmlns = "http://www.w3.org/1999/xhtml">
        <head>
            <meta charset = "UTF-8"></meta>
            <title><fmt:message key = "doctor.fetching.page.title"
                                bundle = "${bundle}"/></title>
            <link href = "${styleURL}" rel = "stylesheet" type = "text/css"/>
        </head>
        <body>
            <div class = "user-info">
                <img src = "${imageURL}"/>
                <span><fmt:message key = "login.label" bundle = "${bundle}"/></span>
                <span><c:out value = "${authenticatedUser.login}" escapeXml = "true"/></span>
                <span><fmt:message key = "role.label" bundle = "${bundle}"/></span>
                <span><fmt:message key = "${roleMap[authenticatedUser.role]}" bundle = "${bundle}"/></span>
                <span><fmt:message key = "dot.label" bundle = "${bundle}"/></span>
            </div>
            <h1><fmt:message key = "doctor.fetching.page.header"
                             bundle = "${bundle}"/></h1>
            <div id = "doctor-fetching" class = "basic-navigation">
                <a href = "${localeChangingURL}">
                    <fmt:message key = "locale.changing.reference.label"
                                 bundle = "${bundle}"/>
                </a>
                <a href = "${mainPageURL}">
                    <fmt:message key = "main.page.reference.label"
                                 bundle = "${bundle}"/>
                </a>
                <a href = "${logoutURL}">
                    <fmt:message key = "logout.reference.label"
                                 bundle = "${bundle}"/>
                </a>
            </div>
            <dspl:doctorFetchingForm targetURL = "${doctorFetchingURL}"
                                     sentCommandParam = "command"
                                     sentSpecialityParam = "speciality"
                                     commandName = "doctor-fetching-command"
                                     formId = "doctorFetchingForm"
                                     resourceBundle = "${bundle}"
                                     mapping = "${specialityMap}">
                <jsp:attribute name = "specialitySelectionLabel" 
                              trim = "true">
                    <fmt:message key = "speciality.selection.label"
                                 bundle = "${bundle}"/>
                </jsp:attribute>
                <jsp:attribute name = "submitButtonLabel" 
                               trim = "true">
                    <fmt:message key = "submit.button.label"
                                 bundle = "${bundle}"/>
                </jsp:attribute>
            </dspl:doctorFetchingForm>
        </body>
    </html>
</jsp:root>
