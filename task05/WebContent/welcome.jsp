<?xml version = "1.0" encoding = "UTF-8" ?>
<jsp:root xmlns:jsp = "http://java.sun.com/JSP/Page" version = "2.0"
          xmlns:c = "http://java.sun.com/jsp/jstl/core"
          xmlns:fmt = "http://java.sun.com/jsp/jstl/fmt"
          xmlns:dspl = "http://www.task05.training.by/PresentationLibrary">
    <jsp:directive.page contentType = "text/html; charset=UTF-8"
		                pageEncoding = "UTF-8"/>
    <jsp:output doctype-root-element = "html"
            doctype-public = "-//W3C//DTD XHTML 1.0 Transitional//EN"
            doctype-system = "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
            omit-xml-declaration = "true" />
    <fmt:setLocale value = "${(not empty locale) ? locale : defaultLocale}"/>
    <fmt:setBundle basename = "by.training.task05.bundle.PageLabels"/>
    <c:url var = "localeChangingURL" 
           value = "/controller">
        <c:param name = "command" 
                 value = "locale-changing-command" />
        <c:param name = "pathPropertyKey" 
                 value = "welcome.page.path" />
    </c:url>
    <c:url var = "authenticationURL" value = "/controller"/>
    <c:url var = "styleURL" value = "${stylePath}"/>
    <c:url var = "jqueryURL" value = "/js/jquery-1.11.3.min.js"/>
    <c:url var = "scriptURL" value = "/js/script.js"/>
    <html xmlns = "http://www.w3.org/1999/xhtml">
        <head>
            <meta charset = "UTF-8"></meta>
            <title><fmt:message key = "welcome.page.title"/></title>
            <link href = "${styleURL}" rel = "stylesheet" type = "text/css"/>
        </head>
        <body>
            <h1><fmt:message key = "welcome.page.header"/></h1>
            <div id = "welcome" class = "basic-navigation">
                <a href = "${localeChangingURL}">
                    <fmt:message key = "locale.changing.reference.label"/>
                </a>
            </div>
            <dspl:inputErrorMessages 
                    inputErrorMessages = "${errorMessages}"
                    listId = "errorList">
            </dspl:inputErrorMessages>
            <dspl:authenticationForm 
                    targetURL = "${authenticationURL}" 
                    sentCommandParam = "command"
                    sentLoginParam = "login" 
                    sentPasswordParam = "password"
                    commandName = "authentication-command"
                    formId = "authenticationForm">
                <jsp:attribute name = "loginInputLabel" trim = "true">
                    <fmt:message key = "login.input.label"/>
                </jsp:attribute>
                <jsp:attribute name = "passwordInputLabel" trim = "true">
                     <fmt:message key = "password.input.label"/>
                </jsp:attribute>
                <jsp:attribute name = "submitButtonLabel" trim = "true">
                    <fmt:message key = "submit.button.label"/>
                </jsp:attribute>
            </dspl:authenticationForm>
            <script src = "${jqueryURL}" type = "text/javascript">content</script>
            <script src = "${scriptURL}" type = "text/javascript">content</script>
        </body>
    </html>
</jsp:root>
