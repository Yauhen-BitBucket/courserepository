<?xml version = "1.0" encoding = "UTF-8" ?>
<jsp:root xmlns:jsp = "http://java.sun.com/JSP/Page" version = "2.0"
          xmlns:c = "http://java.sun.com/jsp/jstl/core"
          xmlns:fmt = "http://java.sun.com/jsp/jstl/fmt"
          xmlns:dspl = "http://www.task05.training.by/PresentationLibrary">
    <jsp:directive.page contentType = "text/html; charset=UTF-8"
                        pageEncoding = "UTF-8"/>
    <jsp:output doctype-root-element = "html"
        doctype-public = "-//W3C//DTD XHTML 1.0 Transitional//EN"
        doctype-system = "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
        omit-xml-declaration = "true" />
    <fmt:setLocale value = "${(not empty locale) ? locale : defaultLocale}"/>
    <fmt:setBundle var = "bundle"
                   basename = "by.training.task05.bundle.PageLabels"/>
    <c:url var = "localeChangingURL" 
           value = "/controller">
        <c:param name = "command" 
                 value = "locale-changing-command" />
        <c:param name = "pathPropertyKey" 
                 value = "service.info.viewing.page.path" />
        <c:param name = "pageNumber" 
                 value = "${param['pageNumber']}" />
    </c:url>
    <c:url var = "logoutURL" value = "/controller">
        <c:param name = "command" value = "logout-command" />
    </c:url>
    <c:url var = "mainPageURL" value = "/main.jsp" />
    <c:url var = "serviceInfoFetchingPageURL"
           value = "/head-physician/service-info-fetching.jsp" />
    <c:url var = "imageURL" 
           value = "/controller">
        <c:param name = "command" 
                 value = "image-fetching-command"/>
        <c:param name = "login" 
                 value = "${authenticatedUser.login}"/>
    </c:url>
    <c:url var = "styleURL" value = "${stylePath}"/>
    <html xmlns = "http://www.w3.org/1999/xhtml">
        <head>
            <meta charset = "UTF-8"></meta>
            <title><fmt:message key = "service.info.viewing.page.title"
                                bundle = "${bundle}"/></title>
            <link href = "${styleURL}" rel = "stylesheet" type = "text/css"/>
        </head>
        <body>
            <div class = "user-info">
                <img src = "${imageURL}"/>
                <span><fmt:message key = "login.label" bundle = "${bundle}"/></span>
                <span><c:out value = "${authenticatedUser.login}" escapeXml = "true"/></span>
                <span><fmt:message key = "role.label" bundle = "${bundle}"/></span>
                <span><fmt:message key = "${roleMap[authenticatedUser.role]}" bundle = "${bundle}"/></span>
                <span><fmt:message key = "dot.label" bundle = "${bundle}"/></span>
            </div>
            <h1><fmt:message key = "service.info.viewing.page.header"
                             bundle = "${bundle}"/></h1>
            <div id = "service-info-viewing" class = "basic-navigation">
                <a href = "${localeChangingURL}">
                    <fmt:message key = "locale.changing.reference.label"
                                 bundle = "${bundle}"/>
                </a>
                <a href = "${mainPageURL}">
                    <fmt:message key = "main.page.reference.label"
                                 bundle = "${bundle}"/>
                </a>
                <a href = "${serviceInfoFetchingPageURL}">
                    <fmt:message key = "service.info.fetching.page.reference.label"
                                 bundle = "${bundle}"/>
                </a>
                <a href = "${logoutURL}">
                    <fmt:message key = "logout.reference.label"
                                 bundle = "${bundle}"/>
                </a>
            </div>
            <dspl:serviceInfoView serviceData = "${serviceInfo}"
                    rowQuantity = "${rowQuantity}"
                    pageURL = "/head-physician/service-info-viewing.jsp"
                    sentPageNumberParam = "pageNumber"
                    specialityMapping = "${specialityMap}"
                    resourceBundle = "${bundle}"
                    tableId = "serviceInfoView"
                    containerId = "pageReferenceContainer">
                <jsp:attribute name = "passportNumberTableLabel" 
                               trim = "true">
                    <fmt:message key = "passport.number.table.label"
                                 bundle = "${bundle}"/>
                </jsp:attribute>
                <jsp:attribute name = "firstNameTableLabel" 
                               trim = "true">
                    <fmt:message key = "first.name.table.label"
                                 bundle = "${bundle}"/>
                </jsp:attribute>
                <jsp:attribute name = "lastNameTableLabel" 
                               trim = "true">
                    <fmt:message key = "last.name.table.label"
                                 bundle = "${bundle}"/>
                </jsp:attribute>
                <jsp:attribute name = "specialityTableLabel" 
                               trim = "true">
                    <fmt:message key = "speciality.table.label"
                                 bundle = "${bundle}"/>
                </jsp:attribute>
                <jsp:attribute name = "patientCountTableLabel" 
                               trim = "true">
                    <fmt:message key = "patient.count.table.label"
                                 bundle = "${bundle}"/>
                </jsp:attribute>
                <jsp:attribute name = "informationLackLabel" 
                               trim = "true">
                    <fmt:message key = "information.lack.label"
                                 bundle = "${bundle}"/>
                </jsp:attribute>
                <jsp:attribute name = "pageNumberLabel" 
                               trim = "true">
                    <fmt:message key = "page.number.label"
                                 bundle = "${bundle}"/>
                </jsp:attribute>
            </dspl:serviceInfoView>
        </body>
    </html>
</jsp:root>
