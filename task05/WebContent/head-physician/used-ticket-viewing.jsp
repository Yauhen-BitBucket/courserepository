<?xml version = "1.0" encoding = "UTF-8" ?>
<jsp:root xmlns:jsp = "http://java.sun.com/JSP/Page" version = "2.0"
          xmlns:c = "http://java.sun.com/jsp/jstl/core"
          xmlns:fmt = "http://java.sun.com/jsp/jstl/fmt"
          xmlns:dspl = "http://www.task05.training.by/PresentationLibrary">
    <jsp:directive.page contentType = "text/html; charset=UTF-8"
                        pageEncoding = "UTF-8"/>
    <jsp:output doctype-root-element = "html"
        doctype-public = "-//W3C//DTD XHTML 1.0 Transitional//EN"
        doctype-system = "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
        omit-xml-declaration = "true" />
    <fmt:setLocale value = "${(not empty locale) ? locale : defaultLocale}"/>
    <fmt:setBundle basename = "by.training.task05.bundle.PageLabels"/>
    <c:url var = "localeChangingURL" 
           value = "/controller">
        <c:param name = "command" 
                 value = "locale-changing-command" />
        <c:param name = "pathPropertyKey" 
                 value = "used.ticket.viewing.page.path" />
        <c:param name = "pageNumber" 
                 value = "${param['pageNumber']}" />
    </c:url>
    <c:url var = "logoutURL" value = "/controller">
        <c:param name = "command" value = "logout-command" />
    </c:url>
    <c:url var = "mainPageURL" value = "/main.jsp" />
    <c:url var = "usedTicketFetchingPageURL"
           value = "/head-physician/used-ticket-fetching.jsp" />
    <c:url var = "imageURL" 
           value = "/controller">
        <c:param name = "command" 
                 value = "image-fetching-command"/>
        <c:param name = "login" 
                 value = "${authenticatedUser.login}"/>
    </c:url>
    <c:url var = "styleURL" value = "${stylePath}"/>
    <html xmlns = "http://www.w3.org/1999/xhtml">
        <head>
            <meta charset = "UTF-8"></meta>
            <title><fmt:message key = "used.ticket.viewing.page.title"/></title>
            <link href = "${styleURL}" rel = "stylesheet" type = "text/css"/>
        </head>
        <body>
            <div class = "user-info">
                <img src = "${imageURL}"/>
                <span><fmt:message key = "login.label"/></span>
                <span><c:out value = "${authenticatedUser.login}" escapeXml = "true"/></span>
                <span><fmt:message key = "role.label"/></span>
                <span><fmt:message key = "${roleMap[authenticatedUser.role]}"/></span>
                <span><fmt:message key = "dot.label"/></span>
            </div>
            <h1><fmt:message key = "used.ticket.viewing.page.header"/></h1>
            <div id = "used-ticket-viewing" class = "basic-navigation">
                <a href = "${localeChangingURL}">
                    <fmt:message key = "locale.changing.reference.label"/>
                </a>
                <a href = "${mainPageURL}">
                    <fmt:message key = "main.page.reference.label"/>
                </a>
                <a href = "${usedTicketFetchingPageURL}">
                    <fmt:message key = "used.ticket.fetching.page.reference.label"/>
                </a>
                <a href = "${logoutURL}">
                    <fmt:message key = "logout.reference.label"/>
                </a>
            </div>
            <dspl:usedTicketView usedTicketData = "${usedTickets}"
                    rowQuantity = "${rowQuantity}"
                    pageURL = "/head-physician/used-ticket-viewing.jsp"
                    sentPageNumberParam = "pageNumber"
                    tableId = "usedTicketView"
                    containerId = "pageReferenceContainer">
                <jsp:attribute name = "passportNumberTableLabel" 
                               trim = "true">
                    <fmt:message key = "passport.number.table.label"/>
                </jsp:attribute>
                <jsp:attribute name = "firstNameTableLabel" 
                               trim = "true">
                    <fmt:message key = "first.name.table.label"/>
                </jsp:attribute>
                <jsp:attribute name = "lastNameTableLabel" 
                               trim = "true">
                    <fmt:message key = "last.name.table.label"/>
                </jsp:attribute>
                <jsp:attribute name = "dateTableLabel" 
                               trim = "true">
                    <fmt:message key = "date.table.label"/>
                </jsp:attribute>
                <jsp:attribute name = "diagnosisTableLabel" 
                               trim = "true">
                    <fmt:message key = "diagnosis.table.label"/>
                </jsp:attribute>
                <jsp:attribute name = "treatmentTableLabel" 
                               trim = "true">
                    <fmt:message key = "treatment.table.label"/>
                </jsp:attribute>
                <jsp:attribute name = "informationLackLabel" 
                               trim = "true">
                    <fmt:message key = "information.lack.label"/>
                </jsp:attribute>
                <jsp:attribute name = "pageNumberLabel" 
                               trim = "true">
                    <fmt:message key = "page.number.label"/>
                </jsp:attribute>
            </dspl:usedTicketView>
        </body>
    </html>
</jsp:root>
