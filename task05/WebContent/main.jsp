<?xml version = "1.0" encoding = "UTF-8" ?>
<jsp:root xmlns:jsp = "http://java.sun.com/JSP/Page" version = "2.0"
          xmlns:c = "http://java.sun.com/jsp/jstl/core"
          xmlns:fmt = "http://java.sun.com/jsp/jstl/fmt">
    <jsp:directive.page contentType = "text/html; charset=UTF-8"
                        pageEncoding = "UTF-8"/>
    <jsp:output doctype-root-element = "html"
        doctype-public = "-//W3C//DTD XHTML 1.0 Transitional//EN"
        doctype-system = "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
        omit-xml-declaration = "true" />
    <fmt:setLocale value = "${(not empty locale) ? locale : defaultLocale}"/>
    <fmt:setBundle basename = "by.training.task05.bundle.PageLabels"/>
    <c:set var = "isPatientRole"
           value = "${authenticatedUser['role'] == patientRole}"
           scope = "page" />
    <c:set var = "isPhysicianRole"
           value = "${authenticatedUser['role'] == physicianRole}"
           scope = "page" />
    <c:set var = "isHeadPhysicianRole"
           value = "${authenticatedUser['role'] == headPhysicianRole}"
           scope = "page" />
    <c:choose>
        <c:when test = "${isPatientRole}">
            <c:url var = "doctorFetchingPageURL"
                   value = "/patient/doctor-fetching.jsp"/>
            <c:url var = "freeTicketFetchingPageURL"
                   value = "/patient/free-ticket-fetching.jsp"/>
            <c:url var = "ownTicketFetchingURL"
                   value = "/controller">
                <c:param name = "command"
                         value = "own-ticket-fetching-command"/>
            </c:url>
            <c:url var = "ticketAssigningPageURL"
                   value = "/patient/ticket-assigning.jsp"/>
            <c:url var = "ticketCancellingPageURL"
                   value = "/patient/ticket-cancelling.jsp"/>
        </c:when>
        <c:when test = "${isPhysicianRole}">
            <c:url var = "ticketAddingPageURL"
                   value = "/physician/ticket-adding.jsp"/>
            <c:url var = "assignedTicketFetchingPageURL"
                   value = "/physician/assigned-ticket-fetching.jsp"/>
            <c:url var = "visitCommittingPageURL"
                   value = "/physician/visit-committing.jsp"/>
        </c:when>
        <c:when test = "${isHeadPhysicianRole}">
            <c:url var = "usedTicketFetchingPageURL"
                   value = "/head-physician/used-ticket-fetching.jsp"/>
            <c:url var = "patientAddingPageURL"
                   value = "/head-physician/patient-adding.jsp"/>
            <c:url var = "serviceInfoFetchingPageURL"
                   value = "/head-physician/service-info-fetching.jsp"/>
            <c:url var = "userFetchingURL"
                   value = "/controller">
                <c:param name = "command"
                         value = "user-fetching-command"/>
            </c:url>
        </c:when>
    </c:choose>
    <c:url var = "localeChangingURL" 
           value = "/controller">
        <c:param name = "command" 
                 value = "locale-changing-command" />
        <c:param name = "pathPropertyKey" 
                 value = "main.page.path" />
    </c:url>
    <c:url var = "logoutURL" 
           value = "/controller">
        <c:param name = "command" 
                 value = "logout-command" />
    </c:url>
    <c:url var = "imageURL" 
           value = "/controller">
        <c:param name = "command" 
                 value = "image-fetching-command"/>
        <c:param name = "login" 
                 value = "${authenticatedUser.login}"/>
    </c:url>
    <c:url var = "imageSettingPageURL" value = "/image-setting.jsp"/>
    <c:url var = "styleURL" value = "${stylePath}"/>
    <html xmlns = "http://www.w3.org/1999/xhtml">
	    <head>
	        <meta charset = "UTF-8"></meta>
	        <title><fmt:message key = "main.page.title"/></title>
	        <link href = "${styleURL}" rel = "stylesheet" type = "text/css"/>
	    </head>
	    <body>
	       <div class = "user-info">
               <img src = "${imageURL}"/>
               <span><fmt:message key = "login.label"/></span>
               <span><c:out value = "${authenticatedUser.login}" escapeXml = "true"/></span>
               <span><fmt:message key = "role.label"/></span>
               <span><fmt:message key = "${roleMap[authenticatedUser.role]}"/></span>
               <span><fmt:message key = "dot.label"/></span>
           </div>
		   <h1><fmt:message key = "main.page.header"/></h1>
		   <div id = "main" class = "basic-navigation">
	           <a href = "${localeChangingURL}">
	               <fmt:message key = "locale.changing.reference.label"/>
	           </a>
	           <a href = "${logoutURL}">
	               <fmt:message key = "logout.reference.label"/>
	           </a>
	       </div>
	       <ul type = "circle" class = "main-navigation">
	           <c:choose>
	               <c:when test = "${isPatientRole}">
	                   <li>
	                       <a href = "${doctorFetchingPageURL}">
			                   <fmt:message key = "doctor.fetching.page.reference.label"/>
			               </a>
	                   </li>
	                   <li>
                           <a href = "${freeTicketFetchingPageURL}">
                               <fmt:message key = "free.ticket.fetching.page.reference.label"/>
                           </a>
                       </li>
                       <li>
                           <a href = "${ownTicketFetchingURL}">
                               <fmt:message key = "own.ticket.fetching.reference.label"/>
                           </a>
                       </li>
                       <li>
                           <a href = "${ticketAssigningPageURL}">
                               <fmt:message key = "ticket.assigning.page.reference.label"/>
                           </a>
                       </li>
                       <li>
                           <a href = "${ticketCancellingPageURL}">
                               <fmt:message key = "ticket.canceling.page.reference.label"/>
                           </a>
                       </li>
	               </c:when>
	               <c:when test = "${isPhysicianRole}">
                       <li>
                           <a href = "${ticketAddingPageURL}">
                               <fmt:message key = "ticket.adding.page.reference.label"/>
                           </a>
                       </li>
                       <li>
                           <a href = "${assignedTicketFetchingPageURL}">
                               <fmt:message key = "assigned.ticket.fetching.page.reference.label"/>
                           </a>
                       </li>
                       <li>
                           <a href = "${visitCommittingPageURL}">
                               <fmt:message key = "visit.committing.page.reference.label"/>
                           </a>
                       </li>
                   </c:when>
                   <c:when test = "${isHeadPhysicianRole}">
                       <li>
                           <a href = "${usedTicketFetchingPageURL}">
                               <fmt:message key = "used.ticket.fetching.page.reference.label"/>
                           </a>
                       </li>
                       <li>
                           <a href = "${patientAddingPageURL}">
                               <fmt:message key = "patient.adding.page.reference.label"/>
                           </a>
                       </li>
                       <li>
                           <a href = "${serviceInfoFetchingPageURL}">
                               <fmt:message key = "service.info.fetching.page.reference.label"/>
                           </a>
                       </li>
                       <li>
                           <a href = "${userFetchingURL}">
                               <fmt:message key = "user.fetching.reference.label"/>
                           </a>
                       </li>
                   </c:when>
	           </c:choose>
	           <li>
	               <a href = "${imageSettingPageURL}">
	                   <fmt:message key = "image.setting.page.reference.label"/>
	               </a>
	           </li>
	       </ul>
	    </body>
    </html>
</jsp:root>
