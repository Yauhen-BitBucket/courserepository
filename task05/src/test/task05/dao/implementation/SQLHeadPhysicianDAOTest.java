/*
 * SQLHeadPhysicianDAOTest.java
 * A TestNG class used to test functionality of the SQLHeadPhysicianDAO class.
 */
package test.task05.dao.implementation;


import static by.training.task05.dao.HeadPhysicianDAO
        .PATIENT_ADDING_FAILURE_VALUE;
import static by.training.task05.dao.HeadPhysicianDAO
        .PATIENT_ADDING_SUCCESS_VALUE;

import static org.testng.Assert.assertEquals;

import by.training.task05.dao.DAOException;
import by.training.task05.dao.HeadPhysicianDAO;

import by.training.task05.dao.implementation.SQLDAOFactory;

import by.training.task05.entity.Person;
import by.training.task05.entity.Physician;
import by.training.task05.entity.Ticket;

import by.training.task05.exception.ApplicationException;

import java.sql.Date;
import java.sql.Time;

import java.util.Collection;
import java.util.LinkedList;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


/**
 * A TestNG class used to test functionality of the
 * <code>SQLHeadPhysicianDAO</code> class.
 * @author Yauhen Sazonau
 * @version 1.0, 07/27/19
 * @since 1.0
 */
public class SQLHeadPhysicianDAOTest {
    /**
     * A ticket identification number. The identifier holds the value of
     * "3030".
     */
    private static final long TICKET_ID_6 = 3030;
    /**
     * A ticket identification number. The identifier holds the value of
     * "3031".
     */
    private static final long TICKET_ID_7 = 3031;
    /**
     * A ticket identification number. The identifier holds the value of
     * "3032".
     */
    private static final long TICKET_ID_8 = 3032;
    /**
     * A ticket identification number. The identifier holds the value of
     * "3033".
     */
    private static final long TICKET_ID_9 = 3033;
    /**
     * A ticket identification number. The identifier holds the value of
     * "3034".
     */
    private static final long TICKET_ID_10 = 3034;
    /**
     * A person. Can't be a <code>null</code> value.
     */
    private Person tempPerson;
    /**
     * Verifies patient adding.
     * @param passportNumber a <code>String</code> value which is a passport
     *                       number
     * @param firstName a <code>String</code> value which is a first name
     * @param lastName a <code>String</code> value which is a last name
     * @param login a <code>String</code> value which is a login
     * @param password a <code>String</code> value which is a password
     * @param expected an <code>int</code> value which is an operation result
     */
    @Test(description = "Verifies patient adding.",
          dataProvider = "patientAddingDataProvider")
    public void testPatientAdding(final String passportNumber,
            final String firstName, final String lastName, final String login,
            final String password, final int expected) {
        int actual;
        SQLDAOFactory factory = SQLDAOFactory.getInstance();
        HeadPhysicianDAO dao = factory.retrieveHeadPhysicianDAO();
        /* The main part of the method. */
        try {
            actual = dao.addPatient(passportNumber, firstName, lastName, login,
                    password);
        } catch (DAOException e) {
            throw new ApplicationException("There are problems with verifying "
                    + "patient adding.", e);
        }
        assertEquals(actual, expected);
    }
    /**
     * Verifies used ticket retrieving.
     * @param startingPoint a <code>Date</code> value which is the starting
     *                      point of a period
     * @param endingPoint a <code>Date</code> value which is the ending
     *                    point of a period
     * @param expected a <code>Collection</code> value which is a collection of
     *                 used tickets
     */
    @Test(description = "Verifies used ticket retrieving.",
          dataProvider = "usedTicketProvider")
    public void testUsedTicketRetrieving(final Date startingPoint,
            final Date endingPoint, final Collection<Ticket> expected) {
        Collection<Ticket> actual;
        SQLDAOFactory factory = SQLDAOFactory.getInstance();
        HeadPhysicianDAO dao = factory.retrieveHeadPhysicianDAO();
        /* The main part of the method. */
        try {
            actual = dao.retrieveUsedTickets(startingPoint, endingPoint);
        } catch (DAOException e) {
            throw new ApplicationException("There are problems with verifying "
                    + "used ticket retrieving.", e);
        }
        assertEquals(actual, expected);
    }
    /**
     * Creates data for testing patient adding.
     * @return data for testing patient adding
     */
    @DataProvider(name = "patientAddingDataProvider")
    public Object[][] providePatientAddingData() {
        return (new Object[][] {
                {"ZZ1111111", "Пласир", "Пласиров", "loginlogin",
                 "passwordpassword", PATIENT_ADDING_SUCCESS_VALUE},
                {"ZZ1111111", "Пласир", "Пласиров", "loginlogin",
                 "passwordpassword", PATIENT_ADDING_FAILURE_VALUE},
                {"ZZ1111112", "Пласир", "Пласиров", "loginlogin",
                 "password", PATIENT_ADDING_FAILURE_VALUE}});
    }
    /**
     * Creates data for testing used ticket retrieving.
     * @return data for testing used ticket retrieving
     */
    @DataProvider(name = "usedTicketProvider")
    public Object[][] provideUsedTickets() {
        Collection<Ticket> emptyCollection = new LinkedList<>();
        Collection<Ticket> tickets = createUsedTickets();
        return (new Object[][] {
            {Date.valueOf("2019-06-17"), Date.valueOf("2019-06-21"), tickets},
            {Date.valueOf("2019-06-16"), Date.valueOf("2019-06-22"), tickets},
            {Date.valueOf("2019-05-15"), Date.valueOf("2019-05-20"),
             emptyCollection},
            {Date.valueOf("2030-05-15"), Date.valueOf("2030-05-20"),
             emptyCollection}});
    }
    /**
     * Fulfills actions needed to be done before testing functionality of the
     * <code>SQLHeadPhysicianDAO</code> class.
     */
    @BeforeTest
    public void configure() {
        tempPerson = new Person();
        /* The main part of the method. */
        tempPerson.setPassportNumber("KK4441122");
        tempPerson.setFirstName("Василий");
        tempPerson.setLastName("Васильев");
    }
    /**
     * Creates a collection of used tickets.
     * @return a collection of used tickets
     */
    private Collection<Ticket> createUsedTickets() {
        Collection<Ticket> collection = new LinkedList<>();
        Physician physician = new Physician();
        Ticket ticket;
        /* The main part of the method. */
        ticket = new Ticket(TICKET_ID_6, physician, tempPerson,
                Date.valueOf("2019-06-17"),
                Time.valueOf("11:30:00"), Time.valueOf("11:40:00"));
        ticket.setDiagnosis("Высокое давление.");
        ticket.setTreatment("Пить таблетки.");
        collection.add(ticket);
        ticket = new Ticket(TICKET_ID_7, physician, tempPerson,
                Date.valueOf("2019-06-18"),
                Time.valueOf("15:30:00"), Time.valueOf("15:40:00"));
        ticket.setDiagnosis("Чешутся глаза.");
        ticket.setTreatment("Направление к афтальмологу.");
        collection.add(ticket);
        ticket = new Ticket(TICKET_ID_8, physician, tempPerson,
                Date.valueOf("2019-06-19"),
                Time.valueOf("08:00:00"), Time.valueOf("08:10:00"));
        ticket.setDiagnosis("Ушиб руки.");
        ticket.setTreatment("Направление к хирургу.");
        collection.add(ticket);
        ticket = new Ticket(TICKET_ID_9, physician, tempPerson,
                Date.valueOf("2019-06-20"),
                Time.valueOf("14:15:00"), Time.valueOf("14:25:00"));
        ticket.setDiagnosis("Боль в животе.");
        ticket.setTreatment("Сдать анализы.");
        collection.add(ticket);
        ticket = new Ticket(TICKET_ID_10, physician, tempPerson,
                Date.valueOf("2019-06-21"),
                Time.valueOf("09:20:00"), Time.valueOf("09:30:00"));
        ticket.setDiagnosis("Боль в сердце.");
        ticket.setTreatment("Сделать кардиограммму.");
        collection.add(ticket);
        return collection;
    }
}
