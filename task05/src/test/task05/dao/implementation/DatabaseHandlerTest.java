/*
 * DatabaseHandlerTest.java
 * A TestNG class used to create a temporary database, tables and data. It's
 * also used to delete the database, tables and data after performing all tests.
 */
package test.task05.dao.implementation;


import static test.task05.dao.implementation.SQLStatementsKeeper
        .DATABASE_CREATION_STMT;
import static test.task05.dao.implementation.SQLStatementsKeeper
        .DATABASE_DROPING_STMT;
import static test.task05.dao.implementation.SQLStatementsKeeper
        .DATABASE_USING_STMT;
import static test.task05.dao.implementation.SQLStatementsKeeper
        .PERMISSION_CREATION_STMT_1;
import static test.task05.dao.implementation.SQLStatementsKeeper
        .PERMISSION_CREATION_STMT_2;
import static test.task05.dao.implementation.SQLStatementsKeeper
        .PERSONAL_DATA_INSERTION_STMT;
import static test.task05.dao.implementation.SQLStatementsKeeper
        .PERSONAL_DATA_TABLE_CREATION_STMT;
import static test.task05.dao.implementation.SQLStatementsKeeper
        .PHYSICIAN_DATA_INSERTION_STMT;
import static test.task05.dao.implementation.SQLStatementsKeeper
        .PHYSICIAN_TABLE_CREATION_STMT;
import static test.task05.dao.implementation.SQLStatementsKeeper
        .SPECIALITY_DATA_INSERTION_STMT;
import static test.task05.dao.implementation.SQLStatementsKeeper
        .SPECIALITY_TABLE_CREATION_STMT;
import static test.task05.dao.implementation.SQLStatementsKeeper
        .TICKET_DATA_INSERTION_STMT;
import static test.task05.dao.implementation.SQLStatementsKeeper
        .TICKET_TABLE_CREATION_STMT;
import static test.task05.dao.implementation.SQLStatementsKeeper
        .USER_CREATION_STMT;
import static test.task05.dao.implementation.SQLStatementsKeeper
        .USER_DATA_INSERTION_STMT;
import static test.task05.dao.implementation.SQLStatementsKeeper
        .USER_DROPING_STMT;
import static test.task05.dao.implementation.SQLStatementsKeeper
        .USER_TABLE_CREATION_STMT;

import by.training.task05.exception.ApplicationException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;


/**
 * A TestNG class used to create a temporary database, tables and data. It's
 * also used to delete the database, tables and data after performing all tests.
 * @author Yauhen Sazonau
 * @version 1.0, 07/25/19
 * @since 1.0
 */
public class DatabaseHandlerTest {
    /**
     * The class name of a driver used to connect to a database. The identifier
     * contains the value of "org.gjt.mm.mysql.Driver".
     */
    private static final String DRIVER_CLASS_NAME = "org.gjt.mm.mysql.Driver";
    /**
     * The location of a used database. The identifier contains the value of
     * "jdbc:mysql://127.0.0.1/?characterEncoding=UTF-8".
     */
    private static final String URL
            = "jdbc:mysql://127.0.0.1/?characterEncoding=UTF-8";
    /**
     * The user name on behalf of which a database is used. The identifier
     * contains the value of "root".
     */
    private static final String USER_NAME = "root";
    /**
     * The user password on behalf of which a database is used. The identifier
     * contains the value of "123456789".
     */
    private static final String PASSWORD = "123456789";
    /**
     * A connection used to communicate with a database. Can't be a
     * <code>null</code> value.
     */
    private Connection connection;
    /**
     * Creates a connection object.
     * @return a connection object
     * @throws ApplicationException if there are problems with creation of a
     *                              connection object
     */
    private Connection createConnection() {
        Connection tempConnection;
        /* Load and register a database driver. */
        try {
            Class.forName(DRIVER_CLASS_NAME);
            /* Establish network connection to a particular database. */
            tempConnection
                    = DriverManager.getConnection(URL, USER_NAME, PASSWORD);
        } catch (ClassNotFoundException e) {
            throw new ApplicationException("Can't find the class for a driver"
                    + DRIVER_CLASS_NAME + ".", e);
        } catch (SQLException e) {
            throw new ApplicationException("Can't create a connection.", e);
        }
        if (tempConnection == null) {
            throw new ApplicationException("The wrowng kind of driver is"
                    + "used: " + DRIVER_CLASS_NAME + ".");
        }
        return tempConnection;
    }
    /**
     * Creates a temporary database, tables and data.
     */
    @BeforeTest(description = "Creates a temporary database, tables and "
                               + "data.")
    public void configure() {
        connection = createConnection();
        Statement statement;
        /* The main part of the method. */
        try {
            statement = connection.createStatement();
            try {
                statement.execute(DATABASE_CREATION_STMT);
                statement.execute(USER_CREATION_STMT);
                statement.execute(PERMISSION_CREATION_STMT_1);
                statement.execute(PERMISSION_CREATION_STMT_2);
                statement.execute(DATABASE_USING_STMT);
                statement.execute(PERSONAL_DATA_TABLE_CREATION_STMT);
                statement.execute(USER_TABLE_CREATION_STMT);
                statement.execute(SPECIALITY_TABLE_CREATION_STMT);
                statement.execute(PHYSICIAN_TABLE_CREATION_STMT);
                statement.execute(TICKET_TABLE_CREATION_STMT);
                statement.execute(PERSONAL_DATA_INSERTION_STMT);
                statement.execute(USER_DATA_INSERTION_STMT);
                statement.execute(SPECIALITY_DATA_INSERTION_STMT);
                statement.execute(PHYSICIAN_DATA_INSERTION_STMT);
                statement.execute(TICKET_DATA_INSERTION_STMT);
            } finally {
                statement.close();
            }
        } catch (SQLException e) {
            throw new ApplicationException("There problems with configuring a "
                    + "test.", e);
        }
    }
    /**
     * Removes a temporary database, tables and data.
     */
    @AfterTest(description = "Removes a temporary database, tables and "
                             + "data.")
    public void cleanUp() {
        Statement statement;
        /* The main part of the method. */
        try {
            statement = connection.createStatement();
            try {
                statement.execute(USER_DROPING_STMT);
                statement.execute(DATABASE_DROPING_STMT);
            } finally {
                statement.close();
            }
            connection.close();
        } catch (SQLException e) {
            throw new ApplicationException("There problems with cleaning up.",
                    e);
        }
    }
}
