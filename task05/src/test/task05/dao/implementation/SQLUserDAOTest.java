/*
 * SQLUserDAOTest.java
 * A TestNG class used to test functionality of the SQLUserDAO class.
 */
package test.task05.dao.implementation;


import static org.testng.Assert.assertEquals;

import by.training.task05.dao.DAOException;
import by.training.task05.dao.UserDAO;

import by.training.task05.dao.implementation.SQLDAOFactory;

import by.training.task05.entity.Role;
import by.training.task05.entity.User;

import by.training.task05.exception.ApplicationException;

import java.util.Optional;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


/**
 * A TestNG class used to test functionality of the <code>SQLUserDAO</code>
 * class.
 * @author Yauhen Sazonau
 * @version 1.0, 07/25/19
 * @since 1.0
 */
public class SQLUserDAOTest {
    /**
     * A user identification number. The identifier holds the value of "1000".
     */
    private static final long USER_ID = 1000;
    /**
     * Verifies user retrieving.
     * @param login a <code>String</code> value which is a login
     * @param password a <code>String</code> value which is a password
     * @param expected an <code>Optional</code> value which is an object
     *                 representing a user
     */
    @Test(description = "Verifies user retrieving.",
          dataProvider = "userProvider")
    public void testUserRetrieving(final String login, final String password,
            final Optional<User> expected) {
        Optional<User> actual;
        SQLDAOFactory factory = SQLDAOFactory.getInstance();
        UserDAO dao = factory.retrieveUserDAO();
        /* The main part of the method. */
        try {
            actual = dao.retrieveUser(login, password);
        } catch (DAOException e) {
            throw new ApplicationException("There are problems with verifying "
                    + "user retrieving.", e);
        }
        assertEquals(actual, expected);
    }
    /**
     * Creates data for testing user retrieving.
     * @return data for testing user retrieving
     */
    @DataProvider(name = "userProvider")
    public Object[][] provideUser() {
        return (new Object[][] {
                {"login1", "password1",
                 Optional.of(new User(false, USER_ID, Role.HEAD_PHYSICIAN,
                                      "login1"))},
                {"login", "password", Optional.empty()}});
    }
}
