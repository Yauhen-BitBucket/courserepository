/*
 * SQLPhysicianDAOTest.java
 * A TestNG class used to test functionality of the SQLPhysicianDAO class.
 */
package test.task05.dao.implementation;


import static by.training.task05.dao.PhysicianDAO.TICKET_ADDING_FAILURE_VALUE;
import static by.training.task05.dao.PhysicianDAO.TICKET_ADDING_SUCCESS_VALUE;
import static by.training.task05.dao.PhysicianDAO
        .VISIT_COMMITING_FAILURE_VALUE;
import static by.training.task05.dao.PhysicianDAO
        .VISIT_COMMITING_SUCCESS_VALUE;

import static org.testng.Assert.assertEquals;

import by.training.task05.dao.DAOException;
import by.training.task05.dao.PhysicianDAO;

import by.training.task05.dao.implementation.SQLDAOFactory;

import by.training.task05.entity.Person;
import by.training.task05.entity.Physician;
import by.training.task05.entity.Ticket;

import by.training.task05.exception.ApplicationException;

import java.sql.Date;
import java.sql.Time;

import java.util.Collection;
import java.util.LinkedList;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


/**
 * A TestNG class used to test functionality of the
 * <code>SQLPhysicianDAO</code> class.
 * @author Yauhen Sazonau
 * @version 1.0, 07/26/19
 * @since 1.0
 */
public class SQLPhysicianDAOTest {
    /**
     * A physician identification number. The identifier holds the value of
     * "1011".
     */
    private static final long PHYSICIAN_ID = 1011;
    /**
     * A ticket identification number. The identifier holds the value of
     * "3010".
     */
    private static final long TICKET_ID_1 = 3010;
    /**
     * A ticket identification number. The identifier holds the value of
     * "3054".
     */
    private static final long TICKET_ID_2 = 3054;
    /**
     * A ticket identification number. The identifier holds the value of
     * "3020".
     */
    private static final long TICKET_ID_3 = 3020;
    /**
     * A ticket identification number. The identifier holds the value of
     * "3021".
     */
    private static final long TICKET_ID_4 = 3021;
    /**
     * A ticket identification number. The identifier holds the value of
     * "3022".
     */
    private static final long TICKET_ID_5 = 3022;
    /**
     * A ticket identification number. The identifier holds the value of
     * "3023".
     */
    private static final long TICKET_ID_6 = 3023;
    /**
     * A ticket identification number. The identifier holds the value of
     * "3024".
     */
    private static final long TICKET_ID_7 = 3024;
    /**
     * A person. Can't be a <code>null</code> value.
     */
    private Person tempPerson;
    /**
     * Verifies ticket adding.
     * @param physicianID a <code>long</code> value which is a physician's
     *                    identification number
     * @param date a <code>Date</code> value which is a desired date
     * @param startingTime a <code>Time</code> value which is a desired
     *                     starting time
     * @param endingTime a <code>Time</code> value which is a desired
     *                     ending time
     * @param expected an <code>int</code> value which is an operation result
     */
    @Test(description = "Verifies ticket adding.",
          dataProvider = "tiketAddingDataProvider")
    public void testTicketAdding(final long physicianID, final Date date,
            final Time startingTime, final Time endingTime,
            final int expected) {
        int actual;
        SQLDAOFactory factory = SQLDAOFactory.getInstance();
        PhysicianDAO dao = factory.retrievePhysicianDAO();
        /* The main part of the method. */
        try {
            actual = dao.addTicket(physicianID, date, startingTime,
                    endingTime);
        } catch (DAOException e) {
            throw new ApplicationException("There are problems with verifying "
                    + "ticket adding.", e);
        }
        assertEquals(actual, expected);
    }
    /**
     * Verifies visit committing.
     * @param ticketID a <code>long</code> value which is a ticket's
     *                 identification number
     * @param physicianID a <code>long</code> value which is the identification
     *                    number of a physician
     * @param diagnosis a <code>String</code> value which is patient's
     *                  diagnosis
     * @param treatment a <code>String</code> value which is patient's
     *                  treatment
     * @param expected an <code>int</code> value which is an operation result
     */
    @Test(description = "Verifies visit committing.",
          dataProvider = "visitCommittingDataProvider")
    public void testVisitCommitting(final long ticketID, final long physicianID,
            final String diagnosis, final String treatment,
            final int expected) {
        int actual;
        SQLDAOFactory factory = SQLDAOFactory.getInstance();
        PhysicianDAO dao = factory.retrievePhysicianDAO();
        /* The main part of the method. */
        try {
            actual = dao.commitVisit(ticketID, physicianID, diagnosis,
                    treatment);
        } catch (DAOException e) {
            throw new ApplicationException("There are problems with verifying "
                    + "visit committing.", e);
        }
        assertEquals(actual, expected);
    }
    /**
     * Verifies assigned ticket retrieving.
     * @param physicianID a <code>long</code> value which is the identification
     *                    number of a physician
     * @param date a <code>Date</code> value which is a desired date
     * @param expected a <code>Collection</code> value which is a collection of
     *                 assigned tickets
     */
    @Test(description = "Verifies assigned ticket retrieving.",
          dataProvider = "assignedTicketProvider")
    public void testAssignedTicketRetrieving(final long physicianID,
            final Date date, final Collection<Ticket> expected) {
        Collection<Ticket> actual;
        SQLDAOFactory factory = SQLDAOFactory.getInstance();
        PhysicianDAO dao = factory.retrievePhysicianDAO();
        /* The main part of the method. */
        try {
            actual = dao.retrieveAssignedTickets(physicianID, date);
        } catch (DAOException e) {
            throw new ApplicationException("There are problems with verifying "
                    + "assigned ticket retrieving.", e);
        }
        assertEquals(actual, expected);
    }
    /**
     * Creates data for testing ticket adding.
     * @return data for testing ticket adding
     */
    @DataProvider(name = "tiketAddingDataProvider")
    public Object[][] provideTicketAddingData() {
        Date date = Date.valueOf("2030-09-03");
        /* The main part of the method. */
        return (new Object[][] {
                {PHYSICIAN_ID, date, Time.valueOf("14:00:00"),
                 Time.valueOf("14:10:00"), TICKET_ADDING_SUCCESS_VALUE},
                {PHYSICIAN_ID, date, Time.valueOf("08:00:00"),
                 Time.valueOf("08:10:00"), TICKET_ADDING_FAILURE_VALUE},
                {PHYSICIAN_ID, date, Time.valueOf("14:05:00"),
                 Time.valueOf("14:30:00"), TICKET_ADDING_FAILURE_VALUE},
                {PHYSICIAN_ID, date, Time.valueOf("14:15:00"),
                     Time.valueOf("14:30:00"), TICKET_ADDING_SUCCESS_VALUE}});
    }
    /**
     * Creates data for testing visit committing.
     * @return data for testing visit committing
     */
    @DataProvider(name = "visitCommittingDataProvider")
    public Object[][] provideVisitCommittingData() {
        return (new Object[][] {
                {TICKET_ID_1, PHYSICIAN_ID, "Насморк", "Пить лекарство.",
                 VISIT_COMMITING_SUCCESS_VALUE},
                {TICKET_ID_2, PHYSICIAN_ID, "Насморк", "Пить лекарство.",
                     VISIT_COMMITING_FAILURE_VALUE}});
    }
    /**
     * Creates data for testing assigned ticket retrieving.
     * @return data for testing assigned ticket retrieving
     */
    @DataProvider(name = "assignedTicketProvider")
    public Object[][] provideAssignedTickets() {
        return (new Object[][] {
            {PHYSICIAN_ID, Date.valueOf("2030-09-04"),
             createAssignedTickets()},
            {PHYSICIAN_ID, Date.valueOf("2030-09-05"), new LinkedList<>()}});
    }
    /**
     * Fulfills actions needed to be done before testing functionality of the
     * <code>SQLPhysicianDAO</code> class.
     */
    @BeforeTest
    public void configure() {
        tempPerson = new Person();
        /* The main part of the method. */
        tempPerson.setPassportNumber("NM8633687");
        tempPerson.setFirstName("Валентин");
        tempPerson.setLastName("Сичкин");
    }
    /**
     * Creates a collection of assigned tickets.
     * @return a collection of assigned tickets
     */
    private Collection<Ticket> createAssignedTickets() {
        Collection<Ticket> collection = new LinkedList<>();
        Date date = Date.valueOf("2030-09-04");
        Physician physician = new Physician();
        Ticket ticket;
        /* The main part of the method. */
        ticket = new Ticket(TICKET_ID_3, physician, tempPerson, date,
                Time.valueOf("10:45:00"), Time.valueOf("10:55:00"));
        collection.add(ticket);
        ticket = new Ticket(TICKET_ID_4, physician, tempPerson, date,
                Time.valueOf("11:00:00"), Time.valueOf("11:10:00"));
        collection.add(ticket);
        ticket = new Ticket(TICKET_ID_5, physician, tempPerson, date,
                Time.valueOf("11:15:00"), Time.valueOf("11:25:00"));
        collection.add(ticket);
        ticket = new Ticket(TICKET_ID_6, physician, tempPerson, date,
                Time.valueOf("11:30:00"), Time.valueOf("11:40:00"));
        collection.add(ticket);
        ticket = new Ticket(TICKET_ID_7, physician, tempPerson, date,
                Time.valueOf("11:45:00"), Time.valueOf("11:55:00"));
        collection.add(ticket);
        return collection;
    }
}
