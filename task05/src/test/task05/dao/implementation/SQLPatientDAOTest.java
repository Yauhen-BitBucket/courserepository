/*
 * SQLPatientDAOTest.java
 * A TestNG class used to test functionality of the SQLPatientDAO class.
 */
package test.task05.dao.implementation;


import static by.training.task05.dao.PatientDAO.ASSIGNMENT_FAILURE_VALUE;
import static by.training.task05.dao.PatientDAO.ASSIGNMENT_SUCCESS_VALUE;
import static by.training.task05.dao.PatientDAO.CANCELLING_FAILURE_VALUE;
import static by.training.task05.dao.PatientDAO.CANCELLING_SUCCESS_VALUE;

import static org.testng.Assert.assertEquals;

import by.training.task05.dao.DAOException;
import by.training.task05.dao.PatientDAO;

import by.training.task05.dao.implementation.SQLDAOFactory;

import by.training.task05.entity.Person;
import by.training.task05.entity.Physician;
import by.training.task05.entity.Speciality;
import by.training.task05.entity.Ticket;

import by.training.task05.exception.ApplicationException;

import java.sql.Date;
import java.sql.Time;

import java.util.Collection;
import java.util.LinkedList;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


/**
 * A TestNG class used to test functionality of the <code>SQLPatientDAO</code>
 * class.
 * @author Yauhen Sazonau
 * @version 1.0, 07/26/19
 * @since 1.0
 */
public class SQLPatientDAOTest {
    /**
     * A patient identification number. The identifier holds the value of
     * "1004".
     */
    private static final long PATIENT_ID_1 = 1004;
    /**
     * A patient identification number. The identifier holds the value of
     * "1100".
     */
    private static final long PATIENT_ID_2 = 1100;
    /**
     * A room number. The identifier holds the value of "100".
     */
    private static final long ROOM_NUMBER = 100;
    /**
     * A ticket identification number. The identifier holds the value of
     * "3054".
     */
    private static final long TICKET_ID_1 = 3054;
    /**
     * A ticket identification number. The identifier holds the value of
     * "3055".
     */
    private static final long TICKET_ID_2 = 3055;
    /**
     * A ticket identification number. The identifier holds the value of
     * "3056".
     */
    private static final long TICKET_ID_3 = 3056;
    /**
     * A ticket identification number. The identifier holds the value of
     * "3057".
     */
    private static final long TICKET_ID_4 = 3057;
    /**
     * A ticket identification number. The identifier holds the value of
     * "3058".
     */
    private static final long TICKET_ID_5 = 3058;
    /**
     * A ticket identification number. The identifier holds the value of
     * "3030".
     */
    private static final long TICKET_ID_6 = 3030;
    /**
     * A ticket identification number. The identifier holds the value of
     * "3031".
     */
    private static final long TICKET_ID_7 = 3031;
    /**
     * A ticket identification number. The identifier holds the value of
     * "3032".
     */
    private static final long TICKET_ID_8 = 3032;
    /**
     * A ticket identification number. The identifier holds the value of
     * "3033".
     */
    private static final long TICKET_ID_9 = 3033;
    /**
     * A ticket identification number. The identifier holds the value of
     * "3034".
     */
    private static final long TICKET_ID_10 = 3034;
    /**
     * A ticket identification number. The identifier holds the value of
     * "3060".
     */
    private static final long TICKET_ID_11 = 3060;
    /**
     * A physician. Can't be a <code>null</code> value.
     */
    private Physician tempPhysician;
    /**
     * Verifies physician retrieving.
     * @param speciality a <code>Speciality</code> value which is a physician
     *                   speciality
     * @param expected a <code>Collection</code> value which is a collection of
     *                 physicians
     */
    @Test(description = "Verifies physician retrieving.",
          dataProvider = "physicianProvider")
    public void testPhysicianRetrieving(final Speciality speciality,
            final Collection<Physician> expected) {
        Collection<Physician> actual;
        SQLDAOFactory factory = SQLDAOFactory.getInstance();
        PatientDAO dao = factory.retrievePatientDAO();
        /* The main part of the method. */
        try {
            actual = dao.retrievePhysicians(speciality);
        } catch (DAOException e) {
            throw new ApplicationException("There are problems with verifying "
                    + "physician retrieving.", e);
        }
        assertEquals(actual, expected);
    }
    /**
     * Verifies free ticket retrieving.
     * @param date a <code>Date</code> value which is a desired date
     * @param speciality a <code>Speciality</code> value which is a physician
     *                   speciality
     * @param expected a <code>Collection</code> value which is a collection of
     *                 free tickets
     */
    @Test(description = "Verifies free ticket retrieving.",
          dataProvider = "freeTiketProvider")
    public void testFreeTicketRetrieving(final Date date,
            final Speciality speciality, final Collection<Ticket> expected) {
        Collection<Ticket> actual;
        SQLDAOFactory factory = SQLDAOFactory.getInstance();
        PatientDAO dao = factory.retrievePatientDAO();
        /* The main part of the method. */
        try {
            actual = dao.retrieveFreeTickets(date, speciality);
        } catch (DAOException e) {
            throw new ApplicationException("There are problems with verifying "
                    + "free ticket retrieving.", e);
        }
        assertEquals(actual, expected);
    }
    /**
     * Verifies own ticket retrieving.
     * @param patientID a <code>long</code> value which is a patient
     *                  identification number
     * @param expected a <code>Collection</code> value which is a collection of
     *                 own tickets
     */
    @Test(description = "Verifies own ticket retrieving.",
          dataProvider = "ownTiketProvider")
    public void testOwnTicketRetrieving(final long patientID,
            final Collection<Ticket> expected) {
        Collection<Ticket> actual;
        SQLDAOFactory factory = SQLDAOFactory.getInstance();
        PatientDAO dao = factory.retrievePatientDAO();
        /* The main part of the method. */
        try {
            actual = dao.retrievePersonalTickets(patientID);
        } catch (DAOException e) {
            throw new ApplicationException("There are problems with verifying "
                    + "own ticket retrieving.", e);
        }
        assertEquals(actual, expected);
    }
    /**
     * Verifies ticket canceling.
     * @param patientID a <code>long</code> value which is a patient's
     *                  identification number
     * @param ticketID a <code>long</code> value which is a ticket's
     *                 identification number
     * @param expected an <code>int</code> value which is an operation result
     */
    @Test(description = "Verifies ticket .",
          dataProvider = "ticketCancelingDataProvider",
          dependsOnMethods = { "testTicketAssigning" })
    public void testTicketCanceling(final long patientID,
            final long ticketID, final int expected) {
        int actual;
        SQLDAOFactory factory = SQLDAOFactory.getInstance();
        PatientDAO dao = factory.retrievePatientDAO();
        /* The main part of the method. */
        try {
            actual = dao.cancellTicket(patientID, ticketID);
        } catch (DAOException e) {
            throw new ApplicationException("There are problems with verifying "
                    + "ticket canceling.", e);
        }
        assertEquals(actual, expected);
    }
    /**
     * Verifies ticket assigning.
     * @param patientID a <code>long</code> value which is a patient's
     *                  identification number
     * @param ticketID a <code>long</code> value which is a ticket's
     *                 identification number
     * @param expected an <code>int</code> value which is an operation result
     */
    @Test(description = "Verifies ticket assigning.",
          dataProvider = "ticketAssigningDataProvider",
          dependsOnMethods = { "testOwnTicketRetrieving" })
    public void testTicketAssigning(final long patientID,
            final long ticketID, final int expected) {
        int actual;
        SQLDAOFactory factory = SQLDAOFactory.getInstance();
        PatientDAO dao = factory.retrievePatientDAO();
        /* The main part of the method. */
        try {
            actual = dao.assignTicket(patientID, ticketID);
        } catch (DAOException e) {
            throw new ApplicationException("There are problems with verifying "
                    + "ticket assigning.", e);
        }
        assertEquals(actual, expected);
    }
    /**
     * Creates data for testing physician retrieving.
     * @return data for testing physician retrieving
     */
    @DataProvider(name = "physicianProvider")
    public Object[][] providePhysicians() {
        return (new Object[][] {
                {Speciality.THERAPEUTIST, createPhysicians()},
                {Speciality.SURGEON, new LinkedList<>()}});
    }
    /**
     * Creates data for testing free ticket retrieving.
     * @return data for testing free ticket retrieving
     */
    @DataProvider(name = "freeTiketProvider")
    public Object[][] provideFreeTickets() {
        return (new Object[][] {
                {Date.valueOf("2030-09-05"), Speciality.THERAPEUTIST,
                 createFreeTickets()},
                {Date.valueOf("2030-09-04"), Speciality.SURGEON,
                 new LinkedList<>()}});
    }
    /**
     * Creates data for testing own ticket retrieving.
     * @return data for testing own ticket retrieving
     */
    @DataProvider(name = "ownTiketProvider")
    public Object[][] provideOwnTickets() {
        return (new Object[][] {
            {PATIENT_ID_1, createOwnTickets()},
            {PATIENT_ID_2, new LinkedList<>()}});
    }
    /**
     * Creates data for testing ticket assigning.
     * @return data for testing ticket assigning
     */
    @DataProvider(name = "ticketAssigningDataProvider")
    public Object[][] provideTiketAssigningData() {
        return (new Object[][] {
            {PATIENT_ID_1, TICKET_ID_11, ASSIGNMENT_SUCCESS_VALUE},
            {PATIENT_ID_1, TICKET_ID_11, ASSIGNMENT_FAILURE_VALUE}});
    }
    /**
     * Creates data for testing ticket canceling.
     * @return data for testing ticket canceling
     */
    @DataProvider(name = "ticketCancelingDataProvider")
    public Object[][] provideTiketCancelingData() {
        return (new Object[][] {
            {PATIENT_ID_1, TICKET_ID_11, CANCELLING_SUCCESS_VALUE},
            {PATIENT_ID_1, TICKET_ID_11, CANCELLING_FAILURE_VALUE}});
    }
    /**
     * Fulfills actions needed to be done before testing functionality of the
     * <code>SQLPatientDAO</code> class.
     */
    @BeforeTest
    public void configure() {
        Time firstTime = Time.valueOf("08:00:00");
        Time secondTime = Time.valueOf("14:00:00");
        Time thirdTime = Time.valueOf("20:00:00");
        /* The main part of the method. */
        tempPhysician = new Physician();
        tempPhysician.setRoomNumber(ROOM_NUMBER);
        tempPhysician.setOddStartingTime(firstTime);
        tempPhysician.setOddEndingTime(secondTime);
        tempPhysician.setEvenStartingTime(secondTime);
        tempPhysician.setEvenEndingTime(thirdTime);
        tempPhysician.setPassportNumber("EE1113377");
        tempPhysician.setFirstName("Андрей");
        tempPhysician.setLastName("Соболев");
        tempPhysician.setSpeciality(Speciality.THERAPEUTIST);
    }
    /**
     * Creates a collection of physicians.
     * @return a collection of physicians
     */
    private Collection<Physician> createPhysicians() {
        Collection<Physician> collection = new LinkedList<>();
        Physician physician;
        Time firstTime = Time.valueOf("08:00:00");
        Time secondTime = Time.valueOf("14:00:00");
        Time thirdTime = Time.valueOf("20:00:00");
        /* The main part of the method. */
        collection.add(tempPhysician);
        /* The second physician creation. */
        physician = new Physician();
        physician.setRoomNumber(ROOM_NUMBER);
        physician.setOddStartingTime(secondTime);
        physician.setOddEndingTime(thirdTime);
        physician.setEvenStartingTime(firstTime);
        physician.setEvenEndingTime(secondTime);
        physician.setPassportNumber("RR9002039");
        physician.setFirstName("Борис");
        physician.setLastName("Борисов");
        physician.setSpeciality(Speciality.THERAPEUTIST);
        collection.add(physician);
        return collection;
    }
    /**
     * Creates a collection of free tickets.
     * @return a collection of free tickets
     */
    private Collection<Ticket> createFreeTickets() {
        Collection<Ticket> collection = new LinkedList<>();
        Date date = Date.valueOf("2030-09-05");
        Person person = new Person();
        Ticket ticket;
        /* The main part of the method. */
        ticket = new Ticket(TICKET_ID_1, tempPhysician, person, date,
                Time.valueOf("10:30:00"), Time.valueOf("10:40:00"));
        collection.add(ticket);
        ticket = new Ticket(TICKET_ID_2, tempPhysician, person, date,
                Time.valueOf("10:45:00"), Time.valueOf("10:55:00"));
        collection.add(ticket);
        ticket = new Ticket(TICKET_ID_3, tempPhysician, person, date,
                Time.valueOf("11:00:00"), Time.valueOf("11:10:00"));
        collection.add(ticket);
        ticket = new Ticket(TICKET_ID_4, tempPhysician, person, date,
                Time.valueOf("11:15:00"), Time.valueOf("11:25:00"));
        collection.add(ticket);
        ticket = new Ticket(TICKET_ID_5, tempPhysician, person, date,
                Time.valueOf("11:30:00"), Time.valueOf("11:40:00"));
        collection.add(ticket);
        return collection;
    }
    /**
     * Creates a collection of own tickets.
     * @return a collection of own tickets
     */
    private Collection<Ticket> createOwnTickets() {
        Collection<Ticket> collection = new LinkedList<>();
        Person person = new Person();
        Ticket ticket;
        /* The main part of the method. */
        ticket = new Ticket(TICKET_ID_6, tempPhysician, person,
                Date.valueOf("2019-06-17"),
                Time.valueOf("11:30:00"), Time.valueOf("11:40:00"));
        ticket.setDiagnosis("Высокое давление.");
        ticket.setTreatment("Пить таблетки.");
        collection.add(ticket);
        ticket = new Ticket(TICKET_ID_7, tempPhysician, person,
                Date.valueOf("2019-06-18"),
                Time.valueOf("15:30:00"), Time.valueOf("15:40:00"));
        ticket.setDiagnosis("Чешутся глаза.");
        ticket.setTreatment("Направление к афтальмологу.");
        collection.add(ticket);
        ticket = new Ticket(TICKET_ID_8, tempPhysician, person,
                Date.valueOf("2019-06-19"),
                Time.valueOf("08:00:00"), Time.valueOf("08:10:00"));
        ticket.setDiagnosis("Ушиб руки.");
        ticket.setTreatment("Направление к хирургу.");
        collection.add(ticket);
        ticket = new Ticket(TICKET_ID_9, tempPhysician, person,
                Date.valueOf("2019-06-20"),
                Time.valueOf("14:15:00"), Time.valueOf("14:25:00"));
        ticket.setDiagnosis("Боль в животе.");
        ticket.setTreatment("Сдать анализы.");
        collection.add(ticket);
        ticket = new Ticket(TICKET_ID_10, tempPhysician, person,
                Date.valueOf("2019-06-21"),
                Time.valueOf("09:20:00"), Time.valueOf("09:30:00"));
        ticket.setDiagnosis("Боль в сердце.");
        ticket.setTreatment("Сделать кардиограммму.");
        collection.add(ticket);
        return collection;
    }
}
