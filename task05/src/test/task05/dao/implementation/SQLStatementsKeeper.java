/*
 * SQLStatementsKeeper.java
 * A container of various SQL statements which will be used by various methods
 * to pass them to a relational database.
 */
package test.task05.dao.implementation;


/**
 * A container of various SQL statements which will be used by various methods
 * to pass them to a relational database.
 * @author Yauhen Sazonau
 * @version 1.0, 07/25/19
 * @since 1.0
 */
class SQLStatementsKeeper {
    /**
     * An SQL statement which is used to create a temporary database. The
     * identifier holds the value of "CREATE DATABASE IF NOT EXISTS
     * `temp_tickets` DEFAULT CHARACTER SET utf8".
     */
    static final String DATABASE_CREATION_STMT
            = "CREATE DATABASE IF NOT EXISTS `temp_tickets` DEFAULT CHARACTER "
              + "SET utf8";
    /**
     * An SQL statement which is used to drop a temporary database. The
     * identifier holds the value of "DROP DATABASE IF EXISTS `temp_tickets`".
     */
    static final String DATABASE_DROPING_STMT
            = "DROP DATABASE IF EXISTS `temp_tickets`";
    /**
     * An SQL statement which is used to select a temporary database. The
     * identifier holds the value of "USE `temp_tickets`".
     */
    static final String DATABASE_USING_STMT
            = "USE `temp_tickets`";
    /**
     * An SQL statement which is used to create a permission on a local host.
     * The identifier holds the value of "GRANT SELECT, INSERT, UPDATE, DELETE
     * ON `temp_tickets`.* TO temp_user@localhost IDENTIFIED BY
     * 'temp_123456789'".
     */
    static final String PERMISSION_CREATION_STMT_1
            = "GRANT SELECT, INSERT, UPDATE, DELETE ON `temp_tickets`.* TO "
              + "temp_user@localhost IDENTIFIED BY 'temp_123456789'";
    /**
     * An SQL statement which is used to create a permission on all hosts.
     * The identifier holds the value of "GRANT SELECT, INSERT, UPDATE, DELETE
     * ON `temp_tickets`.* TO temp_user@'%' IDENTIFIED BY 'temp_123456789'".
     */
    static final String PERMISSION_CREATION_STMT_2
            = "GRANT SELECT, INSERT, UPDATE, DELETE ON `temp_tickets`.* TO "
              + "temp_user@'%' IDENTIFIED BY 'temp_123456789'";
    /**
     * An SQL statement which is used to insert personal data. The identifier
     * holds the value of "INSERT INTO `personaldata` (`PassportNumber`,
     * `FirstName`, `LastName`) VALUES ('AA8882347', 'Алина', 'Григоренко'),
     * ('EE1113377', 'Андрей', 'Соболев'), ('KK4441122', 'Василий',
     * 'Васильев'), ('RR9002039', 'Борис', 'Борисов'), ('NM8633687',
     * 'Валентин', 'Сичкин')".
     */
    static final String PERSONAL_DATA_INSERTION_STMT
            = "INSERT INTO `personaldata` (`PassportNumber`, `FirstName`, "
              + "`LastName`) VALUES ('AA8882347', 'Алина', 'Григоренко'), "
              + "('EE1113377', 'Андрей', 'Соболев'), ('KK4441122', 'Василий', "
              + "'Васильев'), ('RR9002039', 'Борис', 'Борисов'), "
              + "('NM8633687', 'Валентин', 'Сичкин')";
    /**
     * An SQL statement which is used to create a temporary personal data
     * table. The identifier holds the value of "CREATE TABLE IF NOT EXISTS
     * `personaldata` (`PassportNumber` char(9) NOT NULL, `FirstName`
     * varchar(15) NOT NULL, `LastName` varchar(15) NOT NULL, CONSTRAINT
     * `PK_personaldata` PRIMARY KEY (`PassportNumber`)) ENGINE=InnoDB DEFAULT
     * CHARSET=utf8".
     */
    static final String PERSONAL_DATA_TABLE_CREATION_STMT
            = "CREATE TABLE IF NOT EXISTS `personaldata` (`PassportNumber` "
              + "char(9) NOT NULL, `FirstName` varchar(15) NOT NULL, "
              + "`LastName` varchar(15) NOT NULL, CONSTRAINT "
              + "`PK_personaldata` PRIMARY KEY (`PassportNumber`)) "
              + "ENGINE=InnoDB DEFAULT CHARSET=utf8";
    /**
     * An SQL statement which is used to insert physician data. The identifier
     * holds the value of "INSERT INTO `physician` (`PhysicianID`,
     * `Speciality`, `RoomNumber`, `OddStartingTime`, `OddEndingTime`,
     * `EvenStartingTime`, `EvenEndingTime`) VALUES (1001, 2000, 100,
     * '08:00:00', '14:00:00', '14:00:00', '20:00:00'), (1011, 2000, 100,
     * '14:00:00', '20:00:00', '08:00:00', '14:00:00')".
     */
    static final String PHYSICIAN_DATA_INSERTION_STMT
            = "INSERT INTO `physician` (`PhysicianID`, `Speciality`, "
              + "`RoomNumber`, `OddStartingTime`, `OddEndingTime`, "
              + "`EvenStartingTime`, `EvenEndingTime`) VALUES (1001, 2000, "
              + "100, '08:00:00', '14:00:00', '14:00:00', '20:00:00'), "
              + "(1011, 2000, 100, '14:00:00', '20:00:00', '08:00:00', "
              + "'14:00:00')";
    /**
     * An SQL statement which is used to create a temporary physician table.
     * The identifier holds the value of "CREATE TABLE IF NOT EXISTS
     * `physician` (`PhysicianID` int(10) NOT NULL, `Speciality` int(10) NOT
     * NULL, `RoomNumber` int(10) NOT NULL, `OddStartingTime` time NOT NULL,
     * `OddEndingTime` time NOT NULL, `EvenStartingTime` time NOT NULL,
     * `EvenEndingTime` time NOT NULL, KEY `FK_physician_user` (`PhysicianID`),
     * KEY `FK_physician_speciality` (`Speciality`), CONSTRAINT
     * `FK_physician_speciality` FOREIGN KEY (`Speciality`) REFERENCES
     * `speciality` (`SpecialityID`) ON UPDATE CASCADE, CONSTRAINT
     * `FK_physician_user` FOREIGN KEY (`PhysicianID`) REFERENCES `user`
     * (`UserID`) ON UPDATE CASCADE) ENGINE=InnoDB DEFAULT CHARSET=utf8".
     */
    static final String PHYSICIAN_TABLE_CREATION_STMT
            = "CREATE TABLE IF NOT EXISTS `physician` (`PhysicianID` int(10) "
              + "NOT NULL, `Speciality` int(10) NOT NULL, `RoomNumber` "
              + "int(10) NOT NULL, `OddStartingTime` time NOT NULL, "
              + "`OddEndingTime` time NOT NULL, `EvenStartingTime` time NOT "
              + "NULL, `EvenEndingTime` time NOT NULL, KEY "
              + "`FK_physician_user` (`PhysicianID`), KEY "
              + "`FK_physician_speciality` (`Speciality`), CONSTRAINT "
              + "`FK_physician_speciality` FOREIGN KEY (`Speciality`) "
              + "REFERENCES `speciality` (`SpecialityID`) ON UPDATE CASCADE, "
              + "CONSTRAINT `FK_physician_user` FOREIGN KEY (`PhysicianID`) "
              + "REFERENCES `user` (`UserID`) ON UPDATE CASCADE) "
              + "ENGINE=InnoDB DEFAULT CHARSET=utf8";
    /**
     * An SQL statement which is used to insert speciality data. The identifier
     * holds the value of "INSERT INTO `speciality` (`SpecialityID`, `Name`)
     * VALUES (2000, 'THERAPEUTIST'), (2001, 'SURGEON'), (2002,
     * 'OPHTHALMOLOGIST'), (2003, 'UROLOGIST'), (2004, 'OTOLARYNGOLOGIST')".
     */
    static final String SPECIALITY_DATA_INSERTION_STMT
            = "INSERT INTO `speciality` (`SpecialityID`, `Name`) VALUES "
              + "(2000, 'THERAPEUTIST'), (2001, 'SURGEON'), (2002, "
              + "'OPHTHALMOLOGIST'), (2003, 'UROLOGIST'), (2004, "
              + "'OTOLARYNGOLOGIST')";
    /**
     * An SQL statement which is used to create a temporary speciality table.
     * The identifier holds the value of "CREATE TABLE IF NOT EXISTS
     * `speciality` (`SpecialityID` int(10) NOT NULL AUTO_INCREMENT, `Name`
     * enum('THERAPEUTIST','SURGEON','OPHTHALMOLOGIST','UROLOGIST',
     * 'OTOLARYNGOLOGIST') NOT NULL, CONSTRAINT `PK_speciality` PRIMARY KEY
     * (`SpecialityID`)) ENGINE=InnoDB DEFAULT CHARSET=utf8".
     */
    static final String SPECIALITY_TABLE_CREATION_STMT
            = "CREATE TABLE IF NOT EXISTS `speciality` (`SpecialityID` "
              + "int(10) NOT NULL AUTO_INCREMENT, `Name` "
              + "enum('THERAPEUTIST','SURGEON','OPHTHALMOLOGIST','UROLOGIST',"
              + "'OTOLARYNGOLOGIST') NOT NULL, CONSTRAINT `PK_speciality` "
              + "PRIMARY KEY (`SpecialityID`)) ENGINE=InnoDB DEFAULT "
              + "CHARSET=utf8";
    /**
     * An SQL statement which is used to insert ticket data. The identifier
     * holds the value of "INSERT INTO `ticket` (`TicketID`, `Physician`,
     * `Patient`, `Date`, `StartingTime`, `EndingTime`, `Diagnosis`,
     * `Treatment`) VALUES (3054, 1001, NULL, '2030-09-05', '10:30:00',
     * '10:40:00', NULL, NULL), (3055, 1001, NULL, '2030-09-05', '10:45:00',
     * '10:55:00', NULL, NULL), (3056, 1001, NULL, '2030-09-05', '11:00:00',
     * '11:10:00', NULL, NULL), (3057, 1001, NULL, '2030-09-05', '11:15:00',
     * '11:25:00', NULL, NULL), (3058, 1001, NULL, '2030-09-05', '11:30:00',
     * '11:40:00', NULL, NULL), (3030, 1001, 1004, '2019-06-17', '11:30:00',
     * '11:40:00', 'Высокое давление.', 'Пить таблетки.'), (3031, 1001, 1004,
     * '2019-06-18', '15:30:00', '15:40:00', 'Чешутся глаза.',
     * 'Направление к афтальмологу.'), (3032, 1001, 1004, '2019-06-19',
     * '08:00:00', '08:10:00', 'Ушиб руки.', 'Направление к хирургу.'),
     * (3033, 1001, 1004, '2019-06-20', '14:15:00', '14:25:00',
     * 'Боль в животе.', 'Сдать анализы.'), (3034, 1001, 1004, '2019-06-21',
     * '09:20:00', '09:30:00', 'Боль в сердце.', 'Сделать кардиограммму.'),
     * (3060, 1011, NULL, '2030-09-04', '12:00:00', '12:10:00', NULL, NULL),
     * (3010, 1011, 1030, '2019-06-04', '10:45:00', '10:55:00', NULL, NULL),
     * (3020, 1011, 1030, '2030-09-04', '10:45:00', '10:55:00', NULL, NULL),
     * (3021, 1011, 1030, '2030-09-04', '11:00:00', '11:10:00', NULL, NULL),
     * (3022, 1011, 1030, '2030-09-04', '11:15:00', '11:25:00', NULL, NULL),
     * (3023, 1011, 1030, '2030-09-04', '11:30:00', '11:40:00', NULL, NULL),
     * (3024, 1011, 1030, '2030-09-04', '11:45:00', '11:55:00', NULL, NULL)".
     */
    static final String TICKET_DATA_INSERTION_STMT
            = "INSERT INTO `ticket` (`TicketID`, `Physician`, `Patient`,"
              + "`Date`, `StartingTime`, `EndingTime`, `Diagnosis`, "
              + "`Treatment`) VALUES (3054, 1001, NULL, '2030-09-05', "
              + "'10:30:00', '10:40:00', NULL, NULL), (3055, 1001, NULL, "
              + "'2030-09-05', '10:45:00', '10:55:00', NULL, NULL), (3056, "
              + "1001, NULL, '2030-09-05', '11:00:00', '11:10:00', NULL, "
              + "NULL), (3057, 1001, NULL, '2030-09-05', '11:15:00', "
              + "'11:25:00', NULL, NULL), (3058, 1001, NULL, '2030-09-05', "
              + "'11:30:00', '11:40:00', NULL, NULL), (3030, 1001, 1004, "
              + "'2019-06-17', '11:30:00', '11:40:00', 'Высокое давление.', "
              + "'Пить таблетки.'), (3031, 1001, 1004, '2019-06-18', "
              + "'15:30:00', '15:40:00', 'Чешутся глаза.', "
              + "'Направление к афтальмологу.'), (3032, 1001, 1004, "
              + "'2019-06-19', '08:00:00', '08:10:00', 'Ушиб руки.', "
              + "'Направление к хирургу.'), (3033, 1001, 1004, "
              + "'2019-06-20', '14:15:00', '14:25:00', 'Боль в животе.', "
              + "'Сдать анализы.'), (3034, 1001, 1004, '2019-06-21', "
              + "'09:20:00', '09:30:00', 'Боль в сердце.', "
              + "'Сделать кардиограммму.'), (3060, 1011, NULL, "
              + "'2030-09-04', '12:00:00', '12:10:00', NULL, NULL), (3010, "
              + "1011, 1030, '2019-06-04', '10:45:00', '10:55:00', NULL, "
              + "NULL), (3020, 1011, 1030, '2030-09-04', '10:45:00', "
              + "'10:55:00', NULL, NULL), (3021, 1011, 1030, '2030-09-04', "
              + "'11:00:00', '11:10:00', NULL, NULL), (3022, 1011, 1030, "
              + "'2030-09-04', '11:15:00', '11:25:00', NULL, NULL), (3023, "
              + "1011, 1030, '2030-09-04', '11:30:00', '11:40:00', NULL, "
              + "NULL), (3024, 1011, 1030, '2030-09-04', '11:45:00', "
              + "'11:55:00', NULL, NULL)";
    /**
     * An SQL statement which is used to create a temporary ticket table.
     * The identifier holds the value of "CREATE TABLE IF NOT EXISTS `ticket`
     * (`TicketID` int(10) NOT NULL AUTO_INCREMENT, `Physician` int(10) NOT
     * NULL, `Patient` int(10) DEFAULT NULL, `Date` date NOT NULL,
     * `StartingTime` time NOT NULL, `EndingTime` time NOT NULL, `Diagnosis`
     * varchar(300) DEFAULT NULL, `Treatment` varchar(300) DEFAULT NULL,
     * CONSTRAINT `PK_ticket` PRIMARY KEY (`TicketID`), KEY `FK_user_physician`
     * (`Physician`), KEY `FK_user_patient` (`Patient`), CONSTRAINT
     * `FK_user_patient` FOREIGN KEY (`Patient`) REFERENCES `user` (`UserID`)
     * ON UPDATE CASCADE, CONSTRAINT `FK_user_physician` FOREIGN KEY
     * (`Physician`) REFERENCES `user` (`UserID`) ON UPDATE CASCADE)
     * ENGINE=InnoDB DEFAULT CHARSET=utf8".
     */
    static final String TICKET_TABLE_CREATION_STMT
            = "CREATE TABLE IF NOT EXISTS `ticket` (`TicketID` int(10) NOT "
              + "NULL AUTO_INCREMENT, `Physician` int(10) NOT NULL, `Patient` "
              + "int(10) DEFAULT NULL, `Date` date NOT NULL, `StartingTime` "
              + "time NOT NULL, `EndingTime` time NOT NULL, `Diagnosis` "
              + "varchar(300) DEFAULT NULL, `Treatment` varchar(300) DEFAULT "
              + "NULL, CONSTRAINT `PK_ticket` PRIMARY KEY (`TicketID`), KEY "
              + "`FK_user_physician` (`Physician`), KEY `FK_user_patient` "
              + "(`Patient`), CONSTRAINT `FK_user_patient` FOREIGN KEY "
              + "(`Patient`) REFERENCES `user` (`UserID`) ON UPDATE CASCADE, "
              + "CONSTRAINT `FK_user_physician` FOREIGN KEY (`Physician`) "
              + "REFERENCES `user` (`UserID`) ON UPDATE CASCADE) "
              + "ENGINE=InnoDB DEFAULT CHARSET=utf8";
    /**
     * An SQL statement which is used to create a user. The identifier holds
     * the value of "CREATE USER 'temp_user' IDENTIFIED BY 'temp_123456789'".
     */
    static final String USER_CREATION_STMT
            = "CREATE USER 'temp_user' IDENTIFIED BY "
              + "'temp_123456789'";
    /**
     * An SQL statement which is used to insert user data. The identifier
     * holds the value of "INSERT INTO `user` (`UserID`, `Login`, `Password`,
     * `Role`, `PassportNumber`, `Image`) VALUES (1000,
     * '079323A49C300DCD8DFFE1460DEDE02C', '7C6A180B36896A0A8C02787EEAFB0E4C',
     * 'HEAD_PHYSICIAN', 'AA8882347', NULL), (1001,
     * '3B38C223CD0767C5E6F40A7FB86159B4', '6CB75F652A9B52798EB6CF2201057C73',
     * 'PHYSICIAN', 'EE1113377', NULL), (1004,
     * 'F15FF47B76C503839153CB11395AE387', 'DB0EDD04AAAC4506F7EDAB03AC855D56',
     * 'PATIENT', 'KK4441122', NULL), (1011,
     * '60D182B0EA41414452C011C33E5B2A8E', 'C24A542F884E144451F9063B79E7994E',
     * 'PHYSICIAN', 'RR9002039', NULL), (1030,
     * 'AF2B600A623E6034AF79D4C2FC6D8488', '0C75F443030C092D82B67EF876FA4E4E',
     * 'PATIENT', 'NM8633687', NULL)".
     */
    static final String USER_DATA_INSERTION_STMT
            = "INSERT INTO `user` (`UserID`, `Login`, `Password`, `Role`, "
              + "`PassportNumber`, `Image`) VALUES (1000, "
              + "'079323A49C300DCD8DFFE1460DEDE02C', "
              + "'7C6A180B36896A0A8C02787EEAFB0E4C', 'HEAD_PHYSICIAN', "
              + "'AA8882347', NULL), (1001, "
              + "'3B38C223CD0767C5E6F40A7FB86159B4', "
              + "'6CB75F652A9B52798EB6CF2201057C73', 'PHYSICIAN', "
              + "'EE1113377', NULL), (1004, "
              + "'F15FF47B76C503839153CB11395AE387', "
              + "'DB0EDD04AAAC4506F7EDAB03AC855D56', 'PATIENT', 'KK4441122', "
              + "NULL), (1011, '60D182B0EA41414452C011C33E5B2A8E', "
              + "'C24A542F884E144451F9063B79E7994E', 'PHYSICIAN', "
              + "'RR9002039', NULL), (1030, "
              + "'AF2B600A623E6034AF79D4C2FC6D8488', "
              + "'0C75F443030C092D82B67EF876FA4E4E', 'PATIENT', 'NM8633687', "
              + "NULL)";
    /**
     * An SQL statement which is used to drop a user. The identifier holds
     * the value of "DROP USER temp_user".
     */
    static final String USER_DROPING_STMT
            = "DROP USER temp_user";
    /**
     * An SQL statement which is used to create a temporary user table. The
     * identifier holds the value of "CREATE TABLE IF NOT EXISTS `user`
     * (`UserID` int(10) NOT NULL AUTO_INCREMENT, `Login` char(32) NOT NULL,
     * `Password` char(32) NOT NULL, `Role`
     * enum('PATIENT','PHYSICIAN','HEAD_PHYSICIAN') NOT NULL, `PassportNumber`
     * char(9) NOT NULL, `Image` LONGBLOB NULL, `BlockedState` TINYINT(1) NOT
     * NULL DEFAULT '0', CONSTRAINT `PK_user` PRIMARY
     * KEY (`UserID`), CONSTRAINT `CU_login` UNIQUE KEY (`Login`), KEY
     * `FK_user_personaldata` (`PassportNumber`), CONSTRAINT
     * `FK_user_personaldata` FOREIGN KEY (`PassportNumber`) REFERENCES
     * `personaldata` (`PassportNumber`) ON UPDATE CASCADE) ENGINE=InnoDB
     * DEFAULT CHARSET=utf8".
     */
    static final String USER_TABLE_CREATION_STMT
            = "CREATE TABLE IF NOT EXISTS `user` (`UserID` int(10) NOT NULL "
              + "AUTO_INCREMENT, `Login` char(32) NOT NULL, `Password` "
              + "char(32) NOT NULL, `Role` "
              + "enum('PATIENT','PHYSICIAN','HEAD_PHYSICIAN') NOT NULL, "
              + "`PassportNumber` char(9) NOT NULL, `Image` LONGBLOB NULL, "
              + "`BlockedState` TINYINT(1) NOT NULL DEFAULT '0', "
              + "CONSTRAINT `PK_user` PRIMARY KEY (`UserID`), "
              + "CONSTRAINT `CU_login` UNIQUE KEY (`Login`), "
              + "KEY `FK_user_personaldata` (`PassportNumber`), "
              + "CONSTRAINT `FK_user_personaldata` FOREIGN KEY "
              + "(`PassportNumber`) REFERENCES `personaldata` "
              + "(`PassportNumber`) ON UPDATE CASCADE) ENGINE=InnoDB DEFAULT "
              + "CHARSET=utf8";
    /**
     * Constructs an instance of this class.
     */
    private SQLStatementsKeeper() {
        /* The default initialization is sufficient. */
    }
}
