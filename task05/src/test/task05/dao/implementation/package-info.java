/**
 * Contains TestNG classes for verifying functionality of the DAO
 * implementation package.
 * @author Yauhen Sazonau
 * @since 1.0
 */
package test.task05.dao.implementation;
