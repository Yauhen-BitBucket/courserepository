/* UserDAO.java
 * A contract which user DAO implementations should comply with.
 */
package by.training.task05.dao;


import by.training.task05.entity.User;

import java.io.InputStream;

import java.util.Optional;


/**
 * A contract which user DAO implementations should comply with.
 * @author Yauhen Sazonau
 * @version 1.0, 06/21/19
 * @since 1.0
 */
public interface UserDAO {
    /**
     * A constant indicating that an image wasn't set. The identifier holds the
     * value of "0".
     */
    int IMAGE_SETTING_FAILURE_VALUE = 0;
    /**
     * A constant indicating that an image was successfully set. The identifier
     * holds the value of "1".
     */
    int IMAGE_SETTING_SUCCESS_VALUE = 1;
    /**
     * Retrieves a user of this program complex from a data source using the
     * specified login and password.
     * @param login a <code>String</code> value which is a login
     * @param password a <code>String</code> value which is a password
     * @return an <code>Optional</code> object containing the desired user or
     *         an empty <code>Optional</code> object
     * @throws DAOException if there are problems with retrieving a user of
     *                      this program complex from a data source
     */
    Optional<User> retrieveUser(String login, String password)
            throws DAOException;
    /**
     * Retrieves an image from a data source using a specified user login.
     * @param login a <code>String</code> value which is a user login
     * @return an <code>Optional</code> object containing the desired image or
     *         an empty <code>Optional</code> object
     * @throws DAOException if there are problems with retrieving an image from
     *                      a data source
     */
    Optional<InputStream> retrieveImage(String login) throws DAOException;
    /**
     * Retrieves a passport number from a data source using a specified user
     * login.
     * @param login a <code>String</code> value which is a login
     * @return an <code>Optional</code> object containing the passport number
     *         or an empty <code>Optional</code> object
     * @throws DAOException if there are problems with retrieving a passport
     *                      number from a data source
     */
    Optional<String> retrievePassportNumber(String login) throws DAOException;
    /**
     * Sets an image to a user with a specified login.
     * @param login a <code>String</code> value which is a user login
     * @param image an <code>InputStream</code> value which is an image
     * @return <code>IMAGE_SETTING_SUCCESS_VALUE</code> if an image was
     *         successfully set, <code>IMAGE_SETTING_FAILURE_VALUE</code>
     *         otherwise
     * @throws DAOException if there are problems with setting an image
     * @see by.training.task05.dao.UserDAO#IMAGE_SETTING_FAILURE_VALUE
     * @see by.training.task05.dao.UserDAO#IMAGE_SETTING_SUCCESS_VALUE
     */
    int setImage(String login, InputStream image) throws DAOException;
}
