/* PhysicianDAO.java
 * A contract which physician DAO implementations should comply with.
 */
package by.training.task05.dao;


import by.training.task05.entity.Ticket;

import java.sql.Date;
import java.sql.Time;

import java.util.Collection;


/**
 * A contract which physician DAO implementations should comply with.
 * @author Yauhen Sazonau
 * @version 1.0, 06/23/19
 * @since 1.0
 */
public interface PhysicianDAO {
    /**
     * A constant indicating that a ticket wasn't added. The identifier holds
     * the value of "0".
     */
    int TICKET_ADDING_FAILURE_VALUE = 0;
    /**
     * A constant indicating that a ticket was successfully added. The
     * identifier holds the value of "1".
     */
    int TICKET_ADDING_SUCCESS_VALUE = 1;
    /**
     * A constant indicating that a visit wasn't committed. The identifier
     * holds the value of "0".
     */
    int VISIT_COMMITING_FAILURE_VALUE = 0;
    /**
     * A constant indicating that a visit was successfully committed. The
     * identifier holds the value of "1".
     */
    int VISIT_COMMITING_SUCCESS_VALUE = 1;
    /**
     * Adds a new ticket to a doctor on a date at a time.
     * @param physicianID a <code>long</code> value which is a physician's
     *                  identification number
     * @param date a <code>Date</code> value which is a desired date
     * @param startingTime a <code>Time</code> value which is a desired
     *                     starting time
     * @param endingTime a <code>Time</code> value which is a desired
     *                     ending time
     * @return <code>TICKET_ADDING_SUCCESS_VALUE</code> if a ticket was
     *         successfully added, <code>TICKET_ADDING_FAILURE_VALUE</code>
     *         otherwise
     * @throws DAOException if there are problems with adding a new ticket to
     *                      a doctor on a date at a time
     * @see by.training.task05.dao.PhysicianDAO#TICKET_ADDING_FAILURE_VALUE
     * @see by.training.task05.dao.PhysicianDAO#TICKET_ADDING_SUCCESS_VALUE
     */
    int addTicket(long physicianID, Date date, Time startingTime,
                 Time endingTime) throws DAOException;
    /**
     * Retrieves assigned tickets to a doctor on a date.
     * @param physicianID a <code>long</code> value which is a physician's
     *                  identification number
     * @param date a <code>Date</code> value which is a desired date
     * @return assigned tickets to a doctor on a date
     * @throws DAOException if there are problems with retrieving assigned
     *                      tickets to a doctor on a date
     */
    Collection<Ticket> retrieveAssignedTickets(long physicianID, Date date)
            throws DAOException;
    /**
     * Commits a visit of a patient.
     * @param ticketID a <code>long</code> value which is a ticket's
     *                 identification number
     * @param physicianID a <code>long</code> value which is the identification
     *                    number of a physician
     * @param diagnosis a <code>String</code> value which is patient's
     *                  diagnosis
     * @param treatment a <code>String</code> value which is patient's
     *                  treatment
     * @return <code>VISIT_COMMITING_SUCCESS_VALUE</code> if a visit was
     *         successfully committed,
     *         <code>VISIT_COMMITING_FAILURE_VALUE</code> otherwise
     * @throws DAOException if there are problems with committing a visit
     * @see by.training.task05.dao.PhysicianDAO#VISIT_COMMITING_FAILURE_VALUE
     * @see by.training.task05.dao.PhysicianDAO#VISIT_COMMITING_SUCCESS_VALUE
     */
    int commitVisit(long ticketID, long physicianID, String diagnosis,
            String treatment) throws DAOException;
}
