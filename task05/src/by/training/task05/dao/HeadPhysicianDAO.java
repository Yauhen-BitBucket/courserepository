/* HeadPhysicianDAO.java
 * A contract which head physician DAO implementations should comply with.
 */
package by.training.task05.dao;


import by.training.task05.entity.Person;
import by.training.task05.entity.Physician;
import by.training.task05.entity.Ticket;

import java.sql.Date;

import java.util.Collection;
import java.util.Map;


/**
 * A contract which head physician DAO implementations should comply with.
 * @author Yauhen Sazonau
 * @version 1.0, 06/25/19
 * @since 1.0
 */
public interface HeadPhysicianDAO {
    /**
     * A constant indicating that a blocked state wasn't set. The identifier
     * holds the value of "0".
     */
    int BLCOOKED_STATE_SETING_FAILURE_VALUE = 0;
    /**
     * A constant indicating that a blocked state was successfully set. The
     * identifier holds the value of "1".
     */
    int BLCOOKED_STATE_SETING_SUCCESS_VALUE = 1;
    /**
     * A constant indicating that a patient wasn't added. The identifier holds
     * the value of "0".
     */
    int PATIENT_ADDING_FAILURE_VALUE = 0;
    /**
     * A constant indicating that a patient was successfully added. The
     * identifier holds the value of "1".
     */
    int PATIENT_ADDING_SUCCESS_VALUE = 1;
    /**
     * Sets the blocked state of a user with a specified passport number to a
     * specified value.
     * @param passportNumber a <code>String</code> value which is a passport
     *                       number
     * @param blockedState a <code>boolean</code> value which is a blocked
     *                     state to be set
     * @return <code>BLCOOKED_STATE_SETING_SUCCESS_VALUE</code> if a blocked
     *         state was successfully set,
     *         <code>BLCOOKED_STATE_SETING_FAILURE_VALUE</code> otherwise
     * @throws DAOException if there are problems with setting a blocked state
     * @see by.training.task05.dao.HeadPhysicianDAO
     *      #BLCOOKED_STATE_SETING_FAILURE_VALUE
     * @see by.training.task05.dao.HeadPhysicianDAO
     *      #BLCOOKED_STATE_SETING_SUCCESS_VALUE
     */
    int setBlockedState(String passportNumber,
            boolean blockedState) throws DAOException;
    /**
     * Retrieves used tickets. The date of each ticket falls into a specified
     * range. This time period should precede the current date.
     * @param startingPoint a <code>Date</code> value which is the starting
     *                      point of a period
     * @param endingPoint a <code>Date</code> value which is the ending
     *                    point of a period
     * @return used tickets. If there are no such tickets, an empty
     *         <code>Collection</code> is returned.
     * @throws DAOException if there are problems with retrieving used tickets
     */
    Collection<Ticket> retrieveUsedTickets(Date startingPoint,
            Date endingPoint) throws DAOException;
    /**
     * Retrieves all users which are not head physicians.
     * @return all users which are not head physicians. If there are no such
     *         users, an empty <code>Collection</code> is returned.
     * @throws DAOException if there are problems with retrieving all users
     *                      which are not head physicians
     */
    Collection<Person> retrieveUsers() throws DAOException;
    /**
     * Adds a new patient to the system.
     * @param passportNumber a <code>String</code> value which is a passport
     *                       number
     * @param firstName a <code>String</code> value which is a first name
     * @param lastName a <code>String</code> value which is a last name
     * @param login a <code>String</code> value which is a login
     * @param password a <code>String</code> value which is a password
     * @return <code>PATIENT_ADDING_SUCCESS_VALUE</code> if a patient was
     *         successfully added, <code>PATIENT_ADDING_FAILURE_VALUE</code>
     *         otherwise
     * @throws DAOException if there are problems with adding a new patient to
     *                      to the system
     * @see by.training.task05.dao.HeadPhysicianDAO
     *      #PATIENT_ADDING_FAILURE_VALUE
     * @see by.training.task05.dao.HeadPhysicianDAO
     *      #PATIENT_ADDING_SUCCESS_VALUE
     */
    int addPatient(String passportNumber, String firstName, String lastName,
                   String login, String password) throws DAOException;
    /**
     * Retrieves information about how many patients were served by each
     * physician in a specified year and month.
     * @param year an <code>int</code> value which is a desired year
     * @param month an <code>int</code> value which is a desired month.
     *              '1' corresponds to January, '12' corresponds to December.
     * @return a map with keys of the <code>Physician</code> type and values
     *         of the <code>Integer</code> type
     * @throws DAOException if there are problems with retrieving information
     *                      about how many patients were served by each
     *                      physician in a specified year and month
     * @see by.training.task05.entity.Physician
     */
    Map<Physician, Integer> retrieveServiceInfo(int year, int month)
            throws DAOException;
}
