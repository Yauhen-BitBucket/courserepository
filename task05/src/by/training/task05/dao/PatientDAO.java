/* PatientDAO.java
 * A contract which patient DAO implementations should comply with.
 */
package by.training.task05.dao;


import by.training.task05.entity.Physician;
import by.training.task05.entity.Speciality;
import by.training.task05.entity.Ticket;

import java.sql.Date;

import java.util.Collection;


/**
 * A contract which patient DAO implementations should comply with.
 * @author Yauhen Sazonau
 * @version 1.0, 06/21/19
 * @since 1.0
 */
public interface PatientDAO {
    /**
     * A constant indicating that a ticket wasn't assigned to a patient. The
     * identifier holds the value of "0".
     */
    int ASSIGNMENT_FAILURE_VALUE = 0;
    /**
     * A constant indicating that a ticket was successfully assigned to a
     * patient. The identifier holds the value of "1".
     */
    int ASSIGNMENT_SUCCESS_VALUE = 1;
    /**
     * A constant indicating that a ticket of a patient wasn't cancelled. The
     * identifier holds the value of "0".
     */
    int CANCELLING_FAILURE_VALUE = 0;
    /**
     * A constant indicating that a ticket of a patient was successfully
     * cancelled. The identifier holds the value of "1".
     */
    int CANCELLING_SUCCESS_VALUE = 1;
    /**
     * Retrieves all physicians which have the specified specialty.
     * @param speciality a <code>Speciality</code> value which is a speciality
     *                   of retrieved physicians
     * @return all physicians which have the specified specialty. If there are
     *         no such physicians, an empty <code>Collection</code> is
     *         returned.
     * @throws DAOException if there are problems with retrieving all
     *                      physicians which have the specified specialty
     */
    Collection<Physician> retrievePhysicians(Speciality speciality)
            throws DAOException;
    /**
     * Retrieves free tickets to all physicians which have the specified
     * speciality on the specified date.
     * @param date a <code>Date</code> value which is a desired surgery date
     * @param speciality a <code>Speciality</code> value which is a desired
     *                   speciality
     * @return free tickets to all physicians which have the specified
     *         speciality on the specified date. If there are
     *         no such tickets, an empty <code>Collection</code> is returned.
     * @throws DAOException if there are problems with retrieving free tickets
     *                      to all physicians which have the specified
     *                      speciality on the specified date
     */
    Collection<Ticket> retrieveFreeTickets(Date date, Speciality speciality)
            throws DAOException;
    /**
     * Retrieves tickets which have been assigned to a particular patient.
     * @param patientID a <code>long</code> value which is a patient's
     *                  identification number
     * @return tickets which have been assigned to a particular patient. If
     *         there are no such tickets, an empty <code>Collection</code> is
     *         returned.
     * @throws DAOException if there are problems with retrieving tickets which
     *                      have been assigned to a particular patient
     */
    Collection<Ticket> retrievePersonalTickets(long patientID)
            throws DAOException;
    /**
     * Assigns a ticket to a patient.
     * @param patientID a <code>long</code> value which is a patient's
     *                  identification number
     * @param ticketID a <code>long</code> value which is a ticket's
     *                 identification number
     * @return <code>ASSIGNMENT_SUCCESS_VALUE</code> if a ticket was
     *         successfully assigned, <code>ASSIGNMENT_FAILURE_VALUE</code>
     *         otherwise
     * @throws DAOException if there are problems with assignment of a ticket
     *                      to a patient
     * @see by.training.task05.dao.PatientDAO#ASSIGNMENT_FAILURE_VALUE
     * @see by.training.task05.dao.PatientDAO#ASSIGNMENT_SUCCESS_VALUE
     */
    int assignTicket(long patientID, long ticketID) throws DAOException;
    /**
     * Cancels a ticket of a patient.
     * @param patientID a <code>long</code> value which is a patient's
     *                  identification number
     * @param ticketID a <code>long</code> value which is a ticket's
     *                 identification number
     * @return <code>CANCELLING_SUCCESS_VALUE</code> if a ticket was
     *         successfully cancelled, <code>CANCELLING_FAILURE_VALUE</code>
     *         otherwise
     * @throws DAOException if there are problems with canceling a ticket
     *                      of a patient
     * @see by.training.task05.dao.PatientDAO#CANCELLING_FAILURE_VALUE
     * @see by.training.task05.dao.PatientDAO#CANCELLING_SUCCESS_VALUE
     */
    int cancellTicket(long patientID, long ticketID) throws DAOException;
}
