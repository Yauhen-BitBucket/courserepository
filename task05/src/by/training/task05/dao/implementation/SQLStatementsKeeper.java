/*
 * SQLStatementsKeeper.java
 * A container of various SQL statements which will be used by various methods
 * to pass them to a relational database.
 */
package by.training.task05.dao.implementation;


/**
 * A container of various SQL statements which will be used by various methods
 * to pass them to a relational database.
 * @author Yauhen Sazonau
 * @version 1.0, 06/21/19
 * @since 1.0
 */
class SQLStatementsKeeper {
    /**
     * An SQL statement which is used to retrieve assigned tickets to a doctor
     * on a date. The identifier holds the value of "SELECT personaldata.*,
     * TicketID, ticket.Date, StartingTime, EndingTime FROM ticket INNER JOIN
     * personaldata ON personaldata.PassportNumber = (SELECT PassportNumber
     * FROM user WHERE user.UserID = ticket.Patient) WHERE
     * (ticket.Physician = ?) AND (ticket.Date = ?) AND (ticket.Patient IS NOT
     * NULL)".
     */
    static final String ASSIGNED_TICKET_RETRIEVING_STMT
            = "SELECT personaldata.*, TicketID, ticket.Date, StartingTime, "
              + "EndingTime FROM ticket INNER JOIN personaldata ON "
              + "personaldata.PassportNumber = (SELECT PassportNumber FROM "
              + "user WHERE user.UserID = ticket.Patient) "
              + "WHERE (ticket.Physician = ?) AND (ticket.Date = ?) AND "
              + "(ticket.Patient IS NOT NULL)";
    /**
     * An SQL statement which is used to set a blocked state. The identifier
     * holds the value of "UPDATE user SET blockedState = ? WHERE
     * (passportNumber = ?) AND (passportNumber NOT IN(SELECT passportNumber
     * FROM (SELECT * FROM user) temp_user WHERE role = 'HEAD_PHYSICIAN'))".
     */
    static final String BLOCKED_STATE_SETTING_STMT
            = "UPDATE user SET blockedState = ? WHERE (passportNumber = ?) "
              + "AND (passportNumber NOT IN(SELECT passportNumber FROM "
              + "(SELECT * FROM user) temp_user WHERE role = "
              + "'HEAD_PHYSICIAN'))";
    /**
     * An SQL statement which is used to retrieve free tickets to all
     * physicians which have the specified speciality on the specified date.
     * The identifier holds the value of "SELECT RoomNumber, OddStartingTime,
     * OddEndingTime, EvenStartingTime, EvenEndingTime, personaldata.*,
     * speciality.Name, TicketID, ticket.Date, StartingTime, EndingTime FROM
     * ticket INNER JOIN physician ON ticket.Physician = physician.PhysicianID
     * INNER JOIN speciality ON physician.Speciality = speciality.SpecialityID
     * INNER JOIN personaldata ON personaldata.PassportNumber = (SELECT
     * PassportNumber FROM user WHERE user.UserID =
     * ticket.Physician) WHERE (ticket.Date = ?) AND (speciality.Name = ?)
     * AND (Patient IS NULL) AND (TIMESTAMPDIFF(SECOND, SYSDATE(),
     * TIMESTAMP(ticket.Date, ticket.StartingTime)) > 0)".
     */
    static final String FREE_TICKET_RETRIEVING_STMT
            = "SELECT RoomNumber, OddStartingTime, OddEndingTime, "
              + "EvenStartingTime, EvenEndingTime, personaldata.*, "
              + "speciality.Name, TicketID, ticket.Date, StartingTime, "
              + "EndingTime FROM ticket INNER JOIN physician ON "
              + "ticket.Physician = physician.PhysicianID INNER JOIN "
              + "speciality ON physician.Speciality = speciality.SpecialityID "
              + "INNER JOIN personaldata ON personaldata.PassportNumber = "
              + "(SELECT PassportNumber FROM user WHERE "
              + "user.UserID = ticket.Physician) WHERE (ticket.Date "
              + "= ?) AND (speciality.Name = ?) AND (Patient IS NULL) AND"
              + "(TIMESTAMPDIFF(SECOND, SYSDATE(), TIMESTAMP(ticket.Date, "
              + "ticket.StartingTime)) > 0)";
    /**
     * An SQL statement which is used to retrieve an image. The identifier
     * holds the value of "SELECT Image FROM tickets.user WHERE (Login = ?)
     * AND (Image IS NOT NULL)".
     */
    static final String IMAGE_RETRIEVING_STMT
            = "SELECT Image FROM tickets.user WHERE (Login = ?) AND (Image "
              + "IS NOT NULL)";
    /**
     * An SQL statement which is used to set an image. The identifier holds the
     * value of "UPDATE tickets.user SET Image = ? WHERE Login = ?".
     */
    static final String IMAGE_SETTING_STMT
            = "UPDATE tickets.user SET Image = ? WHERE Login = ?";
    /**
     * An SQL statement which is used to retrieve tickets which have been
     * assigned to a particular patient. The identifier holds the value of
     * "SELECT RoomNumber, OddStartingTime, OddEndingTime, EvenStartingTime,
     * EvenEndingTime, personaldata.*, speciality.Name, TicketID, ticket.Date,
     * StartingTime, EndingTime, Diagnosis, Treatment FROM ticket INNER JOIN
     * physician ON ticket.Physician = physician.PhysicianID INNER JOIN
     * speciality ON physician.Speciality = speciality.SpecialityID INNER JOIN
     * personaldata ON personaldata.PassportNumber = (SELECT PassportNumber
     * FROM user WHERE user.UserID = ticket.Physician) WHERE
     * Patient = ?".
     */
    static final String OWN_TICKET_RETRIEVING_STMT
            = "SELECT RoomNumber, OddStartingTime, OddEndingTime, "
              + "EvenStartingTime, EvenEndingTime, personaldata.*, "
              + "speciality.Name, TicketID, ticket.Date, StartingTime, "
              + "EndingTime, Diagnosis, Treatment FROM ticket INNER JOIN "
              + "physician ON ticket.Physician = physician.PhysicianID INNER "
              + "JOIN speciality ON physician.Speciality = "
              + "speciality.SpecialityID INNER JOIN personaldata ON "
              + "personaldata.PassportNumber = (SELECT PassportNumber FROM "
              + "user WHERE user.UserID = ticket.Physician) "
              + "WHERE Patient = ?";
    /**
     * An SQL statement which is used to retrieve a passport number. The
     * identifier holds the value of "SELECT passportNumber FROM user WHERE
     * login = ?".
     */
    static final String PASSPORT_NUMBER_RETRIEVING_STMT
            = "SELECT passportNumber FROM user WHERE login = ?";
    /**
     * An SQL statement which is used to a patient. The identifier holds the
     * value of "INSERT INTO user(Login, user.Password, Role,
     * PassportNumber) SELECT ?, ?, 'PATIENT', ? FROM user users WHERE
     * (UserID = 1000) AND (NOT EXISTS(SELECT Login FROM user pseudonym
     * WHERE (Login = ?))) AND (NOT EXISTS(SELECT Login FROM user label
     * WHERE (PassportNumber = ?) AND (Role = 'PATIENT')))".
     */
    static final String PATIENT_ADDING_STMT
            = "INSERT INTO user(Login, user.Password, Role, "
              + "PassportNumber) SELECT ?, ?, 'PATIENT', ? FROM user "
              + "users WHERE (UserID = 1000) AND (NOT EXISTS(SELECT Login "
              + "FROM user pseudonym WHERE (Login = ?))) AND (NOT "
              + "EXISTS(SELECT Login FROM user label WHERE "
              + "(PassportNumber = ?) AND (Role = 'PATIENT')))";
    /**
     * An SQL statement which is used to add personal data of a patient. The
     * identifier holds the value of "INSERT INTO personaldata(PassportNumber,
     * FirstName, LastName) SELECT ?, ?, ? FROM user WHERE (UserId =
     * 1000) AND (NOT EXISTS(SELECT PassportNumber FROM personaldata
     * passportData WHERE PassportNumber = ?))".
     */
    static final String PERSONAL_DATA_ADDING_STMT
            = "INSERT INTO personaldata(PassportNumber, FirstName, LastName) "
               + "SELECT ?, ?, ? FROM user WHERE (UserId = 1000) AND "
               + "(NOT EXISTS(SELECT PassportNumber FROM personaldata "
               + "passportData WHERE PassportNumber = ?))";
    /**
     * An SQL statement which is used to retrieve all physicians which have the
     * specified specialty. The identifier holds the value of "SELECT
     * RoomNumber, OddStartingTime, OddEndingTime, EvenStartingTime,
     * EvenEndingTime, personaldata.*, speciality.Name FROM physician INNER
     * JOIN personaldata ON personaldata.PassportNumber = (SELECT
     * PassportNumber FROM user WHERE user.UserID =
     * physician.PhysicianID) INNER JOIN speciality ON physician.Speciality =
     * speciality.SpecialityID WHERE speciality.Name = ?".
     */
    static final String PHYSICIAN_RETRIEVING_STMT
            = "SELECT RoomNumber, OddStartingTime, OddEndingTime, "
              + "EvenStartingTime, EvenEndingTime, personaldata.*, "
              + "speciality.Name FROM physician INNER JOIN personaldata ON "
              + "personaldata.PassportNumber = (SELECT PassportNumber FROM "
              + "user WHERE user.UserID = "
              + "physician.PhysicianID) INNER JOIN speciality ON "
              + "physician.Speciality = speciality.SpecialityID "
              + "WHERE speciality.Name = ?";
    /**
     * An SQL statement which is used to find out how many patients were
     * served by each physician in a specified year and month. The
     * identifier holds the value of "SELECT personaldata.*, speciality.Name,
     * RoomNumber, OddStartingTime, OddEndingTime, EvenStartingTime,
     * EvenEndingTime, SUM(IF(ticket.Date, 1, 0)) AS PatientCount FROM
     * physician INNER JOIN personaldata ON PassportNumber = (SELECT
     * PassportNumber FROM user WHERE UserID = physician.PhysicianID) INNER
     * JOIN speciality ON physician.Speciality = speciality.SpecialityID LEFT
     * JOIN ticket ON (physician.PhysicianID = ticket.Physician) AND
     * (YEAR(ticket.Date) = ?) AND (MONTH(ticket.Date) = ?) AND (Diagnosis IS
     * NOT NULL) GROUP BY physician.PhysicianID".
     */
    static final String SERVICE_INFO_RETRIEVING_STMT
            = "SELECT personaldata.*, speciality.Name, RoomNumber, "
              + "OddStartingTime, OddEndingTime, EvenStartingTime, "
              + "EvenEndingTime, SUM(IF(ticket.Date, 1, 0)) AS PatientCount "
              + "FROM physician INNER JOIN personaldata ON PassportNumber = "
              + "(SELECT PassportNumber FROM user WHERE UserID = "
              + "physician.PhysicianID) INNER JOIN speciality ON "
              + "physician.Speciality = speciality.SpecialityID LEFT JOIN "
              + "ticket ON (physician.PhysicianID = ticket.Physician) AND "
              + "(YEAR(ticket.Date) = ?) AND (MONTH(ticket.Date) = ?) AND "
              + "(Diagnosis IS NOT NULL) GROUP BY physician.PhysicianID";
    /**
     * An SQL statement which is used to add a new ticket to a doctor on a date
     * at a time. The identifier holds the value of
     * "INSERT INTO ticket(ticket.Physician, ticket.Date, StartingTime,
     * EndingTime) SELECT ?, ?, ?, ? FROM user WHERE
     * (user.UserID = ?) AND (((DAYOFMONTH(?) MOD 2 != 0) AND
     * (TIME_TO_SEC(TIMEDIFF(?, (SELECT physician.OddStartingTime FROM
     * physician WHERE physician.PhysicianID = ?))) >= 0) AND
     * (TIME_TO_SEC(TIMEDIFF((SELECT physician.OddEndingTime FROM physician
     * WHERE physician.PhysicianID = ?), ?)) >= 0)) OR ((DAYOFMONTH(?) MOD 2 =
     * 0) AND (TIME_TO_SEC(TIMEDIFF(?, (SELECT physician.EvenStartingTime FROM
     * physician WHERE physician.PhysicianID = ?))) >= 0) AND
     * (TIME_TO_SEC(TIMEDIFF((SELECT physician.EvenEndingTime FROM physician
     * WHERE physician.PhysicianID = ?), ?)) >= 0))) AND (NOT EXISTS(SELECT *
     * FROM ticket ticketLabel WHERE (ticketLabel.Date = ?) AND
     * (ticketLabel.Physician = ?) AND (NOT
     * ((TIME_TO_SEC(TIMEDIFF(ticketLabel.StartingTime, ?)) > 0) OR
     * (TIME_TO_SEC(TIMEDIFF(?, ticketLabel.EndingTime)) > 0)))))".
     */
    static final String TICKET_ADDING_STMT
            = "INSERT INTO ticket(ticket.Physician, ticket.Date, "
              + "StartingTime, EndingTime) SELECT ?, ?, ?, ? FROM "
              + "user WHERE (user.UserID = ?) AND "
              + "(((DAYOFMONTH(?) MOD 2 != 0) AND (TIME_TO_SEC(TIMEDIFF(?, "
              + "(SELECT physician.OddStartingTime FROM physician WHERE "
              + "physician.PhysicianID = ?))) >= 0) AND "
              + "(TIME_TO_SEC(TIMEDIFF((SELECT physician.OddEndingTime FROM "
              + "physician WHERE physician.PhysicianID = ?), ?)) >= 0)) OR "
              + "((DAYOFMONTH(?) MOD 2 = 0) AND (TIME_TO_SEC(TIMEDIFF(?, "
              + "(SELECT physician.EvenStartingTime FROM physician WHERE "
              + "physician.PhysicianID = ?))) >= 0) AND "
              + "(TIME_TO_SEC(TIMEDIFF((SELECT physician.EvenEndingTime FROM "
              + "physician WHERE physician.PhysicianID = ?), ?)) >= 0))) AND "
              + "(NOT EXISTS(SELECT * FROM ticket ticketLabel WHERE "
              + "(ticketLabel.Date = ?) AND (ticketLabel.Physician = ?) AND "
              + "(NOT ((TIME_TO_SEC(TIMEDIFF(ticketLabel.StartingTime, ?)) > "
              + "0) OR (TIME_TO_SEC(TIMEDIFF(?, ticketLabel.EndingTime)) > "
              + "0)))))";
    /**
     * An SQL statement which is used to assign a ticket to a patient. The
     * identifier holds the value of "UPDATE ticket SET Patient = ? WHERE
     * (TicketID = ?) AND (TIMESTAMPDIFF(SECOND, SYSDATE(),
     * TIMESTAMP(ticket.Date, ticket.StartingTime)) > 0) AND
     * (Patient IS NULL)".
     */
    static final String TICKET_ASSIGNMENT_STMT
            = "UPDATE ticket SET Patient = ? WHERE (TicketID = ?) AND "
              + "(TIMESTAMPDIFF(SECOND, SYSDATE(), TIMESTAMP(ticket.Date, "
              + "ticket.StartingTime)) > 0) AND (Patient IS NULL)";
    /**
     * An SQL statement which is used to cancel a ticket of a patient. The
     * identifier holds the value of "UPDATE ticket SET Patient = NULL WHERE
     * (TicketID = ?) AND (TIMESTAMPDIFF(SECOND, SYSDATE(),
     * TIMESTAMP(ticket.Date, ticket.StartingTime)) > 0) AND (Patient = ?)".
     */
    static final String TICKET_CANCELLING_STMT
            = "UPDATE ticket SET Patient = NULL WHERE (TicketID = ?) AND "
              + "(TIMESTAMPDIFF(SECOND, SYSDATE(), TIMESTAMP(ticket.Date, "
              + "ticket.StartingTime)) > 0) AND (Patient = ?)";
    /**
     * An SQL statement which is used to retrieve used tickets. The date of
     * each ticket falls into a specified range. This time period should
     * precede the current date. The identifier holds the value of "SELECT
     * PassportNumber, FirstName, LastName, TicketID, ticket.Date,
     * StartingTime, EndingTime, Diagnosis, Treatment FROM ticket INNER JOIN
     * personaldata ON personaldata.PassportNumber = (SELECT PassportNumber
     * FROM user WHERE UserID = Patient) WHERE (Patient IS NOT NULL)
     * AND (ticket.Date BETWEEN ? AND ?) AND (DATEDIFF(?, DATE(SYSDATE())) < 0)
     * AND (Diagnosis IS NOT NULL)".
     */
    static final String USED_TICKET_RETRIEVING_STMT
            = "SELECT PassportNumber, FirstName, LastName, TicketID, "
              + "ticket.Date, StartingTime, EndingTime, Diagnosis, Treatment "
              + "FROM ticket INNER JOIN personaldata ON "
              + "personaldata.PassportNumber = (SELECT PassportNumber FROM "
              + "user WHERE UserID = Patient) WHERE (Patient IS NOT "
              + "NULL) AND (ticket.Date BETWEEN ? AND ?) AND "
              + "(DATEDIFF(?, DATE(SYSDATE())) < 0) AND (Diagnosis IS NOT "
              + "NULL)";
    /**
     * An SQL statement which is used to retrieve a user of this program
     * complex. The identifier holds the value of "SELECT userID, role,
     * blockedState FROM user WHERE (login = ?) AND (password = ?)".
     */
    static final String USER_RETRIEVING_STMT
            = "SELECT userID, role, blockedState FROM user WHERE (login = ?) "
              + "AND (password = ?)";
    /**
     * An SQL statement which is used to retrieve all users which are not head
     * physicians. The identifier holds the value of "SELECT DISTINCT
     * personalData.passportNumber, firstName, lastName FROM personalData JOIN
     * user ON personalData.PassportNumber = user.PassportNumber WHERE (role
     * <> 'HEAD_PHYSICIAN') AND (NOT EXISTS(SELECT role FROM user WHERE
     * passportNumber = personalData.PassportNumber AND role =
     * 'HEAD_PHYSICIAN'))".
     */
    static final String USERS_RETRIEVING_STMT
            = "SELECT DISTINCT personalData.passportNumber, firstName, "
              + "lastName FROM personalData JOIN user ON "
              + "personalData.PassportNumber = user.PassportNumber WHERE "
              + "(role <> 'HEAD_PHYSICIAN') AND (NOT EXISTS(SELECT role FROM "
              + "user WHERE passportNumber = personalData.PassportNumber AND "
              + "role = 'HEAD_PHYSICIAN'))";
    /**
     * An SQL statement which is used to commit a patient visit. The
     * identifier holds the value of "UPDATE ticket SET Diagnosis = ?,
     * Treatment = ? WHERE (TicketID = ?) AND (Patient IS NOT NULL) AND
     * (SYSDATE() > ADDTIME(Date, StartingTime)) AND (Physician = ?)".
     */
    static final String VISIT_COMMITING_STMT
            = "UPDATE ticket SET Diagnosis = ?, Treatment = ? WHERE (TicketID "
              + "= ?) AND (Patient IS NOT NULL) AND (SYSDATE() > "
              + "ADDTIME(Date, StartingTime)) AND (Physician = ?)";
    /**
     * Constructs an instance of this class.
     */
    private SQLStatementsKeeper() {
        /* The default initialization is sufficient. */
    }
}
