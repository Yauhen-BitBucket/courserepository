/*
 * ParameterNumbersKeeper.java
 * A class for storing parameter numbers.
 */
package by.training.task05.dao.implementation;


/**
 * A class for storing parameter numbers.
 * @author Yauhen Sazonau
 * @version 1.0, 06/21/19
 * @since 1.0
 */
class ParameterNumbersKeeper {
    /**
     * A parameter number. The identifier holds the value of "1".
     */
    static final int PARAMETER_NUMBER_1 = 1;
    /**
     * A parameter number. The identifier holds the value of "2".
     */
    static final int PARAMETER_NUMBER_2 = 2;
    /**
     * A parameter number. The identifier holds the value of "3".
     */
    static final int PARAMETER_NUMBER_3 = 3;
    /**
     * A parameter number. The identifier holds the value of "4".
     */
    static final int PARAMETER_NUMBER_4 = 4;
    /**
     * A parameter number. The identifier holds the value of "5".
     */
    static final int PARAMETER_NUMBER_5 = 5;
    /**
     * A parameter number. The identifier holds the value of "6".
     */
    static final int PARAMETER_NUMBER_6 = 6;
    /**
     * A parameter number. The identifier holds the value of "7".
     */
    static final int PARAMETER_NUMBER_7 = 7;
    /**
     * A parameter number. The identifier holds the value of "8".
     */
    static final int PARAMETER_NUMBER_8 = 8;
    /**
     * A parameter number. The identifier holds the value of "9".
     */
    static final int PARAMETER_NUMBER_9 = 9;
    /**
     * A parameter number. The identifier holds the value of "10".
     */
    static final int PARAMETER_NUMBER_10 = 10;
    /**
     * A parameter number. The identifier holds the value of "11".
     */
    static final int PARAMETER_NUMBER_11 = 11;
    /**
     * A parameter number. The identifier holds the value of "12".
     */
    static final int PARAMETER_NUMBER_12 = 12;
    /**
     * A parameter number. The identifier holds the value of "13".
     */
    static final int PARAMETER_NUMBER_13 = 13;
    /**
     * A parameter number. The identifier holds the value of "14".
     */
    static final int PARAMETER_NUMBER_14 = 14;
    /**
     * A parameter number. The identifier holds the value of "15".
     */
    static final int PARAMETER_NUMBER_15 = 15;
    /**
     * A parameter number. The identifier holds the value of "16".
     */
    static final int PARAMETER_NUMBER_16 = 16;
    /**
     * A parameter number. The identifier holds the value of "17".
     */
    static final int PARAMETER_NUMBER_17 = 17;
    /**
     * A parameter number. The identifier holds the value of "18".
     */
    static final int PARAMETER_NUMBER_18 = 18;
    /**
     * A parameter number. The identifier holds the value of "19".
     */
    static final int PARAMETER_NUMBER_19 = 19;
    /**
     * Constructs an instance of this class.
     */
    private ParameterNumbersKeeper() {
        /* The default initialization is sufficient. */
    }
}
