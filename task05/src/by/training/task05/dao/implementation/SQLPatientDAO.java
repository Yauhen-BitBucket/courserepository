/*
 * SQLPatientDAO.java
 * A patient DAO implementation which is based on an SQL.
 */
package by.training.task05.dao.implementation;


import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_1;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_2;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_3;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_4;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_5;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_6;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_7;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_8;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_9;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_10;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_11;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_12;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_13;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_14;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_15;

import static by.training.task05.dao.implementation.SQLStatementsKeeper
        .FREE_TICKET_RETRIEVING_STMT;
import static by.training.task05.dao.implementation.SQLStatementsKeeper
        .OWN_TICKET_RETRIEVING_STMT;
import static by.training.task05.dao.implementation.SQLStatementsKeeper
        .PHYSICIAN_RETRIEVING_STMT;
import static by.training.task05.dao.implementation.SQLStatementsKeeper
        .TICKET_ASSIGNMENT_STMT;
import static by.training.task05.dao.implementation.SQLStatementsKeeper
        .TICKET_CANCELLING_STMT;
import static by.training.task05.dao.implementation.SupportMethodsKeeper
        .closeResultSet;
import static by.training.task05.dao.implementation.SupportMethodsKeeper
        .closeStatement;
import static by.training.task05.dao.implementation.SupportMethodsKeeper
        .returnConnection;
import static by.training.task05.dao.implementation.SupportMethodsKeeper
        .rollbackConnection;

import static java.sql.Connection.TRANSACTION_READ_COMMITTED;

import by.training.task05.dao.DAOException;
import by.training.task05.dao.PatientDAO;
import by.training.task05.entity.Person;
import by.training.task05.entity.Physician;
import by.training.task05.entity.Speciality;
import by.training.task05.entity.Ticket;

import by.training.task05.pool.ConnectionPool;
import by.training.task05.pool.PoolException;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Collection;
import java.util.LinkedList;


/**
 * A patient DAO implementation which is based on an SQL.
 * @author Yauhen Sazonau
 * @version 1.0, 06/21/19
 * @since 1.0
 */
public class SQLPatientDAO implements PatientDAO {
    /**
     * Retrieves all physicians which have the specified specialty.
     * @param speciality a <code>Speciality</code> value which is a speciality
     *                   of retrieved physicians
     * @return all physicians which have the specified specialty. If there are
     *         no such physicians, an empty <code>Collection</code> is
     *         returned.
     * @throws DAOException if there are problems with retrieving all
     *                      physicians which have the specified specialty
     */
    @Override
    public Collection<Physician> retrievePhysicians(
            final Speciality speciality) throws DAOException {
        Collection<Physician> physicians = new LinkedList<>();
        Connection connection = null;
        ConnectionPool pool;
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        int isolationLevel = TRANSACTION_READ_COMMITTED;
        /* The main part of the method. */
        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(isolationLevel);
            /* An attempt to retrieve physicians. */
            preparedStatement
                    = connection.prepareStatement(PHYSICIAN_RETRIEVING_STMT);
            try {
                preparedStatement.setString(PARAMETER_NUMBER_1,
                        speciality.name());
                resultSet = preparedStatement.executeQuery();
                try {
                    while (resultSet.next()) {
                        physicians.add(parsePhysician(resultSet));
                    }
                } finally {
                    closeResultSet(resultSet);
                }
            } finally {
                closeStatement(preparedStatement);
            }
            connection.commit();
        } catch (SQLException | PoolException e) {
            if (connection != null) {
                rollbackConnection(connection);
            }
            throw new DAOException("Can't retrieve physicians with the "
                    + "speciality: " + speciality + ".", e);
        } finally {
            returnConnection(connection);
        }
        return physicians;
    }
    /**
     * Retrieves free tickets to all physicians which have the specified
     * speciality on the specified date.
     * @param date a <code>Date</code> value which is a desired surgery date
     * @param speciality a <code>Speciality</code> value which is a desired
     *                   speciality
     * @return free tickets to all physicians which have the specified
     *         speciality on the specified date. If there are
     *         no such tickets, an empty <code>Collection</code> is returned.
     * @throws DAOException if there are problems with retrieving free tickets
     *                      to all physicians which have the specified
     *                      speciality on the specified date
     */
    @Override
    public Collection<Ticket> retrieveFreeTickets(final Date date,
            final Speciality speciality) throws DAOException {
        Collection<Ticket> tickets = new LinkedList<>();
        Connection connection = null;
        ConnectionPool pool;
        Physician physician;
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        Ticket ticket;
        int isolationLevel = TRANSACTION_READ_COMMITTED;
        /* The main part of the method. */
        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(isolationLevel);
            /* An attempt to retrieve free tickets. */
            preparedStatement
                    = connection.prepareStatement(FREE_TICKET_RETRIEVING_STMT);
            try {
                preparedStatement.setDate(PARAMETER_NUMBER_1, date);
                preparedStatement.setString(PARAMETER_NUMBER_2,
                        speciality.name());
                resultSet = preparedStatement.executeQuery();
                try {
                    while (resultSet.next()) {
                        physician = parsePhysician(resultSet);
                        ticket = parseTicket(resultSet);
                        ticket.setPhysician(physician);
                        tickets.add(ticket);
                    }
                } finally {
                    closeResultSet(resultSet);
                }
            } finally {
                closeStatement(preparedStatement);
            }
            connection.commit();
        } catch (SQLException | PoolException e) {
            if (connection != null) {
                rollbackConnection(connection);
            }
            throw new DAOException("Can't retrieve tickets to physicians with "
                    + "the speciality: " + speciality + "on date:" + date
                    + ".", e);
        } finally {
            returnConnection(connection);
        }
        return tickets;
    }
    /**
     * Retrieves tickets which have been assigned to a particular patient.
     * @param patientID a <code>long</code> value which is a patient's
     *                  identification number
     * @return tickets which have been assigned to a particular patient. If
     *         there are no such tickets, an empty <code>Collection</code> is
     *         returned.
     * @throws DAOException if there are problems with retrieving tickets which
     *                      have been assigned to a particular patient
     */
    @Override
    public Collection<Ticket> retrievePersonalTickets(final long patientID)
            throws DAOException {
        Collection<Ticket> tickets = new LinkedList<>();
        Connection connection = null;
        ConnectionPool pool;
        Physician physician;
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        Ticket ticket;
        int isolationLevel = TRANSACTION_READ_COMMITTED;
        /* The main part of the method. */
        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(isolationLevel);
            /* An attempt to retrieve personal tickets. */
            preparedStatement
                    = connection.prepareStatement(OWN_TICKET_RETRIEVING_STMT);
            try {
                preparedStatement.setLong(PARAMETER_NUMBER_1, patientID);
                resultSet = preparedStatement.executeQuery();
                try {
                    while (resultSet.next()) {
                        physician = parsePhysician(resultSet);
                        ticket = parseTicket(resultSet);
                        ticket.setPhysician(physician);
                        ticket.setDiagnosis(
                                resultSet.getString(PARAMETER_NUMBER_14));
                        ticket.setTreatment(
                                resultSet.getString(PARAMETER_NUMBER_15));
                        tickets.add(ticket);
                    }
                } finally {
                    closeResultSet(resultSet);
                }
            } finally {
                closeStatement(preparedStatement);
            }
            connection.commit();
        } catch (SQLException | PoolException e) {
            if (connection != null) {
                rollbackConnection(connection);
            }
            throw new DAOException("Can't retrieve tickets which have been "
                    + "assigned to a patient with the ID of: " + patientID
                    + ".", e);
        } finally {
            returnConnection(connection);
        }
        return tickets;
    }
    /**
     * Assigns a ticket to a patient.
     * @param patientID a <code>long</code> value which is a patient's
     *                  identification number
     * @param ticketID a <code>long</code> value which is a ticket's
     *                 identification number
     * @return <code>ASSIGNMENT_SUCCESS_VALUE</code> if a ticket was
     *         successfully assigned, <code>ASSIGNMENT_FAILURE_VALUE</code>
     *         otherwise
     * @throws DAOException if there are problems with assignment of a ticket
     *                      to a patient
     * @see by.training.task05.dao.PatientDAO#ASSIGNMENT_FAILURE_VALUE
     * @see by.training.task05.dao.PatientDAO#ASSIGNMENT_SUCCESS_VALUE
     */
    @Override
    public int assignTicket(final long patientID, final long ticketID)
            throws DAOException {
        Connection connection = null;
        ConnectionPool pool;
        PreparedStatement preparedStatement;
        int isolationLevel = TRANSACTION_READ_COMMITTED;
        int result;
        /* The main part of the method. */
        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(isolationLevel);
            /* An attempt to assign a ticket to a patient. */
            preparedStatement
                    = connection.prepareStatement(TICKET_ASSIGNMENT_STMT);
            try {
                preparedStatement.setLong(PARAMETER_NUMBER_1, patientID);
                preparedStatement.setLong(PARAMETER_NUMBER_2, ticketID);
                result = preparedStatement.executeUpdate();
            } finally {
                closeStatement(preparedStatement);
            }
            connection.commit();
        } catch (SQLException | PoolException e) {
            if (connection != null) {
                rollbackConnection(connection);
            }
            throw new DAOException("Can't assign a ticket: " + ticketID
                    + " to a patient: " + patientID + ".", e);
        } finally {
            returnConnection(connection);
        }
        return result;
    }
    /**
     * Cancels a ticket of a patient.
     * @param patientID a <code>long</code> value which is a patient's
     *                  identification number
     * @param ticketID a <code>long</code> value which is a ticket's
     *                 identification number
     * @return <code>CANCELLING_SUCCESS_VALUE</code> if a ticket was
     *         successfully cancelled, <code>CANCELLING_FAILURE_VALUE</code>
     *         otherwise
     * @throws DAOException if there are problems with canceling a ticket
     *                      of a patient
     * @see by.training.task05.dao.PatientDAO#CANCELLING_FAILURE_VALUE
     * @see by.training.task05.dao.PatientDAO#CANCELLING_SUCCESS_VALUE
     */
    @Override
    public int cancellTicket(final long patientID, final long ticketID)
            throws DAOException {
        Connection connection = null;
        ConnectionPool pool;
        PreparedStatement preparedStatement;
        int isolationLevel = TRANSACTION_READ_COMMITTED;
        int result;
        /* The main part of the method. */
        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(isolationLevel);
            /* An attempt to cancel a ticket of a patient. */
            preparedStatement
                    = connection.prepareStatement(TICKET_CANCELLING_STMT);
            try {
                preparedStatement.setLong(PARAMETER_NUMBER_1, ticketID);
                preparedStatement.setLong(PARAMETER_NUMBER_2, patientID);
                result = preparedStatement.executeUpdate();
            } finally {
                closeStatement(preparedStatement);
            }
            connection.commit();
        } catch (SQLException | PoolException e) {
            if (connection != null) {
                rollbackConnection(connection);
            }
            throw new DAOException("Can't cancell a ticket: " + ticketID
                    + " of a patient: " + patientID + ".", e);
        } finally {
            returnConnection(connection);
        }
        return result;
    }
    /**
     * Retrieves a physician from a <code>ResultSet</code> object.
     * @param resultSet a <code>ResultSet</code> value which is a table of data
     *                  to be parsed
     * @return a physician from a <code>ResultSet</code> object. Can't be a
     *         <code>null</code> value.
     * @throws SQLException if there are problems with retrieving a physician
     *                      from a <code>ResultSet</code> object
     */
    private Physician parsePhysician(final ResultSet resultSet)
            throws SQLException {
        Physician physician = new Physician();
        /* The main part of the method. */
        physician.setRoomNumber(resultSet.getLong(PARAMETER_NUMBER_1));
        physician.setOddStartingTime(
                resultSet.getTime(PARAMETER_NUMBER_2));
        physician.setOddEndingTime(
                resultSet.getTime(PARAMETER_NUMBER_3));
        physician.setEvenStartingTime(
                resultSet.getTime(PARAMETER_NUMBER_4));
        physician.setEvenEndingTime(
                resultSet.getTime(PARAMETER_NUMBER_5));
        physician.setPassportNumber(
                resultSet.getString(PARAMETER_NUMBER_6));
        physician.setFirstName(
                resultSet.getString(PARAMETER_NUMBER_7));
        physician.setLastName(
                resultSet.getString(PARAMETER_NUMBER_8));
        physician.setSpeciality(
                Speciality.valueOf(
                        resultSet.getString(PARAMETER_NUMBER_9)));
        return physician;
    }
    /**
     * Retrieves a ticket from a <code>ResultSet</code> object.
     * @param resultSet a <code>ResultSet</code> value which is a table of data
     *                  to be parsed
     * @return a ticket from a <code>ResultSet</code> object. Can't be a
     *         <code>null</code> value.
     * @throws SQLException if there are problems with retrieving a ticket
     *                      from a <code>ResultSet</code> object
     */
    private Ticket parseTicket(final ResultSet resultSet)
            throws SQLException {
        return (new Ticket(resultSet.getLong(PARAMETER_NUMBER_10),
                           new Physician(), new Person(),
                           resultSet.getDate(PARAMETER_NUMBER_11),
                           resultSet.getTime(PARAMETER_NUMBER_12),
                           resultSet.getTime(PARAMETER_NUMBER_13)));
    }
}
