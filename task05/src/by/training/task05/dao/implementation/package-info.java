/**
 * Provides implementations of DAO interfaces.
 * @author Yauhen Sazonau
 * @since 1.0
 */
package by.training.task05.dao.implementation;
