/* SQLDAOFactory.java
 * A factory used to create various DAOs which are based on an SQL.
 */
package by.training.task05.dao.implementation;


import by.training.task05.dao.HeadPhysicianDAO;
import by.training.task05.dao.PatientDAO;
import by.training.task05.dao.PhysicianDAO;
import by.training.task05.dao.UserDAO;


/**
 * A factory used to create various DAOs which are based on an SQL.
 * @version 1.0, 06/25/19
 * @since 1.0
 */
public final class SQLDAOFactory {
    /**
     * Represents a single instance of this class. <code>null</code> value is
     * prohibited.
     */
    private static SQLDAOFactory instance = new SQLDAOFactory();
    /**
     * An implementation of the <code>HeadPhysicianDAO</code> interface.
     * <code>null</code> value is prohibited.
     */
    private HeadPhysicianDAO headPhysicianDAO = new SQLHeadPhysicianDAO();
    /**
     * An implementation of the <code>PatientDAO</code> interface.
     * <code>null</code> value is prohibited.
     */
    private PatientDAO patientDAO = new SQLPatientDAO();
    /**
     * An implementation of the <code>PhysicianDAO</code> interface.
     * <code>null</code> value is prohibited.
     */
    private PhysicianDAO physicianDAO = new SQLPhysicianDAO();
    /**
     * An implementation of the <code>UserDAO</code> interface.
     * <code>null</code> value is prohibited.
     */
    private UserDAO userDAO = new SQLUserDAO();
    /**
     * Creates an instance of this class. You have no ability to create
     * an instance of this class using this constructor. You should use the
     * <code>getInstance</code> method.
     */
    private SQLDAOFactory() {
        /* The default initialization is sufficient. */
    }
    /**
     * Retrieves an implementation of the <code>HeadPhysicianDAO</code>
     * interface.
     * @return an implementation of the <code>HeadPhysicianDAO</code> interface
     */
    public HeadPhysicianDAO retrieveHeadPhysicianDAO() {
        return headPhysicianDAO;
    }
    /**
     * Retrieves an implementation of the <code>PatientDAO</code> interface.
     * @return an implementation of the <code>PatientDAO</code> interface
     */
    public PatientDAO retrievePatientDAO() {
        return patientDAO;
    }
    /**
     * Retrieves an implementation of the <code>PhysicianDAO</code> interface.
     * @return an implementation of the <code>PhysicianDAO</code> interface
     */
    public PhysicianDAO retrievePhysicianDAO() {
        return physicianDAO;
    }
    /**
     * Retrieves an implementation of the <code>UserDAO</code> interface.
     * @return an implementation of the <code>UserDAO</code> interface
     */
    public UserDAO retrieveUserDAO() {
        return userDAO;
    }
    /**
     * Retrieves an instance if this class.
     * @return an instance if this class
     */
    public static SQLDAOFactory getInstance() {
        return instance;
    }
}
