/*
 * SupportMethodsKeeper.java
 * A keeper for methods which are used by DAO implementations.
 */
package by.training.task05.dao.implementation;


import by.training.task05.pool.ConnectionPool;
import by.training.task05.pool.PoolException;

import java.nio.charset.StandardCharsets;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * A keeper for methods which are used by DAO implementations.
 * @author Yauhen Sazonau
 * @version 1.0, 06/21/19
 * @since 1.0
 */
class SupportMethodsKeeper {
    /**
     * A logger for reporting errors. <code>null</code> value is prohibited.
     */
    private static final Logger LOGGER
            = LogManager.getLogger(SupportMethodsKeeper.class);
    /**
     * A radix. The identifier holds the value of "16".
     */
    private static final int RADIX = 16;
    /**
     * A transforming value. The identifier holds the value of "0xff".
     */
    private static final int TRANSFORMING_VALUE_1 = 0xff;
    /**
     * A transforming value. The identifier holds the value of "0x100".
     */
    private static final int TRANSFORMING_VALUE_2 = 0x100;
    /**
     * Constructs an instance of this class.
     */
    private SupportMethodsKeeper() {
        /* The default initialization is sufficient. */
    }
    /**
     * Closes an object the class of which implements the
     * <code>ResultSet</code> interface.
     * @param resultSet a <code>ResultSet</code> value which is a table
     *                  of data
     */
    static void closeResultSet(final ResultSet resultSet) {
        try {
            resultSet.close();
        } catch (SQLException e) {
            /* If closing a result set wasn't successful, report it. */
            LOGGER.error("A problem occured during closing a result "
                    + "set.", e);
        }
    }
    /**
     * Closes an object the class of which implements the
     * <code>Statement</code> interface.
     * @param statement a <code>Statement</code> value which is an object
     *                  used for executing an SQL statement
     */
    static void closeStatement(final Statement statement) {
        try {
            statement.close();
        } catch (SQLException e) {
            /* If closing a statement wasn't successful, report it. */
            LOGGER.error("A problem occured during closing a "
                    + "statement.", e);
        }
    }
    /**
     * Transforms a specified message into a new message using the MD5
     * algorithm.
     * @param message a <code>String</code> value which is a message to be
     *                transformed
     * @return a message transformed using the MD5 algorithm
     * @throws NoSuchAlgorithmException if it's not possible to perform
     *                                  transformation
     */
    static String convertMessage(final String message)
            throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        String hexString;
        StringBuilder buffer = new StringBuilder();
        byte[] resultBytes;
        /* The main part of the method. */
        messageDigest.update(message.getBytes(StandardCharsets.UTF_8));
        resultBytes = messageDigest.digest();
        for (int i = 0; i < resultBytes.length; i++) {
            hexString = Integer.toString(
                    (resultBytes[i] & TRANSFORMING_VALUE_1)
                    + TRANSFORMING_VALUE_2, RADIX);
            buffer.append(hexString.substring(1));
        }
        return (buffer.toString());
    }
    /**
     * Returns a connection back to pool.
     * @param connection a <code>Connection</code> object to be returned
     */
    static void returnConnection(final Connection connection) {
        ConnectionPool pool;
        try {
            pool = ConnectionPool.getInstance();
            if (connection != null) {
                pool.returnConnection(connection);
            }
        } catch (PoolException e) {
            /* If returning a connection wasn't successful, report it. */
            LOGGER.error("A problem occured during returning a "
                    + "connection.", e);
        }
    }
    /**
     * Undoes all changes made in the current transaction and releases any
     * database locks currently held by the specified <code>Connection</code>
     * object.
     * @param connection a <code>Connection</code> object to roll back
     */
    static void rollbackConnection(final Connection connection) {
        try {
            connection.rollback();
        } catch (SQLException e1) {
            /* If rolling back a connection wasn't successful, report it. */
            LOGGER.error("A problem occured during rolling back a "
                    + "connection.", e1);
        }
    }
}
