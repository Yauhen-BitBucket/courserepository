/*
 * SQLPhysicianDAO.java
 * A physician DAO implementation which is based on an SQL.
 */
package by.training.task05.dao.implementation;


import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_1;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_2;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_3;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_4;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_5;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_6;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_7;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_8;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_9;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_10;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_11;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_12;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_13;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_14;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_15;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_16;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_17;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_18;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_19;

import static by.training.task05.dao.implementation.SQLStatementsKeeper
        .ASSIGNED_TICKET_RETRIEVING_STMT;
import static by.training.task05.dao.implementation.SQLStatementsKeeper
        .TICKET_ADDING_STMT;
import static by.training.task05.dao.implementation.SQLStatementsKeeper
        .VISIT_COMMITING_STMT;

import static by.training.task05.dao.implementation.SupportMethodsKeeper
        .closeResultSet;
import static by.training.task05.dao.implementation.SupportMethodsKeeper
        .closeStatement;
import static by.training.task05.dao.implementation.SupportMethodsKeeper
        .returnConnection;
import static by.training.task05.dao.implementation.SupportMethodsKeeper
        .rollbackConnection;

import static java.sql.Connection.TRANSACTION_READ_COMMITTED;

import by.training.task05.dao.DAOException;
import by.training.task05.dao.PhysicianDAO;

import by.training.task05.entity.Person;
import by.training.task05.entity.Physician;
import by.training.task05.entity.Ticket;

import by.training.task05.pool.ConnectionPool;
import by.training.task05.pool.PoolException;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;

import java.util.Collection;
import java.util.LinkedList;


/**
 * A physician DAO implementation which is based on an SQL.
 * @author Yauhen Sazonau
 * @version 1.0, 06/23/19
 * @since 1.0
 */
public class SQLPhysicianDAO implements PhysicianDAO {
    /**
     * Adds a new ticket to a doctor on a date at a time.
     * @param physicianID a <code>long</code> value which is a physician's
     *                  identification number
     * @param date a <code>Date</code> value which is a desired date
     * @param startingTime a <code>Time</code> value which is a desired
     *                     starting time
     * @param endingTime a <code>Time</code> value which is a desired
     *                     ending time
     * @return <code>TICKET_ADDING_SUCCESS_VALUE</code> if a ticket was
     *         successfully added, <code>TICKET_ADDING_FAILURE_VALUE</code>
     *         otherwise
     * @throws DAOException if there are problems with adding a new ticket to
     *                      a doctor on a date at a time
     * @see by.training.task05.dao.PhysicianDAO#TICKET_ADDING_FAILURE_VALUE
     * @see by.training.task05.dao.PhysicianDAO#TICKET_ADDING_SUCCESS_VALUE
     */
    @Override
    public int addTicket(final long physicianID, final Date date,
            final Time startingTime,
            final Time endingTime) throws DAOException {
        Connection connection = null;
        ConnectionPool pool;
        PreparedStatement preparedStatement;
        int isolationLevel = TRANSACTION_READ_COMMITTED;
        int result;
        /* The main part of the method. */
        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(isolationLevel);
            /* An attempt to add a ticket. */
            preparedStatement
                    = connection.prepareStatement(TICKET_ADDING_STMT);
            try {
                setParametersForTicketAdding(preparedStatement, physicianID,
                        date, startingTime, endingTime);
                result = preparedStatement.executeUpdate();
            } finally {
                closeStatement(preparedStatement);
            }
            connection.commit();
        } catch (SQLException | PoolException e) {
            if (connection != null) {
                rollbackConnection(connection);
            }
            throw new DAOException("Can't add a ticket to a doctor: "
                    + physicianID + " on a date: " + date + " between"
                    + startingTime + " and " + endingTime + ".", e);
        } finally {
            returnConnection(connection);
        }
        return result;
    }
    /**
     * Retrieves assigned tickets to a doctor on a date.
     * @param physicianID a <code>long</code> value which is a physician's
     *                  identification number
     * @param date a <code>Date</code> value which is a desired date
     * @return assigned tickets to a doctor on a date
     * @throws DAOException if there are problems with retrieving assigned
     *                      tickets to a doctor on a date
     */
    @Override
    public Collection<Ticket> retrieveAssignedTickets(final long physicianID,
            final Date date) throws DAOException {
        Collection<Ticket> tickets = new LinkedList<>();
        Connection connection = null;
        ConnectionPool pool;
        Person patient;
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        Ticket ticket;
        int isolationLevel = TRANSACTION_READ_COMMITTED;
        /* The main part of the method. */
        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(isolationLevel);
            /* An attempt to retrieve personal tickets. */
            preparedStatement
                    = connection.prepareStatement(
                            ASSIGNED_TICKET_RETRIEVING_STMT);
            try {
                preparedStatement.setLong(PARAMETER_NUMBER_1, physicianID);
                preparedStatement.setDate(PARAMETER_NUMBER_2, date);
                resultSet = preparedStatement.executeQuery();
                try {
                    while (resultSet.next()) {
                        patient = parsePatient(resultSet);
                        ticket = parseTicket(resultSet);
                        ticket.setPatient(patient);
                        tickets.add(ticket);
                    }
                } finally {
                    closeResultSet(resultSet);
                }
            } finally {
                closeStatement(preparedStatement);
            }
            connection.commit();
        } catch (SQLException | PoolException e) {
            if (connection != null) {
                rollbackConnection(connection);
            }
            throw new DAOException("Can't retrieve assigned tickets to a "
                    + "doctor: " + physicianID + " on: " + date + ".", e);
        } finally {
            returnConnection(connection);
        }
        return tickets;
    }
    /**
     * Commits a visit of a patient.
     * @param ticketID a <code>long</code> value which is a ticket's
     *                 identification number
     * @param physicianID a <code>long</code> value which is the identification
     *                    number of a physician
     * @param diagnosis a <code>String</code> value which is patient's
     *                  diagnosis
     * @param treatment a <code>String</code> value which is patient's
     *                  treatment
     * @return <code>VISIT_COMMITING_SUCCESS_VALUE</code> if a visit was
     *         successfully committed,
     *         <code>VISIT_COMMITING_FAILURE_VALUE</code> otherwise
     * @throws DAOException if there are problems with committing a visit
     * @see by.training.task05.dao.PhysicianDAO#VISIT_COMMITING_FAILURE_VALUE
     * @see by.training.task05.dao.PhysicianDAO#VISIT_COMMITING_SUCCESS_VALUE
     */
    @Override
    public int commitVisit(final long ticketID, final long physicianID,
            final String diagnosis, final String treatment)
                    throws DAOException {
        Connection connection = null;
        ConnectionPool pool;
        PreparedStatement preparedStatement;
        int isolationLevel = TRANSACTION_READ_COMMITTED;
        int result;
        /* The main part of the method. */
        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(isolationLevel);
            /* An attempt to assign a ticket to a patient. */
            preparedStatement
                    = connection.prepareStatement(VISIT_COMMITING_STMT);
            try {
                preparedStatement.setString(PARAMETER_NUMBER_1, diagnosis);
                preparedStatement.setString(PARAMETER_NUMBER_2, treatment);
                preparedStatement.setLong(PARAMETER_NUMBER_3, ticketID);
                preparedStatement.setLong(PARAMETER_NUMBER_4, physicianID);
                result = preparedStatement.executeUpdate();
            } finally {
                closeStatement(preparedStatement);
            }
            connection.commit();
        } catch (SQLException | PoolException e) {
            if (connection != null) {
                rollbackConnection(connection);
            }
            throw new DAOException("Can't commit a visit of a patient with a "
                    + "ticket: " + ticketID + ".", e);
        } finally {
            returnConnection(connection);
        }
        return result;
    }
    /**
     * Sets parameters of a prepared statement for adding a new ticket to a
     * doctor on a date at a time.
     * @param statement a <code>PreparedStatement</code> value which is a
     *                  prepared statement to adjust
     * @param physicianID a <code>long</code> value which is a physician's
     *                  identification number
     * @param date a <code>Date</code> value which is a desired date
     * @param startingTime a <code>Time</code> value which is a desired
     *                     starting time
     * @param endingTime a <code>Time</code> value which is a desired
     *                     ending time
     * @throws SQLException if there are problems with setting parameters
     */
    private void setParametersForTicketAdding(
            final PreparedStatement statement, final long physicianID,
            final Date date, final Time startingTime,
            final Time endingTime) throws SQLException {
        statement.setLong(PARAMETER_NUMBER_1, physicianID);
        statement.setDate(PARAMETER_NUMBER_2, date);
        statement.setTime(PARAMETER_NUMBER_3, startingTime);
        statement.setTime(PARAMETER_NUMBER_4, endingTime);
        statement.setLong(PARAMETER_NUMBER_5, physicianID);
        statement.setDate(PARAMETER_NUMBER_6, date);
        statement.setTime(PARAMETER_NUMBER_7, startingTime);
        statement.setLong(PARAMETER_NUMBER_8, physicianID);
        statement.setLong(PARAMETER_NUMBER_9, physicianID);
        statement.setTime(PARAMETER_NUMBER_10, endingTime);
        statement.setDate(PARAMETER_NUMBER_11, date);
        statement.setTime(PARAMETER_NUMBER_12, startingTime);
        statement.setLong(PARAMETER_NUMBER_13, physicianID);
        statement.setLong(PARAMETER_NUMBER_14, physicianID);
        statement.setTime(PARAMETER_NUMBER_15, endingTime);
        statement.setDate(PARAMETER_NUMBER_16, date);
        statement.setLong(PARAMETER_NUMBER_17, physicianID);
        statement.setTime(PARAMETER_NUMBER_18, endingTime);
        statement.setTime(PARAMETER_NUMBER_19, startingTime);
    }
    /**
     * Retrieves a patient from a <code>ResultSet</code> object.
     * @param resultSet a <code>ResultSet</code> value which is a table of data
     *                  to be parsed
     * @return a patient from a <code>ResultSet</code> object. Can't be a
     *         <code>null</code> value.
     * @throws SQLException if there are problems with retrieving a patient
     *                      from a <code>ResultSet</code> object
     */
    private Person parsePatient(final ResultSet resultSet)
            throws SQLException {
        Person person = new Person();
        /* The main part of the method. */
        person.setPassportNumber(resultSet.getString(PARAMETER_NUMBER_1));
        person.setFirstName(resultSet.getString(PARAMETER_NUMBER_2));
        person.setLastName(resultSet.getString(PARAMETER_NUMBER_3));
        return person;
    }
    /**
     * Retrieves a ticket from a <code>ResultSet</code> object.
     * @param resultSet a <code>ResultSet</code> value which is a table of data
     *                  to be parsed
     * @return a ticket from a <code>ResultSet</code> object. Can't be a
     *         <code>null</code> value.
     * @throws SQLException if there are problems with retrieving a ticket
     *                      from a <code>ResultSet</code> object
     */
    private Ticket parseTicket(final ResultSet resultSet)
            throws SQLException {
        return (new Ticket(resultSet.getLong(PARAMETER_NUMBER_4),
                           new Physician(), new Person(),
                           resultSet.getDate(PARAMETER_NUMBER_5),
                           resultSet.getTime(PARAMETER_NUMBER_6),
                           resultSet.getTime(PARAMETER_NUMBER_7)));
    }
}
