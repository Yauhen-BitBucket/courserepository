/*
 * SQLUserDAO.java
 * A user DAO implementation which is based on an SQL.
 */
package by.training.task05.dao.implementation;


import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_1;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_2;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_3;

import static by.training.task05.dao.implementation.SQLStatementsKeeper
        .IMAGE_RETRIEVING_STMT;
import static by.training.task05.dao.implementation.SQLStatementsKeeper
        .IMAGE_SETTING_STMT;
import static by.training.task05.dao.implementation.SQLStatementsKeeper
        .PASSPORT_NUMBER_RETRIEVING_STMT;
import static by.training.task05.dao.implementation.SQLStatementsKeeper
        .USER_RETRIEVING_STMT;

import static by.training.task05.dao.implementation.SupportMethodsKeeper
        .closeResultSet;
import static by.training.task05.dao.implementation.SupportMethodsKeeper
        .closeStatement;
import static by.training.task05.dao.implementation.SupportMethodsKeeper
        .convertMessage;
import static by.training.task05.dao.implementation.SupportMethodsKeeper
        .returnConnection;
import static by.training.task05.dao.implementation.SupportMethodsKeeper
        .rollbackConnection;

import static java.sql.Connection.TRANSACTION_READ_COMMITTED;
import static java.sql.Connection.TRANSACTION_READ_UNCOMMITTED;

import by.training.task05.dao.DAOException;
import by.training.task05.dao.UserDAO;

import by.training.task05.entity.Role;
import by.training.task05.entity.User;

import by.training.task05.pool.ConnectionPool;
import by.training.task05.pool.PoolException;

import java.io.InputStream;

import java.security.NoSuchAlgorithmException;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Optional;


/**
 * A user DAO implementation which is based on an SQL.
 * @author Yauhen Sazonau
 * @version 1.0, 06/21/19
 * @since 1.0
 */
public class SQLUserDAO implements UserDAO {
    /**
     * Retrieves an image from a data source using a specified user login.
     * @param login a <code>String</code> value which is a user login
     * @return an <code>Optional</code> object containing the desired image or
     *         an empty <code>Optional</code> object
     * @throws DAOException if there are problems with retrieving an image from
     *                      a data source
     */
    @Override
    public Optional<InputStream> retrieveImage(final String login)
            throws DAOException {
        Blob blob;
        InputStream image;
        Connection connection = null;
        ConnectionPool pool;
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        int isolationLevel = TRANSACTION_READ_COMMITTED;
        /* The main part of the method. */
        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(isolationLevel);
            /* An attempt to retrieve an image. */
            preparedStatement
                    = connection.prepareStatement(IMAGE_RETRIEVING_STMT);
            try {
                preparedStatement.setString(PARAMETER_NUMBER_1,
                        convertMessage(login));
                resultSet = preparedStatement.executeQuery();
                try {
                    if ((resultSet.next())) {
                        blob = resultSet.getBlob(PARAMETER_NUMBER_1);
                        image = blob.getBinaryStream();
                    } else {
                        image = null;
                    }
                } finally {
                    closeResultSet(resultSet);
                }
            } finally {
                closeStatement(preparedStatement);
            }
            connection.commit();
        } catch (SQLException | PoolException | NoSuchAlgorithmException e) {
            if (connection != null) {
                rollbackConnection(connection);
            }
            throw new DAOException("Can't retrieve an image which corresponds "
                    + "to a specified login: " + login + ".", e);
        } finally {
            returnConnection(connection);
        }
        return (Optional.ofNullable(image));
    }
    /**
     * Retrieves a passport number from a data source using a specified user
     * login.
     * @param login a <code>String</code> value which is a login
     * @return an <code>Optional</code> object containing the passport number
     *         or an empty <code>Optional</code> object
     * @throws DAOException if there are problems with retrieving a passport
     *                      number from a data source
     */
    @Override
    public Optional<String> retrievePassportNumber(final String login)
            throws DAOException {
        Connection connection = null;
        ConnectionPool pool;
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        String passportNumber;
        int isolationLevel = TRANSACTION_READ_COMMITTED;
        /* The main part of the method. */
        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(isolationLevel);
            /* An attempt to retrieve an image. */
            preparedStatement
                    = connection.prepareStatement(
                            PASSPORT_NUMBER_RETRIEVING_STMT);
            try {
                preparedStatement.setString(PARAMETER_NUMBER_1,
                        convertMessage(login));
                resultSet = preparedStatement.executeQuery();
                try {
                    if ((resultSet.next())) {
                        passportNumber
                                = resultSet.getString(PARAMETER_NUMBER_1);
                    } else {
                        passportNumber = null;
                    }
                } finally {
                    closeResultSet(resultSet);
                }
            } finally {
                closeStatement(preparedStatement);
            }
            connection.commit();
        } catch (SQLException | PoolException | NoSuchAlgorithmException e) {
            if (connection != null) {
                rollbackConnection(connection);
            }
            throw new DAOException("Can't retrieve a passport number which "
                    + "corresponds to a specified login: " + login + ".", e);
        } finally {
            returnConnection(connection);
        }
        return (Optional.ofNullable(passportNumber));
    }
    /**
     * Retrieves a user of this program complex from a data source using the
     * specified login and password.
     * @param login a <code>String</code> value which is a login
     * @param password a <code>String</code> value which is a password
     * @return an <code>Optional</code> object containing the desired user or
     *         an empty  <code>Optional</code> object
     * @throws DAOException if there are problems with retrieving a user of
     *                      this program complex from a data source
     */
    @Override
    public Optional<User> retrieveUser(final String login,
            final String password) throws DAOException {
        Connection connection = null;
        ConnectionPool pool;
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        User retrievedUser;
        int isolationLevel = TRANSACTION_READ_COMMITTED;
        /* The main part of the method. */
        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(isolationLevel);
            /* An attempt to retrieve the user. */
            preparedStatement
                    = connection.prepareStatement(USER_RETRIEVING_STMT);
            try {
                preparedStatement.setString(PARAMETER_NUMBER_1,
                        convertMessage(login));
                preparedStatement.setString(PARAMETER_NUMBER_2,
                        convertMessage(password));
                resultSet = preparedStatement.executeQuery();
                try {
                    if ((resultSet.next())) {
                        retrievedUser = parseUser(resultSet);
                        retrievedUser.setLogin(login);
                    } else {
                        retrievedUser = null;
                    }
                } finally {
                    closeResultSet(resultSet);
                }
            } finally {
                closeStatement(preparedStatement);
            }
            connection.commit();
        } catch (SQLException | PoolException | NoSuchAlgorithmException e) {
            if (connection != null) {
                rollbackConnection(connection);
            }
            throw new DAOException("Can't retrieve the user with the "
                    + "'login' = " + login + " and 'password' = "
                    + password + ".", e);
        } finally {
            returnConnection(connection);
        }
        return (Optional.ofNullable(retrievedUser));
    }
    /**
     * Sets an image to a user with a specified login.
     * @param login a <code>String</code> value which is a user login
     * @param image an <code>InputStream</code> value which is an image
     * @return <code>IMAGE_SETTING_SUCCESS_VALUE</code> if an image was
     *         successfully set, <code>IMAGE_SETTING_FAILURE_VALUE</code>
     *         otherwise
     * @throws DAOException if there are problems with setting an image
     * @see by.training.task05.dao.UserDAO#IMAGE_SETTING_FAILURE_VALUE
     * @see by.training.task05.dao.UserDAO#IMAGE_SETTING_SUCCESS_VALUE
     */
    @Override
    public int setImage(final String login, final InputStream image)
            throws DAOException {
        Connection connection = null;
        ConnectionPool pool;
        PreparedStatement preparedStatement;
        int isolationLevel = TRANSACTION_READ_UNCOMMITTED;
        int result;
        /* The main part of the method. */
        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(isolationLevel);
            /* An attempt to retrieve the user. */
            preparedStatement
                    = connection.prepareStatement(IMAGE_SETTING_STMT);
            try {
                preparedStatement.setBlob(PARAMETER_NUMBER_1, image);
                preparedStatement.setString(PARAMETER_NUMBER_2,
                        convertMessage(login));
                result = preparedStatement.executeUpdate();
            } finally {
                closeStatement(preparedStatement);
            }
            connection.commit();
        } catch (SQLException | PoolException | NoSuchAlgorithmException e) {
            if (connection != null) {
                rollbackConnection(connection);
            }
            throw new DAOException("Can't set an image to a user with a "
                    + "login: " + login + ".", e);
        } finally {
            returnConnection(connection);
        }
        return result;
    }
    /**
     * Retrieves a user from a <code>ResultSet</code> object.
     * @param resultSet a <code>ResultSet</code> value which is a table of data
     *                  to be parsed
     * @return a user from a <code>ResultSet</code> object. Can't be a
     *         <code>null</code> value.
     * @throws SQLException if there are problems with retrieving a user
     *                      from a <code>ResultSet</code> object
     */
    private User parseUser(final ResultSet resultSet) throws SQLException {
        boolean blockedState = resultSet.getBoolean(PARAMETER_NUMBER_3);
        long userID = resultSet.getLong(PARAMETER_NUMBER_1);
        Role userRole = Role.valueOf(resultSet.getString(PARAMETER_NUMBER_2));
        /* The main part of the method. */
        return (new User(blockedState, userID, userRole, ""));
    }
}
