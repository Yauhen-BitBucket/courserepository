/*
 * SQLHeadPhysicianDAO.java
 * A head physician DAO implementation which is based on an SQL.
 */
package by.training.task05.dao.implementation;


import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_1;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_2;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_3;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_4;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_5;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_6;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_7;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_8;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_9;
import static by.training.task05.dao.implementation.ParameterNumbersKeeper
        .PARAMETER_NUMBER_10;

import static by.training.task05.dao.implementation.SQLStatementsKeeper
        .BLOCKED_STATE_SETTING_STMT;
import static by.training.task05.dao.implementation.SQLStatementsKeeper
        .PATIENT_ADDING_STMT;
import static by.training.task05.dao.implementation.SQLStatementsKeeper
        .PERSONAL_DATA_ADDING_STMT;
import static by.training.task05.dao.implementation.SQLStatementsKeeper
        .SERVICE_INFO_RETRIEVING_STMT;
import static by.training.task05.dao.implementation.SQLStatementsKeeper
        .USED_TICKET_RETRIEVING_STMT;
import static by.training.task05.dao.implementation.SQLStatementsKeeper
        .USERS_RETRIEVING_STMT;

import static by.training.task05.dao.implementation.SupportMethodsKeeper
        .closeResultSet;
import static by.training.task05.dao.implementation.SupportMethodsKeeper
        .closeStatement;
import static by.training.task05.dao.implementation.SupportMethodsKeeper
        .convertMessage;
import static by.training.task05.dao.implementation.SupportMethodsKeeper
        .returnConnection;
import static by.training.task05.dao.implementation.SupportMethodsKeeper
        .rollbackConnection;

import static java.sql.Connection.TRANSACTION_READ_COMMITTED;

import by.training.task05.dao.DAOException;
import by.training.task05.dao.HeadPhysicianDAO;

import by.training.task05.entity.Person;
import by.training.task05.entity.Physician;
import by.training.task05.entity.Speciality;
import by.training.task05.entity.Ticket;

import by.training.task05.pool.ConnectionPool;
import by.training.task05.pool.PoolException;

import java.security.NoSuchAlgorithmException;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;
import java.util.HashMap;


/**
 * A head physician DAO implementation which is based on an SQL.
 * @author Yauhen Sazonau
 * @version 1.0, 06/25/19
 * @since 1.0
 */
public class SQLHeadPhysicianDAO implements HeadPhysicianDAO {
    /**
     * Retrieves used tickets. The date of each ticket falls into a specified
     * range. This time period should precede the current date.
     * @param startingPoint a <code>Date</code> value which is the starting
     *                      point of a period
     * @param endingPoint a <code>Date</code> value which is the ending
     *                    point of a period
     * @return used tickets. If there are no such tickets, an empty
     *         <code>Collection</code> is returned.
     * @throws DAOException if there are problems with retrieving used tickets
     */
    @Override
    public Collection<Ticket> retrieveUsedTickets(final Date startingPoint,
            final Date endingPoint) throws DAOException {
        Collection<Ticket> tickets = new LinkedList<>();
        Connection connection = null;
        ConnectionPool pool;
        Person patient;
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        Ticket ticket;
        int isolationLevel = TRANSACTION_READ_COMMITTED;
        /* The main part of the method. */
        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(isolationLevel);
            /* An attempt to retrieve used tickets. */
            preparedStatement
                    = connection.prepareStatement(USED_TICKET_RETRIEVING_STMT);
            try {
                preparedStatement.setDate(PARAMETER_NUMBER_1, startingPoint);
                preparedStatement.setDate(PARAMETER_NUMBER_2, endingPoint);
                preparedStatement.setDate(PARAMETER_NUMBER_3, endingPoint);
                resultSet = preparedStatement.executeQuery();
                try {
                    while (resultSet.next()) {
                        patient = parsePatient(resultSet);
                        ticket = parseTicket(resultSet);
                        ticket.setPatient(patient);
                        tickets.add(ticket);
                    }
                } finally {
                    closeResultSet(resultSet);
                }
            } finally {
                closeStatement(preparedStatement);
            }
            connection.commit();
        } catch (SQLException | PoolException e) {
            if (connection != null) {
                rollbackConnection(connection);
            }
            throw new DAOException("Can't retrieve used tickets which were "
                    + "used in a period from " + startingPoint + " to "
                    + endingPoint + ".", e);
        } finally {
            returnConnection(connection);
        }
        return tickets;
    }
    /**
     * Retrieves all users which are not head physicians.
     * @return all users which are not head physicians. If there are no such
     *         users, an empty <code>Collection</code> is returned.
     * @throws DAOException if there are problems with retrieving all users
     *                      which are not head physicians
     */
    @Override
    public Collection<Person> retrieveUsers() throws DAOException {
        Collection<Person> users = new LinkedList<>();
        Connection connection = null;
        ConnectionPool pool;
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        int isolationLevel = TRANSACTION_READ_COMMITTED;
        /* The main part of the method. */
        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(isolationLevel);
            /* An attempt to retrieve users which are not head physicians. */
            preparedStatement
                    = connection.prepareStatement(USERS_RETRIEVING_STMT);
            try {
                resultSet = preparedStatement.executeQuery();
                try {
                    while (resultSet.next()) {
                        users.add(parsePatient(resultSet));
                    }
                } finally {
                    closeResultSet(resultSet);
                }
            } finally {
                closeStatement(preparedStatement);
            }
            connection.commit();
        } catch (SQLException | PoolException e) {
            if (connection != null) {
                rollbackConnection(connection);
            }
            throw new DAOException("Can't retrieve users which are not head "
                    + "physicians.", e);
        } finally {
            returnConnection(connection);
        }
        return users;
    }
    /**
     * Adds a new patient to the system.
     * @param passportNumber a <code>String</code> value which is a passport
     *                       number
     * @param firstName a <code>String</code> value which is a first name
     * @param lastName a <code>String</code> value which is a last name
     * @param login a <code>String</code> value which is a login
     * @param password a <code>String</code> value which is a password
     * @return <code>PATIENT_ADDING_SUCCESS_VALUE</code> if a patient was
     *         successfully added, <code>PATIENT_ADDING_FAILURE_VALUE</code>
     *         otherwise
     * @throws DAOException if there are problems with adding a new patient to
     *                      to the system
     * @see by.training.task05.dao.HeadPhysicianDAO
     *      #PATIENT_ADDING_FAILURE_VALUE
     * @see by.training.task05.dao.HeadPhysicianDAO
     *      #PATIENT_ADDING_FAILURE_VALUE
     */
    @Override
    public int addPatient(final String passportNumber, final String firstName,
            final String lastName, final String login,
            final String password) throws DAOException {
        Connection connection = null;
        ConnectionPool pool;
        PreparedStatement dataAddingStmt;
        PreparedStatement patientAddingStmt;
        int isolationLevel = TRANSACTION_READ_COMMITTED;
        int result = PATIENT_ADDING_SUCCESS_VALUE;
        /* The main part of the method. */
        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(isolationLevel);
            /* An attempt to add a patient. */
            dataAddingStmt
                    = connection.prepareStatement(PERSONAL_DATA_ADDING_STMT);
            try {
                patientAddingStmt
                        = connection.prepareStatement(PATIENT_ADDING_STMT);
                try {
                    setParametersForDataAdding(dataAddingStmt, passportNumber,
                            firstName, lastName);
                    setParametersForPatientAdding(patientAddingStmt,
                            convertMessage(login), convertMessage(password),
                            passportNumber);
                    dataAddingStmt.executeUpdate();
                    result = patientAddingStmt.executeUpdate();
                    if (result == PATIENT_ADDING_FAILURE_VALUE) {
                        connection.rollback();
                    }
                } finally {
                    closeStatement(patientAddingStmt);
                }
            } finally {
                closeStatement(dataAddingStmt);
            }
            connection.commit();
        } catch (SQLException | PoolException | NoSuchAlgorithmException e) {
            if (connection != null) {
                rollbackConnection(connection);
            }
            throw new DAOException("Can't add a patient with a passport "
                    + "number: " + passportNumber + ", login: " + login
                    + " and password: " + password + ".", e);
        } finally {
            returnConnection(connection);
        }
        return result;
    }
    /**
     * Sets the blocked state of a user with a specified passport number to a
     * specified value.
     * @param passportNumber a <code>String</code> value which is a passport
     *                       number
     * @param blockedState a <code>boolean</code> value which is a blocked
     *                     state to be set
     * @return <code>BLCOOKED_STATE_SETING_SUCCESS_VALUE</code> if a blocked
     *         state was successfully set,
     *         <code>BLCOOKED_STATE_SETING_FAILURE_VALUE</code> otherwise
     * @throws DAOException if there are problems with setting a blocked state
     * @see by.training.task05.dao.HeadPhysicianDAO
     *      #BLCOOKED_STATE_SETING_FAILURE_VALUE
     * @see by.training.task05.dao.HeadPhysicianDAO
     *      #BLCOOKED_STATE_SETING_SUCCESS_VALUE
     */
    @Override
    public int setBlockedState(final String passportNumber,
            final boolean blockedState) throws DAOException {
        Connection connection = null;
        ConnectionPool pool;
        PreparedStatement preparedStatement;
        int isolationLevel = TRANSACTION_READ_COMMITTED;
        int result = BLCOOKED_STATE_SETING_SUCCESS_VALUE;
        /* The main part of the method. */
        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(isolationLevel);
            /* An attempt to set a blocked state. */
            preparedStatement
                    = connection.prepareStatement(BLOCKED_STATE_SETTING_STMT);
            try {
                preparedStatement.setBoolean(PARAMETER_NUMBER_1, blockedState);
                preparedStatement.setString(PARAMETER_NUMBER_2,
                        passportNumber);
                result = preparedStatement.executeUpdate();
            } finally {
                closeStatement(preparedStatement);
            }
            connection.commit();
        } catch (SQLException | PoolException e) {
            if (connection != null) {
                rollbackConnection(connection);
            }
            throw new DAOException("Can't set a blocked state to a person "
                    + "with a passport number: " + passportNumber + ".", e);
        } finally {
            returnConnection(connection);
        }
        return result;
    }
    /**
     * Retrieves information about how many patients were served by each
     * physician in a specified year and month.
     * @param year an <code>int</code> value which is a desired year
     * @param month an <code>int</code> value which is a desired month.
     *              '1' corresponds to January, '12' corresponds to December.
     * @return a map with keys of the <code>Physician</code> type and values
     *         of the <code>Integer</code> type
     * @throws DAOException if there are problems with retrieving information
     *                      about how many patients were served by each
     *                      physician in a specified year and month
     * @see by.training.task05.entity.Physician
     */
    @Override
    public Map<Physician, Integer> retrieveServiceInfo(final int year,
            final int month) throws DAOException {
        Map<Physician, Integer> info = new HashMap<>();
        Connection connection = null;
        ConnectionPool pool;
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        int isolationLevel = TRANSACTION_READ_COMMITTED;
        /* The main part of the method. */
        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(isolationLevel);
            /* An attempt to retrieve information. */
            preparedStatement = connection.prepareStatement(
                    SERVICE_INFO_RETRIEVING_STMT);
            try {
                preparedStatement.setInt(PARAMETER_NUMBER_1, year);
                preparedStatement.setInt(PARAMETER_NUMBER_2, month);
                resultSet = preparedStatement.executeQuery();
                try {
                    while (resultSet.next()) {
                        info.put(parsePhysician(resultSet),
                                resultSet.getInt(PARAMETER_NUMBER_10));
                    }
                } finally {
                    closeResultSet(resultSet);
                }
            } finally {
                closeStatement(preparedStatement);
            }
            connection.commit();
        } catch (SQLException | PoolException e) {
            if (connection != null) {
                rollbackConnection(connection);
            }
            throw new DAOException("Can't retrieve information about how many "
                    + "patients were served by each physician in a year: "
                    + year + " and month: " + month + ".", e);
        } finally {
            returnConnection(connection);
        }
        return info;
    }
    /**
     * Retrieves a physician from a <code>ResultSet</code> object.
     * @param resultSet a <code>ResultSet</code> value which is a table of data
     *                  to be parsed
     * @return a physician from a <code>ResultSet</code> object. Can't be a
     *         <code>null</code> value.
     * @throws SQLException if there are problems with retrieving a physician
     *                      from a <code>ResultSet</code> object
     */
    private Physician parsePhysician(final ResultSet resultSet)
            throws SQLException {
        Physician physician = new Physician();
        /* The main part of the method. */
        physician.setPassportNumber(
                resultSet.getString(PARAMETER_NUMBER_1));
        physician.setFirstName(
                resultSet.getString(PARAMETER_NUMBER_2));
        physician.setLastName(
                resultSet.getString(PARAMETER_NUMBER_3));
        physician.setSpeciality(
                Speciality.valueOf(
                        resultSet.getString(PARAMETER_NUMBER_4)));
        physician.setRoomNumber(resultSet.getLong(PARAMETER_NUMBER_5));
        physician.setOddStartingTime(
                resultSet.getTime(PARAMETER_NUMBER_6));
        physician.setOddEndingTime(
                resultSet.getTime(PARAMETER_NUMBER_7));
        physician.setEvenStartingTime(
                resultSet.getTime(PARAMETER_NUMBER_8));
        physician.setEvenEndingTime(
                resultSet.getTime(PARAMETER_NUMBER_9));
        return physician;
    }
    /**
     * Retrieves a patient from a <code>ResultSet</code> object.
     * @param resultSet a <code>ResultSet</code> value which is a table of data
     *                  to be parsed
     * @return a patient from a <code>ResultSet</code> object. Can't be a
     *         <code>null</code> value.
     * @throws SQLException if there are problems with retrieving a patient
     *                      from a <code>ResultSet</code> object
     */
    private Person parsePatient(final ResultSet resultSet)
            throws SQLException {
        Person person = new Person();
        /* The main part of the method. */
        person.setPassportNumber(resultSet.getString(PARAMETER_NUMBER_1));
        person.setFirstName(resultSet.getString(PARAMETER_NUMBER_2));
        person.setLastName(resultSet.getString(PARAMETER_NUMBER_3));
        return person;
    }
    /**
     * Retrieves a ticket from a <code>ResultSet</code> object.
     * @param resultSet a <code>ResultSet</code> value which is a table of data
     *                  to be parsed
     * @return a ticket from a <code>ResultSet</code> object. Can't be a
     *         <code>null</code> value.
     * @throws SQLException if there are problems with retrieving a ticket
     *                      from a <code>ResultSet</code> object
     */
    private Ticket parseTicket(final ResultSet resultSet)
            throws SQLException {
        Ticket ticket = new Ticket(resultSet.getLong(PARAMETER_NUMBER_4),
                                   new Physician(), new Person(),
                                   resultSet.getDate(PARAMETER_NUMBER_5),
                                   resultSet.getTime(PARAMETER_NUMBER_6),
                                   resultSet.getTime(PARAMETER_NUMBER_7));
        ticket.setDiagnosis(resultSet.getString(PARAMETER_NUMBER_8));
        ticket.setTreatment(resultSet.getString(PARAMETER_NUMBER_9));
        return ticket;
    }
    /**
     * Sets parameters of a prepared statement for adding personal data.
     * @param statement a <code>PreparedStatement</code> value which is a
     *                  prepared statement to adjust
     * @param passportNumber a <code>String</code> value which is a passport
     *                       number
     * @param firstName a <code>String</code> value which is a first name
     * @param lastName a <code>String</code> value which is a last name
     * @throws SQLException if there are problems with setting parameters
     */
    private void setParametersForDataAdding(
            final PreparedStatement statement, final String passportNumber,
            final String firstName,
            final String lastName) throws SQLException {
        statement.setString(PARAMETER_NUMBER_1, passportNumber);
        statement.setString(PARAMETER_NUMBER_2, firstName);
        statement.setString(PARAMETER_NUMBER_3, lastName);
        statement.setString(PARAMETER_NUMBER_4, passportNumber);
    }
    /**
     * Sets parameters of a prepared statement for adding a patient.
     * @param statement a <code>PreparedStatement</code> value which is a
     *                  prepared statement to adjust
     * @param login a <code>String</code> value which is a login
     * @param password a <code>String</code> value which is a password
     * @param passportNumber a <code>String</code> value which is a passport
     *                       number
     * @throws SQLException if there are problems with setting parameters
     */
    private void setParametersForPatientAdding(
            final PreparedStatement statement, final String login,
            final String password,
            final String passportNumber) throws SQLException {
        statement.setString(PARAMETER_NUMBER_1, login);
        statement.setString(PARAMETER_NUMBER_2, password);
        statement.setString(PARAMETER_NUMBER_3, passportNumber);
        statement.setString(PARAMETER_NUMBER_4, login);
        statement.setString(PARAMETER_NUMBER_5, passportNumber);
    }
}
