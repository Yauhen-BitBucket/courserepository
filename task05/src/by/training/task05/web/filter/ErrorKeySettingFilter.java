/*
 * ErrorKeySettingFilter.java
 * A filter which will be executed on each invocation of the error page using
 * the error page mechanism. Its main purpose is to set the appropriate error
 * information key to be used inside the error page.
 */
package by.training.task05.web.filter;


import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_FORBIDDEN;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_NOT_FOUND;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


/**
 * A filter which will be executed on each invocation of the error page using
 * the error page mechanism. Its main purpose is to set the appropriate error
 * information key to be used inside the error page.
 * @author Yauhen Sazonau
 * @version 1.0, 07/11/19
 * @since 1.0
 */
public class ErrorKeySettingFilter implements Filter {
    /**
     * The name of a property which retains the information telling a user
     * that he has tried to access a resource that is absent. The identifier
     * holds the value of "resource.absence.error.message".
     */
    public static final String ERROR_INFORMATION_1_KEY
            = "resource.absence.error.message";
    /**
     * The name of a property which retains the information telling a user
     * that his request is syntactically incorrect. The identifier holds the
     * value of "incorrect.request.error.message".
     */
    public static final String ERROR_INFORMATION_2_KEY
            = "incorrect.request.error.message";
    /**
     * The name of a property which retains the information telling a user
     * that his request can't be processed due to technical causes. The
     * identifier holds the value of "internal.problem.error.message".
     */
    public static final String ERROR_INFORMATION_3_KEY
            = "internal.problem.error.message";
    /**
     * The name of a property which retains the information telling a user
     * that he is blocked. The identifier holds the value of
     * "user.blocked.error.message".
     */
    public static final String ERROR_INFORMATION_4_KEY
            = "user.blocked.error.message";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>String</code> class that is the key of error information. The
     * identifier holds the value of "errorInformationKey".
     */
    private static final String ERROR_INFORMATION_KEY_ATTRIBUTE_NAME
            = "errorInformationKey";
    /**
     * The name of a request attribute set after an error arisen to retain
     * the status code. The identifier holds the value of
     * "javax.servlet.error.status_code".
     */
    private static final String STATUS_CODE_ATTRIBUTE_NAME
            = "javax.servlet.error.status_code";
    /**
     * Does nothing. Called by a web container to indicate to this filter
     * that it is being taken out of service.
     */
    @Override
    public void destroy() {
        /* Does nothing. */
    }
    /**
     * Sets the appropriate error information key to be used inside the error
     * page.
     * @param request a <code>ServletRequest</code> value which is a
     *                request to get a resource
     * @param response a <code>ServletResponse</code> value which is a
     *                 response to a requested resource
     * @param chain a <code>FilterChain</code> value which is an object
     *              provided by a servlet container to this filter giving a
     *              view into the invocation chain of the resource
     */
    @Override
    public void doFilter(final ServletRequest request,
            final ServletResponse response,
            final FilterChain chain) throws IOException, ServletException {
        HttpSession session = ((HttpServletRequest) request).getSession();
        String errorInformationKey = null;
        int statusCode = (int) request.getAttribute(
                STATUS_CODE_ATTRIBUTE_NAME);
        /* The main part of the method. */
        if (statusCode == SC_BAD_REQUEST) {
            errorInformationKey = ERROR_INFORMATION_2_KEY;
        } else if (statusCode == SC_NOT_FOUND) {
            errorInformationKey = ERROR_INFORMATION_1_KEY;
        } else if (statusCode == SC_INTERNAL_SERVER_ERROR) {
            errorInformationKey = ERROR_INFORMATION_3_KEY;
        } else if (statusCode == SC_FORBIDDEN) {
            errorInformationKey = ERROR_INFORMATION_4_KEY;
        }
        session.setAttribute(ERROR_INFORMATION_KEY_ATTRIBUTE_NAME,
                errorInformationKey);
        chain.doFilter(request, response);
    }
    /**
     * Does nothing. Called by a web container to indicate to this filter that
     * it is being placed into service.
     * @param filterConfig a <code>FilterConfig</code> value which is a
     *                     filter configuration object used by a servlet
     *                     container to pass the information to this filter
     *                     during the initialization
     */
    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
        /* Does nothing. */
    }
}
