/**
 * Provides filters of an application and some auxiliary classes that are used
 * by the filters.
 * @author Yauhen Sazonau
 * @since 1.0
 */
package by.training.task05.web.filter;
