/*
 * AuthorizationFilter.java
 * Represents a filter which will be executed on each user's direct request
 * to an application. Its main purpose is to verify whether a user is allowed
 * to get a requested resource(the speech is not about syntactical
 * correctness of a request).
 */
package by.training.task05.web.filter;


import static by.training.task05.reader.ApplicationSettingsReader
        .AUTHENTICATION_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .CONTROLLER_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .ERROR_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .IMAGE_FOLDER_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .IMAGE_SETTING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .MAIN_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .RESPONSE_MESSAGE_VIEWING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .STYLE_FOLDER_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .getInstance;

import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_1;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_2;

import static by.training.task05.web.listener.ContextListener
        .convertCommandName;

import static javax.servlet.http.HttpServletResponse.SC_FORBIDDEN;

import by.training.task05.entity.User;

import by.training.task05.reader.ApplicationSettingsReader;

import by.training.task05.web.command.CommandName;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Represents a filter which will be executed on each user's direct request
 * to an application. Its main purpose is to verify whether a user is allowed
 * to get a requested resource(the speech is not about syntactical
 * correctness of a request).
 * @author Yauhen Sazonau
 * @version 1.0, 07/07/19
 * @since 1.0
 */
public class AuthorizationFilter implements Filter {
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>by.training.task05.entity.User</code> class. The identifier
     * holds the value of "authenticatedUser".
     */
    private static final String AUTHENTICATED_USER_ATTRIBUTE_NAME
            = "authenticatedUser";
    /**
     * Does nothing. Called by a web container to indicate to this filter
     * that it is being taken out of service.
     */
    @Override
    public void destroy() {
        /* Does nothing. */
    }
    /**
     * Verifies whether a user is allowed to get a requested resource(the
     * speech is not about syntactical correctness of a request).
     * @param request a <code>ServletRequest</code> value which is a
     *                request to get a resource
     * @param response a <code>ServletResponse</code> value which is a
     *                 response to a requested resource
     * @param chain a <code>FilterChain</code> value which is an object
     *              provided by a servlet container to this filter giving a
     *              view into the invocation chain of the resource
     * @throws IOException if an input or output problem occurs during the
     *                     filtering process
     * @throws ServletException if there are problems during the filtering
     *                          process
     */
    @Override
    public void doFilter(final ServletRequest request,
            final ServletResponse response,
            final FilterChain chain) throws IOException, ServletException {
        ApplicationSettingsReader reader = getInstance();
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        HttpSession session = httpRequest.getSession();
        String redirectionPath;
        User user = (User) session.getAttribute(
                AUTHENTICATED_USER_ATTRIBUTE_NAME);
        boolean isProtectedRequest = checkRequestProtection(httpRequest);
        /* The main part of the method. */
        if ((!isProtectedRequest) || (user != null)) {
            if ((user != null) && (user.getBlockedState())
                    && (checkBlockedState(httpRequest))) {
                httpResponse.sendError(SC_FORBIDDEN);
            } else {
                chain.doFilter(httpRequest, response);
            }
        } else {
            redirectionPath = httpResponse.encodeRedirectURL(
                    httpRequest.getContextPath()
                    + reader.getProperty(AUTHENTICATION_PAGE_PATH_KEY));
            httpResponse.sendRedirect(redirectionPath);
        }
    }
    /**
     * Does nothing. Called by a web container to indicate to this filter that
     * it is being placed into service.
     * @param filterConfig a <code>FilterConfig</code> value which is a
     *                     filter configuration object used by a servlet
     *                     container to pass the information to this filter
     *                     during the initialization
     */
    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
        /* Does nothing. */
    }
    /**
     * Verifies if a request must be blocked.
     * @param request a <code>ServletRequest</code> value which is a
     *                request to get a resource
     * @return <code>true</code> if a request must be blocked,
     *         <code>false</code> otherwise
     */
    private boolean checkBlockedState(final ServletRequest request) {
        ApplicationSettingsReader reader = getInstance();
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        ParameterDataKeeper keeper = ParameterDataKeeper.getInstance();
        String commandName;
        String controllerPath = reader.getProperty(CONTROLLER_PATH_KEY);
        String imageFolderPath = reader.getProperty(IMAGE_FOLDER_PATH_KEY);
        String pathPropertyKey = request.getParameter(PARAMETER_NAME_2);
        String styleFolderPath = reader.getProperty(STYLE_FOLDER_PATH_KEY);
        String servletPath = httpRequest.getServletPath();
        boolean isControllerPath;
        boolean isImageFetchingCommand = false;
        boolean isLocaleChangingCommand = false;
        boolean isLogoutCommand = false;
        boolean isImageFolderPath;
        boolean isStyleFolderPath;
        /* The main part of the method. */
        isControllerPath = controllerPath.equals(servletPath);
        isImageFolderPath = servletPath.indexOf(imageFolderPath) == 0;
        isStyleFolderPath = servletPath.indexOf(styleFolderPath) == 0;
        if (isControllerPath) {
            commandName = request.getParameter(PARAMETER_NAME_1);
            isLocaleChangingCommand
                    = convertCommandName(CommandName.LOCALE_CHANGING_COMMAND)
                      .equals(commandName);
            isLogoutCommand
                    = convertCommandName(CommandName.LOGOUT_COMMAND)
                      .equals(commandName);
            isImageFetchingCommand
                    = convertCommandName(CommandName.IMAGE_FETCHING_COMMAND)
                      .equals(commandName);
        }
        return (!((keeper.checkErrorPagePath(servletPath))
                  || (isImageFolderPath) || (isStyleFolderPath)
                  || ((isControllerPath)
                      && ((isLogoutCommand)
                          || (isImageFetchingCommand)
                          || ((isLocaleChangingCommand)
                              && (pathPropertyKey
                                  .equals(ERROR_PAGE_PATH_KEY)))))));
    }
    /**
     * Verifies if a request is protected.
     * @param request a <code>ServletRequest</code> value which is a
     *                request to get a resource
     * @return <code>true</code> if a request is protected, <code>false</code>
     *         otherwise
     */
    private boolean checkRequestProtection(final ServletRequest request) {
        ApplicationSettingsReader reader = getInstance();
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        ParameterDataKeeper keeper = ParameterDataKeeper.getInstance();
        String commandName;
        String controllerPath = reader.getProperty(CONTROLLER_PATH_KEY);
        String servletPath = httpRequest.getServletPath();
        boolean isControllerPath;
        boolean isProtectedCommand = true;
        /* The main part of the method. */
        isControllerPath = controllerPath.equals(servletPath);
        if (isControllerPath) {
            commandName = request.getParameter(PARAMETER_NAME_1);
            isProtectedCommand
                    = (!convertCommandName(CommandName.AUTHENTICATION_COMMAND)
                        .equals(commandName))
                      && (!convertCommandName(
                              CommandName.LOCALE_CHANGING_COMMAND)
                           .equals(commandName))
                      && (!convertCommandName(
                              CommandName.MESSAGE_FETCHING_COMMAND)
                           .equals(commandName));
        }
        return (((isControllerPath) && (isProtectedCommand))
                || (servletPath.equals(reader.getProperty(MAIN_PAGE_PATH_KEY)))
                || (servletPath.equals(reader.getProperty(
                            RESPONSE_MESSAGE_VIEWING_PAGE_PATH_KEY)))
                || (servletPath.equals(reader.getProperty(
                            IMAGE_SETTING_PAGE_PATH_KEY)))
                || (keeper.checkPatientPath(servletPath))
                || (keeper.checkPhysicianPath(servletPath))
                || (keeper.checkHeadPhysicianPath(servletPath)));
    }
}
