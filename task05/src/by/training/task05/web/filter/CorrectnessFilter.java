/*
 * CorrectnessFilter.java
 * A filter which main purpose is to verify whether a user is allowed to get a
 * requested resource using the information contained inside the request.
 */
package by.training.task05.web.filter;


import static by.training.task05.reader.ApplicationSettingsReader
        .ASSIGNED_TICKET_VIEWING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .AUTHENTICATION_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .CONTROLLER_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .DOCTOR_VIEWING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .ERROR_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .FREE_TICKET_VIEWING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .IMAGE_FOLDER_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .OWN_TICKET_VIEWING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .RESPONSE_MESSAGE_VIEWING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .SCRIPT_FOLDER_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .SERVICE_INFO_VIEWING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .STYLE_FOLDER_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .USED_TICKET_VIEWING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .USER_VIEWING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .WELCOME_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .getInstance;

import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_1;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_2;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_5;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_6;

import static by.training.task05.web.listener.ContextListener
        .convertCommandName;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

import by.training.task05.entity.Role;
import by.training.task05.entity.User;

import by.training.task05.reader.ApplicationSettingsReader;

import by.training.task05.web.command.CommandName;

import java.io.IOException;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * A filter which main purpose is to verify whether a user is allowed to get a
 * requested resource using the information contained inside the request.
 * @author Yauhen Sazonau
 * @version 1.0, 07/07/19
 * @since 1.0
 */
public class CorrectnessFilter implements Filter {
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>java.util.Collection</code> interface that is a collection of
     * assigned tickets. The identifier holds the value of "assignedTickets".
     */
    private static final String ASSIGNED_TICKETS_ATTRIBUTE_NAME
            = "assignedTickets";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>by.training.task05.entity.User</code> class. The identifier
     * holds the value of "authenticatedUser".
     */
    private static final String AUTHENTICATED_USER_ATTRIBUTE_NAME
            = "authenticatedUser";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>java.util.Collection</code> interface that is a collection of
     * doctors. The identifier holds the value of "doctors".
     */
    private static final String DOCTORS_ATTRIBUTE_NAME
            = "doctors";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>String</code> class that is the key of error information. The
     * identifier holds the value of "errorInformationKey".
     */
    private static final String ERROR_INFORMATION_KEY_ATTRIBUTE_NAME
            = "errorInformationKey";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>java.util.Map</code> interface. The identifier holds the value
     * of "errorMessages".
     */
    private static final String ERROR_MESSAGES_ATTRIBUTE_NAME
            = "errorMessages";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>java.util.Collection</code> interface that is a collection of
     * free tickets. The identifier holds the value of "freeTickets".
     */
    private static final String FREE_TICKETS_ATTRIBUTE_NAME
            = "freeTickets";
    /**
     * A logger for reporting errors. <code>null</code> value is prohibited.
     */
    private static final Logger LOGGER
            = LogManager.getLogger(CorrectnessFilter.class);
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>java.util.Collection</code> interface that is a collection of
     * own tickets. The identifier holds the value of "ownTickets".
     */
    private static final String OWN_TICKETS_ATTRIBUTE_NAME
            = "ownTickets";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>String</code> class that is the key of a property of a
     * response message. The identifier holds the value of
     * "responseMessageKey".
     */
    private static final String RESPONSE_MESSAGE_KEY_ATTRIBUTE_NAME
            = "responseMessageKey";
    /**
     * The name of a context attribute which is used to retain an instance of
     * the <code>java.lang.Integer</code> class that is the maximum row
     * quantity of some information to display on a page. The identifier
     * holds the value of "rowQuantity".
     */
    private static final String ROW_QUANTITY_ATTRIBUTE_NAME
            = "rowQuantity";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>java.util.Map</code> interface that is a map of physicians to
     * serviced patient count. The identifier holds the value of "serviceInfo".
     */
    private static final String SERVICE_INFO_ATTRIBUTE_NAME
            = "serviceInfo";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>String</code> class that is a target page path. The identifier
     * holds the value of "targetPagePath".
     */
    private static final String TARGET_PAGE_PATH_ATTRIBUTE_NAME
            = "targetPagePath";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>java.util.Collection</code> interface that is a collection of
     * used tickets. The identifier holds the value of "usedTickets".
     */
    private static final String USED_TICKETS_ATTRIBUTE_NAME
            = "usedTickets";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>java.util.Collection</code> interface that is a collection of
     * users. The identifier holds the value of "users".
     */
    private static final String USERS_ATTRIBUTE_NAME
            = "users";
    /**
     * The filter configuration object presented by a servlet container.
     */
    private FilterConfig filterConfig;
    /**
     * A map of target paths to attribute names. Can't be a <code>null</code>
     * value.
     */
    private Map<String, String> targetPathMap = new HashMap<>();
    /**
     * Does nothing. Called by a web container to indicate to this filter
     * that it is being taken out of service.
     */
    @Override
    public void destroy() {
        /* Does nothing. */
    }
    /**
     * Verifies whether a user is allowed to get a requested resource using the
     * information contained inside the request.
     * @param request a <code>ServletRequest</code> value which is a
     *                request to get a resource
     * @param response a <code>ServletResponse</code> value which is a
     *                 response to a requested resource
     * @param chain a <code>FilterChain</code> value which is an object
     *              provided by a servlet container to this filter giving a
     *              view into the invocation chain of the resource
     * @throws IOException if an input or output problem occurs during the
     *                     filtering process
     * @throws ServletException if there are problems during the filtering
     *                          process
     */
    @Override
    public void doFilter(final ServletRequest request,
            final ServletResponse response,
            final FilterChain chain) throws IOException, ServletException {
        ApplicationSettingsReader reader = getInstance();
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpSession session = httpRequest.getSession();
        ParameterDataKeeper keeper = ParameterDataKeeper.getInstance();
        String controllerPath = reader.getProperty(CONTROLLER_PATH_KEY);
        String errorInformationKey
                = (String) session.getAttribute(
                        ERROR_INFORMATION_KEY_ATTRIBUTE_NAME);
        String servletPath = httpRequest.getServletPath();
        User user = (User) session.getAttribute(
                AUTHENTICATED_USER_ATTRIBUTE_NAME);
        boolean isControllerPath;
        boolean requestCorrectness = true;
        /* The main part of the method. */
        isControllerPath = controllerPath.equals(servletPath);
        if (isControllerPath) {
            requestCorrectness = checkParameterPresence(request);
        }
        if ((requestCorrectness) && (user == null) && (isControllerPath)) {
            requestCorrectness = !checkUnauthenticatedRequest(request);
        }
        if ((requestCorrectness) && (user != null)) {
            requestCorrectness = !checkAuthenticatedRequest(httpRequest, user);
        }
        if ((requestCorrectness) && (user != null)) {
            requestCorrectness = !checkRequestedPage(request, user);
        }
        if ((user == null) && (keeper.checkErrorPagePath(servletPath))
                && (errorInformationKey == null)) {
            requestCorrectness = false;
        }
        doCleanup(requestCorrectness, httpRequest);
        if (requestCorrectness) {
            try {
                chain.doFilter(request, response);
            } catch (IOException | RuntimeException | ServletException e) {
                /* If the request processing wasn't successful, report it. */
                LOGGER.error("A problem occured during request processing.",
                        e);
                ((HttpServletResponse) response).sendError(
                        SC_INTERNAL_SERVER_ERROR);
            }
        } else {
            ((HttpServletResponse) response).sendError(SC_BAD_REQUEST);
        }
    }
    /**
     * Checks if assigned ticket data viewing isn't allowed.
     * @param request a <code>ServletRequest</code> value that is a
     *                request to get a page
     * @return <code>true</code> value if assigned ticket data viewing isn't
     *         allowed, <code>false</code> otherwise
     */
    private boolean checkAssignedTicketDataViewing(
            final ServletRequest request) {
        ApplicationSettingsReader reader = getInstance();
        Collection<?> data;
        HttpSession session = ((HttpServletRequest) request).getSession();
        String servletPath = ((HttpServletRequest) request).getServletPath();
        /* The main part of the method. */
        data = (Collection<?>) session.getAttribute(
                ASSIGNED_TICKETS_ATTRIBUTE_NAME);
        return ((servletPath
                 .equals(reader
                         .getProperty(ASSIGNED_TICKET_VIEWING_PAGE_PATH_KEY)))
                && ((data == null)
                    || ((!data.isEmpty())
                        && (!checkPageValue(data, request)))));
    }
    /**
     * Verifies whether a specified request object contains the following
     * request parameters: a command name with the value known to the
     * controller; request parameters required by the command(some of them
     * must have correct values(the speciality, the path key parameters)).
     * @param request a <code>ServletRequest</code> value that is a
     *                request to get a resource
     * @return <code>true</code> value if a specified request object
     *         contains required parameters and some of them have correct
     *         values(the speciality, the path key parameters),
     *         <code>false</code> otherwise
     */
    private boolean checkParameterPresence(final ServletRequest request) {
        CommandNamesKeeper namesKeeper = CommandNamesKeeper.getInstance();
        ParameterDataKeeper dataKeeper = ParameterDataKeeper.getInstance();
        Set<String> requiredParameters;
        String commandName = request.getParameter(PARAMETER_NAME_1);
        String parameterValue;
        boolean result = true;
        /* The main part of the method. */
        if ((commandName == null)
                || (!namesKeeper.checkCommandNameExistence(commandName))) {
            result = false;
        } else {
            requiredParameters = dataKeeper.getParameterNames(commandName);
            for (String parameterName : requiredParameters) {
                parameterValue = request.getParameter(parameterName);
                if ((parameterValue == null)
                        || ((PARAMETER_NAME_2.equals(parameterName))
                            && (!dataKeeper.checkPathKeyValue(parameterValue)))
                        || ((PARAMETER_NAME_6.equals(parameterName))
                            && (!dataKeeper
                                 .checkSpecialityValue(parameterValue)))) {
                    result = false;
                    break;
                }
            }
        }
        return result;
    }
    /**
     * Verifies whether a user's request is to get the authentication or
     * welcome page; whether a user asks for an application controller to
     * execute a command which is not permitted for him; whether a user asks
     * for an application controller to change the language of the
     * authentication, welcome or error page(if an error hasn't previously
     * occurred); whether a user asks for an application controller to
     * authenticate him.
     * @param request a <code>ServletRequest</code> value that is a
     *                request to get a resource
     * @param user a <code>User</code> value that is an authenticated user
     * @return <code>true</code> value if a user's request is to get the
     *         authentication or welcome page; if a user asks for an
     *         application controller to execute a command which is not
     *         permitted for him; if a user asks for an application controller
     *         to change the language of the authentication, welcome or error
     *         page(if an error hasn't previously occurred); if a user asks
     *         for an application controller to authenticate him,
     *         <code>false</code> otherwise
     */
    private boolean checkAuthenticatedRequest(final ServletRequest request,
            final User user) {
        ApplicationSettingsReader reader = getInstance();
        CommandNamesKeeper keeper = CommandNamesKeeper.getInstance();
        HttpSession session = ((HttpServletRequest) request).getSession();
        Role role = user.getRole();
        String commandName = request.getParameter(PARAMETER_NAME_1);
        String errorInformationKey
                = (String) (session.getAttribute(
                        ERROR_INFORMATION_KEY_ATTRIBUTE_NAME));
        String pathPropertyKey = request.getParameter(PARAMETER_NAME_2);
        String servletPath = ((HttpServletRequest) request).getServletPath();
        boolean isControllerPath;
        /* The main part of the method. */
        isControllerPath = servletPath.equals(
                reader.getProperty(CONTROLLER_PATH_KEY));
        return ((servletPath
                 .equals(reader.getProperty(AUTHENTICATION_PAGE_PATH_KEY)))
                || (servletPath
                    .equals(reader.getProperty(WELCOME_PAGE_PATH_KEY)))
                || ((isControllerPath)
                    && (!((keeper.checkSharedCommandName(commandName))
                          || ((role.equals(Role.PATIENT))
                              && (keeper.checkPatientCommandName(commandName)))
                          || ((role.equals(Role.PHYSICIAN))
                              && (keeper
                                  .checkPhysicianCommandName(commandName)))
                          || ((role.equals(Role.HEAD_PHYSICIAN))
                              && (keeper
                                  .checkHeadPhysicianCommandName(
                                          commandName))))))
                || ((isControllerPath)
                    && (convertCommandName(
                            CommandName
                            .LOCALE_CHANGING_COMMAND).equals(commandName))
                    && ((pathPropertyKey.equals(AUTHENTICATION_PAGE_PATH_KEY))
                        || (pathPropertyKey.equals(WELCOME_PAGE_PATH_KEY))
                        || ((pathPropertyKey.equals(ERROR_PAGE_PATH_KEY))
                            && (errorInformationKey == null))))
                || ((isControllerPath)
                    && (convertCommandName(
                            CommandName
                            .AUTHENTICATION_COMMAND).equals(commandName))));
    }
    /**
     * Checks if doctor data viewing isn't allowed.
     * @param request a <code>ServletRequest</code> value that is a
     *                request to get a page
     * @return <code>true</code> value if doctor data viewing isn't allowed,
     *         <code>false</code> otherwise
     */
    private boolean checkDoctorDataViewing(final ServletRequest request) {
        ApplicationSettingsReader reader = getInstance();
        Collection<?> data;
        HttpSession session = ((HttpServletRequest) request).getSession();
        String servletPath = ((HttpServletRequest) request).getServletPath();
        /* The main part of the method. */
        data = (Collection<?>) session.getAttribute(DOCTORS_ATTRIBUTE_NAME);
        return ((servletPath
                 .equals(reader.getProperty(DOCTOR_VIEWING_PAGE_PATH_KEY)))
                && ((data == null)
                    || ((!data.isEmpty())
                        && (!checkPageValue(data, request)))));
    }
    /**
     * Checks if free ticket data viewing isn't allowed.
     * @param request a <code>ServletRequest</code> value that is a
     *                request to get a page
     * @return <code>true</code> value if free ticket data viewing isn't
     *         allowed, <code>false</code> otherwise
     */
    private boolean checkFreeTicketDataViewing(final ServletRequest request) {
        ApplicationSettingsReader reader = getInstance();
        Collection<?> data;
        HttpSession session = ((HttpServletRequest) request).getSession();
        String servletPath = ((HttpServletRequest) request).getServletPath();
        /* The main part of the method. */
        data = (Collection<?>) session.getAttribute(
                FREE_TICKETS_ATTRIBUTE_NAME);
        return ((servletPath
                 .equals(reader
                         .getProperty(FREE_TICKET_VIEWING_PAGE_PATH_KEY)))
                && ((data == null)
                    || ((!data.isEmpty())
                        && (!checkPageValue(data, request)))));
    }
    /**
     * Checks if own ticket data viewing isn't allowed.
     * @param request a <code>ServletRequest</code> value that is a
     *                request to get a page
     * @return <code>true</code> value if own ticket data viewing isn't
     *         allowed, <code>false</code> otherwise
     */
    private boolean checkOwnTicketDataViewing(final ServletRequest request) {
        ApplicationSettingsReader reader = getInstance();
        Collection<?> data;
        HttpSession session = ((HttpServletRequest) request).getSession();
        String servletPath = ((HttpServletRequest) request).getServletPath();
        /* The main part of the method. */
        data = (Collection<?>) session.getAttribute(
                OWN_TICKETS_ATTRIBUTE_NAME);
        return ((servletPath
                 .equals(reader.getProperty(OWN_TICKET_VIEWING_PAGE_PATH_KEY)))
                && ((data == null)
                    || ((!data.isEmpty())
                        && (!checkPageValue(data, request)))));
    }
    /**
     * Checks page number correctness.
     * @param container an <code>Object</code> value which is a collection or a
     *                  map
     * @param request a <code>ServletRequest</code> value that is a
     *                request to get a page
     * @return <code>true</code> value if a page number is correct,
     *         <code>false</code> otherwise
     */
    private boolean checkPageValue(final Object container,
            final ServletRequest request) {
        ServletContext context = filterConfig.getServletContext();
        String pageNumberAsString
                = ((HttpServletRequest) request).getParameter(
                        PARAMETER_NAME_5);
        boolean result = true;
        int integerPart;
        int pageNumber = 0;
        int pageQuantity;
        int rowQuantity
                = Integer.parseInt(
                        ((String) (context.getAttribute(
                                ROW_QUANTITY_ATTRIBUTE_NAME))));
        int size = 0;
        /* The main part of the method. */
        if (container instanceof Map) {
            size = ((Map<?, ?>) container).size();
        }
        if (container instanceof Collection) {
            size = ((Collection<?>) container).size();
        }
        if (pageNumberAsString == null) {
            result = false;
        }
        if (result) {
            try {
                pageNumber = Integer.parseInt(pageNumberAsString);
            } catch (NumberFormatException e) {
                result = false;
            }
        }
        if (result) {
            integerPart = size / rowQuantity;
            if (size % rowQuantity == 0) {
                pageQuantity = integerPart;
            } else {
                pageQuantity = integerPart + 1;
            }
            if ((pageNumber < 1) || (pageNumber > pageQuantity)) {
                result = false;
            }
        }
        return result;
    }
    /**
     * Verifies if a requested page is not allowed to be requested.
     * @param request a <code>ServletRequest</code> value that is a
     *                request to get a page
     * @param user a <code>User</code> value that is an authenticated user
     * @return <code>true</code> value if a requested page is not allowed to be
     *         requested
     */
    private boolean checkRequestedPage(final ServletRequest request,
            final User user) {
        HttpSession session = ((HttpServletRequest) request).getSession();
        ParameterDataKeeper keeper = ParameterDataKeeper.getInstance();
        Role role = user.getRole();
        String errorInformationKey
                = (String) session.getAttribute(
                        ERROR_INFORMATION_KEY_ATTRIBUTE_NAME);
        String responseMessageKey
                = (String) session.getAttribute(
                        RESPONSE_MESSAGE_KEY_ATTRIBUTE_NAME);
        String servletPath = ((HttpServletRequest) request).getServletPath();
        /* The main part of the method. */
        return (((role.equals(Role.PHYSICIAN))
                         && ((keeper.checkHeadPhysicianPath(servletPath))
                             || (keeper.checkPatientPath(servletPath))))
                || ((role.equals(Role.PATIENT))
                            && ((keeper.checkHeadPhysicianPath(servletPath))
                                || (keeper.checkPhysicianPath(servletPath))))
                || ((role.equals(Role.HEAD_PHYSICIAN))
                            && ((keeper.checkPhysicianPath(servletPath))
                                || (keeper.checkPatientPath(servletPath))))
                || ((keeper.checkErrorPagePath(servletPath))
                    && (errorInformationKey == null))
                || ((keeper.checkResponsePagePath(servletPath))
                    && (responseMessageKey == null))
                || (checkDoctorDataViewing(request))
                || (checkFreeTicketDataViewing(request))
                || (checkOwnTicketDataViewing(request))
                || (checkAssignedTicketDataViewing(request))
                || (checkUsedTicketDataViewing(request))
                || (checkServiceDataViewing(request))
                || (checkUserDataViewing(request)));
    }
    /**
     * Checks if service data viewing isn't allowed.
     * @param request a <code>ServletRequest</code> value that is a
     *                request to get a page
     * @return <code>true</code> value if service data viewing isn't
     *         allowed, <code>false</code> otherwise
     */
    private boolean checkServiceDataViewing(final ServletRequest request) {
        ApplicationSettingsReader reader = getInstance();
        Map<?, ?> data;
        HttpSession session = ((HttpServletRequest) request).getSession();
        String servletPath = ((HttpServletRequest) request).getServletPath();
        /* The main part of the method. */
        data = (Map<?, ?>) session.getAttribute(
                SERVICE_INFO_ATTRIBUTE_NAME);
        return ((servletPath
                 .equals(reader
                         .getProperty(SERVICE_INFO_VIEWING_PAGE_PATH_KEY)))
                && ((data == null)
                    || ((!data.isEmpty())
                        && (!checkPageValue(data, request)))));
    }
    /**
     * Verifies whether changing a language is fulfilled for a page other than
     * the authentication page, the welcome page or the error page(if an error
     * has previously occurred).
     * @param request a <code>ServletRequest</code> value that is a request to
     *                get a resource
     * @return <code>true</code> if changing a language is fulfilled for a page
     *         other than the authentication page, the welcome page or the
     *         error page(if an error has previously occurred),
     *         <code>false</code> otherwise
     */
    private boolean checkUnauthenticatedRequest(final ServletRequest request) {
        HttpSession session = ((HttpServletRequest) request).getSession();
        String commandName  = request.getParameter(PARAMETER_NAME_1);
        String errorInformationKey
                = (String) (session.getAttribute(
                        ERROR_INFORMATION_KEY_ATTRIBUTE_NAME));
        String pathPropertyKey = request.getParameter(PARAMETER_NAME_2);
        /* The main part of the method. */
        return ((convertCommandName(CommandName.LOCALE_CHANGING_COMMAND)
                 .equals(commandName))
                && (!pathPropertyKey.equals(AUTHENTICATION_PAGE_PATH_KEY))
                && (!pathPropertyKey.equals(WELCOME_PAGE_PATH_KEY))
                && (!((pathPropertyKey.equals(ERROR_PAGE_PATH_KEY))
                      && (errorInformationKey != null))));
    }
    /**
     * Checks if used ticket data viewing isn't allowed.
     * @param request a <code>ServletRequest</code> value that is a
     *                request to get a page
     * @return <code>true</code> value if used ticket data viewing isn't
     *         allowed, <code>false</code> otherwise
     */
    private boolean checkUsedTicketDataViewing(
            final ServletRequest request) {
        ApplicationSettingsReader reader = getInstance();
        Collection<?> data;
        HttpSession session = ((HttpServletRequest) request).getSession();
        String servletPath = ((HttpServletRequest) request).getServletPath();
        /* The main part of the method. */
        data = (Collection<?>) session.getAttribute(
                USED_TICKETS_ATTRIBUTE_NAME);
        return ((servletPath
                 .equals(reader
                         .getProperty(USED_TICKET_VIEWING_PAGE_PATH_KEY)))
                && ((data == null)
                    || ((!data.isEmpty())
                        && (!checkPageValue(data, request)))));
    }
    /**
     * Checks if user data viewing isn't allowed.
     * @param request a <code>ServletRequest</code> value that is a
     *                request to get a page
     * @return <code>true</code> value if user data viewing isn't
     *         allowed, <code>false</code> otherwise
     */
    private boolean checkUserDataViewing(final ServletRequest request) {
        ApplicationSettingsReader reader = getInstance();
        Collection<?> data;
        HttpSession session = ((HttpServletRequest) request).getSession();
        String servletPath = ((HttpServletRequest) request).getServletPath();
        /* The main part of the method. */
        data = (Collection<?>) session.getAttribute(USERS_ATTRIBUTE_NAME);
        return ((servletPath
                 .equals(reader.getProperty(USER_VIEWING_PAGE_PATH_KEY)))
                && ((data == null)
                    || ((!data.isEmpty())
                        && (!checkPageValue(data, request)))));
    }
    /**
     * Deletes information stored in a session.
     * @param requestCorrectness a <code>boolean</code> value that is a
     *                           condition representing request correctness
     * @param request a <code>ServletRequest</code> value that is a
     *                request to get a resource
     * @param targetPath a <code>String</code> value that is a path of a page
     *        where information is used
     * @param attributeName a <code>String</code> value that is the name of an
     *                      attribute to be deleted
     */
    private void deleteSeesionInfo(final boolean requestCorrectness,
            final ServletRequest request, final String targetPath,
            final String attributeName) {
        ApplicationSettingsReader reader = getInstance();
        HttpSession session = ((HttpServletRequest) request).getSession();
        String command = request.getParameter(PARAMETER_NAME_1);
        String imageFolderPath = reader.getProperty(IMAGE_FOLDER_PATH_KEY);
        String pathKey = request.getParameter(PARAMETER_NAME_2);
        String servletPath = ((HttpServletRequest) request).getServletPath();
        String scriptFolderPath = reader.getProperty(SCRIPT_FOLDER_PATH_KEY);
        String styleFolderPath = reader.getProperty(STYLE_FOLDER_PATH_KEY);
        boolean isControllerPath;
        boolean isImagePath;
        boolean isScriptPath;
        boolean isStylePath;
        /* The main part of the method. */
        isControllerPath = servletPath.equals(
                reader.getProperty(CONTROLLER_PATH_KEY));
        isImagePath = servletPath.indexOf(imageFolderPath) == 0;
        isScriptPath = servletPath.indexOf(scriptFolderPath) == 0;
        isStylePath = servletPath.indexOf(styleFolderPath) == 0;
        if (!((requestCorrectness)
                && ((servletPath.equals(targetPath))
                    || ((isControllerPath)
                        && (convertCommandName(CommandName
                                               .LOCALE_CHANGING_COMMAND)
                            .equals(command))
                        && (targetPath
                            .equals(reader.getProperty(pathKey))))
                    || ((isControllerPath)
                        && (convertCommandName(CommandName
                                               .IMAGE_FETCHING_COMMAND)
                            .equals(command)))
                    || (isImagePath)
                    || (isStylePath)
                    || (isScriptPath)))) {
            session.removeAttribute(attributeName);
        }
    }
    /**
     * Removes unnecessary attributes from a session.
     * @param requestCorrectness a <code>boolean</code> value that is a
     *                           condition representing request correctness
     * @param request a <code>ServletRequest</code> value that is a
     *                request to get a resource
     */
    private void doCleanup(final boolean requestCorrectness,
            final ServletRequest request) {
        HttpSession session = ((HttpServletRequest) request).getSession();
        String targetPath = (String) session.getAttribute(
                TARGET_PAGE_PATH_ATTRIBUTE_NAME);
        /* The main part of the method. */
        if (targetPath != null) {
            deleteSeesionInfo(requestCorrectness, request, targetPath,
                    ERROR_MESSAGES_ATTRIBUTE_NAME);
        }
        for (Entry<String, String> entry : targetPathMap.entrySet()) {
            deleteSeesionInfo(requestCorrectness, request, entry.getKey(),
                    entry.getValue());
        }
    }
    /**
     * Fills a map of target paths to attribute names.
     */
    private void fillTargetPathMap() {
        ApplicationSettingsReader reader = getInstance();
        /* The main part of the method. */
        targetPathMap.put(reader.getProperty(ERROR_PAGE_PATH_KEY),
                ERROR_INFORMATION_KEY_ATTRIBUTE_NAME);
        targetPathMap.put(
                reader.getProperty(RESPONSE_MESSAGE_VIEWING_PAGE_PATH_KEY),
                RESPONSE_MESSAGE_KEY_ATTRIBUTE_NAME);
        targetPathMap.put(reader.getProperty(DOCTOR_VIEWING_PAGE_PATH_KEY),
                DOCTORS_ATTRIBUTE_NAME);
        targetPathMap.put(
                reader.getProperty(ASSIGNED_TICKET_VIEWING_PAGE_PATH_KEY),
                ASSIGNED_TICKETS_ATTRIBUTE_NAME);
        targetPathMap.put(
                reader.getProperty(USED_TICKET_VIEWING_PAGE_PATH_KEY),
                USED_TICKETS_ATTRIBUTE_NAME);
        targetPathMap.put(
                reader.getProperty(SERVICE_INFO_VIEWING_PAGE_PATH_KEY),
                SERVICE_INFO_ATTRIBUTE_NAME);
    }
    /**
     * Stores a filter configuration object for later use. Called by a web
     * container to indicate to this filter that it is being placed into
     * service.
     * @param config a <code>FilterConfig</code> value which is a
     *               filter configuration object used by a servlet
     *               container to pass the information to this filter
     *               during the initialization
     */
    @Override
    public void init(final FilterConfig config) throws ServletException {
        this.filterConfig = config;
        fillTargetPathMap();
    }
}
