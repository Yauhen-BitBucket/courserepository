/*
 * CommandNamesKeeper.java
 * The utility class which provides some useful methods with the help of
 * which it is possible to determine whether a specified command name belongs
 * to a particular set of command names.
 */
package by.training.task05.web.filter;


import static by.training.task05.web.listener.ContextListener
        .convertCommandName;

import by.training.task05.exception.ApplicationException;

import by.training.task05.web.command.CommandName;

import java.util.HashSet;
import java.util.Set;


/**
 * The utility class which provides some useful methods with the help of
 * which it is possible to determine whether a specified command name belongs
 * to a particular set of command names.
 * @author Yauhen Sazonau
 * @version 1.0, 07/07/19
 * @since 1.0
 */
public final class CommandNamesKeeper {
    /**
     * Represents a single instance of this class. <code>null</code> value is
     * prohibited.
     */
    private static CommandNamesKeeper instance = new CommandNamesKeeper();
    /**
     * The set of all available command names which can be used by an
     * application controller. Can't be a <code>null</code> value.
     */
    private Set<String> availableCommandNames = new HashSet<>();
    /**
     * The set of command names which can be requested to be performed only by
     * a user with the head physician role. Can't be a <code>null</code> value.
     */
    private Set<String> headPhysicianCommandNames = new HashSet<>();
    /**
     * The set of command names which can be requested to be performed only by
     * a user with the patient role. Can't be a <code>null</code> value.
     */
    private Set<String> patientCommandNames = new HashSet<>();
    /**
     * The set of command names which can be requested to be performed only by
     * a user with the physician role. Can't be a <code>null</code> value.
     */
    private Set<String> physicianCommandNames = new HashSet<>();
    /**
     * The set of shared command names which can be used by an application
     * controller. Can't be a <code>null</code> value.
     */
    private Set<String> sharedCommandNames = new HashSet<>();
    /**
     * Creates an instance of this class. You have no ability to create
     * an instance of this class using this constructor. You should use the
     * <code>getInstance</code> method.
     */
    private CommandNamesKeeper() {
        for (CommandName commandName : CommandName.values()) {
            availableCommandNames.add(convertCommandName(commandName));
        }
        fillSharedSet();
        fillPatientSet();
        fillPhysicianSet();
        fillHeadPhysicianSet();
    }
    /**
     * Checks whether a specified command name belongs to the set of all
     * available command names which can be used by an application controller.
     * @param commandName a <code>String</code> value which is a command name
     *                    to be checked
     * @return <code>true</code> value if a specified
     *         <code>commandName</code> value belongs to the set of all
     *         available command names which can be used by an application
     *         controller, <code>false</code> otherwise
     * @throws ApplicationException if the <code>commandName</code> argument
     *                              is a <code>null</code> value
     */
    public boolean checkCommandNameExistence(final String commandName) {
        if (commandName == null) {
            throw new ApplicationException("The 'commandName' argument is "
                    + "'null'.");
        }
        return (availableCommandNames.contains(commandName));
    }
    /**
     * Checks whether a specified command name belongs to the set of head
     * physician command names.
     * @param commandName a <code>String</code> value to be checked
     * @return <code>true</code> value if a specified <code>commandName</code>
     *         belongs to the set of head physician command names,
     *         <code>false</code> otherwise
     * @throws ApplicationException if the <code>commandName</code> argument
     *                              is a <code>null</code> value
     */
    public boolean checkHeadPhysicianCommandName(final String commandName) {
        if (commandName == null) {
            throw new ApplicationException("The 'commandName' argument is "
                    + "'null'.");
        }
        return (headPhysicianCommandNames.contains(commandName));
    }
    /**
     * Checks whether a specified command name belongs to the set of patient
     * command names.
     * @param commandName a <code>String</code> value to be checked
     * @return <code>true</code> value if a specified <code>commandName</code>
     *         belongs to the set of patient command names, <code>false</code>
     *         otherwise
     * @throws ApplicationException if the <code>commandName</code> argument
     *                              is a <code>null</code> value
     */
    public boolean checkPatientCommandName(final String commandName) {
        if (commandName == null) {
            throw new ApplicationException("The 'commandName' argument is "
                    + "'null'.");
        }
        return (patientCommandNames.contains(commandName));
    }
    /**
     * Checks whether a specified command name belongs to the set of physician
     * command names.
     * @param commandName a <code>String</code> value to be checked
     * @return <code>true</code> value if a specified <code>commandName</code>
     *         belongs to the set of physician command names,
     *         <code>false</code> otherwise
     * @throws ApplicationException if the <code>commandName</code> argument
     *                              is a <code>null</code> value
     */
    public boolean checkPhysicianCommandName(final String commandName) {
        if (commandName == null) {
            throw new ApplicationException("The 'commandName' argument is "
                    + "'null'.");
        }
        return (physicianCommandNames.contains(commandName));
    }
    /**
     * Checks whether a specified command name belongs to the set of shared
     * command names.
     * @param commandName a <code>String</code> value to be checked
     * @return <code>true</code> value if a specified <code>commandName</code>
     *         belongs to the set of shared command names, <code>false</code>
     *         otherwise
     * @throws ApplicationException if the <code>commandName</code> argument
     *                              is a <code>null</code> value
     */
    public boolean checkSharedCommandName(final String commandName) {
        if (commandName == null) {
            throw new ApplicationException("The 'commandName' argument is "
                    + "'null'.");
        }
        return (sharedCommandNames.contains(commandName));
    }
    /**
     * Fills the set of head physician command names.
     */
    private void fillHeadPhysicianSet() {
        headPhysicianCommandNames.add(
                convertCommandName(CommandName.PATIENT_ADDING_COMMAND));
        headPhysicianCommandNames.add(
                convertCommandName(CommandName.SERVICE_INFO_FETCHING_COMMAND));
        headPhysicianCommandNames.add(
                convertCommandName(CommandName.USED_TICKET_FETCHING_COMMAND));
        headPhysicianCommandNames.add(
                convertCommandName(CommandName.USER_FETCHING_COMMAND));
        headPhysicianCommandNames.add(
                convertCommandName(CommandName.BLOCKED_STATE_SETTING_COMMAND));
    }
    /**
     * Fills the set of patient command names.
     */
    private void fillPatientSet() {
        patientCommandNames.add(
                convertCommandName(CommandName.DOCTOR_FETCHING_COMMAND));
        patientCommandNames.add(
                convertCommandName(CommandName.FREE_TICKET_FETCHING_COMMAND));
        patientCommandNames.add(
                convertCommandName(CommandName.OWN_TICKET_FETCHING_COMMAND));
        patientCommandNames.add(
                convertCommandName(CommandName.TICKET_ASSIGNING_COMMAND));
        patientCommandNames.add(
                convertCommandName(CommandName.TICKET_CANCELLING_COMMAND));
    }
    /**
     * Fills the set of physician command names.
     */
    private void fillPhysicianSet() {
        physicianCommandNames.add(
                convertCommandName(CommandName
                                   .ASSIGNED_TICKET_FETCHING_COMMAND));
        physicianCommandNames.add(
                convertCommandName(CommandName.TICKET_ADDING_COMMAND));
        physicianCommandNames.add(
                convertCommandName(CommandName.VISIT_COMMITTING_COMMAND));
    }
    /**
     * Fills the set of shared command names.
     */
    private void fillSharedSet() {
        sharedCommandNames.add(
                convertCommandName(CommandName.AUTHENTICATION_COMMAND));
        sharedCommandNames.add(
                convertCommandName(CommandName.IMAGE_FETCHING_COMMAND));
        sharedCommandNames.add(
                convertCommandName(CommandName.IMAGE_SETTING_COMMAND));
        sharedCommandNames.add(
                convertCommandName(CommandName.LOCALE_CHANGING_COMMAND));
        sharedCommandNames.add(convertCommandName(CommandName.LOGOUT_COMMAND));
        sharedCommandNames.add(
                convertCommandName(CommandName.MESSAGE_FETCHING_COMMAND));
    }
    /**
     * Retrieves an instance if this class.
     * @return an instance if this class
     */
    public static CommandNamesKeeper getInstance() {
        return instance;
    }
}
