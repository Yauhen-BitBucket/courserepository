/*
 * ParameterDataKeeper.java
 * The utility class which provides some useful methods with the help of
 * which it is possible to determine the set of required request parameters
 * for the specified command name and whether some request parameters have
 * the correct values.
 */
package by.training.task05.web.filter;


import static by.training.task05.reader.ApplicationSettingsReader
        .ASSIGNED_TICKET_FETCHING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .ASSIGNED_TICKET_VIEWING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .AUTHENTICATION_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .DOCTOR_FETCHING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .DOCTOR_VIEWING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .ERROR_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .FREE_TICKET_FETCHING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .FREE_TICKET_VIEWING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .IMAGE_SETTING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .MAIN_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .OWN_TICKET_VIEWING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .PATIENT_ADDING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .RESPONSE_MESSAGE_VIEWING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .SERVICE_INFO_FETCHING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .SERVICE_INFO_VIEWING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .TICKET_ADDING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .TICKET_ASSIGNING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .TICKET_CANCELLING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .USED_TICKET_FETCHING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .USED_TICKET_VIEWING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .USER_VIEWING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .VISIST_COMMITTING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .WELCOME_PAGE_PATH_KEY;

import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_2;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_3;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_4;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_6;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_7;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_8;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_9;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_10;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_11;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_12;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_13;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_14;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_15;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_16;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_17;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_18;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_19;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_21;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_22;

import static by.training.task05.web.listener.ContextListener
        .convertCommandName;

import by.training.task05.entity.Speciality;

import by.training.task05.exception.ApplicationException;

import by.training.task05.reader.ApplicationSettingsReader;

import by.training.task05.web.command.CommandName;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


/**
 * The utility class which provides some useful methods with the help of
 * which it is possible to determine the set of required request parameters
 * for the specified command name and whether some request parameters have
 * the correct values.
 * @author Yauhen Sazonau
 * @version 1.0, 07/07/19
 * @since 1.0
 */
public final class ParameterDataKeeper {
    /**
     * Represents a single instance of this class. <code>null</code> value is
     * prohibited.
     */
    private static ParameterDataKeeper instance = new ParameterDataKeeper();
    /**
     * A map that maps a command name to the set of required request parameters
     * used by that command. Can't be a <code>null</code> value.
     */
    private Map<String, Set<String>> commandNameMapping = new HashMap<>();
    /**
     * A set of paths which are allowed to be reached by a head physician when
     * he is authenticated. Can't be a <code>null</code> value.
     */
    private Set<String> headPhysicianPaths = new HashSet<>();
    /**
     * A set of allowed values the speciality request parameter can have. Can't
     * be a <code>null</code> value.
     */
    private Set<String> specialityValues = new HashSet<>();
    /**
     * A set of allowed values the path key request parameter can have. Can't
     * be a <code>null</code> value.
     */
    private Set<String> pathKeyValues = new HashSet<>();
    /**
     * A set of paths which are allowed to be reached by a patient when he is
     * authenticated. Can't be a <code>null</code> value.
     */
    private Set<String> patientPaths = new HashSet<>();
    /**
     * A set of paths which are allowed to be reached by a physician when he is
     * authenticated. Can't be a <code>null</code> value.
     */
    private Set<String> physicianPaths = new HashSet<>();
    /**
     * Creates an instance of this class. You have no ability to create
     * an instance of this class using this constructor. You should use the
     * <code>getInstance</code> method.
     */
    private ParameterDataKeeper() {
        commandNameMapping.put(convertCommandName(
                CommandName.ASSIGNED_TICKET_FETCHING_COMMAND),
                createNameSet1());
        commandNameMapping.put(convertCommandName(
                CommandName.AUTHENTICATION_COMMAND), createNameSet2());
        commandNameMapping.put(convertCommandName(
                CommandName.DOCTOR_FETCHING_COMMAND), createNameSet3());
        commandNameMapping.put(convertCommandName(
                CommandName.FREE_TICKET_FETCHING_COMMAND), createNameSet4());
        commandNameMapping.put(convertCommandName(
                CommandName.LOCALE_CHANGING_COMMAND), createNameSet5());
        commandNameMapping.put(convertCommandName(
                CommandName.LOGOUT_COMMAND), createNameSet6());
        commandNameMapping.put(convertCommandName(
                CommandName.OWN_TICKET_FETCHING_COMMAND), createNameSet7());
        commandNameMapping.put(convertCommandName(
                CommandName.PATIENT_ADDING_COMMAND), createNameSet8());
        commandNameMapping.put(convertCommandName(
                CommandName.SERVICE_INFO_FETCHING_COMMAND), createNameSet9());
        commandNameMapping.put(convertCommandName(
                CommandName.TICKET_ADDING_COMMAND), createNameSet10());
        commandNameMapping.put(convertCommandName(
                CommandName.TICKET_ASSIGNING_COMMAND), createNameSet11());
        commandNameMapping.put(convertCommandName(
                CommandName.TICKET_CANCELLING_COMMAND), createNameSet11());
        commandNameMapping.put(convertCommandName(
                CommandName.USED_TICKET_FETCHING_COMMAND), createNameSet12());
        commandNameMapping.put(convertCommandName(
                CommandName.VISIT_COMMITTING_COMMAND), createNameSet13());
        commandNameMapping.put(convertCommandName(
                CommandName.IMAGE_FETCHING_COMMAND), createNameSet14());
        commandNameMapping.put(convertCommandName(
                CommandName.IMAGE_SETTING_COMMAND), createNameSet15());
        commandNameMapping.put(convertCommandName(
                CommandName.MESSAGE_FETCHING_COMMAND), createNameSet16());
        commandNameMapping.put(convertCommandName(
                CommandName.USER_FETCHING_COMMAND), createNameSet17());
        commandNameMapping.put(convertCommandName(
                CommandName.BLOCKED_STATE_SETTING_COMMAND), createNameSet18());
        for (Speciality speciality : Speciality.values()) {
            specialityValues.add(speciality.name());
        }
        fillPathKeySet();
        fillPatientPaths();
        fillPhysicianPaths();
        fillHeadPhysicianPaths();
    }
    /**
     * Creates a set of required parameters used by the assigned ticket
     * fetching command.
     * @return a set of required parameters used by the assigned ticket
     *         fetching command.
     */
    private Set<String> createNameSet1() {
        Set<String> set = new HashSet<>();
        /* The main part of the method. */
        set.add(PARAMETER_NAME_7);
        return set;
    }
    /**
     * Creates a set of required parameters used by the authentication command.
     * @return a set of required parameters used by the authentication command
     */
    private Set<String> createNameSet2() {
        Set<String> set = new HashSet<>();
        /* The main part of the method. */
        set.add(PARAMETER_NAME_3);
        set.add(PARAMETER_NAME_4);
        return set;
    }
    /**
     * Creates a set of required parameters used by the doctor fetching
     * command.
     * @return a set of required parameters used by the the doctor fetching
     *         command
     */
    private Set<String> createNameSet3() {
        Set<String> set = new HashSet<>();
        /* The main part of the method. */
        set.add(PARAMETER_NAME_6);
        return set;
    }
    /**
     * Creates a set of required parameters used by the free ticket fetching
     * command.
     * @return a set of required parameters used by the the free ticket
     *         fetching command
     */
    private Set<String> createNameSet4() {
        Set<String> set = new HashSet<>();
        /* The main part of the method. */
        set.add(PARAMETER_NAME_6);
        set.add(PARAMETER_NAME_7);
        return set;
    }
    /**
     * Creates a set of required parameters used by the locale changing
     * command.
     * @return a set of required parameters used by the locale changing command
     */
    private Set<String> createNameSet5() {
        Set<String> set = new HashSet<>();
        /* The main part of the method. */
        set.add(PARAMETER_NAME_2);
        return set;
    }
    /**
     * Creates a set of required parameters used by the logout command.
     * @return a set of required parameters used by the logout command
     */
    private Set<String> createNameSet6() {
        return (new HashSet<>());
    }
    /**
     * Creates a set of required parameters used by the own ticket fetching
     * command.
     * @return a set of required parameters used by the own ticket fetching
     *         command
     */
    private Set<String> createNameSet7() {
        return (new HashSet<>());
    }
    /**
     * Creates a set of required parameters used by the patient adding command.
     * @return a set of required parameters used by the patient adding command
     */
    private Set<String> createNameSet8() {
        Set<String> set = new HashSet<>();
        /* The main part of the method. */
        set.add(PARAMETER_NAME_3);
        set.add(PARAMETER_NAME_4);
        set.add(PARAMETER_NAME_17);
        set.add(PARAMETER_NAME_18);
        set.add(PARAMETER_NAME_19);
        return set;
    }
    /**
     * Creates a set of required parameters used by the service info fetching
     * command.
     * @return a set of required parameters used by the service info fetching
     *         command
     */
    private Set<String> createNameSet9() {
        Set<String> set = new HashSet<>();
        /* The main part of the method. */
        set.add(PARAMETER_NAME_15);
        set.add(PARAMETER_NAME_16);
        return set;
    }
    /**
     * Creates a set of required parameters used by the ticket adding command.
     * @return a set of required parameters used by the ticket adding command
     */
    private Set<String> createNameSet10() {
        Set<String> set = new HashSet<>();
        /* The main part of the method. */
        set.add(PARAMETER_NAME_7);
        set.add(PARAMETER_NAME_9);
        set.add(PARAMETER_NAME_10);
        return set;
    }
    /**
     * Creates a set of required parameters used by the ticket assigning
     * command or ticket canceling command.
     * @return a set of required parameters used by the ticket assigning
     *         command or ticket canceling command
     */
    private Set<String> createNameSet11() {
        Set<String> set = new HashSet<>();
        /* The main part of the method. */
        set.add(PARAMETER_NAME_8);
        return set;
    }
    /**
     * Creates a set of required parameters used by the used ticket fetching
     * command.
     * @return a set of required parameters used by the used ticket fetching
     *         command
     */
    private Set<String> createNameSet12() {
        Set<String> set = new HashSet<>();
        /* The main part of the method. */
        set.add(PARAMETER_NAME_13);
        set.add(PARAMETER_NAME_14);
        return set;
    }
    /**
     * Creates a set of required parameters used by the visit committing
     * command.
     * @return a set of required parameters used by the visit committing
     *         command
     */
    private Set<String> createNameSet13() {
        Set<String> set = new HashSet<>();
        /* The main part of the method. */
        set.add(PARAMETER_NAME_8);
        set.add(PARAMETER_NAME_11);
        set.add(PARAMETER_NAME_12);
        return set;
    }
    /**
     * Creates a set of required parameters used by the image fetching
     * command.
     * @return a set of required parameters used by the image fetching
     *         command
     */
    private Set<String> createNameSet14() {
        Set<String> set = new HashSet<>();
        /* The main part of the method. */
        set.add(PARAMETER_NAME_3);
        return set;
    }
    /**
     * Creates a set of required parameters used by the image setting
     * command.
     * @return a set of required parameters used by the image setting
     *         command
     */
    private Set<String> createNameSet15() {
        return (new HashSet<>());
    }
    /**
     * Creates a set of required parameters used by the message fetching
     * command.
     * @return a set of required parameters used by the message fetching
     *         command
     */
    private Set<String> createNameSet16() {
        Set<String> set = new HashSet<>();
        /* The main part of the method. */
        set.add(PARAMETER_NAME_21);
        return set;
    }
    /**
     * Creates a set of required parameters used by the user fetching command.
     * @return a set of required parameters used by the user fetching command
     */
    private Set<String> createNameSet17() {
        return (new HashSet<>());
    }
    /**
     * Creates a set of required parameters used by the blocked state setting
     * command.
     * @return a set of required parameters used by the blocked state setting
     *         command
     */
    private Set<String> createNameSet18() {
        Set<String> set = new HashSet<>();
        /* The main part of the method. */
        set.add(PARAMETER_NAME_17);
        set.add(PARAMETER_NAME_22);
        return set;
    }
    /**
     * Fills a set of paths which are allowed to be reached by a head physician
     * when he is authenticated.
     */
    private void fillHeadPhysicianPaths() {
        ApplicationSettingsReader reader
                = ApplicationSettingsReader.getInstance();
        headPhysicianPaths.add(
                reader.getProperty(PATIENT_ADDING_PAGE_PATH_KEY));
        headPhysicianPaths.add(
                reader.getProperty(SERVICE_INFO_FETCHING_PAGE_PATH_KEY));
        headPhysicianPaths.add(
                reader.getProperty(SERVICE_INFO_VIEWING_PAGE_PATH_KEY));
        headPhysicianPaths.add(
                reader.getProperty(USED_TICKET_FETCHING_PAGE_PATH_KEY));
        headPhysicianPaths.add(
                reader.getProperty(USED_TICKET_VIEWING_PAGE_PATH_KEY));
        headPhysicianPaths.add(
                reader.getProperty(USER_VIEWING_PAGE_PATH_KEY));
    }
    /**
     * Fills a path key set.
     */
    private void fillPathKeySet() {
        pathKeyValues.add(ASSIGNED_TICKET_FETCHING_PAGE_PATH_KEY);
        pathKeyValues.add(ASSIGNED_TICKET_VIEWING_PAGE_PATH_KEY);
        pathKeyValues.add(AUTHENTICATION_PAGE_PATH_KEY);
        pathKeyValues.add(DOCTOR_FETCHING_PAGE_PATH_KEY);
        pathKeyValues.add(DOCTOR_VIEWING_PAGE_PATH_KEY);
        pathKeyValues.add(ERROR_PAGE_PATH_KEY);
        pathKeyValues.add(FREE_TICKET_FETCHING_PAGE_PATH_KEY);
        pathKeyValues.add(FREE_TICKET_VIEWING_PAGE_PATH_KEY);
        pathKeyValues.add(IMAGE_SETTING_PAGE_PATH_KEY);
        pathKeyValues.add(MAIN_PAGE_PATH_KEY);
        pathKeyValues.add(OWN_TICKET_VIEWING_PAGE_PATH_KEY);
        pathKeyValues.add(PATIENT_ADDING_PAGE_PATH_KEY);
        pathKeyValues.add(RESPONSE_MESSAGE_VIEWING_PAGE_PATH_KEY);
        pathKeyValues.add(SERVICE_INFO_FETCHING_PAGE_PATH_KEY);
        pathKeyValues.add(SERVICE_INFO_VIEWING_PAGE_PATH_KEY);
        pathKeyValues.add(TICKET_ADDING_PAGE_PATH_KEY);
        pathKeyValues.add(TICKET_ASSIGNING_PAGE_PATH_KEY);
        pathKeyValues.add(TICKET_CANCELLING_PAGE_PATH_KEY);
        pathKeyValues.add(USED_TICKET_FETCHING_PAGE_PATH_KEY);
        pathKeyValues.add(USED_TICKET_VIEWING_PAGE_PATH_KEY);
        pathKeyValues.add(USER_VIEWING_PAGE_PATH_KEY);
        pathKeyValues.add(VISIST_COMMITTING_PAGE_PATH_KEY);
        pathKeyValues.add(WELCOME_PAGE_PATH_KEY);
    }
    /**
     * Fills a set of paths which are allowed to be reached by a patient when
     * he is authenticated.
     */
    private void fillPatientPaths() {
        ApplicationSettingsReader reader
                = ApplicationSettingsReader.getInstance();
        patientPaths.add(reader.getProperty(DOCTOR_FETCHING_PAGE_PATH_KEY));
        patientPaths.add(reader.getProperty(DOCTOR_VIEWING_PAGE_PATH_KEY));
        patientPaths.add(
                reader.getProperty(FREE_TICKET_FETCHING_PAGE_PATH_KEY));
        patientPaths.add(
                reader.getProperty(FREE_TICKET_VIEWING_PAGE_PATH_KEY));
        patientPaths.add(reader.getProperty(OWN_TICKET_VIEWING_PAGE_PATH_KEY));
        patientPaths.add(reader.getProperty(TICKET_ASSIGNING_PAGE_PATH_KEY));
        patientPaths.add(reader.getProperty(TICKET_CANCELLING_PAGE_PATH_KEY));
    }
    /**
     * Fills a set of paths which are allowed to be reached by a physician when
     * he is authenticated.
     */
    private void fillPhysicianPaths() {
        ApplicationSettingsReader reader
                = ApplicationSettingsReader.getInstance();
        physicianPaths.add(
                reader.getProperty(ASSIGNED_TICKET_FETCHING_PAGE_PATH_KEY));
        physicianPaths.add(
                reader.getProperty(ASSIGNED_TICKET_VIEWING_PAGE_PATH_KEY));
        physicianPaths.add(reader.getProperty(TICKET_ADDING_PAGE_PATH_KEY));
        physicianPaths.add(
                reader.getProperty(VISIST_COMMITTING_PAGE_PATH_KEY));
    }
    /**
     * Checks whether a specified path value is an error page path.
     * @param path a <code>String</code> value which is a path to be checked
     * @return <code>true</code> value if a specified <code>path</code> value
     *         is an error page path, <code>false</code> otherwise
     * @throws ApplicationException if the <code>path</code> argument is a
     *                              <code>null</code> value
     */
    public boolean checkErrorPagePath(final String path) {
        ApplicationSettingsReader reader
                = ApplicationSettingsReader.getInstance();
        if (path == null) {
            throw new ApplicationException("The 'path' argument is 'null'.");
        }
        return (path.equals(reader.getProperty(ERROR_PAGE_PATH_KEY)));
    }
    /**
     * Checks whether a specified path value belongs to the set of head
     * physician paths.
     * @param path a <code>String</code> value which is a path to be checked
     * @return <code>true</code> value if a specified <code>path</code> value
     *         belongs to the set of head physician paths, <code>false</code>
     *         otherwise
     * @throws ApplicationException if the <code>path</code> argument is a
     *                              <code>null</code> value
     */
    public boolean checkHeadPhysicianPath(final String path) {
        if (path == null) {
            throw new ApplicationException("The 'path' argument is 'null'.");
        }
        return (headPhysicianPaths.contains(path));
    }
    /**
     * Checks whether a specified value of the path key request parameter
     * belongs to the set of allowed values.
     * @param pathPropertyKey a <code>String</code> value which is a path key
     *                        to be checked
     * @return <code>true</code> value if a specified
     *         <code>pathPropertyKey</code> value belongs to the set of
     *         allowed values, <code>false</code> otherwise
     * @throws ApplicationException if the <code>pathPropertyKey</code>
     *                              argument is a <code>null</code> value
     */
    public boolean checkPathKeyValue(final String pathPropertyKey) {
        if (pathPropertyKey == null) {
            throw new ApplicationException("The 'pathPropertyKey' argument "
                    + "is 'null'.");
        }
        return (pathKeyValues.contains(pathPropertyKey));
    }
    /**
     * Checks whether a specified path value belongs to the set of patient
     * paths.
     * @param path a <code>String</code> value which is a path to be checked
     * @return <code>true</code> value if a specified <code>path</code> value
     *         belongs to the set of patient paths, <code>false</code>
     *         otherwise
     * @throws ApplicationException if the <code>path</code> argument is a
     *                              <code>null</code> value
     */
    public boolean checkPatientPath(final String path) {
        if (path == null) {
            throw new ApplicationException("The 'path' argument is 'null'.");
        }
        return (patientPaths.contains(path));
    }
    /**
     * Checks whether a specified path value belongs to the set of physician
     * paths.
     * @param path a <code>String</code> value which is a path to be checked
     * @return <code>true</code> value if a specified <code>path</code> value
     *         belongs to the set of physician paths, <code>false</code>
     *         otherwise
     * @throws ApplicationException if the <code>path</code> argument is a
     *                              <code>null</code> value
     */
    public boolean checkPhysicianPath(final String path) {
        if (path == null) {
            throw new ApplicationException("The 'path' argument is 'null'.");
        }
        return (physicianPaths.contains(path));
    }
    /**
     * Checks whether a specified path value is a response page path.
     * @param path a <code>String</code> value which is a path to be checked
     * @return <code>true</code> value if a specified <code>path</code> value
     *         is a response page path, <code>false</code> otherwise
     * @throws ApplicationException if the <code>path</code> argument is a
     *                              <code>null</code> value
     */
    public boolean checkResponsePagePath(final String path) {
        ApplicationSettingsReader reader
                = ApplicationSettingsReader.getInstance();
        if (path == null) {
            throw new ApplicationException("The 'path' argument is 'null'.");
        }
        return (path
                .equals(reader
                        .getProperty(RESPONSE_MESSAGE_VIEWING_PAGE_PATH_KEY)));
    }
    /**
     * Checks whether a specified value of the speciality parameter belongs to
     * the set of allowed values.
     * @param speciality a <code>String</code> value which is a speciality
     *                   to be checked
     * @return <code>true</code> value if a specified <code>speciality</code>
     *         value belongs to the set of allowed values, <code>false</code>
     *         otherwise
     * @throws ApplicationException if the <code>speciality</code>
     *                              argument is a <code>null</code> value
     */
    public boolean checkSpecialityValue(final String speciality) {
        if (speciality == null) {
            throw new ApplicationException("The 'speciality' argument "
                    + "is 'null'.");
        }
        return (specialityValues.contains(speciality));
    }
    /**
     * Retrieves the set of required request parameters for a specified
     * command name.
     * @param commandName a <code>String</code> value which is the name of
     *                    a command for which the set of required request
     *                    parameters should be retrieved
     * @return the set of required request parameters. It may be empty, but
     *         can't be a <code>null</code> value.
     * @throws ApplicationException if the set of required request
     *                              parameters doesn't exist for a
     *                              specified command name or if the
     *                              <code>commandName</code> argument is a
     *                              <code>null</code> value
     */
    public Set<String> getParameterNames(final String commandName) {
        if (commandName == null) {
            throw new ApplicationException("The 'commandName' argument is "
                    + "'null'.");
        }
        if (!(commandNameMapping.containsKey(commandName))) {
            throw new ApplicationException("The set of required request "
                    + "parameters doesn't exist for a specified command "
                    + "name! 'commandName' = " + commandName);
        }
        return (commandNameMapping.get(commandName));
    }
    /**
     * Retrieves an instance if this class.
     * @return an instance if this class
     */
    public static ParameterDataKeeper getInstance() {
        return instance;
    }
}
