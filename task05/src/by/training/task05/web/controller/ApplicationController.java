/*
 * ApplicationController.java
 * An application controller.
 */
package by.training.task05.web.controller;


import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_1;

import by.training.task05.web.command.Command;
import by.training.task05.web.command.CommandException;

import java.io.IOException;

import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * An application controller.
 * @author Yauhen Sazonau
 * @version 1.0, 06/30/19
 * @since 1.0
 */
@SuppressWarnings("serial")
public class ApplicationController extends HttpServlet {
    /**
     * A dummy path value. The identifier holds the value of "".
     */
    public static final String DUMMY_PATH = "";
    /**
     * A command retriever. <code>null</code> value is prohibited.
     */
    private final CommandRetriever commandRetriever = new CommandRetriever();
    /**
     * Processes a GET request received from the <code>protected</code>
     * <code>service</code> method.
     * @param request an <code>HttpServletRequest</code> value which is a
     *                GET request a user has made to the servlet
     * @param response an <code>HttpServletResponse</code> value which is
     *                 a response the servlet sends to a user
     * @throws IOException if an input or output error occurs while this
     *                     controller handles a GET request
     * @throws ServletException if a GET request could not be handled
     */
    @Override
    protected void doGet(final HttpServletRequest request,
            final HttpServletResponse response) throws ServletException,
                                                       IOException {
        processRequest(request, response);
    }
    /**
     * Processes a POST request received from the <code>protected</code>
     * <code>service</code> method.
     * @param request an <code>HttpServletRequest</code> value which is a
     *                POST request a user has made to the servlet
     * @param response an <code>HttpServletResponse</code> value which is
     *                 a response the servlet sends to a user
     * @throws IOException if an input or output error occurs while this
     *                     controller handles a POST request
     * @throws ServletException if a POST request could not be handled
     */
    @Override
    protected void doPost(final HttpServletRequest request,
            final HttpServletResponse response) throws ServletException,
                                                       IOException {
        processRequest(request, response);
    }
    /**
     * Responses to a user request.
     * @param request an <code>HttpServletRequest</code> value which is a
     *                GET or POST request a user has made to the servlet
     * @param response an <code>HttpServletResponse</code> value which is
     *                 a response the servlet sends to a user
     * @throws IOException if an input or output error occurs while this
     *                     controller handles a GET or POST request
     * @throws ServletException if a GET or POST request could not be handled
     */
    private void processRequest(final HttpServletRequest request,
            final HttpServletResponse response) throws ServletException,
                                                       IOException {
        Command command;
        String commandName = request.getParameter(PARAMETER_NAME_1);
        String path;
        /* The main part of the method. */
        command = commandRetriever.retrieveCommand(commandName);
        try {
            path = command.execute(request, response, getServletContext());
        } catch (CommandException e) {
            throw new ServletException("A request can't be handled.", e);
        }
        if (!path.equals(DUMMY_PATH)) {
            response
            .sendRedirect(response
                          .encodeRedirectURL(request.getContextPath()
                                             + path));
        }
    }
}
