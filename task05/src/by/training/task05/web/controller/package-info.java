/**
 * Contains a class which is a controller of this application.
 * @author Yauhen Sazonau
 * @since 1.0
 */
package by.training.task05.web.controller;
