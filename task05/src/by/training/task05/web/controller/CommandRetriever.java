/* CommandRetriever.java
 * A command retriever.
 */
package by.training.task05.web.controller;


import by.training.task05.web.command.AssignedTicketFetchingCommand;
import by.training.task05.web.command.AuthenticationCommand;
import by.training.task05.web.command.BlockedStateSettingCommand;
import by.training.task05.web.command.Command;
import by.training.task05.web.command.CommandName;
import by.training.task05.web.command.DoctorFetchingCommand;
import by.training.task05.web.command.FreeTicketFetchingCommand;
import by.training.task05.web.command.ImageFetchingCommand;
import by.training.task05.web.command.ImageSettingCommand;
import by.training.task05.web.command.LocaleChangingCommand;
import by.training.task05.web.command.LogoutCommand;
import by.training.task05.web.command.MessageFetchingCommand;
import by.training.task05.web.command.OwnTicketFetchingCommand;
import by.training.task05.web.command.PatientAddingCommand;
import by.training.task05.web.command.ServiceInfoFetchingCommand;
import by.training.task05.web.command.TicketAddingCommand;
import by.training.task05.web.command.TicketAssigningCommand;
import by.training.task05.web.command.TicketCancellingCommand;
import by.training.task05.web.command.UsedTicketFetchingCommand;
import by.training.task05.web.command.UserFetchingCommand;
import by.training.task05.web.command.VisitCommittingCommand;

import java.util.EnumMap;
import java.util.Map;

import javax.servlet.ServletException;


/**
 * A command retriever.
 * @version 1.0, 06/30/19
 * @since 1.0
 */
class CommandRetriever {
    /**
     * A mapping of command names to command implementations. <code>null</code>
     * value is prohibited.
     */
    private final Map<CommandName, Command> commandMapping
            = new EnumMap<>(CommandName.class);
    /**
     * Creates an instance of this class.
     */
    CommandRetriever() {
        commandMapping.put(CommandName.LOCALE_CHANGING_COMMAND,
                new LocaleChangingCommand());
        commandMapping.put(CommandName.AUTHENTICATION_COMMAND,
                new AuthenticationCommand());
        commandMapping.put(CommandName.LOGOUT_COMMAND, new LogoutCommand());
        commandMapping.put(CommandName.OWN_TICKET_FETCHING_COMMAND,
                new OwnTicketFetchingCommand());
        commandMapping.put(CommandName.DOCTOR_FETCHING_COMMAND,
                new DoctorFetchingCommand());
        commandMapping.put(CommandName.FREE_TICKET_FETCHING_COMMAND,
                new FreeTicketFetchingCommand());
        commandMapping.put(CommandName.TICKET_ASSIGNING_COMMAND,
                new TicketAssigningCommand());
        commandMapping.put(CommandName.TICKET_CANCELLING_COMMAND,
                new TicketCancellingCommand());
        commandMapping.put(CommandName.TICKET_ADDING_COMMAND,
                new TicketAddingCommand());
        commandMapping.put(CommandName.ASSIGNED_TICKET_FETCHING_COMMAND,
                new AssignedTicketFetchingCommand());
        commandMapping.put(CommandName.VISIT_COMMITTING_COMMAND,
                new VisitCommittingCommand());
        commandMapping.put(CommandName.USED_TICKET_FETCHING_COMMAND,
                new UsedTicketFetchingCommand());
        commandMapping.put(CommandName.SERVICE_INFO_FETCHING_COMMAND,
                new ServiceInfoFetchingCommand());
        commandMapping.put(CommandName.PATIENT_ADDING_COMMAND,
                new PatientAddingCommand());
        commandMapping.put(CommandName.IMAGE_FETCHING_COMMAND,
                new ImageFetchingCommand());
        commandMapping.put(CommandName.IMAGE_SETTING_COMMAND,
                new ImageSettingCommand());
        commandMapping.put(CommandName.MESSAGE_FETCHING_COMMAND,
                new MessageFetchingCommand());
        commandMapping.put(CommandName.USER_FETCHING_COMMAND,
                new UserFetchingCommand());
        commandMapping.put(CommandName.BLOCKED_STATE_SETTING_COMMAND,
                new BlockedStateSettingCommand());
    }
    /**
     * Retrieves a command using a command name.
     * @param name a <code>String</code> value which is the name of a command
     *             to retrieve. It should be a hyphen-joined, lower-cased
     *             string. Accepted values are "locale-changing-command",
     *             "authentication-command", "logout-command",
     *             "own-ticket-fetching-command", "doctor-fetching-command",
     *             "free-ticket-fetching-command", "ticket-assigning-command",
     *             ticket-cancelling-command, "ticket-adding-command",
     *             "assigned-ticket-fetching-command",
     *             "visit-committing-command", "used-ticket-fetching-command",
     *             "service-info-fetching-command", "patient-adding-command",
     *             "image-fetching-command", "image-setting-command",
     *             "message-fetching-command", "user-fetching-command",
     *             "blocked-state-setting-command".
     * @return a command. Can't be a <code>null</code> value.
     * @throws ServletException if the argument is a <code>null</code>
     *                          value or a command name isn't known
     */
    Command retrieveCommand(final String name) throws ServletException {
        Command command = null;
        CommandName commandName = null;
        String processedName;
        /* The main part of the method. */
        try {
            processedName = name.toUpperCase();
            processedName = processedName.replace("-", "_");
            commandName = CommandName.valueOf(processedName);
            command = commandMapping.get(commandName);
        } catch (NullPointerException | IllegalArgumentException e) {
            throw new ServletException("The specified command name isn't "
                    + "known: " + name + ".", e);
        }
        return command;
    }
}
