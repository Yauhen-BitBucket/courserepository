/*
 * ParameterNamesKeeper.java
 * A class for storing request parameter names.
 */
package by.training.task05.web.command;


/**
 * A class for storing request parameter names.
 * @author Yauhen Sazonau
 * @version 1.0, 06/30/19
 * @since 1.0
 */
public final class ParameterNamesKeeper {
    /**
     * A request parameter name. The identifier holds the value of "command".
     */
    public static final String PARAMETER_NAME_1 = "command";
    /**
     * A request parameter name. The identifier holds the value of
     * "pathPropertyKey".
     */
    public static final String PARAMETER_NAME_2 = "pathPropertyKey";
    /**
     * A request parameter name. The identifier holds the value of
     * "login".
     */
    public static final String PARAMETER_NAME_3 = "login";
    /**
     * A request parameter name. The identifier holds the value of
     * "password".
     */
    public static final String PARAMETER_NAME_4 = "password";
    /**
     * A request parameter name. The identifier holds the value of
     * "pageNumber".
     */
    public static final String PARAMETER_NAME_5 = "pageNumber";
    /**
     * A request parameter name. The identifier holds the value of
     * "speciality".
     */
    public static final String PARAMETER_NAME_6 = "speciality";
    /**
     * A request parameter name. The identifier holds the value of
     * "date".
     */
    public static final String PARAMETER_NAME_7 = "date";
    /**
     * A request parameter name. The identifier holds the value of
     * "ticketID".
     */
    public static final String PARAMETER_NAME_8 = "ticketID";
    /**
     * A request parameter name. The identifier holds the value of
     * "startingTime".
     */
    public static final String PARAMETER_NAME_9 = "startingTime";
    /**
     * A request parameter name. The identifier holds the value of
     * "endingTime".
     */
    public static final String PARAMETER_NAME_10 = "endingTime";
    /**
     * A request parameter name. The identifier holds the value of
     * "diagnosis".
     */
    public static final String PARAMETER_NAME_11 = "diagnosis";
    /**
     * A request parameter name. The identifier holds the value of
     * "treatment".
     */
    public static final String PARAMETER_NAME_12 = "treatment";
    /**
     * A request parameter name. The identifier holds the value of
     * "startingDate".
     */
    public static final String PARAMETER_NAME_13 = "startingDate";
    /**
     * A request parameter name. The identifier holds the value of
     * "endingDate".
     */
    public static final String PARAMETER_NAME_14 = "endingDate";
    /**
     * A request parameter name. The identifier holds the value of
     * "year".
     */
    public static final String PARAMETER_NAME_15 = "year";
    /**
     * A request parameter name. The identifier holds the value of
     * "month".
     */
    public static final String PARAMETER_NAME_16 = "month";
    /**
     * A request parameter name. The identifier holds the value of
     * "passportNumber".
     */
    public static final String PARAMETER_NAME_17 = "passportNumber";
    /**
     * A request parameter name. The identifier holds the value of
     * "firstName".
     */
    public static final String PARAMETER_NAME_18 = "firstName";
    /**
     * A request parameter name. The identifier holds the value of
     * "lastName".
     */
    public static final String PARAMETER_NAME_19 = "lastName";
    /**
     * A request parameter name. The identifier holds the value of
     * "fileName".
     */
    public static final String PARAMETER_NAME_20 = "fileName";
    /**
     * A request parameter name. The identifier holds the value of
     * "messageKey".
     */
    public static final String PARAMETER_NAME_21 = "messageKey";
    /**
     * A request parameter name. The identifier holds the value of
     * "blockedState".
     */
    public static final String PARAMETER_NAME_22 = "blockedState";
    /**
     * Constructs an instance of this class.
     */
    private ParameterNamesKeeper() {
        /* The default initialization is sufficient. */
    }
}
