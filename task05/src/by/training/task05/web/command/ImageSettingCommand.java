/*
 * ImageSettingCommand.java
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to set an image.
 */
package by.training.task05.web.command;


import static by.training.task05.dao.UserDAO.IMAGE_SETTING_SUCCESS_VALUE;

import static by.training.task05.reader.ApplicationSettingsReader
        .IMAGE_SETTING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .MAXIMUM_IMAGE_FILE_SIZE_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .RESPONSE_MESSAGE_VIEWING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader.getInstance;

import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_1;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_20;

import by.training.task05.dao.DAOException;
import by.training.task05.dao.UserDAO;

import by.training.task05.dao.implementation.SQLDAOFactory;

import by.training.task05.entity.User;

import by.training.task05.reader.ApplicationSettingsReader;

import java.awt.image.BufferedImage;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;


/**
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to set an image.
 * @author Yauhen Sazonau
 * @version 1.0, 07/12/19
 * @since 1.0
 */
public class ImageSettingCommand implements Command {
    /**
     * A keeper of a part.
     * @author Yauhen Sazonau
     * @version 1.0, 07/12/19
     * @since 1.0
     */
    private static class PartKeeper {
        /**
         * A part.
         */
        private Part part;
    }
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>by.training.task05.entity.User</code> class. The identifier
     * holds the value of "authenticatedUser".
     */
    private static final String AUTHENTICATED_USER_ATTRIBUTE_NAME
            = "authenticatedUser";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>java.util.Map</code> interface. The identifier holds the value
     * of "errorMessages".
     */
    private static final String ERROR_MESSAGES_ATTRIBUTE_NAME
            = "errorMessages";
    /**
     * The name of a property which retains the text that will be displayed
     * as an item of an unordered list to represent an error which means that
     * an image was not sent. The identifier holds the value of
     * "image.absence.error.message".
     */
    private static final String INPUT_ERROR_MESSAGE_1_KEY
            = "image.absence.error.message";
    /**
     * The name of a property which retains the text that will be displayed
     * as an item of an unordered list to represent an error which means that
     * a file sent is too big. The identifier holds the value of
     * "huge.file.error.message".
     */
    private static final String INPUT_ERROR_MESSAGE_2_KEY
            = "huge.file.error.message";
    /**
     * The name of a property which retains the text that will be displayed
     * as an item of an unordered list to represent an error which means that
     * a file sent is of a wrong type. The identifier holds the value of
     * "wrong.file.type.error.message".
     */
    private static final String INPUT_ERROR_MESSAGE_3_KEY
            = "wrong.file.type.error.message";
    /**
     * The name of a property which retains the text which will be placed to
     * present the information that an image has been successfully set.
     * The identifier holds the value of
     * "image.setting.success.response.message".
     */
    private static final String RESPONSE_MESSAGE_1_KEY
            = "image.setting.success.response.message";
    /**
     * The name of a property which retains the text which will be placed to
     * present the information that an image hasn't been set. The identifier
     * holds the value of "image.setting.failure.response.message".
     */
    private static final String RESPONSE_MESSAGE_2_KEY
            = "image.setting.failure.response.message";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>String</code> class that is the key of a property of a
     * response message. The identifier holds the value of
     * "responseMessageKey".
     */
    private static final String RESPONSE_MESSAGE_KEY_ATTRIBUTE_NAME
            = "responseMessageKey";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>String</code> class that is a target page path. The identifier
     * holds the value of "targetPagePath".
     */
    private static final String TARGET_PAGE_PATH_ATTRIBUTE_NAME
            = "targetPagePath";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>java.lang.String</code> class that is the name of a utilized
     * command. The identifier holds the value of "utilizedCommand".
     */
    public static final String UTILIZED_COMMAND_NAME_ATTRIBUTE_NAME
            = "utilizedCommand";
    /**
     * Fulfills actions needed to set an image.
     * @param request an <code>HttpServletRequest</code> value that is a
     *                request to set an image
     * @param response an <code>HttpServletResponse</code> value which is a
     *                 response to a request
     * @param servletContext a <code>ServletContext</code> value that is
     *                       the servlet context of the web application in
     *                       which a caller is executing
     * @return a <code>String</code> specifying the path to a resource. The
     *         path points to the page of setting an image if there have been
     *         input errors; otherwise it points to the page of viewing a
     *         response message. It can't be a <code>null</code> value.
     * @throws CommandException if one of the method arguments is a
     *                          <code>null</code> value or there are problems
     *                          with setting an image
     */
    @Override
    public String execute(final HttpServletRequest request,
            final HttpServletResponse response,
            final ServletContext servletContext) throws CommandException {
        ApplicationSettingsReader reader = getInstance();
        HttpSession session = request.getSession();
        Map<String, String> errorMessages = new LinkedHashMap<>();
        Part part = null;
        PartKeeper keeper = new PartKeeper();
        SQLDAOFactory factory = SQLDAOFactory.getInstance();
        String resourcePath;
        UserDAO userDAO = factory.retrieveUserDAO();
        User user = (User) session.getAttribute(
                AUTHENTICATED_USER_ATTRIBUTE_NAME);
        boolean correctnessCondition = true;
        int operationResult;
        /* The main part of the method. */
        correctnessCondition = checkImageInput(request, errorMessages, keeper);
        if (correctnessCondition) {
            try {
                part = keeper.part;
                operationResult = userDAO.setImage(user.getLogin(),
                        part.getInputStream());
            } catch (DAOException | IOException e) {
                throw new CommandException("Can't set an image.", e);
            }
            if (operationResult == IMAGE_SETTING_SUCCESS_VALUE) {
                session.setAttribute(RESPONSE_MESSAGE_KEY_ATTRIBUTE_NAME,
                        RESPONSE_MESSAGE_1_KEY);
            } else {
                session.setAttribute(RESPONSE_MESSAGE_KEY_ATTRIBUTE_NAME,
                        RESPONSE_MESSAGE_2_KEY);
            }
            session.setAttribute(UTILIZED_COMMAND_NAME_ATTRIBUTE_NAME,
                    request.getParameter(PARAMETER_NAME_1));
            resourcePath = reader.getProperty(
                    RESPONSE_MESSAGE_VIEWING_PAGE_PATH_KEY);
        } else {
            session.setAttribute(ERROR_MESSAGES_ATTRIBUTE_NAME, errorMessages);
            resourcePath = reader.getProperty(IMAGE_SETTING_PAGE_PATH_KEY);
            session.setAttribute(TARGET_PAGE_PATH_ATTRIBUTE_NAME,
                    resourcePath);
        }
        return resourcePath;
    }
    /**
     * Checks whether a supplied image file is correct.
     * @param request an <code>HttpServletRequest</code> value that is a
     *                request to set an image
     * @param errorMessages a <code>Map</code> value which is a map of keys of
     *                      input error messages to input error messages
     * @param keeper a <code>PartKeeper</code> value which is an object
     *               containing a part
     * @return <code>true</code> if a supplied image file is correct,
     *         <code>false</code> otherwise
     * @throws CommandException if there are problems with retrieving an image
     *                          file
     */
    private boolean checkImageInput(final HttpServletRequest request,
            final Map<String, String> errorMessages,
            final PartKeeper keeper) throws CommandException {
        ApplicationSettingsReader reader = getInstance();
        BufferedImage bufferedImage;
        InputStream stream;
        Part part = null;
        boolean correctnessCondition = true;
        int maximumSize = Integer.parseInt(
                reader.getProperty(MAXIMUM_IMAGE_FILE_SIZE_KEY));
        /* The main part of the method. */
        try {
            part = request.getPart(PARAMETER_NAME_20);
        } catch (ServletException e) {
            errorMessages.put(INPUT_ERROR_MESSAGE_1_KEY, "");
            correctnessCondition = false;
        } catch (IllegalStateException e) {
            errorMessages.put(INPUT_ERROR_MESSAGE_2_KEY, "");
            correctnessCondition = false;
        } catch (IOException e) {
            throw new CommandException("Can't set an image.", e);
        }
        if ((correctnessCondition) && (part.getSize() == 0)) {
            errorMessages.put(INPUT_ERROR_MESSAGE_1_KEY, "");
            correctnessCondition = false;
        }
        if ((correctnessCondition) && (part.getSize() > maximumSize)) {
            errorMessages.put(INPUT_ERROR_MESSAGE_2_KEY, "");
            correctnessCondition = false;
        }
        if (correctnessCondition) {
            keeper.part = part;
            try {
                stream = part.getInputStream();
                bufferedImage = ImageIO.read(stream);
                if (bufferedImage == null) {
                    errorMessages.put(INPUT_ERROR_MESSAGE_3_KEY, "");
                    correctnessCondition = false;
                }
                stream.close();
            } catch (IOException e) {
                errorMessages.put(INPUT_ERROR_MESSAGE_3_KEY, "");
                correctnessCondition = false;
            }
        }
        return correctnessCondition;
    }
}
