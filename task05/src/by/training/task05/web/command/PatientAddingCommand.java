/*
 * PatientAddingCommand.java
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to add a patient.
 */
package by.training.task05.web.command;


import static by.training.task05.dao.HeadPhysicianDAO
        .PATIENT_ADDING_SUCCESS_VALUE;

import static by.training.task05.reader.ApplicationSettingsReader
        .PATIENT_ADDING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .RESPONSE_MESSAGE_VIEWING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader.getInstance;

import static by.training.task05.web.command.DataValidator.checkMinLength;
import static by.training.task05.web.command.DataValidator.checkPassportNumber;
import static by.training.task05.web.command.DataValidator.checkWord;

import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_1;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_3;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_4;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_17;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_18;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_19;

import static by.training.task05.web.command.SupportMethodsKeeper
        .convertString;
import static by.training.task05.web.command.SupportMethodsKeeper
        .encodeString;

import by.training.task05.dao.DAOException;
import by.training.task05.dao.HeadPhysicianDAO;

import by.training.task05.dao.implementation.SQLDAOFactory;

import by.training.task05.reader.ApplicationSettingsReader;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to add a patient.
 * @author Yauhen Sazonau
 * @version 1.0, 07/06/19
 * @since 1.0
 */
public class PatientAddingCommand implements Command {
    /**
     * A keeper of request parameters.
     * @author Yauhen Sazonau
     * @version 1.0, 07/06/19
     * @since 1.0
     */
    private static class ParameterKeeper {
        /**
         * A passport number.
         */
        private String passportNumber;
        /**
         * A first name.
         */
        private String firstName;
        /**
         * A last name.
         */
        private String lastName;
        /**
         * A login.
         */
        private String login;
        /**
         * A password.
         */
        private String password;
    }
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>java.util.Map</code> interface. The identifier holds the value
     * of "errorMessages".
     */
    private static final String ERROR_MESSAGES_ATTRIBUTE_NAME
            = "errorMessages";
    /**
     * The name of a property which retains the text that will be displayed
     * as an item of an unordered list to represent an error of a passport
     * number input. The identifier holds the value of
     * "passport.number.input.error.message".
     */
    private static final String INPUT_ERROR_MESSAGE_1_KEY
            = "passport.number.input.error.message";
    /**
     * The name of a property which retains the text that will be displayed
     * as an item of an unordered list to represent an error of a first name
     * input. The identifier holds the value of
     * "first.name.input.error.message".
     */
    private static final String INPUT_ERROR_MESSAGE_2_KEY
            = "first.name.input.error.message";
    /**
     * The name of a property which retains the text that will be displayed
     * as an item of an unordered list to represent an error of a last name
     * input. The identifier holds the value of
     * "last.name.input.error.message".
     */
    private static final String INPUT_ERROR_MESSAGE_3_KEY
            = "last.name.input.error.message";
    /**
     * The name of a property which retains the text that will be displayed
     * as an item of an unordered list to represent an error of a login input.
     * The identifier holds the value of "login.input.error.message".
     */
    private static final String INPUT_ERROR_MESSAGE_4_KEY
            = "login.input.error.message";
    /**
     * The name of a property which retains the text that will be displayed
     * as an item of an unordered list to represent an error of a password
     * input. The identifier holds the value of "password.input.error.message".
     */
    private static final String INPUT_ERROR_MESSAGE_5_KEY
            = "password.input.error.message";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>String</code> class that is the key of a property of a
     * response message. The identifier holds the value of
     * "responseMessageKey".
     */
    private static final String RESPONSE_MESSAGE_KEY_ATTRIBUTE_NAME
            = "responseMessageKey";
    /**
     * The name of a property which retains the text which will be placed to
     * present the information that a patient has been successfully added.
     * The identifier holds the value of
     * "patient.adding.success.response.message".
     */
    private static final String RESPONSE_MESSAGE_1_KEY
            = "patient.adding.success.response.message";
    /**
     * The name of a property which retains the text which will be placed to
     * present the information that a patient hasn't been added. The
     * identifier holds the value of
     * "patient.adding.failure.response.message".
     */
    private static final String RESPONSE_MESSAGE_2_KEY
            = "patient.adding.failure.response.message";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>String</code> class that is a target page path. The identifier
     * holds the value of "targetPagePath".
     */
    private static final String TARGET_PAGE_PATH_ATTRIBUTE_NAME
            = "targetPagePath";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>java.lang.String</code> class that is the name of a utilized
     * command. The identifier holds the value of "utilizedCommand".
     */
    private static final String UTILIZED_COMMAND_NAME_ATTRIBUTE_NAME
            = "utilizedCommand";
    /**
     * Fulfills actions needed to add a patient.
     * @param request an <code>HttpServletRequest</code> value that is a
     *                request to add a patient
     * @param response an <code>HttpServletResponse</code> value which is a
     *                 response to a request
     * @param servletContext a <code>ServletContext</code> value that is
     *                       the servlet context of the web application in
     *                       which a caller is executing
     * @return a <code>String</code> specifying the path to a resource. The
     *         path points to the page of adding a patient if there have
     *         been input errors; otherwise it points to the page of viewing
     *         a response message. It can't be a <code>null</code> value.
     * @throws CommandException if one of the method arguments is a
     *                          <code>null</code> value or there are problems
     *                          with adding a patient
     */
    @Override
    public String execute(final HttpServletRequest request,
            final HttpServletResponse response,
            final ServletContext servletContext) throws CommandException {
        ApplicationSettingsReader reader = getInstance();
        HttpSession session;
        Map<String, String> errorMessages = new LinkedHashMap<>();
        ParameterKeeper keeper;
        SQLDAOFactory factory = SQLDAOFactory.getInstance();
        HeadPhysicianDAO headPhysicianDAO = factory.retrieveHeadPhysicianDAO();
        String path;
        String resourcePath;
        boolean correctnessCondition;
        int operationResult;
        /* The main part of the method. */
        if (request == null) {
            throw new CommandException("The 'request' argument is "
                    + "'null'.");
        }
        if (servletContext == null) {
            throw new CommandException("The 'servletContext' argument "
                    + "is 'null'.");
        }
        keeper = readParameters(request);
        session = request.getSession();
        correctnessCondition = checkParameters(keeper.passportNumber,
                keeper.firstName, keeper.lastName, keeper.login,
                keeper.password, errorMessages);
        if (correctnessCondition) {
            try {
                operationResult = headPhysicianDAO.addPatient(
                        keeper.passportNumber, keeper.firstName,
                        keeper.lastName, keeper.login, keeper.password);
            } catch (DAOException e) {
                throw new CommandException("Can't add a patient.", e);
            }
            if (operationResult == PATIENT_ADDING_SUCCESS_VALUE) {
                session.setAttribute(RESPONSE_MESSAGE_KEY_ATTRIBUTE_NAME,
                        RESPONSE_MESSAGE_1_KEY);
            } else {
                session.setAttribute(RESPONSE_MESSAGE_KEY_ATTRIBUTE_NAME,
                        RESPONSE_MESSAGE_2_KEY);
            }
            session.setAttribute(UTILIZED_COMMAND_NAME_ATTRIBUTE_NAME,
                    request.getParameter(PARAMETER_NAME_1));
            resourcePath = reader.getProperty(
                    RESPONSE_MESSAGE_VIEWING_PAGE_PATH_KEY);
        } else {
            session.setAttribute(ERROR_MESSAGES_ATTRIBUTE_NAME, errorMessages);
            path = reader.getProperty(PATIENT_ADDING_PAGE_PATH_KEY);
            session.setAttribute(TARGET_PAGE_PATH_ATTRIBUTE_NAME, path);
            resourcePath = encodeQueryString(path, keeper);
        }
        return resourcePath;
    }
    /**
     * Checks that request parameters comply with defined constraints.
     * @param passportNumber a <code>String</code> value which is a passport
     *                       number parameter value
     * @param firstName a <code>String</code> value which is a first name
     *                  parameter value
     * @param lastName a <code>String</code> value which is a last name
     *                 parameter value
     * @param login a <code>String</code> value which is a login
     *              parameter value
     * @param password a <code>String</code> value which is a password
     *                 parameter value
     * @param messages a <code>Map</code> value which is a map of keys of
     *                 input error messages to input error messages
     * @return <code>true</code> if request parameters comply with defined
     *         constraints, <code>false</code> otherwise
     */
    private boolean checkParameters(final String passportNumber,
            final String firstName, final String lastName,
            final String login, final String password,
            final Map<String, String> messages) {
        boolean correctnessCondition = true;
        /* The main part of the method. */
        if (!checkPassportNumber(passportNumber)) {
            messages.put(INPUT_ERROR_MESSAGE_1_KEY, "");
            correctnessCondition = false;
        }
        if (!checkWord(firstName)) {
            messages.put(INPUT_ERROR_MESSAGE_2_KEY, "");
            correctnessCondition = false;
        }
        if (!checkWord(lastName)) {
            messages.put(INPUT_ERROR_MESSAGE_3_KEY, "");
            correctnessCondition = false;
        }
        if (!checkMinLength(login)) {
            messages.put(INPUT_ERROR_MESSAGE_4_KEY, "");
            correctnessCondition = false;
        }
        if (!checkMinLength(password)) {
            messages.put(INPUT_ERROR_MESSAGE_5_KEY, "");
            correctnessCondition = false;
        }
        return correctnessCondition;
    }
    /**
     * Adds a query string to a specified path.
     * @param path a <code>String</code> value which is a path
     * @param keeper an <code>ParameterKeeper</code> value which is an object
     *               containing request parameters
     * @return a path with an added query string. Can't be a <code>null</code>
     *         value.
     */
    private String encodeQueryString(final String path,
            final ParameterKeeper keeper) {
        return (path + "?" + PARAMETER_NAME_17 + "="
                + encodeString(keeper.passportNumber) + "&"
                + PARAMETER_NAME_18 + "=" + encodeString(keeper.firstName)
                + "&" + PARAMETER_NAME_19 + "=" + encodeString(keeper.lastName)
                + "&" + PARAMETER_NAME_3 + "=" + encodeString(keeper.login)
                + "&" + PARAMETER_NAME_4 + "="
                + encodeString(keeper.password));
    }
    /**
     * Reads request parameters.
     * @param request an <code>HttpServletRequest</code> value that is a
     *                request to add a patient
     * @return an object containing read parameters
     */
    private ParameterKeeper readParameters(final HttpServletRequest request) {
        ParameterKeeper keeper = new ParameterKeeper();
        /* The main part of the method. */
        keeper.passportNumber
                = convertString(request.getParameter(PARAMETER_NAME_17));
        keeper.firstName
                = convertString(request.getParameter(PARAMETER_NAME_18));
        keeper.lastName
                = convertString(request.getParameter(PARAMETER_NAME_19));
        keeper.login
                = convertString(request.getParameter(PARAMETER_NAME_3));
        keeper.password
                = convertString(request.getParameter(PARAMETER_NAME_4));
        return keeper;
    }
}
