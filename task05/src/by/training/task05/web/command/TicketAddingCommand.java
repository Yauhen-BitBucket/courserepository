/*
 * TicketAddingCommand.java
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to add a ticket.
 */
package by.training.task05.web.command;


import static by.training.task05.dao.PhysicianDAO.TICKET_ADDING_SUCCESS_VALUE;

import static by.training.task05.reader.ApplicationSettingsReader
        .RESPONSE_MESSAGE_VIEWING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .TICKET_ADDING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader.getInstance;

import static by.training.task05.web.command.DataValidator
        .checkDateCorrectness;
import static by.training.task05.web.command.DataValidator
        .checkTimeCorrectness;

import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_1;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_7;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_9;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_10;

import static by.training.task05.web.command.SupportMethodsKeeper
        .convertString;
import static by.training.task05.web.command.SupportMethodsKeeper.encodeString;
import static by.training.task05.web.command.SupportMethodsKeeper
        .retrieveCurrentDate;

import by.training.task05.dao.DAOException;
import by.training.task05.dao.PhysicianDAO;

import by.training.task05.dao.implementation.SQLDAOFactory;

import by.training.task05.entity.User;

import by.training.task05.reader.ApplicationSettingsReader;

import java.sql.Date;
import java.sql.Time;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to add a ticket.
 * @author Yauhen Sazonau
 * @version 1.0, 07/04/19
 * @since 1.0
 */
public class TicketAddingCommand implements Command {
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>by.training.task05.entity.User</code> class. The identifier
     * holds the value of "authenticatedUser".
     */
    private static final String AUTHENTICATED_USER_ATTRIBUTE_NAME
            = "authenticatedUser";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>java.util.Map</code> interface. The identifier holds the value
     * of "errorMessages".
     */
    private static final String ERROR_MESSAGES_ATTRIBUTE_NAME
            = "errorMessages";
    /**
     * An exception message. The identifier holds the value
     * of "The 'servletContext' argument is 'null'.".
     */
    private static final String EXCEPTION_MESSAGE
            = "The 'servletContext' argument is 'null'.";
    /**
     * The name of a property which retains the text that will be displayed
     * as an item of an unordered list to represent an error of a date input.
     * The identifier holds the value of "date.input.error.message".
     */
    private static final String INPUT_ERROR_MESSAGE_1_KEY
            = "date.input.error.message";
    /**
     * The name of a property which retains the text that will be displayed
     * as an item of an unordered list to represent an error which means that
     * a date precedes the current date. The identifier holds the value of
     * "current.date.precendence.error.message".
     */
    private static final String INPUT_ERROR_MESSAGE_2_KEY
            = "current.date.precendence.error.message";
    /**
     * The name of a property which retains the text that will be displayed
     * as an item of an unordered list to represent an error of a starting time
     * input. The identifier holds the value of
     * "starting.time.input.error.message".
     */
    private static final String INPUT_ERROR_MESSAGE_3_KEY
            = "starting.time.input.error.message";
    /**
     * The name of a property which retains the text that will be displayed
     * as an item of an unordered list to represent an error of an ending time
     * input. The identifier holds the value of
     * "ending.time.input.error.message".
     */
    private static final String INPUT_ERROR_MESSAGE_4_KEY
            = "ending.time.input.error.message";
    /**
     * The name of a property which retains the text that will be displayed
     * as an item of an unordered list to represent an error which means that
     * a starting time doesn't precede an ending time. The identifier holds the
     * value of "time.precendence.error.message".
     */
    private static final String INPUT_ERROR_MESSAGE_5_KEY
            = "time.precendence.error.message";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>String</code> class that is the key of a property of a
     * response message. The identifier holds the value of
     * "responseMessageKey".
     */
    private static final String RESPONSE_MESSAGE_KEY_ATTRIBUTE_NAME
            = "responseMessageKey";
    /**
     * The name of a property which retains the text which will be placed to
     * present the information that a ticket has been successfully added.
     * The identifier holds the value of
     * "ticket.adding.success.response.message".
     */
    private static final String RESPONSE_MESSAGE_1_KEY
            = "ticket.adding.success.response.message";
    /**
     * The name of a property which retains the text which will be placed to
     * present the information that a ticket hasn't been added. The
     * identifier holds the value of
     * "ticket.adding.failure.response.message".
     */
    private static final String RESPONSE_MESSAGE_2_KEY
            = "ticket.adding.failure.response.message";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>String</code> class that is a target page path. The identifier
     * holds the value of "targetPagePath".
     */
    private static final String TARGET_PAGE_PATH_ATTRIBUTE_NAME
            = "targetPagePath";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>java.lang.String</code> class that is the name of a utilized
     * command. The identifier holds the value of "utilizedCommand".
     */
    private static final String UTILIZED_COMMAND_NAME_ATTRIBUTE_NAME
            = "utilizedCommand";
    /**
     * Fulfills actions needed to add a ticket.
     * @param request an <code>HttpServletRequest</code> value that is a
     *                request to add a ticket
     * @param response an <code>HttpServletResponse</code> value which is a
     *                 response to a request
     * @param servletContext a <code>ServletContext</code> value that is
     *                       the servlet context of the web application in
     *                       which a caller is executing
     * @return a <code>String</code> specifying the path to a resource. The
     *         path points to the page of adding a ticket if there have
     *         been input errors; otherwise it points to the page of viewing
     *         a response message. It can't be a <code>null</code> value.
     * @throws CommandException if one of the method arguments is a
     *                          <code>null</code> value or there are problems
     *                          with adding a ticket
     */
    @Override
    public String execute(final HttpServletRequest request,
            final HttpServletResponse response,
            final ServletContext servletContext) throws CommandException {
        ApplicationSettingsReader reader = getInstance();
        HttpSession session;
        Map<String, String> errorMessages = new LinkedHashMap<>();
        SQLDAOFactory factory = SQLDAOFactory.getInstance();
        PhysicianDAO physicianDAO = factory.retrievePhysicianDAO();
        String dateValue;
        String path;
        String resourcePath;
        String startingValue;
        String endingValue;
        User user;
        boolean correctnessCondition;
        int operationResult;
        /* The main part of the method. */
        checkArguments(request, servletContext);
        session = request.getSession();
        dateValue = request.getParameter(PARAMETER_NAME_7);
        startingValue = request.getParameter(PARAMETER_NAME_9);
        endingValue = request.getParameter(PARAMETER_NAME_10);
        user = (User) session.getAttribute(AUTHENTICATED_USER_ATTRIBUTE_NAME);
        correctnessCondition = checkParameters(dateValue, startingValue,
                endingValue, errorMessages);
        if (correctnessCondition) {
            try {
                operationResult = physicianDAO.addTicket(user.getUserID(),
                        Date.valueOf(dateValue), Time.valueOf(startingValue),
                        Time.valueOf(endingValue));
            } catch (DAOException e) {
                throw new CommandException("Can't add a ticket.", e);
            }
            if (operationResult == TICKET_ADDING_SUCCESS_VALUE) {
                session.setAttribute(RESPONSE_MESSAGE_KEY_ATTRIBUTE_NAME,
                        RESPONSE_MESSAGE_1_KEY);
            } else {
                session.setAttribute(RESPONSE_MESSAGE_KEY_ATTRIBUTE_NAME,
                        RESPONSE_MESSAGE_2_KEY);
            }
            session.setAttribute(UTILIZED_COMMAND_NAME_ATTRIBUTE_NAME,
                    request.getParameter(PARAMETER_NAME_1));
            resourcePath = reader.getProperty(
                    RESPONSE_MESSAGE_VIEWING_PAGE_PATH_KEY);
        } else {
            session.setAttribute(ERROR_MESSAGES_ATTRIBUTE_NAME, errorMessages);
            path = reader.getProperty(TICKET_ADDING_PAGE_PATH_KEY);
            session.setAttribute(TARGET_PAGE_PATH_ATTRIBUTE_NAME, path);
            resourcePath = encodeQueryString(path, request);
        }
        return resourcePath;
    }
    /**
     * Checks that request parameters comply with defined constraints.
     * @param dateAsString a <code>String</code> value which is a date
     *                     parameter value
     * @param startingTime a <code>String</code> value which is a starting time
     *                     to be validated
     * @param endingTime a <code>String</code> value which is an ending time to
     *                   be validated
     * @param messages a <code>Map</code> value which is a map of keys of
     *                 input error messages to input error messages
     * @return <code>true</code> if request parameters comply with defined
     *         constraints, <code>false</code> otherwise
     */
    private boolean checkParameters(final String dateAsString,
            final String startingTime, final String endingTime,
            final Map<String, String> messages) {
        Date date;
        boolean correctnessCondition = true;
        boolean firstCondition = true;
        boolean secondCondition = true;
        /* The main part of the method. */
        if (!checkDateCorrectness(dateAsString)) {
            messages.put(INPUT_ERROR_MESSAGE_1_KEY, "");
            correctnessCondition = false;
        } else {
            date = Date.valueOf(dateAsString);
            if (date.before(retrieveCurrentDate())) {
                messages.put(INPUT_ERROR_MESSAGE_2_KEY, "");
                correctnessCondition = false;
            }
        }
        if (!checkTimeCorrectness(startingTime)) {
            messages.put(INPUT_ERROR_MESSAGE_3_KEY, "");
            firstCondition = false;
            correctnessCondition = false;
        }
        if (!checkTimeCorrectness(endingTime)) {
            messages.put(INPUT_ERROR_MESSAGE_4_KEY, "");
            secondCondition = false;
            correctnessCondition = false;
        }
        if ((firstCondition) && (secondCondition)
                && (!((Time.valueOf(startingTime))
                      .before(Time.valueOf(endingTime))))) {
            messages.put(INPUT_ERROR_MESSAGE_5_KEY, "");
            correctnessCondition = false;
        }
        return correctnessCondition;
    }
    /**
     * Adds a query string to a specified path.
     * @param path a <code>String</code> value which is a path
     * @param request an <code>HttpServletRequest</code> value which is a
     *                request
     * @return a path with an added query string. Can't be a <code>null</code>
     *         value.
     */
    private String encodeQueryString(final String path,
            final HttpServletRequest request) {
        /* The main part of the method. */
        return (path + "?" + PARAMETER_NAME_7 + "="
                + encodeString(convertString(
                        request.getParameter(PARAMETER_NAME_7)))
                + "&" + PARAMETER_NAME_9 + "="
                + encodeString(convertString(
                        request.getParameter(PARAMETER_NAME_9)))
                + "&" + PARAMETER_NAME_10 + "="
                + encodeString(convertString(
                        request.getParameter(PARAMETER_NAME_10))));
    }
    /**
     * Verifies if arguments are <code>null</code>.
     * @param request an <code>HttpServletRequest</code> value that is a
     *                request to add a ticket
     * @param servletContext a <code>ServletContext</code> value that is
     *                       the servlet context of the web application in
     *                       which a caller is executing
     * @throws CommandException if one of the method arguments is a
     *                          <code>null</code> value
     */
    private void checkArguments(final HttpServletRequest request,
            final ServletContext servletContext) throws CommandException {
        if (request == null) {
            throw new CommandException("The 'request' argument is 'null'.");
        }
        if (servletContext == null) {
            throw new CommandException(EXCEPTION_MESSAGE);
        }
    }
}
