/*
 * AssignedTicketFetchingCommand.java
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to retrieve assigned ticket
 * information.
 */
package by.training.task05.web.command;


import static by.training.task05.reader.ApplicationSettingsReader
        .ASSIGNED_TICKET_FETCHING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .ASSIGNED_TICKET_VIEWING_PAGE_PATH_KEY;

import static by.training.task05.web.command.DataValidator
        .checkDateCorrectness;

import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_5;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_7;

import static by.training.task05.web.command.SupportMethodsKeeper
        .convertString;
import static by.training.task05.web.command.SupportMethodsKeeper.encodeString;
import static by.training.task05.web.command.SupportMethodsKeeper
        .retrieveCurrentDate;

import by.training.task05.dao.DAOException;
import by.training.task05.dao.PhysicianDAO;

import by.training.task05.dao.implementation.SQLDAOFactory;

import by.training.task05.entity.User;

import by.training.task05.reader.ApplicationSettingsReader;

import java.sql.Date;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to retrieve assigned ticket
 * information.
 * @author Yauhen Sazonau
 * @version 1.0, 07/05/19
 * @since 1.0
 */
public class AssignedTicketFetchingCommand implements Command {
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>java.util.Collection</code> interface that is a collection of
     * assigned tickets. The identifier holds the value of "assignedTickets".
     */
    private static final String ASSIGNED_TICKETS_ATTRIBUTE_NAME
            = "assignedTickets";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>by.training.task05.entity.User</code> class. The identifier
     * holds the value of "authenticatedUser".
     */
    private static final String AUTHENTICATED_USER_ATTRIBUTE_NAME
            = "authenticatedUser";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>java.util.Map</code> interface. The identifier holds the value
     * of "errorMessages".
     */
    private static final String ERROR_MESSAGES_ATTRIBUTE_NAME
            = "errorMessages";
    /**
     * The name of a property which retains the text that will be displayed
     * as an item of an unordered list to represent an error of a date input.
     * The identifier holds the value of "date.input.error.message".
     */
    private static final String INPUT_ERROR_MESSAGE_1_KEY
            = "date.input.error.message";
    /**
     * The name of a property which retains the text that will be displayed
     * as an item of an unordered list to represent an error which means that
     * a date precedes the current date. The identifier holds the value of
     * "current.date.precendence.error.message".
     */
    private static final String INPUT_ERROR_MESSAGE_2_KEY
            = "current.date.precendence.error.message";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>String</code> class that is a target page path. The identifier
     * holds the value of "targetPagePath".
     */
    private static final String TARGET_PAGE_PATH_ATTRIBUTE_NAME
            = "targetPagePath";
    /**
     * Fulfills actions needed to retrieve assigned ticket information.
     * @param request an <code>HttpServletRequest</code> value that is a
     *                request to retrieve assigned ticket information
     * @param response an <code>HttpServletResponse</code> value which is a
     *                 response to a request
     * @param servletContext a <code>ServletContext</code> value that is
     *                       the servlet context of the web application in
     *                       which a caller is executing
     * @return a <code>String</code> specifying the path to a resource. The
     *         path points to the page of fetching assigned ticket information
     *         if there have been input errors; otherwise it points to the
     *         page of viewing assigned ticket information. It can't be a
     *         <code>null</code> value.
     * @throws CommandException if one of the method arguments is a
     *                          <code>null</code> value or there are problems
     *                          with retrieving assigned ticket information
     */
    @Override
    public String execute(final HttpServletRequest request,
            final HttpServletResponse response,
            final ServletContext servletContext) throws CommandException {
        ApplicationSettingsReader reader
                = ApplicationSettingsReader.getInstance();
        HttpSession session;
        Map<String, String> errorMessages = new LinkedHashMap<>();
        SQLDAOFactory factory = SQLDAOFactory.getInstance();
        PhysicianDAO physicianDAO = factory.retrievePhysicianDAO();
        String dateAsString;
        String path;
        String resourcePath;
        User user;
        boolean correctnessCondition;
        /* The main part of the method. */
        if (request == null) {
            throw new CommandException("The 'request' argument is "
                    + "'null'.");
        }
        if (servletContext == null) {
            throw new CommandException("The 'servletContext' argument "
                    + "is 'null'.");
        }
        session = request.getSession();
        user = (User) session.getAttribute(AUTHENTICATED_USER_ATTRIBUTE_NAME);
        dateAsString = convertString(request.getParameter(PARAMETER_NAME_7));
        correctnessCondition = checkParameters(dateAsString, errorMessages);
        if (correctnessCondition) {
            try {
                session.setAttribute(ASSIGNED_TICKETS_ATTRIBUTE_NAME,
                        physicianDAO.retrieveAssignedTickets(user.getUserID(),
                                Date.valueOf(dateAsString)));
            } catch (DAOException e) {
                throw new CommandException("Can't retrieve assigned ticket "
                        + "data.", e);
            }
            resourcePath
                    = reader.getProperty(ASSIGNED_TICKET_VIEWING_PAGE_PATH_KEY)
                      + "?" + PARAMETER_NAME_5 + "=" + 1;
        } else {
            session.setAttribute(ERROR_MESSAGES_ATTRIBUTE_NAME, errorMessages);
            path = reader.getProperty(ASSIGNED_TICKET_FETCHING_PAGE_PATH_KEY);
            session.setAttribute(TARGET_PAGE_PATH_ATTRIBUTE_NAME, path);
            resourcePath = path + "?" + PARAMETER_NAME_7 + "="
                           + encodeString(dateAsString);
        }
        return resourcePath;
    }
    /**
     * Checks that request parameters comply with defined constraints.
     * @param dateAsString a <code>String</code> value which is a date
     *                     parameter value
     * @param messages a <code>Map</code> value which is a map of keys of
     *                 input error messages to input error messages
     * @return <code>true</code> if request parameters comply with defined
     *         constraints, <code>false</code> otherwise
     */
    private boolean checkParameters(final String dateAsString,
            final Map<String, String> messages) {
        Date date;
        boolean correctnessCondition = true;
        /* The main part of the method. */
        if (!checkDateCorrectness(dateAsString)) {
            messages.put(INPUT_ERROR_MESSAGE_1_KEY, "");
            correctnessCondition = false;
        } else {
            date = Date.valueOf(dateAsString);
            if (date.before(retrieveCurrentDate())) {
                messages.put(INPUT_ERROR_MESSAGE_2_KEY, "");
                correctnessCondition = false;
            }
        }
        return correctnessCondition;
    }
}
