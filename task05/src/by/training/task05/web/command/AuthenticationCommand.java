/*
 * AuthenticationCommand.java
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to verify if a user is known to
 * the application.
 */
package by.training.task05.web.command;


import static by.training.task05.reader.ApplicationSettingsReader
        .AUTHENTICATION_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .MAIN_PAGE_PATH_KEY;

import static by.training.task05.web.command.DataValidator.checkMinLength;

import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_3;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_4;

import static by.training.task05.web.command.SupportMethodsKeeper
        .convertString;
import static by.training.task05.web.command.SupportMethodsKeeper
        .encodeString;

import by.training.task05.dao.DAOException;
import by.training.task05.dao.UserDAO;

import by.training.task05.dao.implementation.SQLDAOFactory;

import by.training.task05.entity.User;

import by.training.task05.reader.ApplicationSettingsReader;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

import javax.servlet.ServletContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to verify if a user is known to
 * the application.
 * @author Yauhen Sazonau
 * @version 1.0, 06/30/19
 * @since 1.0
 */
public class AuthenticationCommand implements Command {
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>by.training.task05.entity.User</code> class. The identifier
     * holds the value of "authenticatedUser".
     */
    private static final String AUTHENTICATED_USER_ATTRIBUTE_NAME
            = "authenticatedUser";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>java.util.Map</code> interface. The identifier holds the value
     * of "errorMessages".
     */
    private static final String ERROR_MESSAGES_ATTRIBUTE_NAME
            = "errorMessages";
    /**
     * The name of a property which retains the text that will be displayed
     * as an item of an unordered list to represent an error of a login input.
     * The identifier holds the value of "login.input.error.message".
     */
    private static final String INPUT_ERROR_MESSAGE_1_KEY
            = "login.input.error.message";
    /**
     * The name of a property which retains the text that will be displayed
     * as an item of an unordered list to represent an error of a password
     * input. The identifier holds the value of "password.input.error.message".
     */
    private static final String INPUT_ERROR_MESSAGE_2_KEY
            = "password.input.error.message";
    /**
     * The name of a property which retains the text that will be displayed
     * as an item of an unordered list to represent an error of authentication.
     * The identifier holds the value of "authentication.error.message".
     */
    private static final String INPUT_ERROR_MESSAGE_3_KEY
            = "authentication.error.message";
    /**
     * The name of a context attribute which is used to retain an instance of
     * the <code>Map</code> interface that represents a map which maps a
     * passport number to a session. The identifier holds the value of
     * "sessionMap".
     */
    private static final String SESSION_MAP_ATTRIBUTE_NAME = "sessionMap";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>String</code> class that is a target page path. The identifier
     * holds the value of "targetPagePath".
     */
    private static final String TARGET_PAGE_PATH_ATTRIBUTE_NAME
            = "targetPagePath";
    /**
     * Fulfills actions needed to verify if a user is known to an application.
     * @param request an <code>HttpServletRequest</code> value that is a
     *                request to verify if a user is known to an application
     * @param response an <code>HttpServletResponse</code> value which is a
     *                 response to a request
     * @param servletContext a <code>ServletContext</code> value that is
     *                       the servlet context of the web application in
     *                       which a caller is executing
     * @return a <code>String</code> specifying the path to a resource. The
     *         path points to the authentication page if there have been input
     *         errors; otherwise it points to the main page. It can't be a
     *         <code>null</code> value.
     * @throws CommandException if there are problems with verifying
     *                          whether a user is known to an application
     */
    @Override
    public String execute(final HttpServletRequest request,
            final HttpServletResponse response,
            final ServletContext servletContext) throws CommandException {
        ApplicationSettingsReader reader
                = ApplicationSettingsReader.getInstance();
        SQLDAOFactory factory = SQLDAOFactory.getInstance();
        HttpSession session;
        Map<String, String> errorMessages;
        Optional<User> optional;
        String login;
        String password;
        String path;
        String resourcePath = null;
        UserDAO userDAO;
        boolean correctnessCondition;
        /* The main part of the method. */
        errorMessages = new LinkedHashMap<>();
        session = request.getSession();
        login = convertString(request.getParameter(PARAMETER_NAME_3));
        password = convertString(request.getParameter(PARAMETER_NAME_4));
        correctnessCondition = checkParameters(login, password, errorMessages);
        if (correctnessCondition) {
            userDAO = factory.retrieveUserDAO();
            try {
                optional = userDAO.retrieveUser(login, password);
            } catch (DAOException e) {
                throw new CommandException("An error of communication with a "
                        + "source of data arisen while trying to retrieve the "
                        + "information about a user.", e);
            }
            if (optional.isPresent()) {
                storeSession(session, login, servletContext, userDAO);
                session.setAttribute(AUTHENTICATED_USER_ATTRIBUTE_NAME,
                        optional.get());
                resourcePath
                        = reader.getProperty(MAIN_PAGE_PATH_KEY);
            } else {
                errorMessages.put(INPUT_ERROR_MESSAGE_3_KEY, "");
                correctnessCondition = false;
            }
        }
        if (!correctnessCondition) {
            session.setAttribute(ERROR_MESSAGES_ATTRIBUTE_NAME, errorMessages);
            path = reader.getProperty(AUTHENTICATION_PAGE_PATH_KEY);
            session.setAttribute(TARGET_PAGE_PATH_ATTRIBUTE_NAME, path);
            resourcePath = path + "?" + PARAMETER_NAME_3 + "="
                           + encodeString(login);
        }
        return resourcePath;
    }
    /**
     * Checks that request parameters comply with defined constraints.
     * @param login a <code>String</code> value which is a login parameter
     *              value
     * @param password a <code>String</code> value which is a password
     *                 parameter value
     * @param messages a <code>Map</code> value which is a map of keys of
     *                 input error messages to input error messages
     * @return <code>true</code> if request parameters comply with defined
     *         constraints, <code>false</code> otherwise
     */
    private boolean checkParameters(final String login, final String password,
            final Map<String, String> messages) {
        boolean correctnessCondition = true;
        /* The main part of the method. */
        if (!checkMinLength(login)) {
            messages.put(INPUT_ERROR_MESSAGE_1_KEY, "");
            correctnessCondition = false;
        }
        if (!checkMinLength(password)) {
            messages.put(INPUT_ERROR_MESSAGE_2_KEY, "");
            correctnessCondition = false;
        }
        return correctnessCondition;
    }
    /**
     * Stores a session into a servlet context map.
     * @param session an <code>HttpSession</code> value which is a session to
     *                be stored
     * @param login a <code>String</code> value which is a login parameter
     *              value
     * @param context a <code>ServletContext</code> value that is the servlet
     *                context of the web application in which a caller is
     *                executing
     * @param userDAO a <code>UserDAO</code> value that is a user DAO
     * @throws CommandException if there are problems with storing a session
     */
    private void storeSession(final HttpSession session, final String login,
            final ServletContext context,
            final UserDAO userDAO) throws CommandException {
        Map<String, HttpSession> sessionMap;
        Optional<String> optional;
        /* The main part of the method. */
        sessionMap = (Map) context.getAttribute(SESSION_MAP_ATTRIBUTE_NAME);
        try {
            optional = userDAO.retrievePassportNumber(login);
            sessionMap.put(optional.get(), session);
        } catch (DAOException e) {
            throw new CommandException("An error of communication "
                    + "with a source of data arisen while trying to "
                    + "retrieve the information about a user.", e);
        }
    }
}
