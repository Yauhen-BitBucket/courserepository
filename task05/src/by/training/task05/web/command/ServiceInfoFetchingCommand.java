/*
 * ServiceInfoFetchingCommand.java
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to retrieve service information.
 */
package by.training.task05.web.command;


import static by.training.task05.reader.ApplicationSettingsReader
        .SERVICE_INFO_FETCHING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .SERVICE_INFO_VIEWING_PAGE_PATH_KEY;

import static by.training.task05.web.command.DataValidator
        .checkMonthCorrectness;
import static by.training.task05.web.command.DataValidator
        .checkYearCorrectness;

import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_5;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_15;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_16;

import static by.training.task05.web.command.SupportMethodsKeeper
        .convertString;
import static by.training.task05.web.command.SupportMethodsKeeper.encodeString;

import by.training.task05.dao.DAOException;
import by.training.task05.dao.HeadPhysicianDAO;

import by.training.task05.dao.implementation.SQLDAOFactory;

import by.training.task05.reader.ApplicationSettingsReader;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to retrieve service information.
 * @author Yauhen Sazonau
 * @version 1.0, 07/06/19
 * @since 1.0
 */
public class ServiceInfoFetchingCommand implements Command {
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>java.util.Map</code> interface. The identifier holds the value
     * of "errorMessages".
     */
    private static final String ERROR_MESSAGES_ATTRIBUTE_NAME
            = "errorMessages";
    /**
     * The name of a property which retains the text that will be displayed
     * as an item of an unordered list to represent an error of a year input.
     * The identifier holds the value of "year.input.error.message".
     */
    private static final String INPUT_ERROR_MESSAGE_1_KEY
            = "year.input.error.message";
    /**
     * The name of a property which retains the text that will be displayed
     * as an item of an unordered list to represent an error of a month input.
     * The identifier holds the value of "month.input.error.message".
     */
    private static final String INPUT_ERROR_MESSAGE_2_KEY
            = "month.input.error.message";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>java.util.Map</code> interface that is a map of physicians to
     * serviced patient count. The identifier holds the value of "serviceInfo".
     */
    private static final String SERVICE_INFO_ATTRIBUTE_NAME
            = "serviceInfo";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>String</code> class that is a target page path. The identifier
     * holds the value of "targetPagePath".
     */
    private static final String TARGET_PAGE_PATH_ATTRIBUTE_NAME
            = "targetPagePath";
    /**
     * Fulfills actions needed to retrieve service information.
     * @param request an <code>HttpServletRequest</code> value that is a
     *                request to retrieve service information
     * @param response an <code>HttpServletResponse</code> value which is a
     *                 response to a request
     * @param servletContext a <code>ServletContext</code> value that is
     *                       the servlet context of the web application in
     *                       which a caller is executing
     * @return a <code>String</code> specifying the path to a resource. The
     *         path points to the page of fetching service information
     *         if there have been input errors; otherwise it points to the
     *         page of viewing service information. It can't be a
     *         <code>null</code> value.
     * @throws CommandException if one of the method arguments is a
     *                          <code>null</code> value or there are problems
     *                          with retrieving service information
     */
    @Override
    public String execute(final HttpServletRequest request,
            final HttpServletResponse response,
            final ServletContext servletContext) throws CommandException {
        ApplicationSettingsReader reader
                = ApplicationSettingsReader.getInstance();
        HttpSession session;
        Map<String, String> errorMessages = new LinkedHashMap<>();
        SQLDAOFactory factory = SQLDAOFactory.getInstance();
        HeadPhysicianDAO headPhysicianDAO = factory.retrieveHeadPhysicianDAO();
        String month;
        String path;
        String resourcePath;
        String year;
        boolean correctnessCondition;
        /* The main part of the method. */
        if (request == null) {
            throw new CommandException("The 'request' argument is "
                    + "'null'.");
        }
        if (servletContext == null) {
            throw new CommandException("The 'servletContext' argument "
                    + "is 'null'.");
        }
        session = request.getSession();
        year = request.getParameter(PARAMETER_NAME_15);
        month = request.getParameter(PARAMETER_NAME_16);
        correctnessCondition = checkParameters(year, month, errorMessages);
        if (correctnessCondition) {
            try {
                session.setAttribute(SERVICE_INFO_ATTRIBUTE_NAME,
                        headPhysicianDAO.retrieveServiceInfo(
                                Integer.parseInt(year),
                                Integer.parseInt(month)));
            } catch (DAOException e) {
                throw new CommandException("Can't retrieve service info.", e);
            }
            resourcePath
                    = reader.getProperty(SERVICE_INFO_VIEWING_PAGE_PATH_KEY)
                    + "?" + PARAMETER_NAME_5 + "=" + 1;
        } else {
            session.setAttribute(ERROR_MESSAGES_ATTRIBUTE_NAME, errorMessages);
            path = reader.getProperty(SERVICE_INFO_FETCHING_PAGE_PATH_KEY);
            session.setAttribute(TARGET_PAGE_PATH_ATTRIBUTE_NAME, path);
            resourcePath = path + "?" + PARAMETER_NAME_15 + "="
                    + encodeString(convertString(year)) + "&"
                    + PARAMETER_NAME_16 + "="
                    + encodeString(convertString(month));
        }
        return resourcePath;
    }
    /**
     * Checks that request parameters comply with defined constraints.
     * @param year a <code>String</code> value which is a year
     *             parameter value
     * @param month a <code>String</code> value which is a month parameter
     *              value
     * @param messages a <code>Map</code> value which is a map of keys of
     *                 input error messages to input error messages
     * @return <code>true</code> if request parameters comply with defined
     *         constraints, <code>false</code> otherwise
     */
    private boolean checkParameters(final String year,
            final String month, final Map<String, String> messages) {
        boolean correctnessCondition = true;
        /* The main part of the method. */
        if (!checkYearCorrectness(year)) {
            messages.put(INPUT_ERROR_MESSAGE_1_KEY, "");
            correctnessCondition = false;
        }
        if (!checkMonthCorrectness(month)) {
            messages.put(INPUT_ERROR_MESSAGE_2_KEY, "");
            correctnessCondition = false;
        }
        return correctnessCondition;
    }
}
