/*
 * UserFetchingCommand.java
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to retrieve all users which are not
 * head physicians.
 */
package by.training.task05.web.command;


import static by.training.task05.reader.ApplicationSettingsReader
        .USER_VIEWING_PAGE_PATH_KEY;

import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_5;

import by.training.task05.dao.DAOException;
import by.training.task05.dao.HeadPhysicianDAO;

import by.training.task05.dao.implementation.SQLDAOFactory;

import by.training.task05.reader.ApplicationSettingsReader;

import javax.servlet.ServletContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to retrieve all users which are not
 * head physicians.
 * @author Yauhen Sazonau
 * @version 1.0, 08/02/19
 * @since 1.0
 */
public class UserFetchingCommand implements Command {
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>java.util.Collection</code> interface that is a collection of
     * users. The identifier holds the value of "users".
     */
    private static final String USERS_ATTRIBUTE_NAME
            = "users";
    /**
     * Fulfills actions needed to retrieve all users which are not head
     * physicians.
     * @param request an <code>HttpServletRequest</code> value that is a
     *                request to retrieve all users which are not head
     *                physicians
     * @param response an <code>HttpServletResponse</code> value which is a
     *                 response to a request
     * @param servletContext a <code>ServletContext</code> value that is
     *                       the servlet context of the web application in
     *                       which a caller is executing
     * @return a <code>String</code> specifying the path to a resource. The
     *         path points to the page of viewing all users which are not head
     *         physicians. It can't be a <code>null</code> value.
     * @throws CommandException if one of the method arguments is a
     *                          <code>null</code> value or there are problems
     *                          with retrieving all users which are not head
     *                          physicians
     */
    @Override
    public String execute(final HttpServletRequest request,
            final HttpServletResponse response,
            final ServletContext servletContext) throws CommandException {
        ApplicationSettingsReader reader
                = ApplicationSettingsReader.getInstance();
        HttpSession session;
        SQLDAOFactory factory = SQLDAOFactory.getInstance();
        HeadPhysicianDAO headPhysicianDAO = factory.retrieveHeadPhysicianDAO();
        String resourcePath;
        /* The main part of the method. */
        if (request == null) {
            throw new CommandException("The 'request' argument is "
                    + "'null'.");
        }
        if (servletContext == null) {
            throw new CommandException("The 'servletContext' argument "
                    + "is 'null'.");
        }
        session = request.getSession();
        try {
            session.setAttribute(USERS_ATTRIBUTE_NAME,
                    headPhysicianDAO.retrieveUsers());
        } catch (DAOException e) {
            throw new CommandException("Can't retrieve users which are not "
                    + "head physicians.", e);
        }
        resourcePath = reader.getProperty(USER_VIEWING_PAGE_PATH_KEY) + "?"
                       + PARAMETER_NAME_5 + "=" + 1;
        return resourcePath;
    }
}
