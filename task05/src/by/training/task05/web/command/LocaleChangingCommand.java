/*
 * LocaleChangingCommand.java
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to change the locale in use.
 */
package by.training.task05.web.command;


import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_2;

import static by.training.task05.web.command.SupportMethodsKeeper
        .addParameters;

import static by.training.task05.web.listener.ContextListener
        .DEFAULT_LOCALE_ATTRIBUTE_NAME;

import by.training.task05.reader.ApplicationSettingsReader;

import java.util.Locale;

import javax.servlet.ServletContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to change the locale in use.
 * @author Yauhen Sazonau
 * @version 1.0, 06/30/19
 * @since 1.0
 */
public class LocaleChangingCommand implements Command {
    /**
     * The Belarussian language code. The identifier holds the value of "by".
     */
    public static final String BELARUSSIAN_LANGUAGE_CODE = "by";
    /**
     * The locale which represents Belarussian language. Can't be a
     * <code>null</code> value.
     */
    public static final Locale BELARUSSIAN_LANGUAGE_LOCALE
            = new Locale(BELARUSSIAN_LANGUAGE_CODE);
    /**
     * The locale which represents English language. Can't be a
     * <code>null</code> value.
     */
    public static final Locale ENGLISH_LANGUAGE_LOCALE = Locale.ENGLISH;
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>java.util.Locale</code> class. The identifier holds the
     * value of "locale".
     */
    public static final String LOCALE_ATTRIBUTE_NAME = "locale";
    /**
     * The Russian language code. The identifier holds the value of "ru".
     */
    public static final String RUSSIAN_LANGUAGE_CODE = "ru";
    /**
     * The locale which represents Russian language. Can't be a
     * <code>null</code> value.
     */
    public static final Locale RUSSIAN_LANGUAGE_LOCALE
            = new Locale(RUSSIAN_LANGUAGE_CODE);
    /**
     * Constructs an instance of this class.
     */
    public LocaleChangingCommand() {
        /* The default initialization is sufficient. */
    }
    /**
     * Fulfills actions needed to change the locale in use.
     * @param request an <code>HttpServletRequest</code> value that is a
     *                request to change the locale in use
     * @param response an <code>HttpServletResponse</code> value which is a
     *                 response to a request
     * @param servletContext a <code>ServletContext</code> value that is
     *                       the servlet context of the web application in
     *                       which a caller is executing
     * @return a <code>String</code> specifying the path to a resource. It
     *         can't be a <code>null</code> value.
     * @throws CommandException if one of the method arguments is a
     *                          <code>null</code> value
     */
    public String execute(final HttpServletRequest request,
            final HttpServletResponse response,
            final ServletContext servletContext) throws CommandException {
        ApplicationSettingsReader applicationSettingsReader;
        HttpSession session;
        Locale defaultLocale;
        Locale localeInUse;
        Locale sessionLocale;
        String pathKey;
        StringBuilder resourcePath = new StringBuilder();
        /* The main part of the method. */
        if (request == null) {
            throw new CommandException("The 'request' argument is "
                    + "'null'.");
        }
        if (servletContext == null) {
            throw new CommandException("The 'servletContext' argument "
                    + "is 'null'.");
        }
        applicationSettingsReader = ApplicationSettingsReader.getInstance();
        session = request.getSession();
        pathKey = request.getParameter(PARAMETER_NAME_2);
        defaultLocale
                = (Locale) servletContext
                           .getAttribute(DEFAULT_LOCALE_ATTRIBUTE_NAME);
        sessionLocale = (Locale) session.getAttribute(LOCALE_ATTRIBUTE_NAME);
        if (sessionLocale != null) {
            localeInUse = sessionLocale;
        } else {
            localeInUse = defaultLocale;
        }
        if (ENGLISH_LANGUAGE_LOCALE.equals(localeInUse)) {
            session.setAttribute(LOCALE_ATTRIBUTE_NAME,
                                 RUSSIAN_LANGUAGE_LOCALE);
        } else if (RUSSIAN_LANGUAGE_LOCALE.equals(localeInUse)) {
            session.setAttribute(LOCALE_ATTRIBUTE_NAME,
                                 BELARUSSIAN_LANGUAGE_LOCALE);
        } else if (BELARUSSIAN_LANGUAGE_LOCALE.equals(localeInUse)) {
            session.setAttribute(LOCALE_ATTRIBUTE_NAME,
                                 ENGLISH_LANGUAGE_LOCALE);
        }
        resourcePath.append(applicationSettingsReader.getProperty(pathKey)
                            + "?");
        addParameters(resourcePath, request);
        return (resourcePath.toString());
    }
}
