/*
 * VisitCommittingCommand.java
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to commit a visit.
 */
package by.training.task05.web.command;


import static by.training.task05.dao.PhysicianDAO
        .VISIT_COMMITING_SUCCESS_VALUE;

import static by.training.task05.reader.ApplicationSettingsReader
        .RESPONSE_MESSAGE_VIEWING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .VISIST_COMMITTING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader.getInstance;

import static by.training.task05.web.command.DataValidator
        .checkMinLength;
import static by.training.task05.web.command.DataValidator
        .checkTicketIDCorrectness;

import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_8;
import static by.training.task05.web.command.ParameterNamesKeeper.PARAMETER_NAME_1;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_11;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_12;

import static by.training.task05.web.command.SupportMethodsKeeper
        .convertString;
import static by.training.task05.web.command.SupportMethodsKeeper.encodeString;

import by.training.task05.dao.DAOException;
import by.training.task05.dao.PhysicianDAO;

import by.training.task05.dao.implementation.SQLDAOFactory;

import by.training.task05.entity.User;

import by.training.task05.reader.ApplicationSettingsReader;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to commit a visit.
 * @author Yauhen Sazonau
 * @version 1.0, 07/05/19
 * @since 1.0
 */
public class VisitCommittingCommand implements Command {
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>by.training.task05.entity.User</code> class. The identifier
     * holds the value of "authenticatedUser".
     */
    private static final String AUTHENTICATED_USER_ATTRIBUTE_NAME
            = "authenticatedUser";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>java.util.Map</code> interface. The identifier holds the value
     * of "errorMessages".
     */
    private static final String ERROR_MESSAGES_ATTRIBUTE_NAME
            = "errorMessages";
    /**
     * The name of a property which retains the text that will be displayed
     * as an item of an unordered list to represent an error of a ticket
     * identification number input. The identifier holds the value of
     * "ticket.id.input.error.message".
     */
    private static final String INPUT_ERROR_MESSAGE_1_KEY
            = "ticket.id.input.error.message";
    /**
     * The name of a property which retains the text that will be displayed
     * as an item of an unordered list to represent an error of a diagnosis
     * input. The identifier holds the value of
     * "diagnosis.input.error.message".
     */
    private static final String INPUT_ERROR_MESSAGE_2_KEY
            = "diagnosis.input.error.message";
    /**
     * The name of a property which retains the text that will be displayed
     * as an item of an unordered list to represent an error of a treatment
     * input. The identifier holds the value of
     * "treatment.input.error.message".
     */
    private static final String INPUT_ERROR_MESSAGE_3_KEY
            = "treatment.input.error.message";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>String</code> class that is the key of a property of a
     * response message. The identifier holds the value of
     * "responseMessageKey".
     */
    private static final String RESPONSE_MESSAGE_KEY_ATTRIBUTE_NAME
            = "responseMessageKey";
    /**
     * The name of a property which retains the text which will be placed to
     * present the information that a visit has been successfully committed.
     * The identifier holds the value of
     * "visit.committing.success.response.message".
     */
    private static final String RESPONSE_MESSAGE_1_KEY
            = "visit.committing.success.response.message";
    /**
     * The name of a property which retains the text which will be placed to
     * present the information that a visit hasn't been committed. The
     * identifier holds the value of
     * "visit.committing.failure.response.message".
     */
    private static final String RESPONSE_MESSAGE_2_KEY
            = "visit.committing.failure.response.message";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>String</code> class that is a target page path. The identifier
     * holds the value of "targetPagePath".
     */
    private static final String TARGET_PAGE_PATH_ATTRIBUTE_NAME
            = "targetPagePath";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>java.lang.String</code> class that is the name of a utilized
     * command. The identifier holds the value of "utilizedCommand".
     */
    private static final String UTILIZED_COMMAND_NAME_ATTRIBUTE_NAME
            = "utilizedCommand";
    /**
     * Fulfills actions needed to commit a visit.
     * @param request an <code>HttpServletRequest</code> value that is a
     *                request to commit a visit
     * @param response an <code>HttpServletResponse</code> value which is a
     *                 response to a request
     * @param servletContext a <code>ServletContext</code> value that is
     *                       the servlet context of the web application in
     *                       which a caller is executing
     * @return a <code>String</code> specifying the path to a resource. The
     *         path points to the page of committing a visit if there have
     *         been input errors; otherwise it points to the page of viewing
     *         a response message. It can't be a <code>null</code> value.
     * @throws CommandException if one of the method arguments is a
     *                          <code>null</code> value or there are problems
     *                          with committing a visit
     */
    @Override
    public String execute(final HttpServletRequest request,
            final HttpServletResponse response,
            final ServletContext servletContext) throws CommandException {
        ApplicationSettingsReader reader = getInstance();
        HttpSession session;
        Map<String, String> errorMessages = new LinkedHashMap<>();
        SQLDAOFactory factory = SQLDAOFactory.getInstance();
        PhysicianDAO physicianDAO = factory.retrievePhysicianDAO();
        String diagnosisValue;
        String path;
        String resourcePath;
        String ticketIDValue;
        String treatmentValue;
        User user;
        boolean correctnessCondition;
        int operationResult;
        /* The main part of the method. */
        checkArguments(request, servletContext);
        session = request.getSession();
        ticketIDValue = request.getParameter(PARAMETER_NAME_8);
        diagnosisValue = request.getParameter(PARAMETER_NAME_11);
        treatmentValue = request.getParameter(PARAMETER_NAME_12);
        user = (User) session.getAttribute(AUTHENTICATED_USER_ATTRIBUTE_NAME);
        correctnessCondition = checkParameters(ticketIDValue, diagnosisValue,
                treatmentValue, errorMessages);
        if (correctnessCondition) {
            try {
                operationResult = physicianDAO.commitVisit(
                        Long.parseLong(ticketIDValue), user.getUserID(),
                        convertString(diagnosisValue),
                        convertString(treatmentValue));
            } catch (DAOException e) {
                throw new CommandException("Can't commit a visit.", e);
            }
            if (operationResult == VISIT_COMMITING_SUCCESS_VALUE) {
                session.setAttribute(RESPONSE_MESSAGE_KEY_ATTRIBUTE_NAME,
                        RESPONSE_MESSAGE_1_KEY);
            } else {
                session.setAttribute(RESPONSE_MESSAGE_KEY_ATTRIBUTE_NAME,
                        RESPONSE_MESSAGE_2_KEY);
            }
            session.setAttribute(UTILIZED_COMMAND_NAME_ATTRIBUTE_NAME,
                    request.getParameter(PARAMETER_NAME_1));
            resourcePath = reader.getProperty(
                    RESPONSE_MESSAGE_VIEWING_PAGE_PATH_KEY);
        } else {
            session.setAttribute(ERROR_MESSAGES_ATTRIBUTE_NAME, errorMessages);
            path = reader.getProperty(VISIST_COMMITTING_PAGE_PATH_KEY);
            session.setAttribute(TARGET_PAGE_PATH_ATTRIBUTE_NAME, path);
            resourcePath = encodeQueryString(path, request);
        }
        return resourcePath;
    }
    /**
     * Checks that request parameters comply with defined constraints.
     * @param ticketID a <code>String</code> value which is an identification
     *                 number of a ticket to be validated
     * @param diagnosis a <code>String</code> value which is a diagnosis
     *                  parameter value
     * @param treatment a <code>String</code> value which is a treatment
     *                  parameter value
     * @param messages a <code>Map</code> value which is a map of keys of
     *                 input error messages to input error messages
     * @return <code>true</code> if request parameters comply with defined
     *         constraints, <code>false</code> otherwise
     */
    private boolean checkParameters(final String ticketID,
            final String diagnosis, final String treatment,
            final Map<String, String> messages) {
        boolean correctnessCondition = true;
        /* The main part of the method. */
        if (!checkTicketIDCorrectness(ticketID)) {
            messages.put(INPUT_ERROR_MESSAGE_1_KEY, "");
            correctnessCondition = false;
        }
        if (!checkMinLength(diagnosis)) {
            messages.put(INPUT_ERROR_MESSAGE_2_KEY, "");
            correctnessCondition = false;
        }
        if (!checkMinLength(treatment)) {
            messages.put(INPUT_ERROR_MESSAGE_3_KEY, "");
            correctnessCondition = false;
        }
        return correctnessCondition;
    }
    /**
     * Adds a query string to a specified path.
     * @param path a <code>String</code> value which is a path
     * @param request an <code>HttpServletRequest</code> value which is a
     *                request
     * @return a path with an added query string. Can't be a <code>null</code>
     *         value.
     */
    private String encodeQueryString(final String path,
            final HttpServletRequest request) {
        /* The main part of the method. */
        return (path + "?" + PARAMETER_NAME_8 + "="
                + encodeString(convertString(
                        request.getParameter(PARAMETER_NAME_8)))
                + "&" + PARAMETER_NAME_11 + "="
                + encodeString(convertString(
                        request.getParameter(PARAMETER_NAME_11)))
                + "&" + PARAMETER_NAME_12 + "="
                + encodeString(convertString(
                        request.getParameter(PARAMETER_NAME_12))));
    }
    /**
     * Verifies if arguments are <code>null</code>.
     * @param request an <code>HttpServletRequest</code> value that is a
     *                request to add a ticket
     * @param servletContext a <code>ServletContext</code> value that is
     *                       the servlet context of the web application in
     *                       which a caller is executing
     * @throws CommandException if one of the method arguments is a
     *                          <code>null</code> value
     */
    private void checkArguments(final HttpServletRequest request,
            final ServletContext servletContext) throws CommandException {
        if (request == null) {
            throw new CommandException("The 'request' argument is 'null'.");
        }
        if (servletContext == null) {
            throw new CommandException("The 'servletContext' argument is "
                    + "'null'.");
        }
    }
}
