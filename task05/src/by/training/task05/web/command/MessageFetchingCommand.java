/*
 * MessageFetchingCommand.java
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to retrieve a message.
 */
package by.training.task05.web.command;


import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_21;

import static by.training.task05.web.controller.ApplicationController
        .DUMMY_PATH;

import static by.training.task05.web.listener.ContextListener
        .DEFAULT_LOCALE_ATTRIBUTE_NAME;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;

import by.training.task05.reader.MessageReader;
import by.training.task05.reader.ReaderException;

import java.io.IOException;
import java.io.Writer;

import java.util.Locale;

import javax.servlet.ServletContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to retrieve a message.
 * @author Yauhen Sazonau
 * @version 1.0, 07/12/19
 * @since 1.0
 */
public class MessageFetchingCommand implements Command {
    /**
     * A content type. The identifier holds the value of
     * "text/plain; charset=UTF-8".
     */
    private static final String CONTENT_TYPE = "text/plain; charset=UTF-8";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>java.util.Locale</code> class. The identifier holds the
     * value of "locale".
     */
    private static final String LOCALE_ATTRIBUTE_NAME = "locale";
    /**
     * Fulfills actions needed to retrieve a message.
     * @param request an <code>HttpServletRequest</code> value that is a
     *                request to retrieve a message
     * @param response an <code>HttpServletResponse</code> value which is a
     *                 response to a request
     * @param servletContext a <code>ServletContext</code> value that is
     *                       the servlet context of the web application in
     *                       which a caller is executing
     * @return a <code>String</code> specifying the path to a resource. The
     *         path is a dummy string. It can't be a <code>null</code> value.
     * @throws CommandException if one of the method arguments is a
     *                          <code>null</code> value or there are problems
     *                          with retrieving a message
     */
    @Override
    public String execute(final HttpServletRequest request,
            final HttpServletResponse response,
            final ServletContext servletContext) throws CommandException {
        HttpSession session;
        Locale localeInUse;
        MessageReader reader;
        String message = null;
        String messageKey;
        Writer writer;
        boolean messageExistence = true;
        /* The main part of the method. */
        if (request == null) {
            throw new CommandException("The 'request' argument is "
                    + "'null'.");
        }
        if (servletContext == null) {
            throw new CommandException("The 'servletContext' argument "
                    + "is 'null'.");
        }
        try {
            reader = MessageReader.getInstance();
        } catch (ReaderException e) {
            throw new CommandException("An underlying source of data is "
                    + "absent.", e);
        }
        session = request.getSession();
        messageKey = request.getParameter(PARAMETER_NAME_21);
        localeInUse = retrieveLocale(servletContext, session);
        try {
            message = reader.getProperty(messageKey, localeInUse);
        } catch (ReaderException e) {
            messageExistence = false;
        }
        try {
            if (messageExistence) {
                response.setContentType(CONTENT_TYPE);
                writer = response.getWriter();
                writer.write(message);
            } else {
                response.sendError(SC_BAD_REQUEST);
            }
        } catch (IOException e) {
            throw new CommandException("An error of returning a message "
                    + "arsisen.", e);
        }
        return DUMMY_PATH;
    }
    /**
     * Retrieves a locale in use.
     * @param servletContext a <code>ServletContext</code> value that is
     *                       the servlet context of the web application in
     *                       which a caller is executing
     * @param session an <code>HttpSession</code> value that is a session
     * @return a locale in use. It can't be a <code>null</code> value.
     */
    private Locale retrieveLocale(final ServletContext servletContext,
            final HttpSession session) {
        Locale defaultLocale;
        Locale localeInUse;
        Locale sessionLocale;
        defaultLocale
                = (Locale) servletContext
                           .getAttribute(DEFAULT_LOCALE_ATTRIBUTE_NAME);
        sessionLocale = (Locale) session.getAttribute(LOCALE_ATTRIBUTE_NAME);
        if (sessionLocale != null) {
            localeInUse = sessionLocale;
        } else {
            localeInUse = defaultLocale;
        }
        return localeInUse;
    }
}
