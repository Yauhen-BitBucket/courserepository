/*
 * DoctorFetchingCommand.java
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to retrieve doctor information.
 */
package by.training.task05.web.command;


import static by.training.task05.reader.ApplicationSettingsReader
        .DOCTOR_VIEWING_PAGE_PATH_KEY;

import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_5;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_6;

import by.training.task05.dao.DAOException;
import by.training.task05.dao.PatientDAO;

import by.training.task05.dao.implementation.SQLDAOFactory;

import by.training.task05.entity.Speciality;

import by.training.task05.reader.ApplicationSettingsReader;

import javax.servlet.ServletContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to retrieve doctor information.
 * @author Yauhen Sazonau
 * @version 1.0, 07/02/19
 * @since 1.0
 */
public class DoctorFetchingCommand implements Command {
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>java.util.Collection</code> interface that is a collection of
     * doctors. The identifier holds the value of "doctors".
     */
    private static final String DOCTORS_ATTRIBUTE_NAME
            = "doctors";
    /**
     * Fulfills actions needed to retrieve doctor information.
     * @param request an <code>HttpServletRequest</code> value that is a
     *                request to retrieve doctor information
     * @param response an <code>HttpServletResponse</code> value which is a
     *                 response to a request
     * @param servletContext a <code>ServletContext</code> value that is
     *                       the servlet context of the web application in
     *                       which a caller is executing
     * @return a <code>String</code> specifying the path to a resource. The
     *         path points to the page of viewing doctor information. It can't
     *         be a <code>null</code> value.
     * @throws CommandException if one of the method arguments is a
     *                          <code>null</code> value or there are problems
     *                          with retrieving doctor information
     */
    @Override
    public String execute(final HttpServletRequest request,
            final HttpServletResponse response,
            final ServletContext servletContext) throws CommandException {
        ApplicationSettingsReader reader
                = ApplicationSettingsReader.getInstance();
        HttpSession session;
        SQLDAOFactory factory = SQLDAOFactory.getInstance();
        PatientDAO patientDAO = factory.retrievePatientDAO();
        String resourcePath;
        /* The main part of the method. */
        if (request == null) {
            throw new CommandException("The 'request' argument is "
                    + "'null'.");
        }
        if (servletContext == null) {
            throw new CommandException("The 'servletContext' argument "
                    + "is 'null'.");
        }
        session = request.getSession();
        try {
            session.setAttribute(DOCTORS_ATTRIBUTE_NAME,
                    patientDAO.retrievePhysicians(Speciality.valueOf(
                            request.getParameter(PARAMETER_NAME_6))));
        } catch (DAOException e) {
            throw new CommandException("Can't retrieve doctor data.", e);
        }
        resourcePath = reader.getProperty(DOCTOR_VIEWING_PAGE_PATH_KEY) + "?"
                       + PARAMETER_NAME_5 + "=" + 1;
        return resourcePath;
    }
}
