/* CommandName.java
 * A command name. The following commands are considered: a command of changing
 * a locale in use, an authentication command, a logout command, a command of
 * fetching own tickets, a command of fetching doctors, a command of fetching
 * free tickets, a command of assigning a ticket, a command of canceling a
 * ticket, a command of adding a ticket, a command of fetching assigned
 * tickets, a command of committing a visit, a command of fetching used
 * tickets, a command of fetching service info, a command of adding a patient,
 * a command of fetching an image, a command of setting an image, a command of
 * fetching a message, a command of fetching users, a command of setting a
 * blocked state.
 */
package by.training.task05.web.command;


/**
 * A command name. The following commands are considered: a command of changing
 * a locale in use, an authentication command, a logout command, a command of
 * fetching own tickets, a command of fetching doctors, a command of fetching
 * free tickets, a command of assigning a ticket, a command of canceling a
 * ticket, a command of adding a ticket, a command of fetching assigned
 * tickets, a command of committing a visit, a command of fetching used
 * tickets, a command of fetching service info, a command of adding a patient,
 * a command of fetching an image, a command of setting an image, a command of
 * fetching a message, a command of fetching users, a command of setting a
 * blocked state.
 * @version 1.0, 06/30/19
 * @since 1.0
 */
public enum CommandName {
    /**
     * Represents an authentication command.
     */
    AUTHENTICATION_COMMAND,
    /**
     * Represents a command of changing a locale in use.
     */
    LOCALE_CHANGING_COMMAND,
    /**
     * Represents a logout command.
     */
    LOGOUT_COMMAND,
    /**
     * Represents a command of fetching own tickets.
     */
    OWN_TICKET_FETCHING_COMMAND,
    /**
     * Represents a command of fetching doctors.
     */
    DOCTOR_FETCHING_COMMAND,
    /**
     * Represents a command of fetching free tickets.
     */
    FREE_TICKET_FETCHING_COMMAND,
    /**
     * Represents a command of assigning a ticket.
     */
    TICKET_ASSIGNING_COMMAND,
    /**
     * Represents a command of canceling a ticket.
     */
    TICKET_CANCELLING_COMMAND,
    /**
     * Represents a command of adding a ticket.
     */
    TICKET_ADDING_COMMAND,
    /**
     * Represents a command of fetching assigned tickets.
     */
    ASSIGNED_TICKET_FETCHING_COMMAND,
    /**
     * Represents a command of committing a visit.
     */
    VISIT_COMMITTING_COMMAND,
    /**
     * Represents a command of fetching used tickets.
     */
    USED_TICKET_FETCHING_COMMAND,
    /**
     * Represents a command of fetching service info.
     */
    SERVICE_INFO_FETCHING_COMMAND,
    /**
     * Represents a command of adding a patient.
     */
    PATIENT_ADDING_COMMAND,
    /**
     * Represents a command of fetching an image.
     */
    IMAGE_FETCHING_COMMAND,
    /**
     * Represents a command of setting an image.
     */
    IMAGE_SETTING_COMMAND,
    /**
     * Represents a command of fetching a message.
     */
    MESSAGE_FETCHING_COMMAND,
    /**
     * Represents a command of fetching users.
     */
    USER_FETCHING_COMMAND,
    /**
     * Represents a command of setting a blocked state.
     */
    BLOCKED_STATE_SETTING_COMMAND
}
