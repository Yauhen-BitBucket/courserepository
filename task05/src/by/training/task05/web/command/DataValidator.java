/*
 * DataValidator.java
 * A class which contains methods to perform various data checks.
 */
package by.training.task05.web.command;


import static java.util.Calendar.APRIL;
import static java.util.Calendar.AUGUST;
import static java.util.Calendar.DECEMBER;
import static java.util.Calendar.FEBRUARY;
import static java.util.Calendar.JANUARY;
import static java.util.Calendar.JULY;
import static java.util.Calendar.JUNE;
import static java.util.Calendar.MARCH;
import static java.util.Calendar.MAY;
import static java.util.Calendar.NOVEMBER;
import static java.util.Calendar.OCTOBER;
import static java.util.Calendar.SEPTEMBER;

import by.training.task05.exception.ApplicationException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * A class which contains methods to perform various data checks.
 * @version 1.0, 06/30/19
 * @since 1.0
 */
class DataValidator {
    /**
     * A regular expression to be compiled which is used to create a
     * <code>Pattern</code> object that will match a sequence in the form of
     * "yyyy-mm-dd"(without double quotes). Where "y", "m" and "d" is a
     * decimal digit. The identifier holds the value of
     * "\\d{4}[-]\\d{2}[-]\\d{2}".
     */
    private static final String DATE_EXPRESSION = "\\d{4}[-]\\d{2}[-]\\d{2}";
    /**
     * A compiled pattern that is used to find out if a <code>String</code>
     * value corresponds to a date format in the form of "yyyy-mm-dd"(without
     * double quotes). Where "y", "m" and "d" is a decimal digit. Can't be a
     * <code>null</code> value.
     */
    private static final Pattern DATE_PATTERN
            = Pattern.compile(DATE_EXPRESSION);
    /**
     * A position of the beginning of the day part within a
     * <code>String</code> value which represents a date in the form of
     * "yyyy-mm-dd"(without double quotes). Where "y", "m" and "d" is a
     * decimal digit. The identifier holds the value of "8".
     */
    private static final int DAY_PART_BEGINNING_POSITION = 8;
    /**
     * A position of the ending of the day part within a <code>String</code>
     * value which represents a date in the form of "yyyy-mm-dd"(without
     * double quotes). Where "y", "m" and "d" is a decimal digit. The
     * identifier holds the value of "9".
     */
    private static final int DAY_PART_ENDING_POSITION = 9;
    /**
     * A divisor. It is an auxiliary value that helps in determining the fact
     * whether a year is leap. The identifier holds the value of "4".
     */
    private static final int DIVISOR_4 = 4;
    /**
     * A divisor. It is an auxiliary value that helps in determining the fact
     * whether a year is leap. The identifier holds the value of "100".
     */
    private static final int DIVISOR_100 = 100;
    /**
     * A divisor. It is an auxiliary value that helps in determining the fact
     * whether a year is leap. The identifier holds the value of "400".
     */
    private static final int DIVISOR_400 = 400;
    /**
     * An ordinal number of the first month. The identifier holds the value of
     * "1".
     */
    private static final int FIRST_MONTH_ORDINAL_NUMBER = 1;
    /**
     * An hour value. The identifier holds the value of "23".
     */
    private static final int HOUR_VALUE_23 = 23;
    /**
     * An ordinal number of the last month. The identifier holds the value of
     * "12".
     */
    private static final int LAST_MONTH_ORDINAL_NUMBER = 12;
    /**
     * A regular expression to be compiled which is used to create a
     * <code>Pattern</code> object that will match a sequence of at least six
     * characters. The identifier holds the value of "(?s).{6,}".
     */
    private static final String MIN_LENGTH_EXPRESSION = "(?s).{6,}";
    /**
     * A compiled pattern that is used to find out if a <code>String</code>
     * value contains a sequence of at least six characters. Can't be a
     * <code>null</code> value.
     */
    private static final Pattern MIN_LENGTH_PATTERN
            = Pattern.compile(MIN_LENGTH_EXPRESSION);
    /**
     * The day quantity of a month. The identifier holds the value of "28".
     */
    private static final int MONTH_DAY_QUANTITY_28 = 28;
    /**
     * The day quantity of a month. The identifier holds the value of "29".
     */
    private static final int MONTH_DAY_QUANTITY_29 = 29;
    /**
     * The day quantity of a month. The identifier holds the value of "30".
     */
    private static final int MONTH_DAY_QUANTITY_30 = 30;
    /**
     * The day quantity of a month. The identifier holds the value of "31".
     */
    private static final int MONTH_DAY_QUANTITY_31 = 31;
    /**
     * A regular expression to be compiled which is used to create a
     * <code>Pattern</code> object that will match a sequence of one or two
     * digits. The identifier holds the value of "\\d{1,2}".
     */
    private static final String MONTH_EXPRESSION = "\\d{1,2}";
    /**
     * A month number. The identifier holds the value of "12}".
     */
    private static final int MONTH_NUMBER_12 = 12;
    /**
     * A position of the beginning of the month part within a
     * <code>String</code> value which represents a date in the form of
     * "yyyy-mm-dd"(without double quotes). Where "y", "m" and "d" is a
     * decimal digit. The identifier holds the value of "5".
     */
    private static final int MONTH_PART_BEGINNING_POSITION = 5;
    /**
     * A position of the ending of the month part within a
     * <code>String</code> value which represents a date in the form of
     * "yyyy-mm-dd"(without double quotes). Where "y", "m" and "d" is a
     * decimal digit. The identifier holds the value of "6".
     */
    private static final int MONTH_PART_ENDING_POSITION = 6;
    /**
     * A compiled pattern that is used to find out if a <code>String</code>
     * value represents a month. Can't be a <code>null</code> value.
     */
    private static final Pattern MONTH_PATTERN
            = Pattern.compile(MONTH_EXPRESSION);
    /**
     * A regular expression to be compiled which is used to create a
     * <code>Pattern</code> object that will match a sequence in the form of
     * "CCddddddd"(without double quotes). Where "C" is an upper-cased
     * alphabetic character and "d" - a decimal digit. The identifier holds
     * the value of "\\p{Upper}{2}\\d{7}".
     */
    private static final String PASSPORT_NUMBER_EXPRESSION
            = "\\p{Upper}{2}\\d{7}";
    /**
     * A compiled pattern that is used to find out if a <code>String</code>
     * value corresponds to a passport number standard in the form of
     * "CCddddddd"(without double quotes). Where "C" is an upper-cased
     * alphabetic character and "d" - a decimal digit. Can't be a
     * <code>null</code> value.
     */
    private static final Pattern PASSPORT_NUMBER_PATTERN
            = Pattern.compile(PASSPORT_NUMBER_EXPRESSION);
    /**
     * A maximum value the identification number of a ticket can have. The
     * identifier holds the value of "100_000_000".
     */
    private static final long TICKET_ID_MAX_VALUE = 100_000_000;
    /**
     * A minimum value the identification number of a ticket can have. The
     * identifier holds the value of "1".
     */
    private static final long TICKET_ID_MIN_VALUE = 1;
    /**
     * A regular expression to be compiled which is used to create a
     * <code>Pattern</code> object that will match a sequence in the form of
     * "hh:mm:ss"(without double quotes). Where "h", "m" and "s" is a
     * decimal digit. The identifier holds the value of
     * "[012][123456789](?::[012345][0123456789]){2}".
     */
    private static final String TIME_EXPRESSION
            = "[012][123456789](?::[012345][0123456789]){2}";
    /**
     * A compiled pattern that is used to find out if a <code>String</code>
     * value corresponds to a time format in the form of "hh:mm:ss"(without
     * double quotes). Where "h", "m" and "s" is a decimal digit. Can't be a
     * <code>null</code> value.
     */
    private static final Pattern TIME_PATTERN
            = Pattern.compile(TIME_EXPRESSION);
    /**
     * A regular expression to be compiled which is used to create a
     * <code>Pattern</code> object that will match a sequence of four digits.
     * The identifier holds the value of "\\d{4}".
     */
    private static final String YEAR_EXPRESSION = "\\d{4}";
    /**
     * A minimum value the year part of a date can have. The identifier holds
     * the value of "2000".
     */
    private static final int YEAR_MIN_VALUE = 2000;
    /**
     * A position of the beginning of the year part within a
     * <code>String</code> value which represents a date in the form of
     * "yyyy-mm-dd"(without double quotes). Where "y", "m" and "d" is a
     * decimal digit. The identifier holds the value of "0".
     */
    private static final int YEAR_PART_BEGINNING_POSITION = 0;
    /**
     * A position of the ending of the year part within a <code>String</code>
     * value which represents a date in the form of "yyyy-mm-dd"(without
     * double quotes). Where "y", "m" and "d" is a decimal digit. The
     * identifier holds the value of "3".
     */
    private static final int YEAR_PART_ENDING_POSITION = 3;
    /**
     * A compiled pattern that is used to find out if a <code>String</code>
     * value represents a year. Can't be a <code>null</code> value.
     */
    private static final Pattern YEAR_PATTERN
            = Pattern.compile(YEAR_EXPRESSION);
    /**
     * A regular expression to be compiled which is used to create a
     * <code>Pattern</code> object that will match a sequence of at least one
     * character. The identifier holds the value of
     * "(?:[^\p{Punct}\p{Space}]|'){1,}(?: (?:[^\p{Punct}\p{Space}]|'){1,})*".
     */
    private static final String WORD_EXPRESSION
            = "(?:[^\\p{Punct}\\p{Space}]|'){1,}(?: "
              + "(?:[^\\p{Punct}\\p{Space}]|'){1,})*";
    /**
     * A compiled pattern that is used to find out if a <code>String</code>
     * value contains a sequence of at least one character. Can't be a
     * <code>null</code> value.
     */
    private static final Pattern WORD_PATTERN
            = Pattern.compile(WORD_EXPRESSION);
    /**
     * Constructs an instance of this class.
     */
    private DataValidator() {
        /* The default initialization is sufficient. */
    }
    /**
     * Verifies if a specified <code>inputSequence</code> value contains
     * a sequence of at least six characters.
     * @param inputSequence a <code>String</code> value which is a
     *                      sequence to be verified
     * @return <code>true</code> value if a specified
     *         <code>inputSequence</code> value contains a sequence of at least
     *         six characters, <code>false</code> otherwise
     */
    public static boolean checkMinLength(final String inputSequence) {
        Matcher matcher;
        /* The main part of the method. */
        matcher = MIN_LENGTH_PATTERN.matcher(inputSequence);
        return (matcher.matches());
    }
    /**
     * Computes the days quantity that a specified month has(this
     * computation depends on a year).
     * @param month an <code>int</code> value which is the ordinal number of a
     *              month. Allowed values are from zero to eleven.
     * @param year an <code>int</code> value which is a year to find out if
     *             it is a leap year. It mustn't be less than the minimum value
     *             for year.
     * @return the days quantity in a month. A returned value must be
     *         thirty or thirty one, for February this value must be twenty
     *         eight or twenty nine(for leap years).
     * @throws ApplicationException if the <code>month</code> argument is
     *                              less than zero or greater than eleven
     *                              or the <code>year</code> argument is
     *                              less than the minimum value for year
     */
    public static int retrieveMonthDayCount(final int month, final int year) {
        boolean firstCase;
        boolean isALeapYear;
        boolean secondCase;
        int amount = 0;
        /* The main part of the method. */
        if ((month < JANUARY) || (month > DECEMBER)) {
            throw new ApplicationException("The value of the 'month' "
                    + "argument mustn't be less than zero or greater "
                    + "than eleven! 'month' = " + month);
        }
        if (year < YEAR_MIN_VALUE) {
            throw new IllegalArgumentException("The value of the 'year' "
                    + "argument mustn't be less than the minimum value for "
                    + "year! 'year = '" + year);
        }
        if ((month == JANUARY) || (month == MARCH) || (month == MAY)
                || (month == JULY) || (month == AUGUST) || (month == OCTOBER)
                || (month == DECEMBER)) {
            amount = MONTH_DAY_QUANTITY_31;
        } else if ((month == APRIL) || (month == JUNE)
                || (month == SEPTEMBER) || (month == NOVEMBER)) {
            amount = MONTH_DAY_QUANTITY_30;
        } else if (month == FEBRUARY) {
            firstCase = (year % DIVISOR_400) == 0;
            secondCase = ((year % DIVISOR_4) == 0)
                         && ((year % DIVISOR_100) != 0);
            isALeapYear =  firstCase || secondCase;
            if (isALeapYear) {
                amount = MONTH_DAY_QUANTITY_29;
            } else {
                amount = MONTH_DAY_QUANTITY_28;
            }
        }
        return amount;
    }
    /**
     * Verifies a specified <code>inputSequence</code> value if it
     * corresponds to a date format in the form of "yyyy-mm-dd"(without
     * double quotes) and if it represents an existent date value. Where "y",
     * "m" and "d" is a decimal digit.
     * @param inputSequence a <code>String</code> value which is a sequence to
     *                      be verified
     * @return <code>true</code> value if a specified
     *         <code>inputSequence</code> value corresponds to a date format
     *         and represents an existent date value, <code>false</code>
     *         otherwise
     * @throws ApplicationException if the <code>inputSequence</code>
     *                              argument is a <code>null</code> value
     */
    public static boolean checkDateCorrectness(final String inputSequence) {
        boolean result = true;
        int year;
        int month;
        int day;
        Matcher matcher;
        String  yearAsString;
        String  monthAsString;
        String  dayAsString;
        /* The main part of the method. */
        if (inputSequence == null) {
            throw new ApplicationException("The 'inputSequence' argument is "
                    + "'null'.");
        }
        matcher = DATE_PATTERN.matcher(inputSequence);
        if ((matcher.matches())) {
            yearAsString = inputSequence.substring(
                    YEAR_PART_BEGINNING_POSITION,
                    YEAR_PART_ENDING_POSITION + 1);
            monthAsString = inputSequence.substring(
                    MONTH_PART_BEGINNING_POSITION,
                    MONTH_PART_ENDING_POSITION + 1);
            dayAsString = inputSequence.substring(
                    DAY_PART_BEGINNING_POSITION,
                    DAY_PART_ENDING_POSITION + 1);
            year = Integer.valueOf(yearAsString);
            month = Integer.parseInt(monthAsString);
            day = Integer.parseInt(dayAsString);
            if (year >= YEAR_MIN_VALUE) {
                if ((month < FIRST_MONTH_ORDINAL_NUMBER)
                        || (month > LAST_MONTH_ORDINAL_NUMBER)
                        || (day < 1)
                        || (day > (retrieveMonthDayCount(month - 1, year)))) {
                    result = false;
                }
            } else {
                result = false;
            }
        } else {
            result = false;
        }
        return result;
    }
    /**
     * Verifies a specified <code>inputSequence</code> value if it
     * represents a month number.
     * @param inputSequence a <code>String</code> value which is a sequence to
     *                      be verified
     * @return <code>true</code> value if a specified
     *         <code>inputSequence</code> value represents a month number,
     *         <code>false</code> otherwise
     * @throws ApplicationException if the <code>inputSequence</code>
     *                              argument is a <code>null</code> value
     */
    public static boolean checkMonthCorrectness(final String inputSequence) {
        Matcher matcher;
        boolean result = true;
        int month;
        /* The main part of the method. */
        if (inputSequence == null) {
            throw new ApplicationException("The 'inputSequence' argument is "
                    + "'null'.");
        }
        matcher = MONTH_PATTERN.matcher(inputSequence);
        if ((matcher.matches())) {
            month = Integer.parseInt(inputSequence);
            if ((month < 1) || (month > MONTH_NUMBER_12)) {
                result = false;
            }
        } else {
            result = false;
        }
        return result;
    }
    /**
     * Verifies a specified <code>inputSequence</code> value if it
     * corresponds to a passport number standard in the form of
     * "CCddddddd"(without double quotes). Where "C" is an upper-cased
     * alphabetic character and "d" - a decimal digit.
     * @param inputSequence a <code>String</code> value which is a
     *                      sequence to be verified
     * @return <code>true</code> value if a specified
     *         <code>inputSequence</code> value corresponds to a passport
     *         number standard, <code>false</code> otherwise
     * @throws ApplicationException if the <code>inputSequence</code>
     *                              argument is a <code>null</code> value
     */
    public static boolean checkPassportNumber(final String inputSequence) {
        Matcher matcher;
        /* The main part of the method. */
        if (inputSequence == null) {
            throw new ApplicationException("The 'inputSequence' argument "
                    + "is 'null'.");
        }
        matcher = PASSPORT_NUMBER_PATTERN.matcher(inputSequence);
        return (matcher.matches());
    }
    /**
     * Verifies a specified <code>inputSequence</code> value if it
     * corresponds to a time format in the form of "hh:mm:ss"(without
     * double quotes) and if it represents an existent time value. Where "h",
     * "m" and "s" is a decimal digit.
     * @param inputSequence a <code>String</code> value which is a sequence to
     *                      be verified
     * @return <code>true</code> value if a specified
     *         <code>inputSequence</code> value corresponds to a time format
     *         and represents an existent time value, <code>false</code>
     *         otherwise
     * @throws ApplicationException if the <code>inputSequence</code>
     *                              argument is a <code>null</code> value
     */
    public static boolean checkTimeCorrectness(final String inputSequence) {
        Matcher matcher;
        String  hourAsString;
        boolean result = true;
        int hour;
        /* The main part of the method. */
        if (inputSequence == null) {
            throw new ApplicationException("The 'inputSequence' argument is "
                    + "'null'.");
        }
        matcher = TIME_PATTERN.matcher(inputSequence);
        if ((matcher.matches())) {
            hourAsString = inputSequence.substring(0, 2);
            hour = Integer.parseInt(hourAsString);
            if (hour > HOUR_VALUE_23) {
                result = false;
            }
        } else {
            result = false;
        }
        return result;
    }
    /**
     * Verifies a specified <code>ticketID</code> value if it isn't less
     * than the minimum value for the ticket identification number and if it
     * isn't greater than the maximum value for the ticket identification
     * number.
     * @param ticketID a <code>long</code> value to be validated
     * @return <code>true</code> value if a specified
     *         <code>ticketID</code> value isn't less than the minimum value
     *         for the ticket identification number and if it isn't greater
     *         than the maximum value for the ticket identification number,
     *         <code>false</code> otherwise
     */
    public static boolean checkTicketIDCorrectness(final String ticketID) {
        long value;
        try {
            value = Long.parseLong(ticketID);
        } catch (RuntimeException e) {
            return false;
        }
        return ((value >= TICKET_ID_MIN_VALUE)
                && (value <= TICKET_ID_MAX_VALUE));
    }
    /**
     * Verifies a specified <code>inputSequence</code> value if it
     * represents a year. It shouldn't be less than the minimum value for a
     * year.
     * @param inputSequence a <code>String</code> value which is a sequence to
     *                      be verified
     * @return <code>true</code> value if a specified
     *         <code>inputSequence</code> value represents a year,
     *         <code>false</code> otherwise
     * @throws ApplicationException if the <code>inputSequence</code>
     *                              argument is a <code>null</code> value
     */
    public static boolean checkYearCorrectness(final String inputSequence) {
        Matcher matcher;
        boolean result = true;
        int year;
        /* The main part of the method. */
        if (inputSequence == null) {
            throw new ApplicationException("The 'inputSequence' argument is "
                    + "'null'.");
        }
        matcher = YEAR_PATTERN.matcher(inputSequence);
        if ((matcher.matches())) {
            year = Integer.parseInt(inputSequence);
            if (year < YEAR_MIN_VALUE) {
                result = false;
            }
        } else {
            result = false;
        }
        return result;
    }
    /**
     * Verifies if a specified <code>inputSequence</code> value contains
     * a sequence of at least one character.
     * @param inputSequence a <code>String</code> value which is a
     *                      sequence to be verified
     * @return <code>true</code> value if a specified
     *         <code>inputSequence</code> value contains a sequence of at least
     *         one character, <code>false</code> otherwise
     */
    public static boolean checkWord(final String inputSequence) {
        Matcher matcher;
        /* The main part of the method. */
        matcher = WORD_PATTERN.matcher(inputSequence);
        return (matcher.matches());
    }
}
