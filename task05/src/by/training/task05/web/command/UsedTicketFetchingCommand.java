/*
 * UsedTicketFetchingCommand.java
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to retrieve used ticket
 * information.
 */
package by.training.task05.web.command;


import static by.training.task05.reader.ApplicationSettingsReader
        .USED_TICKET_FETCHING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .USED_TICKET_VIEWING_PAGE_PATH_KEY;

import static by.training.task05.web.command.DataValidator
        .checkDateCorrectness;

import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_5;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_13;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_14;

import static by.training.task05.web.command.SupportMethodsKeeper
        .convertString;
import static by.training.task05.web.command.SupportMethodsKeeper.encodeString;
import static by.training.task05.web.command.SupportMethodsKeeper
        .retrieveCurrentDate;

import by.training.task05.dao.DAOException;
import by.training.task05.dao.HeadPhysicianDAO;

import by.training.task05.dao.implementation.SQLDAOFactory;

import by.training.task05.reader.ApplicationSettingsReader;

import java.sql.Date;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to retrieve used ticket
 * information.
 * @author Yauhen Sazonau
 * @version 1.0, 07/05/19
 * @since 1.0
 */
public class UsedTicketFetchingCommand implements Command {
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>java.util.Map</code> interface. The identifier holds the value
     * of "errorMessages".
     */
    private static final String ERROR_MESSAGES_ATTRIBUTE_NAME
            = "errorMessages";
    /**
     * The name of a property which retains the text that will be displayed
     * as an item of an unordered list to represent an error of a starting date
     * input. The identifier holds the value of
     * "starting.date.input.error.message".
     */
    private static final String INPUT_ERROR_MESSAGE_1_KEY
            = "starting.date.input.error.message";
    /**
     * The name of a property which retains the text that will be displayed
     * as an item of an unordered list to represent an error of an ending date
     * input. The identifier holds the value of
     * "starting.date.precedence.error.message".
     */
    private static final String INPUT_ERROR_MESSAGE_2_KEY
            = "starting.date.precedence.error.message";
    /**
     * The name of a property which retains the text that will be displayed
     * as an item of an unordered list to represent an error which means that
     * a starting date follows the current date. The identifier holds the value
     * of "ending.date.input.error.message".
     */
    private static final String INPUT_ERROR_MESSAGE_3_KEY
            = "ending.date.input.error.message";
    /**
     * The name of a property which retains the text that will be displayed
     * as an item of an unordered list to represent an error which means that
     * an ending date follows the current date. The identifier holds the value
     * of "ending.date.precedence.error.message".
     */
    private static final String INPUT_ERROR_MESSAGE_4_KEY
            = "ending.date.precedence.error.message";
    /**
     * The name of a property which retains the text that will be displayed
     * as an item of an unordered list to represent an error which means that a
     * starting date follows an ending date. The identifier holds the value
     * of "period.precedence.error.message".
     */
    private static final String INPUT_ERROR_MESSAGE_5_KEY
            = "period.precedence.error.message";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>String</code> class that is a target page path. The identifier
     * holds the value of "targetPagePath".
     */
    private static final String TARGET_PAGE_PATH_ATTRIBUTE_NAME
            = "targetPagePath";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>java.util.Collection</code> interface that is a collection of
     * used tickets. The identifier holds the value of "usedTickets".
     */
    private static final String USED_TICKETS_ATTRIBUTE_NAME
            = "usedTickets";
    /**
     * Fulfills actions needed to retrieve used ticket information.
     * @param request an <code>HttpServletRequest</code> value that is a
     *                request to retrieve used ticket information
     * @param response an <code>HttpServletResponse</code> value which is a
     *                 response to a request
     * @param servletContext a <code>ServletContext</code> value that is
     *                       the servlet context of the web application in
     *                       which a caller is executing
     * @return a <code>String</code> specifying the path to a resource. The
     *         path points to the page of fetching used ticket information
     *         if there have been input errors; otherwise it points to the
     *         page of viewing used ticket information. It can't be a
     *         <code>null</code> value.
     * @throws CommandException if one of the method arguments is a
     *                          <code>null</code> value or there are problems
     *                          with retrieving used ticket information
     */
    @Override
    public String execute(final HttpServletRequest request,
            final HttpServletResponse response,
            final ServletContext servletContext) throws CommandException {
        ApplicationSettingsReader reader
                = ApplicationSettingsReader.getInstance();
        HttpSession session;
        Map<String, String> errorMessages = new LinkedHashMap<>();
        SQLDAOFactory factory = SQLDAOFactory.getInstance();
        HeadPhysicianDAO headPhysicianDAO = factory.retrieveHeadPhysicianDAO();
        String endingDate;
        String path;
        String resourcePath;
        String startingDate;
        boolean correctnessCondition;
        /* The main part of the method. */
        if (request == null) {
            throw new CommandException("The 'request' argument is "
                    + "'null'.");
        }
        if (servletContext == null) {
            throw new CommandException("The 'servletContext' argument "
                    + "is 'null'.");
        }
        session = request.getSession();
        startingDate = request.getParameter(PARAMETER_NAME_13);
        endingDate = request.getParameter(PARAMETER_NAME_14);
        correctnessCondition = checkParameters(startingDate, endingDate,
                errorMessages);
        if (correctnessCondition) {
            try {
                session.setAttribute(USED_TICKETS_ATTRIBUTE_NAME,
                        headPhysicianDAO.retrieveUsedTickets(
                                Date.valueOf(startingDate),
                                Date.valueOf(endingDate)));
            } catch (DAOException e) {
                throw new CommandException("Can't retrieve used ticket data.",
                        e);
            }
            resourcePath
                    = reader.getProperty(USED_TICKET_VIEWING_PAGE_PATH_KEY)
                      + "?" + PARAMETER_NAME_5 + "=" + 1;
        } else {
            session.setAttribute(ERROR_MESSAGES_ATTRIBUTE_NAME, errorMessages);
            path = reader.getProperty(USED_TICKET_FETCHING_PAGE_PATH_KEY);
            session.setAttribute(TARGET_PAGE_PATH_ATTRIBUTE_NAME, path);
            resourcePath = path + "?" + PARAMETER_NAME_13 + "="
                    + encodeString(convertString(startingDate)) + "&"
                    + PARAMETER_NAME_14 + "="
                    + encodeString(convertString(endingDate));
        }
        return resourcePath;
    }
    /**
     * Checks that request parameters comply with defined constraints.
     * @param startingDate a <code>String</code> value which is a starting date
     *                     parameter value
     * @param endingDate a <code>String</code> value which is an ending date
     *                   parameter value
     * @param messages a <code>Map</code> value which is a map of keys of
     *                 input error messages to input error messages
     * @return <code>true</code> if request parameters comply with defined
     *         constraints, <code>false</code> otherwise
     */
    private boolean checkParameters(final String startingDate,
            final String endingDate, final Map<String, String> messages) {
        Date endingPoint = null;
        Date startingPoint = null;
        boolean correctnessCondition = true;
        boolean firstCondition = true;
        boolean secondCondition = true;
        /* The main part of the method. */
        if (!checkDateCorrectness(startingDate)) {
            messages.put(INPUT_ERROR_MESSAGE_1_KEY, "");
            correctnessCondition = false;
            firstCondition = false;
        } else {
            startingPoint = Date.valueOf(startingDate);
            if (startingPoint.after(retrieveCurrentDate())) {
                messages.put(INPUT_ERROR_MESSAGE_2_KEY, "");
                correctnessCondition = false;
                firstCondition = false;
            }
        }
        if (!checkDateCorrectness(endingDate)) {
            messages.put(INPUT_ERROR_MESSAGE_3_KEY, "");
            correctnessCondition = false;
            secondCondition = false;
        } else {
            endingPoint = Date.valueOf(endingDate);
            if (endingPoint.after(retrieveCurrentDate())) {
                messages.put(INPUT_ERROR_MESSAGE_4_KEY, "");
                correctnessCondition = false;
                secondCondition = false;
            }
        }
        if ((firstCondition) && (secondCondition)
                && (startingPoint.after(endingPoint))) {
            messages.put(INPUT_ERROR_MESSAGE_5_KEY, "");
            correctnessCondition = false;
        }
        return correctnessCondition;
    }
}
