/*
 * LogoutCommand.java
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to logout of the web application.
 */
package by.training.task05.web.command;


import static by.training.task05.reader.ApplicationSettingsReader
        .WELCOME_PAGE_PATH_KEY;

import by.training.task05.reader.ApplicationSettingsReader;

import javax.servlet.ServletContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to logout of the web application.
 * @author Yauhen Sazonau
 * @version 1.0, 07/01/19
 * @since 1.0
 */
public class LogoutCommand implements Command {
    /**
     * Fulfills actions needed to logout of a web application.
     * @param request an <code>HttpServletRequest</code> value that is a
     *                request to logout of a web application
     * @param response an <code>HttpServletResponse</code> value which is a
     *                 response to a request
     * @param servletContext a <code>ServletContext</code> value that is
     *                       the servlet context of the web application in
     *                       which a caller is executing
     * @return a <code>String</code> specifying the path to a resource. The
     *         path points to the welcome page. It can't be a <code>null</code>
     *         value.
     */
    @Override
    public String execute(final HttpServletRequest request,
            final HttpServletResponse response,
            final ServletContext servletContext) throws CommandException {
        ApplicationSettingsReader reader
                = ApplicationSettingsReader.getInstance();
        HttpSession session = request.getSession();
        /* The main part of the method. */
        session.invalidate();
        return (reader.getProperty(WELCOME_PAGE_PATH_KEY));
    }

}
