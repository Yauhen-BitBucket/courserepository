/*
 * TicketAssigningCommand.java
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to assign a ticket.
 */
package by.training.task05.web.command;


import static by.training.task05.dao.PatientDAO.ASSIGNMENT_SUCCESS_VALUE;

import static by.training.task05.reader.ApplicationSettingsReader
        .TICKET_ASSIGNING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .RESPONSE_MESSAGE_VIEWING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader.getInstance;

import static by.training.task05.web.command.DataValidator
        .checkTicketIDCorrectness;

import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_1;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_8;

import static by.training.task05.web.command.SupportMethodsKeeper
        .convertString;
import static by.training.task05.web.command.SupportMethodsKeeper.encodeString;

import by.training.task05.dao.DAOException;
import by.training.task05.dao.PatientDAO;

import by.training.task05.dao.implementation.SQLDAOFactory;

import by.training.task05.entity.User;

import by.training.task05.reader.ApplicationSettingsReader;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to assign a ticket.
 * @author Yauhen Sazonau
 * @version 1.0, 07/04/19
 * @since 1.0
 */
public class TicketAssigningCommand implements Command {
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>by.training.task05.entity.User</code> class. The identifier
     * holds the value of "authenticatedUser".
     */
    private static final String AUTHENTICATED_USER_ATTRIBUTE_NAME
            = "authenticatedUser";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>java.util.Map</code> interface. The identifier holds the value
     * of "errorMessages".
     */
    private static final String ERROR_MESSAGES_ATTRIBUTE_NAME
            = "errorMessages";
    /**
     * The name of a property which retains the text that will be displayed
     * as an item of an unordered list to represent an error of a ticket
     * identification number input. The identifier holds the value of
     * "ticket.id.input.error.message".
     */
    private static final String INPUT_ERROR_MESSAGE_KEY
            = "ticket.id.input.error.message";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>String</code> class that is the key of a property of a
     * response message. The identifier holds the value of
     * "responseMessageKey".
     */
    private static final String RESPONSE_MESSAGE_KEY_ATTRIBUTE_NAME
            = "responseMessageKey";
    /**
     * The name of a property which retains the text which will be placed to
     * present the information that a ticket has been successfully assigned.
     * The identifier holds the value of
     * "ticket.assigning.success.response.message".
     */
    private static final String RESPONSE_MESSAGE_1_KEY
            = "ticket.assigning.success.response.message";
    /**
     * The name of a property which retains the text which will be placed to
     * present the information that a ticket hasn't been assigned. The
     * identifier holds the value of
     * "ticket.assigning.failure.response.message".
     */
    private static final String RESPONSE_MESSAGE_2_KEY
            = "ticket.assigning.failure.response.message";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>String</code> class that is a target page path. The identifier
     * holds the value of "targetPagePath".
     */
    private static final String TARGET_PAGE_PATH_ATTRIBUTE_NAME
            = "targetPagePath";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>java.lang.String</code> class that is the name of a utilized
     * command. The identifier holds the value of "utilizedCommand".
     */
    private static final String UTILIZED_COMMAND_NAME_ATTRIBUTE_NAME
            = "utilizedCommand";
    /**
     * Fulfills actions needed to assign a ticket.
     * @param request an <code>HttpServletRequest</code> value that is a
     *                request to assign a ticket
     * @param response an <code>HttpServletResponse</code> value which is a
     *                 response to a request
     * @param servletContext a <code>ServletContext</code> value that is
     *                       the servlet context of the web application in
     *                       which a caller is executing
     * @return a <code>String</code> specifying the path to a resource. The
     *         path points to the page of assigning a ticket if there have
     *         been input errors; otherwise it points to the page of viewing
     *         a response message. It can't be a <code>null</code> value.
     * @throws CommandException if one of the method arguments is a
     *                          <code>null</code> value or there are problems
     *                          with assigning a ticket
     */
    @Override
    public String execute(final HttpServletRequest request,
            final HttpServletResponse response,
            final ServletContext servletContext) throws CommandException {
        ApplicationSettingsReader reader = getInstance();
        HttpSession session;
        Map<String, String> errorMessages = new LinkedHashMap<>();
        SQLDAOFactory factory = SQLDAOFactory.getInstance();
        PatientDAO patientDAO = factory.retrievePatientDAO();
        String path;
        String resourcePath;
        String stringTicketID;
        User user;
        boolean correctnessCondition;
        int operationResult;
        /* The main part of the method. */
        if (request == null) {
            throw new CommandException("The 'request' argument is "
                    + "'null'.");
        }
        if (servletContext == null) {
            throw new CommandException("The 'servletContext' argument "
                    + "is 'null'.");
        }
        session = request.getSession();
        user = (User) session.getAttribute(AUTHENTICATED_USER_ATTRIBUTE_NAME);
        stringTicketID = convertString(request.getParameter(PARAMETER_NAME_8));
        correctnessCondition = checkParameters(stringTicketID, errorMessages);
        if (correctnessCondition) {
            try {
                operationResult = patientDAO.assignTicket(user.getUserID(),
                        Long.parseLong(stringTicketID));
            } catch (DAOException e) {
                throw new CommandException("Can't assign a ticket.", e);
            }
            if (operationResult == ASSIGNMENT_SUCCESS_VALUE) {
                session.setAttribute(RESPONSE_MESSAGE_KEY_ATTRIBUTE_NAME,
                        RESPONSE_MESSAGE_1_KEY);
            } else {
                session.setAttribute(RESPONSE_MESSAGE_KEY_ATTRIBUTE_NAME,
                        RESPONSE_MESSAGE_2_KEY);
            }
            session.setAttribute(UTILIZED_COMMAND_NAME_ATTRIBUTE_NAME,
                    request.getParameter(PARAMETER_NAME_1));
            resourcePath = reader.getProperty(
                    RESPONSE_MESSAGE_VIEWING_PAGE_PATH_KEY);
        } else {
            session.setAttribute(ERROR_MESSAGES_ATTRIBUTE_NAME, errorMessages);
            path = reader.getProperty(TICKET_ASSIGNING_PAGE_PATH_KEY);
            session.setAttribute(TARGET_PAGE_PATH_ATTRIBUTE_NAME, path);
            resourcePath = path + "?" + PARAMETER_NAME_8 + "="
                           + encodeString(stringTicketID);
        }
        return resourcePath;
    }
    /**
     * Checks that request parameters comply with defined constraints.
     * @param ticketID a <code>String</code> value which is an identification
     *                 number of a ticket to be validated
     * @param messages a <code>Map</code> value which is a map of keys of
     *                 input error messages to input error messages
     * @return <code>true</code> if request parameters comply with defined
     *         constraints, <code>false</code> otherwise
     */
    private boolean checkParameters(final String ticketID,
            final Map<String, String> messages) {
        boolean correctnessCondition = true;
        /* The main part of the method. */
        if (!checkTicketIDCorrectness(ticketID)) {
            messages.put(INPUT_ERROR_MESSAGE_KEY, "");
            correctnessCondition = false;
        }
        return correctnessCondition;
    }
}
