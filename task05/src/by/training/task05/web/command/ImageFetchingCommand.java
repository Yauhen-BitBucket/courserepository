/*
 * ImageFetchingCommand.java
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to retrieve an image.
 */
package by.training.task05.web.command;


import static by.training.task05.reader.ApplicationSettingsReader
        .PORTRAIT_IMAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .getInstance;

import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_3;

import static by.training.task05.web.command.SupportMethodsKeeper
        .convertString;

import static by.training.task05.web.controller.ApplicationController
        .DUMMY_PATH;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;

import by.training.task05.dao.DAOException;
import by.training.task05.dao.UserDAO;

import by.training.task05.dao.implementation.SQLDAOFactory;

import by.training.task05.entity.User;
import by.training.task05.reader.ApplicationSettingsReader;

import java.io.IOException;
import java.io.InputStream;

import java.util.Optional;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to retrieve an image.
 * @author Yauhen Sazonau
 * @version 1.0, 07/12/19
 * @since 1.0
 */
public class ImageFetchingCommand implements Command {
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>by.training.task05.entity.User</code> class. The identifier
     * holds the value of "authenticatedUser".
     */
    private static final String AUTHENTICATED_USER_ATTRIBUTE_NAME
            = "authenticatedUser";
    /**
     * Fulfills actions needed to retrieve an image.
     * @param request an <code>HttpServletRequest</code> value that is a
     *                request to retrieve an image
     * @param response an <code>HttpServletResponse</code> value which is a
     *                 response to a request
     * @param servletContext a <code>ServletContext</code> value that is
     *                       the servlet context of the web application in
     *                       which a caller is executing
     * @return a <code>String</code> specifying the path to a resource. The
     *         path is a dummy string. It can't be a <code>null</code> value.
     * @throws CommandException if one of the method arguments is a
     *                          <code>null</code> value or there are problems
     *                          with retrieving an image
     */
    @Override
    public String execute(final HttpServletRequest request,
            final HttpServletResponse response,
            final ServletContext servletContext) throws CommandException {
        ApplicationSettingsReader reader = getInstance();
        HttpSession session;
        InputStream image;
        Optional<InputStream> optional;
        ServletOutputStream stream;
        SQLDAOFactory factory = SQLDAOFactory.getInstance();
        String login;
        User user;
        UserDAO userDAO = factory.retrieveUserDAO();
        int value;
        /* The main part of the method. */
        if (request == null) {
            throw new CommandException("The 'request' argument is "
                    + "'null'.");
        }
        if (servletContext == null) {
            throw new CommandException("The 'servletContext' argument "
                    + "is 'null'.");
        }
        session = request.getSession();
        user = (User) session.getAttribute(AUTHENTICATED_USER_ATTRIBUTE_NAME);
        login = convertString(request.getParameter(PARAMETER_NAME_3));
        try {
            if (!login.equals(user.getLogin())) {
                response.sendError(SC_BAD_REQUEST);
            } else {
                optional = userDAO.retrieveImage(login);
                if (optional.isPresent()) {
                    image = optional.get();
                } else {
                    image = servletContext.getResourceAsStream(
                            reader.getProperty(PORTRAIT_IMAGE_PATH_KEY));
                }
                stream = response.getOutputStream();
                value = image.read();
                while (value != -1) {
                    stream.write(value);
                    value = image.read();
                }
                image.close();
            }
        } catch (DAOException e) {
            throw new CommandException("An error of communication with a "
                    + "source of data arisen while trying to retrieve an "
                    + "image.", e);
        } catch (IOException e) {
            throw new CommandException("An error of returning an image "
                    + "arsisen.", e);
        }
        return DUMMY_PATH;
    }
}
