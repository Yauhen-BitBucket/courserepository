/*
 * SupportMethodsKeeper.java
 * A keeper for methods which are used by command implementations.
 */
package by.training.task05.web.command;


import static java.net.URLEncoder.encode;

import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;

import by.training.task05.exception.ApplicationException;

import java.io.UnsupportedEncodingException;

import java.sql.Date;

import java.util.Enumeration;
import java.util.GregorianCalendar;

import javax.servlet.http.HttpServletRequest;


/**
 * A keeper for methods which are used by command implementations.
 * @author Yauhen Sazonau
 * @version 1.0, 07/02/19
 * @since 1.0
 */
public final class SupportMethodsKeeper {
    /**
     * An "ISO-8859-1" character set name. The identifier holds the value of
     * "ISO-8859-1".
     */
    private static final String ISO_8859_1 = "ISO-8859-1";
    /**
     * An "UTF-8" character set name. The identifier holds the value of
     * "UTF-8".
     */
    private static final String UTF_8 = "UTF-8";
    /**
     * Constructs an instance of this class.
     */
    private SupportMethodsKeeper() {
        /* The default initialization is sufficient. */
    }
    /**
     * Converts a specified <code>String</code> value from the "ISO-8859-1"
     * character set into the "UTF-8" character set.
     * @param value a <code>String</code> value which is a value to be
     *              converted
     * @return converted value
     * @throws ApplicationException if there are problems with converting
     */
    public static String convertString(final String value) {
        if (value == null) {
            throw new ApplicationException("The 'value' argument is a 'null' "
                    + "value.");
        }
        try {
            return (new String(value.getBytes(ISO_8859_1), UTF_8));
        } catch (UnsupportedEncodingException e) {
            throw new ApplicationException("Can't perform conversion.", e);
        }
    }
    /**
     * Translates a specified <code>String</code> value into the
     * <code>application/x-www-form-urlencoded</code> format using a "UTF-8"
     * encoding scheme.
     * @param value a <code>String</code> value which is a value to be encoded
     * @return an encoded value
     * @throws ApplicationException if there are problems with translating
     */
    public static String encodeString(final String value) {
        if (value == null) {
            throw new ApplicationException("The 'value' argument is a 'null' "
                    + "value.");
        }
        try {
            return (encode(value, UTF_8));
        } catch (UnsupportedEncodingException e) {
            throw new ApplicationException("Can't perform encoding.", e);
        }
    }
    /**
     * Adds query string parameters to a specified <code>StringBuilder</code>
     * from a request object.
     * @param builder a <code>StringBuilder</code> value which is a container
     *                where parameters will be added to
     * @param request a <code>HttpServletRequest</code> value which is a
     *                request which parameters will be added from
     * @throws ApplicationException if there are problems with adding query
     *                              string parameters
     */
    public static void addParameters(final StringBuilder builder,
            final HttpServletRequest request) {
        Enumeration<String> enumeration;
        String parameterName;
        /* The main part of the method. */
        if (builder == null) {
            throw new ApplicationException("The 'builder' argument is a "
                    + "'null' value.");
        }
        if (request == null) {
            throw new ApplicationException("The 'request' argument is a "
                    + "'null' value.");
        }
        enumeration = request.getParameterNames();
        while (enumeration.hasMoreElements()) {
            parameterName = enumeration.nextElement();
            builder.append(encodeString(convertString(parameterName))
                    + "=" + encodeString(convertString(
                            request.getParameter(parameterName))));
            if (enumeration.hasMoreElements()) {
                builder.append("&");
            }
        }
    }
    /**
     * Retrieves the name of a specified <code>enum</code> constant, exactly
     * as declared in its <code>enum</code> declaration.
     * @param enumConstant an <code>Enum</code> value which is an arbitrary
     *                     <code>enum</code> constant the name of which
     *                     should be retrieved
     * @return the name of the specified <code>enum</code> constant, exactly as
     *         declared in its <code>enum</code> declaration. Can't be a
     *         <code>null</code> value.
     * @throws ApplicationException if the <code>enumConstant</code> argument
     *                              is a <code>null</code> value
     */
    public static String retrieveEnumName(final Enum<?> enumConstant) {
        if (enumConstant == null) {
            throw new ApplicationException("The 'enumConstant' argument is a "
                    + "'null' value.");
        }
        return enumConstant.name();
    }
    /**
     * Retrieves the current date as the <code>java.sql.Date</code> value.
     * @return the current date as the <code>java.sql.Date</code> value
     */
    public static Date retrieveCurrentDate() {
        GregorianCalendar calendar = new GregorianCalendar();
        /* The main part of the method. */
        return (Date.valueOf(calendar.get(YEAR) + "-"
                             + (calendar.get(MONTH) + 1) + "-"
                             + calendar.get(DAY_OF_MONTH)));
    }
}
