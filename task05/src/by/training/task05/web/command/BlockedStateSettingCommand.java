/*
 * BlockedStateSettingCommand.java
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to set a blocked state.
 */
package by.training.task05.web.command;


import static by.training.task05.dao.HeadPhysicianDAO
        .BLCOOKED_STATE_SETING_SUCCESS_VALUE;

import static by.training.task05.reader.ApplicationSettingsReader
        .RESPONSE_MESSAGE_VIEWING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader.getInstance;

import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_1;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_17;
import static by.training.task05.web.command.ParameterNamesKeeper
        .PARAMETER_NAME_22;

import by.training.task05.dao.DAOException;
import by.training.task05.dao.HeadPhysicianDAO;

import by.training.task05.dao.implementation.SQLDAOFactory;

import by.training.task05.entity.User;

import by.training.task05.reader.ApplicationSettingsReader;

import java.util.Map;

import javax.servlet.ServletContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Represents a command which will be executed each time the controller of an
 * application receives an incoming request to set a blocked state.
 * @author Yauhen Sazonau
 * @version 1.0, 08/03/19
 * @since 1.0
 */
public class BlockedStateSettingCommand implements Command {
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>by.training.task05.entity.User</code> class. The identifier
     * holds the value of "authenticatedUser".
     */
    private static final String AUTHENTICATED_USER_ATTRIBUTE_NAME
            = "authenticatedUser";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>String</code> class that is the key of a property of a
     * response message. The identifier holds the value of
     * "responseMessageKey".
     */
    private static final String RESPONSE_MESSAGE_KEY_ATTRIBUTE_NAME
            = "responseMessageKey";
    /**
     * The name of a property which retains the text which will be placed to
     * present the information that a person has been successfully blocked.
     * The identifier holds the value of
     * "blocked.state.setting.success.response.message".
     */
    private static final String RESPONSE_MESSAGE_1_KEY
            = "blocked.state.setting.success.response.message";
    /**
     * The name of a property which retains the text which will be placed to
     * present the information that a person hasn't been blocked. The
     * identifier holds the value of
     * "blocked.state.setting.failure.response.message".
     */
    private static final String RESPONSE_MESSAGE_2_KEY
            = "blocked.state.setting.failure.response.message";
    /**
     * The name of a property which retains the text which will be placed to
     * present the information that a person has been successfully unblocked.
     * The identifier holds the value of
     * "unblocked.state.setting.success.response.message".
     */
    private static final String RESPONSE_MESSAGE_3_KEY
            = "unblocked.state.setting.success.response.message";
    /**
     * The name of a property which retains the text which will be placed to
     * present the information that a person hasn't been unblocked. The
     * identifier holds the value of
     * "unblocked.state.setting.failure.response.message".
     */
    private static final String RESPONSE_MESSAGE_4_KEY
            = "unblocked.state.setting.failure.response.message";
    /**
     * The name of a context attribute which is used to retain an instance of
     * the <code>Map</code> interface that represents a map which maps a
     * passport number to a session. The identifier holds the value of
     * "sessionMap".
     */
    private static final String SESSION_MAP_ATTRIBUTE_NAME = "sessionMap";
    /**
     * The name of a session attribute which is used to retain an instance of
     * the <code>java.lang.String</code> class that is the name of a utilized
     * command. The identifier holds the value of "utilizedCommand".
     */
    private static final String UTILIZED_COMMAND_NAME_ATTRIBUTE_NAME
            = "utilizedCommand";
    /**
     * Fulfills actions needed to set a blocked state.
     * @param request an <code>HttpServletRequest</code> value that is a
     *                request to set a blocked state
     * @param response an <code>HttpServletResponse</code> value which is a
     *                 response to a request
     * @param servletContext a <code>ServletContext</code> value that is
     *                       the servlet context of the web application in
     *                       which a caller is executing
     * @return a <code>String</code> specifying the path to a resource. The
     *         path points to the page of viewing a response message. It can't
     *         be a <code>null</code> value.
     * @throws CommandException if one of the method arguments is a
     *                          <code>null</code> value or there are problems
     *                          with setting a blocked state
     */
    @Override
    public String execute(final HttpServletRequest request,
            final HttpServletResponse response,
            final ServletContext servletContext) throws CommandException {
        ApplicationSettingsReader reader = getInstance();
        HttpSession session;
        SQLDAOFactory factory = SQLDAOFactory.getInstance();
        HeadPhysicianDAO headPhysicianDAO = factory.retrieveHeadPhysicianDAO();
        String resourcePath;
        boolean blockedState;
        int operationResult;
        /* The main part of the method. */
        if (request == null) {
            throw new CommandException("The 'request' argument is "
                    + "'null'.");
        }
        if (servletContext == null) {
            throw new CommandException("The 'servletContext' argument "
                    + "is 'null'.");
        }
        session = request.getSession();
        blockedState = Boolean.parseBoolean(
                request.getParameter(PARAMETER_NAME_22));
        try {
            operationResult = headPhysicianDAO.setBlockedState(
                    request.getParameter(PARAMETER_NAME_17), blockedState);
        } catch (DAOException e) {
            throw new CommandException("Can't set a blocked state.", e);
        }
        if (operationResult == BLCOOKED_STATE_SETING_SUCCESS_VALUE) {
            if (blockedState) {
                session.setAttribute(RESPONSE_MESSAGE_KEY_ATTRIBUTE_NAME,
                        RESPONSE_MESSAGE_1_KEY);
            } else {
                session.setAttribute(RESPONSE_MESSAGE_KEY_ATTRIBUTE_NAME,
                        RESPONSE_MESSAGE_3_KEY);
            }
            modifyBlockedState(request.getParameter(PARAMETER_NAME_17),
                    blockedState, servletContext);
        } else {
            if (blockedState) {
                session.setAttribute(RESPONSE_MESSAGE_KEY_ATTRIBUTE_NAME,
                        RESPONSE_MESSAGE_2_KEY);
            } else {
                session.setAttribute(RESPONSE_MESSAGE_KEY_ATTRIBUTE_NAME,
                        RESPONSE_MESSAGE_4_KEY);
            }
        }
        session.setAttribute(UTILIZED_COMMAND_NAME_ATTRIBUTE_NAME,
                request.getParameter(PARAMETER_NAME_1));
        resourcePath = reader.getProperty(
                RESPONSE_MESSAGE_VIEWING_PAGE_PATH_KEY);
        return resourcePath;
    }
    /**
     * Modifies a blocked state of a user.
     * @param passportNumber a <code>String</code> value which is a passport
     *                       number of a user
     * @param blockedState a <code>boolean</code> value which is a blocked
     *                     state
     * @param servletContext a <code>ServletContext</code> value that is
     *                       the servlet context of the web application in
     *                       which a caller is executing
     */
    private void modifyBlockedState(final String passportNumber,
            final boolean blockedState, final ServletContext servletContext) {
        Map<String, HttpSession> sessionMap
                = (Map) servletContext.getAttribute(
                        SESSION_MAP_ATTRIBUTE_NAME);
        HttpSession session = sessionMap.get(passportNumber);
        User user;
        /* The main part of the method. */
        if (session != null) {
            try {
                user = (User) session.getAttribute(
                        AUTHENTICATED_USER_ATTRIBUTE_NAME);
                user.setBlockedState(blockedState);
            } catch (IllegalStateException e) {
                /* There is no sense to do something here, even logging. */
            }
        }
    }
}
