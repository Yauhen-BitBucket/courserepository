/*
 * ContextListener.java
 * The listener which fulfills an initialization of a web application and
 * releases resources used during its life cycle.
 */
package by.training.task05.web.listener;


import static by.training.task05.reader.ApplicationSettingsReader
        .IMAGE_SETTING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .PATIENT_ADDING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .ROW_QUANTITY_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .STYLE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .TICKET_ADDING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .TICKET_ASSIGNING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .TICKET_CANCELLING_PAGE_PATH_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .VISIST_COMMITTING_PAGE_PATH_KEY;

import by.training.task05.entity.Role;
import by.training.task05.entity.Speciality;

import by.training.task05.exception.ApplicationException;

import by.training.task05.pool.ConnectionPool;
import by.training.task05.pool.PoolException;

import by.training.task05.reader.ApplicationSettingsReader;

import by.training.task05.web.command.CommandName;

import by.training.task05.web.filter.CorrectnessFilter;

import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * The listener which fulfills an initialization of a web application and
 * releases resources used during its life cycle.
 * @author Yauhen Sazonau
 * @version 1.0, 06/30/19
 * @since 1.0
 */
public class ContextListener implements ServletContextListener {
    /**
     * The name of a context attribute which is used to retain an instance of
     * the <code>java.util.Locale</code> class. The instance will be used if
     * a user doesn't change the default setting. The identifier holds the
     * value of "defaultLocale".
     */
    public static final String DEFAULT_LOCALE_ATTRIBUTE_NAME
            = "defaultLocale";
    /**
     * The name of a context attribute which is used to retain an instance of
     * the <code>by.training.task05.entity.Role</code> class that represents
     * the head physician physician role. The identifier holds the value of
     * "headPhysicianRole".
     */
    public static final String HEAD_PHYSICIAN_ROLE_ATTRIBUTE_NAME
            = "headPhysicianRole";
    /**
     * The name of a context attribute which is used to retain an instance of
     * the <code>by.training.task05.entity.Role</code> class that represents
     * the patient role. The identifier holds the value of "patientRole".
     */
    public static final String PATIENT_ROLE_ATTRIBUTE_NAME
            = "patientRole";
    /**
     * The name of a context attribute which is used to retain an instance of
     * the <code>by.training.task05.entity.Role</code> class that represents
     * the physician role. The identifier holds the value of "physicianRole".
     */
    public static final String PHYSICIAN_ROLE_ATTRIBUTE_NAME
            = "physicianRole";
    /**
     * The name of a property which retains the label for a reference that
     * allows a user to be redirected to a page which provides a form for
     * assigning a ticket. The identifier holds the value of
     * "ticket.assigning.page.reference.label".
     */
    public static final String REFERENCE_LABEL_1_KEY
            = "ticket.assigning.page.reference.label";
    /**
     * The name of a property which retains the label for a reference that
     * allows a user to be redirected to a page which provides a form for
     * canceling a ticket. The identifier holds the value of
     * "ticket.canceling.page.reference.label".
     */
    public static final String REFERENCE_LABEL_2_KEY
            = "ticket.canceling.page.reference.label";
    /**
     * The name of a property which retains the label for a reference that
     * allows a user to be redirected to a page which provides a form for
     * adding a ticket. The identifier holds the value of
     * "ticket.adding.page.reference.label".
     */
    public static final String REFERENCE_LABEL_3_KEY
            = "ticket.adding.page.reference.label";
    /**
     * The name of a property which retains the label for a reference that
     * allows a user to be redirected to a page which provides a form for
     * committing a visit. The identifier holds the value of
     * "visit.committing.page.reference.label".
     */
    public static final String REFERENCE_LABEL_4_KEY
            = "visit.committing.page.reference.label";
    /**
     * The name of a property which retains the label for a reference that
     * allows a user to be redirected to a page which provides a form for
     * adding a patient. The identifier holds the value of
     * "patient.adding.page.reference.label".
     */
    public static final String REFERENCE_LABEL_5_KEY
            = "patient.adding.page.reference.label";
    /**
     * The name of a property which retains the label for a reference that
     * allows a user to be redirected to a page which provides a form for
     * setting an image. The identifier holds the value of
     * "image.setting.page.reference.label".
     */
    public static final String REFERENCE_LABEL_6_KEY
            = "image.setting.page.reference.label";
    /**
     * The name of a context attribute which is used to retain an instance of
     * the <code>java.util.Map</code> interface that is the mapping of command
     * names to reference label keys. The identifier holds the value
     * "referenceLabelKeys".
     */
    public static final String REFERENCE_LABEL_KEYS_ATTRIBUTE_NAME
            = "referenceLabelKeys";
    /**
     * The name of a context attribute which is used to retain an instance of
     * the <code>java.util.Map</code> interface that is the mapping of command
     * names to reference paths. The identifier holds the value
     * "referencePaths".
     */
    public static final String REFERENCE_PATHS_ATTRIBUTE_NAME
            = "referencePaths";
    /**
     * The name of a property which retains the label for a patient role. The
     * identifier holds the value of "patient.role.label".
     */
    public static final String ROLE_1_LABEL_KEY
            = "patient.role.label";
    /**
     * The name of a property which retains the label for a physician role. The
     * identifier holds the value of "physician.role.label".
     */
    public static final String ROLE_2_LABEL_KEY
            = "physician.role.label";
    /**
     * The name of a property which retains the label for a head physician
     * role. The identifier holds the value of "head.physician.role.label".
     */
    public static final String ROLE_3_LABEL_KEY
            = "head.physician.role.label";
    /**
     * The name of a context attribute which is used to retain an instance of
     * the <code>Map</code> interface that represents a map which maps a role
     * to a role label key. The identifier holds the value of "roleMap".
     */
    public static final String ROLE_MAP_ATTRIBUTE_NAME = "roleMap";
    /**
     * The name of a context attribute which is used to retain an instance of
     * the <code>java.lang.Integer</code> class that is the maximum row
     * quantity of some information to display on a page. The identifier
     * holds the value of "rowQuantity".
     */
    public static final String ROW_QUANTITY_ATTRIBUTE_NAME
            = "rowQuantity";
    /**
     * The name of a context attribute which is used to retain an instance of
     * the <code>Map</code> interface that represents a map which maps a
     * passport number to a session. The identifier holds the value of
     * "sessionMap".
     */
    public static final String SESSION_MAP_ATTRIBUTE_NAME = "sessionMap";
    /**
     * The name of a property which retains the label for a therapeutist
     * speciality. The identifier holds the value of
     * "therapeutist.speciality.label".
     */
    public static final String SPECIALITY_1_LABEL_KEY
            = "therapeutist.speciality.label";
    /**
     * The name of a property which retains the label for a surgeon
     * speciality. The identifier holds the value of
     * "surgeon.speciality.label".
     */
    public static final String SPECIALITY_2_LABEL_KEY
            = "surgeon.speciality.label";
    /**
     * The name of a property which retains the label for an ophthalmologist
     * speciality. The identifier holds the value of
     * "ophthalmologist.speciality.label".
     */
    public static final String SPECIALITY_3_LABEL_KEY
            = "ophthalmologist.speciality.label";
    /**
     * The name of a property which retains the label for a uroligist
     * speciality. The identifier holds the value of
     * "uroligist.speciality.label".
     */
    public static final String SPECIALITY_4_LABEL_KEY
            = "uroligist.speciality.label";
    /**
     * The name of a property which retains the label for an otolaryngologist
     * speciality. The identifier holds the value of
     * "otolaryngologist.speciality.label".
     */
    public static final String SPECIALITY_5_LABEL_KEY
            = "otolaryngologist.speciality.label";
    /**
     * The name of a context attribute which is used to retain an instance of
     * the <code>Map</code> interface that represents a map which maps a
     * speciality to a speciality label key. The identifier holds the value of
     * "specialityMap".
     */
    public static final String SPECIALITY_MAP_ATTRIBUTE_NAME = "specialityMap";
    /**
     * The name of a context attribute which is used to retain an instance of
     * the <code>java.lang.String</code> class that is a path to the style
     * sheet. The identifier holds the value of "stylePath".
     */
    public static final String STYLE_PATH_ATTRIBUTE_NAME
            = "stylePath";
    /**
     * A logger for reporting errors. <code>null</code> value is prohibited.
     */
    private static final Logger LOGGER
            = LogManager.getLogger(CorrectnessFilter.class);
    /**
     * Releases resources used during the life cycle of an application.
     * @param servletContextEvent a <code>ServletContextEvent</code> value
     *                            which is the event notifying that the
     *                            servlet context is about to be shut down
     */
    @Override
    public void contextDestroyed(
            final ServletContextEvent servletContextEvent) {
        ConnectionPool pool;
        /* The main part of the method. */
        try {
            pool = ConnectionPool.getInstance();
            pool.closeAllConnections();
        } catch (PoolException e) {
            LOGGER.error("A problem occured during resource releasing.", e);
        }
    }
    /**
     * Fulfills an initialization of a web application. It includes
     * instantiating the Singleton classes. Also context attributes used by
     * the application are stored.
     * @param servletContextEvent a <code>ServletContextEvent</code> value
     *                            which is the event of starting a web
     *                            application initialization process
     */
    @Override
    public void contextInitialized(
            final ServletContextEvent servletContextEvent) {
        ApplicationSettingsReader reader
                = ApplicationSettingsReader.getInstance();
        Locale defaultLocale = Locale.ENGLISH;
        ServletContext context = servletContextEvent.getServletContext();
        /* The main part of the method. */
        context.setAttribute(DEFAULT_LOCALE_ATTRIBUTE_NAME, defaultLocale);
        context.setAttribute(PATIENT_ROLE_ATTRIBUTE_NAME, Role.PATIENT);
        context.setAttribute(PHYSICIAN_ROLE_ATTRIBUTE_NAME, Role.PHYSICIAN);
        context.setAttribute(HEAD_PHYSICIAN_ROLE_ATTRIBUTE_NAME,
                Role.HEAD_PHYSICIAN);
        context.setAttribute(SPECIALITY_MAP_ATTRIBUTE_NAME,
                createSpecialityMap());
        context.setAttribute(ROW_QUANTITY_ATTRIBUTE_NAME,
                reader.getProperty(ROW_QUANTITY_KEY));
        context.setAttribute(REFERENCE_LABEL_KEYS_ATTRIBUTE_NAME,
                createReferenceLabelKeyMap());
        context.setAttribute(REFERENCE_PATHS_ATTRIBUTE_NAME,
                createReferencePathMap());
        context.setAttribute(ROLE_MAP_ATTRIBUTE_NAME, createRoleMap());
        context.setAttribute(STYLE_PATH_ATTRIBUTE_NAME,
                reader.getProperty(STYLE_PATH_KEY));
        context.setAttribute(SESSION_MAP_ATTRIBUTE_NAME,
                new LinkedHashMap<>());
    }
    /**
     * Creates a map which maps a speciality to a speciality label key.
     * @return a map which maps a speciality to a speciality label key
     */
    private Map<Speciality, String> createSpecialityMap() {
        Map<Speciality, String> map = new LinkedHashMap<>();
        /* The main part of the method. */
        map.put(Speciality.THERAPEUTIST, SPECIALITY_1_LABEL_KEY);
        map.put(Speciality.SURGEON, SPECIALITY_2_LABEL_KEY);
        map.put(Speciality.OPHTHALMOLOGIST, SPECIALITY_3_LABEL_KEY);
        map.put(Speciality.UROLOGIST, SPECIALITY_4_LABEL_KEY);
        map.put(Speciality.OTOLARYNGOLOGIST, SPECIALITY_5_LABEL_KEY);
        return map;
    }
    /**
     * Creates a map which maps a command name to a reference label key.
     * @return a map which maps a command name to a reference label key
     */
    private Map<String, String> createReferenceLabelKeyMap() {
        Map<String, String> map = new LinkedHashMap<>();
        /* The main part of the method. */
        map.put(convertCommandName(CommandName.TICKET_ASSIGNING_COMMAND),
                REFERENCE_LABEL_1_KEY);
        map.put(convertCommandName(CommandName.TICKET_CANCELLING_COMMAND),
                REFERENCE_LABEL_2_KEY);
        map.put(convertCommandName(CommandName.TICKET_ADDING_COMMAND),
                REFERENCE_LABEL_3_KEY);
        map.put(convertCommandName(CommandName.VISIT_COMMITTING_COMMAND),
                REFERENCE_LABEL_4_KEY);
        map.put(convertCommandName(CommandName.PATIENT_ADDING_COMMAND),
                REFERENCE_LABEL_5_KEY);
        map.put(convertCommandName(CommandName.IMAGE_SETTING_COMMAND),
                REFERENCE_LABEL_6_KEY);
        return map;
    }
    /**
     * Creates a map which maps a command name to a reference path.
     * @return a map which maps a command name to a reference path
     */
    private Map<String, String> createReferencePathMap() {
        ApplicationSettingsReader reader
                = ApplicationSettingsReader.getInstance();
        Map<String, String> map = new LinkedHashMap<>();
        /* The main part of the method. */
        map.put(convertCommandName(CommandName.IMAGE_SETTING_COMMAND),
                reader.getProperty(IMAGE_SETTING_PAGE_PATH_KEY));
        map.put(convertCommandName(CommandName.PATIENT_ADDING_COMMAND),
                reader.getProperty(PATIENT_ADDING_PAGE_PATH_KEY));
        map.put(convertCommandName(CommandName.TICKET_ASSIGNING_COMMAND),
                reader.getProperty(TICKET_ASSIGNING_PAGE_PATH_KEY));
        map.put(convertCommandName(CommandName.TICKET_CANCELLING_COMMAND),
                reader.getProperty(TICKET_CANCELLING_PAGE_PATH_KEY));
        map.put(convertCommandName(CommandName.TICKET_ADDING_COMMAND),
                reader.getProperty(TICKET_ADDING_PAGE_PATH_KEY));
        map.put(convertCommandName(CommandName.VISIT_COMMITTING_COMMAND),
                reader.getProperty(VISIST_COMMITTING_PAGE_PATH_KEY));
        return map;
    }
    /**
     * Creates a map which maps a role to a role label key.
     * @return a map which maps a role to a role label key
     */
    private Map<Role, String> createRoleMap() {
        Map<Role, String> map = new LinkedHashMap<>();
        /* The main part of the method. */
        map.put(Role.PATIENT, ROLE_1_LABEL_KEY);
        map.put(Role.PHYSICIAN, ROLE_2_LABEL_KEY);
        map.put(Role.HEAD_PHYSICIAN, ROLE_3_LABEL_KEY);
        return map;
    }
    /**
     * Converts the name of a command to a lower case and replaces all
     * underscores with dashes.
     * @param command a <code>CommandName</code> value which is a command name
     * @return a converted value
     * @throws ApplicationException if the <code>command</code> argument is a
     *                              <code>null</code> value
     */
    public static String convertCommandName(final CommandName command) {
        String tempString;
        /* The main part of the method. */
        if (command == null) {
            throw new ApplicationException("The 'command' argument is a "
                    + "'null' value.");
        }
        tempString = command.name();
        tempString = tempString.toLowerCase();
        return (tempString.replaceAll("_", "-"));
    }
}
