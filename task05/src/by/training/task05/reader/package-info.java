/**
 * Contains a reader for reading configuration data.
 * @author Yauhen Sazonau
 * @since 1.0
 */
package by.training.task05.reader;
