/*
 * ApplicationSettingsReader.java
 * A reader for deriving text strings which represent application settings.
 * These settings include various paths to resources.
 */
package by.training.task05.reader;


import java.util.MissingResourceException;
import java.util.ResourceBundle;

import java.util.concurrent.locks.ReentrantLock;


/**
 * A reader for deriving text strings which represent application settings.
 * These settings include various paths to resources.
 * @author Yauhen Sazonau
 * @version 06/16/19
 * @since 1.0
 */
public final class ApplicationSettingsReader {
    /**
     * The name of a property which retains the path to the assigned ticket
     * fetching page. The identifier holds the value of
     * "assigned.ticket.fetching.page.path".
     */
    public static final String ASSIGNED_TICKET_FETCHING_PAGE_PATH_KEY
            = "assigned.ticket.fetching.page.path";
    /**
     * The name of a property which retains the path to the page of viewing
     * assigned ticket information. The identifier holds the value of
     * "assigned.ticket.viewing.page.path".
     */
    public static final String ASSIGNED_TICKET_VIEWING_PAGE_PATH_KEY
            = "assigned.ticket.viewing.page.path";
    /**
     * The name of a property which retains the path to the authentication
     * page. The identifier holds the value of "authentication.page.path".
     */
    public static final String AUTHENTICATION_PAGE_PATH_KEY
            = "authentication.page.path";
    /**
     * The name of a property which retains the path to the controller. The
     * identifier holds the value of "controller.path".
     */
    public static final String CONTROLLER_PATH_KEY
            = "controller.path";
    /**
     * The name of a property which retains the driver class name of a
     * database. The identifier holds the value of "driver.class.name".
     */
    public static final String DATABASE_DRIVER_CLASS_NAME_KEY
            = "driver.class.name";
    /**
     * The name of a property which retains the user password. The identifier
     * holds the value of "database.password".
     */
    public static final String DATABASE_PASSWORD_KEY = "database.password";
    /**
     * The name of a property which retains the location of a database.
     * The identifier holds the value of "database.url".
     */
    public static final String DATABASE_URL_KEY = "database.url";
    /**
     * The name of a property which retains the user name on behalf of which
     * a database is used. The identifier holds the value of "database.user".
     */
    public static final String DATABASE_USER_KEY = "database.user";
    /**
     * The name of a property which retains the path to the doctor fetching
     * page. The identifier holds the value of "doctor.fetching.page.path".
     */
    public static final String DOCTOR_FETCHING_PAGE_PATH_KEY
            = "doctor.fetching.page.path";
    /**
     * The name of a property which retains the path to the page of viewing
     * doctor information. The identifier holds the value of
     * "doctor.viewing.page.path".
     */
    public static final String DOCTOR_VIEWING_PAGE_PATH_KEY
            = "doctor.viewing.page.path";
    /**
     * The name of a property which retains the path to the error page. The
     * identifier holds the value of "error.page.path".
     */
    public static final String ERROR_PAGE_PATH_KEY
            = "error.page.path";
    /**
     * The name of a property which retains the path to the free ticket
     * fetching page. The identifier holds the value of
     * "free.ticket.fetching.page.path".
     */
    public static final String FREE_TICKET_FETCHING_PAGE_PATH_KEY
            = "free.ticket.fetching.page.path";
    /**
     * The name of a property which retains the path to the page of viewing
     * free ticket information. The identifier holds the value of
     * "free.ticket.viewing.page.path".
     */
    public static final String FREE_TICKET_VIEWING_PAGE_PATH_KEY
            = "free.ticket.viewing.page.path";
    /**
     * The name of a property which retains the path to the folder that
     * contains image files. The identifier holds the value of
     * "image.folder.path".
     */
    public static final String IMAGE_FOLDER_PATH_KEY
            = "image.folder.path";
    /**
     * The name of a property which retains the path to the image setting page.
     * The identifier holds the value of "image.setting.page.path".
     */
    public static final String IMAGE_SETTING_PAGE_PATH_KEY
            = "image.setting.page.path";
    /**
     * The name of a property which retains the initial number of
     * connections which will be within the newly created connection pool.
     * The identifier holds the value of "initial.connection.number".
     */
    public static final String INITIAL_CONNECTON_NUMBER_KEY
            = "initial.connection.number";
    /**
     * The name of a property which retains the path to the main page. The
     * identifier holds the value of "main.page.path".
     */
    public static final String MAIN_PAGE_PATH_KEY
            = "main.page.path";
    /**
     * The name of a property which retains the maximum number of connections
     * that can be instantiated in the connection pool. The identifier holds
     * the value of "maximum.connection.number".
     */
    public static final String MAXIMUM_CONNECTION_NUMBER_KEY
            = "maximum.connection.number";
    /**
     * The name of a property which retains a maximum value the size of an
     * image can have. The identifier holds the value of "max.image.file.size".
     */
    public static final String MAXIMUM_IMAGE_FILE_SIZE_KEY
            = "max.image.file.size";
    /**
     * The name of a property which retains the path to the page of viewing own
     * tickets. The identifier holds the value of
     * "own.ticket.viewing.page.path".
     */
    public static final String OWN_TICKET_VIEWING_PAGE_PATH_KEY
            = "own.ticket.viewing.page.path";
    /**
     * The name of a property which retains the path to the patient adding
     * page. The identifier holds the value of "patient.adding.page.path".
     */
    public static final String PATIENT_ADDING_PAGE_PATH_KEY
            = "patient.adding.page.path";
    /**
     * The name of a property which retains the path to the portrait image. The
     * identifier holds the value of "portrait.image.path".
     */
    public static final String PORTRAIT_IMAGE_PATH_KEY
            = "portrait.image.path";
    /**
     * The name of a property which retains the path to the page of viewing
     * response message. The identifier holds the value of
     * "response.message.viewing.page.path".
     */
    public static final String RESPONSE_MESSAGE_VIEWING_PAGE_PATH_KEY
            = "response.message.viewing.page.path";
    /**
     * The name of a property which retains the maximum row quantity of some
     * information to display on a page. The identifier holds the value
     * "rowQuantity".
     */
    public static final String ROW_QUANTITY_KEY = "rowQuantity";
    /**
     * The name of a property which retains the path to the folder that
     * contains scripts. The identifier holds the value of
     * "script.folder.path".
     */
    public static final String SCRIPT_FOLDER_PATH_KEY
            = "script.folder.path";
    /**
     * The name of a property which retains the path to the service info
     * fetching page. The identifier holds the value of
     * "service.info.fetching.page.path".
     */
    public static final String SERVICE_INFO_FETCHING_PAGE_PATH_KEY
            = "service.info.fetching.page.path";
    /**
     * The name of a property which retains the path to the service info
     * viewing page. The identifier holds the value of
     * "service.info.viewing.page.path".
     */
    public static final String SERVICE_INFO_VIEWING_PAGE_PATH_KEY
            = "service.info.viewing.page.path";
    /**
     * The name of a property which retains the path to the folder that
     * contains a css. The identifier holds the value of
     * "style.folder.path".
     */
    public static final String STYLE_FOLDER_PATH_KEY
            = "style.folder.path";
    /**
     * The name of a property which retains the path to the style sheet that is
     * relative to the current context root. The identifier holds the value of
     * "style.path".
     */
    public static final String STYLE_PATH_KEY
            = "style.path";
    /**
     * The name of a property which retains the path to the page of ticket
     * adding. The identifier holds the value of "ticket.adding.page.path".
     */
    public static final String TICKET_ADDING_PAGE_PATH_KEY
            = "ticket.adding.page.path";
    /**
     * The name of a property which retains the path to the page of ticket
     * assigning. The identifier holds the value of
     * "ticket.assigning.page.path".
     */
    public static final String TICKET_ASSIGNING_PAGE_PATH_KEY
            = "ticket.assigning.page.path";
    /**
     * The name of a property which retains the path to the page of ticket
     * canceling. The identifier holds the value of
     * "ticket.cancelling.page.path".
     */
    public static final String TICKET_CANCELLING_PAGE_PATH_KEY
            = "ticket.cancelling.page.path";
    /**
     * The name of a property which retains the path to the used ticket
     * fetching page. The identifier holds the value of
     * "used.ticket.fetching.page.path".
     */
    public static final String USED_TICKET_FETCHING_PAGE_PATH_KEY
            = "used.ticket.fetching.page.path";
    /**
     * The name of a property which retains the path to the used ticket
     * viewing page. The identifier holds the value of
     * "used.ticket.viewing.page.path".
     */
    public static final String USED_TICKET_VIEWING_PAGE_PATH_KEY
            = "used.ticket.viewing.page.path";
    /**
     * The name of a property which retains the path to the page of viewing
     * users which are not head physicians. The identifier holds the value of
     * "user.viewing.page.path".
     */
    public static final String USER_VIEWING_PAGE_PATH_KEY
            = "user.viewing.page.path";
    /**
     * The name of a property which retains the path to the visit committing
     * page. The identifier holds the value of
     * "visit.committing.page.path".
     */
    public static final String VISIST_COMMITTING_PAGE_PATH_KEY
            = "visit.committing.page.path";
    /**
     * The name of a property which retains the condition which determines
     * what to do in a situation when all available connections have been
     * handed out and the maximum number of connections have been
     * instantiated. The choice is to throw an exception or wait for another
     * thread until it releases a connection. The identifier holds the value of
     * "waiting.condition".
     */
    public static final String WAITING_CONDITION_KEY
            = "waiting.condition";
    /**
     * The name of a property which retains the path to the welcome page. The
     * identifier holds the value of "welcome.page.path".
     */
    public static final String WELCOME_PAGE_PATH_KEY
            = "welcome.page.path";
    /**
     * The name of the resource bundle that contains all the necessary
     * information. The identifier holds the value of
     * by.training.task05.bundle.ApplicationSettings".
     */
    private static final String BUNDLE_NAME
            = "by.training.task05.bundle.ApplicationSettings";
    /**
     * The single instance of this class. <code>null</code> value is
     * permitted and indicates that the single instance hasn't yet been
     * created.
     */
    private static ApplicationSettingsReader instance;
    /**
     * A lock. Can't be a <code>null</code> value.
     */
    private static ReentrantLock lock = new ReentrantLock();
    /**
     * An instance of the <code>ResourceBundle</code> class that will help in
     * reading application settings. Can't be a <code>null</code> value.
     */
    private ResourceBundle bundle;
    /**
     * Creates an instance of the application settings reader. We use the
     * <code>private</code> key word to prevent creating an instance of this
     * class outside of it.
     */
    private ApplicationSettingsReader() {
        /* The default initialization is sufficient. */
    }
    /**
     * Gets the value of a property by the key.
     * @param key the <code>String</code> value which is the key of a property
     * @return the value of a property. Can't be a <code>null</code> value.
     * @throws ReaderException if no resource has been found by the
     *                            <code>key</code> or if the <code>key</code>
     *                            argument is a <code>null</code> value
     */
    public String getProperty(final String key) {
        String result;
        /* The main part of the method. */
        if (key == null) {
            throw new ReaderException("The 'key' argument is 'null'.");
        }
        try {
            result = bundle.getString(key);
        } catch (MissingResourceException e) {
            throw new ReaderException("There is no value which "
                    + "corresponds to the specified key: " + key + ".", e);
        }
        return result;
    }
    /**
     * Creates an instance of the application settings reader if it is needed
     * or returns an existing. You must invoke this method because of no
     * having an ability to invoke the constructor.
     * @return a newly created instance of the
     *         <code>ApplicationSettingsReader</code> class or an existing
     *         if it has been created before. Can't be a <code>null</code>
     *         value.
     * @throws ReaderException if an underlying source of data is
     *                         missed
     */
    public static ApplicationSettingsReader getInstance() {
        lock.lock();
        if (instance == null) {
            instance = new ApplicationSettingsReader();
            try {
                instance.bundle = ResourceBundle.getBundle(BUNDLE_NAME);
            } catch (MissingResourceException e) {
                throw new ReaderException("An underlying source of "
                        + "data is missed: " + BUNDLE_NAME + ".", e);
            }
        }
        lock.unlock();
        return instance;
    }
}
