/*
 * MessageReader.java
 * A reader for deriving text strings which are displayed to a user.
 */
package by.training.task05.reader;


import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import java.util.concurrent.locks.ReentrantLock;


/**
 * A reader for deriving text strings which are displayed to a user.
 * @author Yauhen Sazonau
 * @version 07/18/19
 * @since 1.0
 */
public final class MessageReader {
    /**
     * The Belarussian language code. The identifier holds the value of "by".
     */
    public static final String BELARUSSIAN_LANGUAGE_CODE = "by";
    /**
     * The locale which represents Belarussian language. Can't be a
     * <code>null</code> value.
     */
    public static final Locale BELARUSSIAN_LANGUAGE_LOCALE
            = new Locale(BELARUSSIAN_LANGUAGE_CODE);
    /**
     * The name of a family of resource bundles that contain messages. The
     * identifier holds the value of by.training.task05.bundle.PageLabels".
     */
    private static final String BUNDLE_FAMILY_NAME
            = "by.training.task05.bundle.PageLabels";
    /**
     * The locale which represents English language. Can't be a
     * <code>null</code> value.
     */
    private static final Locale ENGLISH_LANGUAGE_LOCALE = Locale.ENGLISH;
    /**
     * The Russian language code. The identifier holds the value "ru".
     */
    private static final String RUSSIAN_LANGUAGE_CODE = "ru";
    /**
     * The locale which represents Russian language. Can't be a
     * <code>null</code> value.
     */
    private static final Locale RUSSIAN_LANGUAGE_LOCALE
            = new Locale(RUSSIAN_LANGUAGE_CODE);
    /**
     * The single instance of this class. <code>null</code> value is
     * permitted and indicates that the single instance hasn't yet been
     * created.
     */
    private static MessageReader instance;
    /**
     * A lock. Can't be a <code>null</code> value.
     */
    private static ReentrantLock lock = new ReentrantLock();
    /**
     * An instance of the <code>ResourceBundle</code> class that will help in
     * reading Belarussian text strings. Can't be a <code>null</code> value.
     */
    private ResourceBundle belarussianBundle;
    /**
     * An instance of the <code>ResourceBundle</code> class that will help in
     * reading English text strings. Can't be a <code>null</code> value.
     */
    private ResourceBundle englishBundle;
    /**
     * An instance of the <code>ResourceBundle</code> class that will help in
     * reading Russian text strings. Can't be a <code>null</code> value.
     */
    private ResourceBundle russianBundle;
    /**
     * Creates an instance of the message reader. We use the
     * <code>private</code> key word to prevent creating an instance of this
     * class outside of it.
     */
    private MessageReader() {
        /* The default initialization is sufficient. */
    }
    /**
     * Retrieves the value of a property by the key for the locale.
     * @param key a <code>String</code> value which is the key of a property
     * @param locale a <code>Locale</code> value according to which the
     *               value of a property is retrieved. Only locales that
     *               represent English, Russian or Belarussian language are
     *               accepted.
     * @return the value of a property. Can't be a <code>null</code> value.
     * @throws ReaderException if no resource has been found by the
     *                         <code>key</code>; if one of the method arguments
     *                         is a <code>null</code> value; if the
     *                         <code>locale</code> argument doesn't represent
     *                         English, Russian or Belarussian language
     */
    public String getProperty(final String key, final Locale locale) {
        boolean             belarussianLocaleVerifyingCondition;
        boolean             englishLocaleVerifyingCondition;
        boolean             russianLocaleVerifyingCondition;
        ResourceBundle      usedBundle;
        String result;
        /* The main part of the method. */
        if (key == null) {
            throw new ReaderException("The 'key' argument is 'null'.");
        }
        if (locale == null) {
            throw new ReaderException("The 'locale' argument is "
                    + "'null'.");
        }
        belarussianLocaleVerifyingCondition
                = BELARUSSIAN_LANGUAGE_LOCALE.equals(locale);
        englishLocaleVerifyingCondition
                = ENGLISH_LANGUAGE_LOCALE.equals(locale);
        russianLocaleVerifyingCondition
                = RUSSIAN_LANGUAGE_LOCALE.equals(locale);
        if ((!englishLocaleVerifyingCondition)
                && (!russianLocaleVerifyingCondition)
                && (!belarussianLocaleVerifyingCondition)) {
            throw new ReaderException("The value of the 'locale' "
                    + "argument must represent English, Russian or "
                    + "Belarussian language!");
        }
        if (englishLocaleVerifyingCondition) {
            usedBundle = englishBundle;
        } else if (russianLocaleVerifyingCondition) {
            usedBundle = russianBundle;
        } else {
            usedBundle = belarussianBundle;
        }
        try {
            result = usedBundle.getString(key);
        } catch (MissingResourceException e) {
            throw new ReaderException("There is no value which "
                    + "corresponds to the specified key: " + key + ".", e);
        }
        return result;
    }
    /**
     * Creates an instance of the message reader if it is needed or returns an
     * existing. You must invoke this method because of no having an ability to
     * invoke the constructor.
     * @return a newly created instance of the <code>MessageReader</code> class
     *         or an existing if it has been created before. Can't be a
     *         <code>null</code> value.
     * @throws ReaderException if an underlying source of data is missed
     */
    public static MessageReader getInstance() {
        lock.lock();
        if (instance == null) {
            instance = new MessageReader();
            try {
                instance.belarussianBundle = ResourceBundle.getBundle(
                        BUNDLE_FAMILY_NAME, BELARUSSIAN_LANGUAGE_LOCALE);
                instance.englishBundle = ResourceBundle.getBundle(
                        BUNDLE_FAMILY_NAME, ENGLISH_LANGUAGE_LOCALE);
                instance.russianBundle = ResourceBundle.getBundle(
                        BUNDLE_FAMILY_NAME, RUSSIAN_LANGUAGE_LOCALE);
            } catch (MissingResourceException e) {
                throw new ReaderException("An underlying source of "
                        + "data is missed: " + BUNDLE_FAMILY_NAME + ".", e);
            }
        }
        lock.unlock();
        return instance;
    }
}
