/**
 * Provides classes each of which encapsulates the information about some
 * entity. These entities are: a role, a user, a physician speciality,
 * a person, a physician, a ticket. The main purpose of these classes is
 * that they are used as data transfer objects.
 * @author Yauhen Sazonau
 * @since 1.0
 */
package by.training.task05.entity;
