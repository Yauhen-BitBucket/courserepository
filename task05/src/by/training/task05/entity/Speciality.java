/*
 * Speciality.java
 * A physician speciality. It can be one of the following: therapeutist,
 * surgeon, ophthalmologist, urologist, otolaryngologist.
 */
package by.training.task05.entity;


/**
 * A physician speciality. It can be one of the following: therapeutist,
 * surgeon, ophthalmologist, urologist, otolaryngologist.
 * @author Yauhen Sazonau
 * @version 1.0, 06/20/19
 * @since 1.0
 */
public enum Speciality {
    /**
     * Represents a therapeutist speciality.
     */
    THERAPEUTIST,
    /**
     * Represents a surgeon speciality.
     */
    SURGEON,
    /**
     * Represents an ophthalmologist speciality.
     */
    OPHTHALMOLOGIST,
    /**
     * Represents an urologist speciality.
     */
    UROLOGIST,
    /**
     * Represents an otolaryngologist speciality.
     */
    OTOLARYNGOLOGIST
}
