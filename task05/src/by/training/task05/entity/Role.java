/*
 * Role.java
 * A user type of this program complex. Depending on this type, a user is
 * given a particular interface for working with this program complex. A user
 * type can be one of the following: patient, physician, head physician.
 */
package by.training.task05.entity;


/**
 * A user type of this program complex. Depending on this type, a user is
 * given a particular interface for working with this program complex. A user
 * type can be one of the following: patient, physician, head physician.
 * @author Yauhen Sazonau
 * @version 1.0, 06/20/19
 * @since 1.0
 */
public enum Role {
    /**
     * Represents a head physician role.
     */
    HEAD_PHYSICIAN,
    /**
     * Represents a patient role.
     */
    PATIENT,
    /**
     * Represents a physician role.
     */
    PHYSICIAN
}
