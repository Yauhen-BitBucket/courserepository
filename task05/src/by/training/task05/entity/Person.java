/*
 * Person.java
 * A person (for example, a patient or a physician).
 */
package by.training.task05.entity;


import java.util.Objects;


/**
 * A person (for example, a patient or a physician).
 * @author Yauhen Sazonau
 * @version 1.0, 06/20/19
 * @since 1.0
 */
public class Person {
    /**
     * A first name of a person.
     */
    private String firstName;
    /**
     * A last name of a person.
     */
    private String lastName;
    /**
     * A passport number of a person.
     */
    private String passportNumber;
    /**
     * Constructs an instance of this class.
     */
    public Person() {
        /* The default initialization is sufficient. */
    }
    /**
     * Retrieves the first name of a person.
     * @return the first name of a person
     */
    public String getFirstName() {
        return firstName;
    }
    /**
     * Sets the first name of a person.
     * @param name a <code>String</code> value which is a first name to be set
     */
    public void setFirstName(final String name) {
        this.firstName = name;
    }
    /**
     * Retrieves the last name of a person.
     * @return the last name of a person
     */
    public String getLastName() {
        return lastName;
    }
    /**
     * Sets the last name of a person.
     * @param surname a <code>String</code> value which is a last name to be
     *                set
     */
    public void setLastName(final String surname) {
        this.lastName = surname;
    }
    /**
     * Retrieves the passport number of a person.
     * @return the passport number of a person
     */
    public String getPassportNumber() {
        return passportNumber;
    }
    /**
     * Sets the passport number of a person.
     * @param personPassportNumber a <code>String</code> value which is a
     *                             passport number to be set
     */
    public void setPassportNumber(final String personPassportNumber) {
        this.passportNumber = personPassportNumber;
    }
    /**
     * Indicates whether some other object is "equal to" this one.
     * @param object the <code>Object</code> value that is an object with
     *               which to compare. <code>null</code> value is possible.
     * @return <code>true</code> if this object is the same as the object
     *         supplied through the <code>object</code> argument;
     *         <code>false</code> otherwise
     */
    @Override
    public boolean equals(final Object object) {
        Person person;
        boolean result = true;
        /* The main part of the method. */
        if (this == object) {
            return true;
        } else if (object == null) {
            return false;
        }
        /* The supplied object should belong to the person hierarchy. */
        result = object instanceof Person;
        if (result) {
            person = (Person) object;
            result = (Objects.equals(firstName, person.firstName))
                     && (Objects.equals(lastName, person.lastName))
                     && (Objects.equals(passportNumber,
                                        person.passportNumber));
        }
        return result;
    }
    /**
     * Retrieves a hash code value for this object.
     * @return a hash code value for this object
     */
    @Override
    public int hashCode() {
        return (Objects.hash(firstName, lastName, passportNumber));
    }
    /**
     * Returns a string representation of this object.
     * @return a string representation of the object. <code>null</code> value
     *         is prohibited.
     */
    @Override
    public String toString() {
        return ("passport number: " + passportNumber + ", first name: "
                + firstName + ", last name: " + lastName);
    }
}
