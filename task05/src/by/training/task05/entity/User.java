/*
 * User.java
 * A user of this program complex. Depending on a role, a user is given
 * a particular interface for working with this program complex.
 */
package by.training.task05.entity;


import java.util.Objects;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * A user of this program complex. Depending on a role, a user is given
 * a particular interface for working with this program complex.
 * @author Yauhen Sazonau
 * @version 1.0, 06/20/19
 * @since 1.0
 */
public class User implements HttpSessionBindingListener {
    /**
     * A logger for reporting errors. <code>null</code> value is prohibited.
     */
    private static final Logger LOGGER = LogManager.getLogger(User.class);
    /**
     * A blocked state of a user. <code>false</code> value means that a user
     * isn't blocked.
     */
    private boolean blockedState;
    /**
     * An identification number of a user.
     */
    private long userID;
    /**
     * A role of a user.
     */
    private Role role;
    /**
     * A login of a user.
     */
    private String login;
    /**
     * Constructs an instance of this class.
     * @param state a <code>boolean</code> value which is a blocked state
     * @param identifier a <code>long</code> value which is an identification
     *                   number to bes set
     * @param userRole a <code>Role</code> value which is a role to be set
     * @param userLogin a <code>String</code> value which is a login to be set
     * @see by.training.task05.entity.Role
     */
    public User(final boolean state, final long identifier,
            final Role userRole, final String userLogin) {
        this.blockedState = state;
        this.userID = identifier;
        this.role = userRole;
        this.login = userLogin;
    }
    /**
     * Retrieves a blocked state of a user.
     * @return a blocked state of a user
     */
    public boolean getBlockedState() {
        return blockedState;
    }
    /**
     * Sets a blocked state of a user.
     * @param state a <code>boolean</code> value which is a blocked state
     */
    public void setBlockedState(final boolean state) {
        this.blockedState = state;
    }
    /**
     * Retrieves an identification number of a user.
     * @return an identification number of a user
     */
    public long getUserID() {
        return userID;
    }
    /**
     * Sets an identification number of a user.
     * @param identifier a <code>long</code> value which is an identification
     *                   number to bes set
     */
    public void setUserID(final long identifier) {
        this.userID = identifier;
    }
    /**
     * Retrieves a role of a user.
     * @return a role of a user
     * @see by.training.task05.entity.Role
     */
    public Role getRole() {
        return role;
    }
    /**
     * Sets a role of a user.
     * @param userRole a <code>Role</code> value which is a role to be set
     * @see by.training.task05.entity.Role
     */
    public void setRole(final Role userRole) {
        this.role = userRole;
    }
    /**
     * Retrieves a login of a user.
     * @return a login of a user
     */
    public String getLogin() {
        return login;
    }
    /**
     * Sets a login of a user.
     * @param userLogin a <code>String</code> value which is a login to be set
     */
    public void setLogin(final String userLogin) {
        this.login = userLogin;
    }
    /**
     * Indicates whether some other object is "equal to" this one.
     * @param object the <code>Object</code> value that is an object with
     *               which to compare. <code>null</code> value is possible.
     * @return <code>true</code> if this object is the same as the object
     *         supplied through the <code>object</code> argument;
     *         <code>false</code> otherwise
     */
    @Override
    public boolean equals(final Object object) {
        User user;
        /* The main part of the method. */
        if (this == object) {
            return true;
        } else if (object == null) {
            return false;
        }
        /* The supplied object should be of the same class. */
        if (getClass() != object.getClass()) {
            return false;
        }
        user = (User) object;
        return ((Objects.equals(blockedState, user.blockedState))
                && (Objects.equals(userID, user.userID))
                && (Objects.equals(role, user.role))
                && (Objects.equals(login, user.login)));
    }
    /**
     * Retrieves a hash code value for this object.
     * @return a hash code value for this object
     */
    @Override
    public int hashCode() {
        return (Objects.hash(blockedState, userID, role, login));
    }
    /**
     * Returns a string representation of this object.
     * @return a string representation of the object. <code>null</code> value
     *         is prohibited.
     */
    @Override
    public String toString() {
        return ("blocke state: " + blockedState + ", user ID: " + userID
                + ", login: " + login + ", role: " + role);
    }
    /**
     * Notifies this user that it is being bound to a session and identifies
     * the session.
     * @param event a <code>HttpSessionBindingEvent</code> value which is an
     *              event that identifies the session
     */
    @Override
    public void valueBound(final HttpSessionBindingEvent event) {
        LOGGER.info("A user with a login: '" + login + "' has entered the "
                + "system.");
    }
    /**
     * Notifies this user that it is being unbound from a session and
     * identifies the session.
     * @param event a <code>HttpSessionBindingEvent</code> value which is an
     *              event that identifies the session
     */
    @Override
    public void valueUnbound(final HttpSessionBindingEvent event) {
        LOGGER.info("A user with a login: '" + login + "' has left the "
                + "system.");
    }
}
