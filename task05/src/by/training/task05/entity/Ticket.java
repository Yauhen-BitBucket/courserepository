/*
 * Ticket.java
 * A ticket to be able to visit a physician.
 */
package by.training.task05.entity;


import java.sql.Date;
import java.sql.Time;

import java.util.Objects;


/**
 * A ticket to be able to visit a physician.
 * @author Yauhen Sazonau
 * @version 1.0, 06/20/19
 * @since 1.0
 */
public class Ticket {
    /**
     * An identification number of a ticket.
     */
    private long ticketID;
    /**
     * A physician who conducts surgery.
     */
    private Physician physician;
    /**
     * A patient who owns a ticket.
     */
    private Person patient;
    /**
     * A surgery date.
     */
    private Date date;
    /**
     * A starting time of surgery.
     */
    private Time startingTime;
    /**
     * An ending time of surgery.
     */
    private Time endingTime;
    /**
     * A diagnosis of a patient.
     */
    private String diagnosis;
    /**
     * A treatment to cure a patient.
     */
    private String treatment;
    /**
     * Constructs an instance of this class.
     * @param identifier a <code>long</code> value which is an identification
     *                   number to be set
     * @param doctor a <code>Physician</code> value which is a physician to be
     *               set
     * @param person a <code>Person</code> value which is a patient
     * @param surgeryDate a <code>Date</code> value which is a surgery date to
     *                    be set
     * @param startinPoint a <code>Time</code> value which is a starting time
     *                     of surgery to be set
     * @param endingPoint a <code>Time</code> value which is an ending time
     *                    of surgery to be set
     */
    public Ticket(final long identifier, final Physician doctor,
                  final Person person, final Date surgeryDate,
                  final Time startinPoint,
                  final Time endingPoint) {
        this.ticketID = identifier;
        this.physician = doctor;
        this.patient = person;
        this.date = surgeryDate;
        this.startingTime = startinPoint;
        this.endingTime = endingPoint;
    }
    /**
     * Retrieves the identification number of a ticket.
     * @return the identification number of a ticket
     */
    public long getTicketID() {
        return ticketID;
    }
    /**
     * Sets the identification number of a ticket.
     * @param identifier a <code>long</code> value which is an identification
     *                   number to be set
     */
    public void setTicketID(final long identifier) {
        this.ticketID = identifier;
    }
    /**
     * Retrieves the physician who conducts surgery.
     * @return the physician who conducts surgery
     */
    public Physician getPhysician() {
        return physician;
    }
    /**
     * Sets the physician who conducts surgery.
     * @param doctor a <code>Physician</code> value which is a physician to be
     *               set
     */
    public void setPhysician(final Physician doctor) {
        this.physician = doctor;
    }
    /**
     * Retrieves the patient who owns a ticket.
     * @return the patient who owns a ticket
     */
    public Person getPatient() {
        return patient;
    }
    /**
     * Sets the patient who owns a ticket.
     * @param person a <code>Person</code> value which is a patient to be set
     */
    public void setPatient(final Person person) {
        this.patient = person;
    }
    /**
     * Retrieves the surgery date.
     * @return the surgery date
     */
    public Date getDate() {
        return date;
    }
    /**
     * Sets the surgery date.
     * @param surgeryDate a <code>Date</code> value which is a surgery date to
     *                    be set
     */
    public void setDate(final Date surgeryDate) {
        this.date = surgeryDate;
    }
    /**
     * Retrieves the starting time of surgery.
     * @return the starting time of surgery
     */
    public Time getStartingTime() {
        return startingTime;
    }
    /**
     * Sets the starting time of surgery.
     * @param startinPoint a <code>Time</code> value which is a starting time
     *                     of surgery to be set
     */
    public void setStartingTime(final Time startinPoint) {
        this.startingTime = startinPoint;
    }
    /**
     * Retrieves the ending time of surgery.
     * @return the ending time of surgery
     */
    public Time getEndingTime() {
        return endingTime;
    }
    /**
     * Sets the ending time of surgery.
     * @param endingPoint a <code>Time</code> value which is an ending time
     *                    of surgery to be set
     */
    public void setEndingTime(final Time endingPoint) {
        this.endingTime = endingPoint;
    }
    /**
     * Retrieves the diagnosis of a patient.
     * @return the diagnosis of a patient
     */
    public String getDiagnosis() {
        return diagnosis;
    }
    /**
     * Sets the diagnosis of a patient.
     * @param patientDiagnosis a <code>String</code> value which is a diagnosis
     *                         of a patient to be set
     */
    public void setDiagnosis(final String patientDiagnosis) {
        this.diagnosis = patientDiagnosis;
    }
    /**
     * Retrieves the treatment to cure a patient.
     * @return the treatment to cure a patient
     */
    public String getTreatment() {
        return treatment;
    }
    /**
     * Sets the treatment to cure a patient.
     * @param patientTreatment a <code>String</code> value which is a treatment
     *                         to be set
     */
    public void setTreatment(final String patientTreatment) {
        this.treatment = patientTreatment;
    }
    /**
     * Indicates whether some other object is "equal to" this one.
     * @param object the <code>Object</code> value that is an object with
     *               which to compare. <code>null</code> value is possible.
     * @return <code>true</code> if this object is the same as the object
     *         supplied through the <code>object</code> argument;
     *         <code>false</code> otherwise
     */
    @Override
    public boolean equals(final Object object) {
        Ticket ticket;
        /* The main part of the method. */
        if (this == object) {
            return true;
        } else if (object == null) {
            return false;
        }
        /* The supplied object should be of the same class. */
        if (getClass() != object.getClass()) {
            return false;
        }
        ticket = (Ticket) object;
        return ((Objects.equals(ticketID, ticket.ticketID))
                && (Objects.equals(physician, ticket.physician))
                && (Objects.equals(patient, ticket.patient))
                && (Objects.equals(date, ticket.date))
                && (Objects.equals(startingTime, ticket.startingTime))
                && (Objects.equals(endingTime, ticket.endingTime))
                && (Objects.equals(diagnosis, ticket.diagnosis))
                && (Objects.equals(treatment, ticket.treatment)));
    }
    /**
     * Retrieves a hash code value for this object.
     * @return a hash code value for this object
     */
    @Override
    public int hashCode() {
        return (Objects.hash(ticketID, physician, patient, date, startingTime,
                             endingTime, diagnosis, treatment));
    }
    /**
     * Returns a string representation of this object.
     * @return a string representation of the object. <code>null</code> value
     *         is prohibited.
     */
    @Override
    public String toString() {
        return ("ticket ID: " + ticketID + ", physician: " + physician
                + ", patient: " + patient + ", date: " + date
                + ", starting time: " + startingTime + ", ending time: "
                + endingTime + ", diagnosis: " + diagnosis + ", treatment: "
                + treatment);
    }
}
