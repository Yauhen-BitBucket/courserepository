/*
 * Physician.java
 * A physician.
 */
package by.training.task05.entity;


import java.sql.Time;

import java.util.Objects;


/**
 * A physician.
 * @author Yauhen Sazonau
 * @version 1.0, 06/20/19
 * @since 1.0
 */
public class Physician extends Person {
    /**
     * A number of a room where a physician is available to see patients.
     */
    private long roomNumber;
    /**
     * A speciality of a physician.
     */
    private Speciality speciality;
    /**
     * A surgery starting time when a day of month is even.
     */
    private Time evenStartingTime;
    /**
     * A surgery ending time when a day of month is even.
     */
    private Time evenEndingTime;
    /**
     * A surgery starting time when a day of month is odd.
     */
    private Time oddStartingTime;
    /**
     * A surgery ending time when a day of month is odd.
     */
    private Time oddEndingTime;
    /**
     * Constructs an instance of this class.
     */
    public Physician() {
        /* The default initialization is sufficient. */
    }
    /**
     * Retrieves the number of a room where a physician is available to see
     * patients.
     * @return the number of a room where a physician is available to see
     *         patients
     */
    public long getRoomNumber() {
        return roomNumber;
    }
    /**
     * Sets the number of a room where a physician is available to see
     * patients.
     * @param room a <code>long</code> value which is a number of a room to be
     *             set
     */
    public void setRoomNumber(final long room) {
        this.roomNumber = room;
    }
    /**
     * Retrieves the speciality of a physician.
     * @return the speciality of a physician
     */
    public Speciality getSpeciality() {
        return speciality;
    }
    /**
     * Sets the speciality of a physician.
     * @param physicianSpeciality a <code>Speciality</code> value which is a
     *                            speciality of a physician to be set
     */
    public void setSpeciality(final Speciality physicianSpeciality) {
        this.speciality = physicianSpeciality;
    }
    /**
     * Retrieves the surgery starting time when a day of month is even.
     * @return the surgery starting time when a day of month is even
     */
    public Time getEvenStartingTime() {
        return evenStartingTime;
    }
    /**
     * Sets the surgery starting time when a day of month is even.
     * @param startingTime a <code>Time</code> value which is a surgery
     *                     starting time to be set
     */
    public void setEvenStartingTime(final Time startingTime) {
        this.evenStartingTime = startingTime;
    }
    /**
     * Retrieves the surgery ending time when a day of month is even.
     * @return the surgery ending time when a day of month is even
     */
    public Time getEvenEndingTime() {
        return evenEndingTime;
    }
    /**
     * Sets the surgery ending time when a day of month is even.
     * @param endingTime a <code>Time</code> value which is a surgery
     *                   ending time to be set
     */
    public void setEvenEndingTime(final Time endingTime) {
        this.evenEndingTime = endingTime;
    }
    /**
     * Retrieves the surgery starting time when a day of month is odd.
     * @return the surgery starting time when a day of month is odd
     */
    public Time getOddStartingTime() {
        return oddStartingTime;
    }
    /**
     * Sets the surgery starting time when a day of month is odd.
     * @param startingTime a <code>Time</code> value which is a surgery
     *                     starting time to be set
     */
    public void setOddStartingTime(final Time startingTime) {
        this.oddStartingTime = startingTime;
    }
    /**
     * Retrieves the surgery ending time when a day of month is odd.
     * @return the surgery ending time when a day of month is odd
     */
    public Time getOddEndingTime() {
        return oddEndingTime;
    }
    /**
     * Sets the surgery ending time when a day of month is odd.
     * @param endingTime a <code>Time</code> value which is a surgery ending
     *                   time to be set
     */
    public void setOddEndingTime(final Time endingTime) {
        this.oddEndingTime = endingTime;
    }
    /**
     * Indicates whether some other object is "equal to" this one.
     * @param object the <code>Object</code> value that is an object with
     *               which to compare. <code>null</code> value is possible.
     * @return <code>true</code> if this object is the same as the object
     *         supplied through the <code>object</code> argument;
     *         <code>false</code> otherwise
     */
    @Override
    public boolean equals(final Object object) {
        Physician physician;
        boolean result = true;
        /* The main part of the method. */
        if (this == object) {
            return true;
        } else if (!super.equals(object)) {
            return false;
        }
        /* The class of the supplied object should be the same or extend this
         * class.
         */
        result = object instanceof Physician;
        if (result) {
            physician = (Physician) object;
            result = (Objects.equals(roomNumber, physician.roomNumber))
                     && (Objects.equals(speciality, physician.speciality))
                     && (Objects.equals(evenStartingTime,
                                        physician.evenStartingTime))
                     && (Objects.equals(evenEndingTime,
                                        physician.evenEndingTime))
                     && (Objects.equals(oddStartingTime,
                                        physician.oddStartingTime))
                     && (Objects.equals(oddEndingTime,
                                        physician.oddEndingTime));
        }
        return result;
    }
    /**
     * Retrieves a hash code value for this object.
     * @return a hash code value for this object
     */
    @Override
    public int hashCode() {
        return (Objects.hash(getFirstName(), getLastName(),
                             getPassportNumber(), roomNumber, speciality,
                             evenStartingTime, evenEndingTime,
                             oddStartingTime, oddEndingTime));
    }
    /**
     * Returns a string representation of this object.
     * @return a string representation of the object. <code>null</code> value
     *         is prohibited.
     */
    @Override
    public String toString() {
        return ("personal data: " + super.toString() + ", room number: "
                + roomNumber + ", speciality: " + speciality
                + ", even staring time: " + evenStartingTime
                + ", even ending time: " + evenEndingTime
                + ", odd starting time: " + oddStartingTime
                + ", odd ending time: " + oddEndingTime);
    }
}
