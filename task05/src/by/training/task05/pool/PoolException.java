/*
 * PoolException.java
 * An exception that provides the information about an interaction error with a
 * connection pool. The exception doesn't add any additional information to
 * the basic information that you can derive from a typical exception.
 */
package by.training.task05.pool;


/**
 * An exception that provides the information about an interaction error with a
 * connection pool. The exception doesn't add any additional information to
 * the basic information that you can derive from a typical exception.
 * @author Yauhen Sazonau
 * @version 1.0, 06/20/19
 * @since 1.0
 */
@SuppressWarnings("serial")
public class PoolException extends Exception {
    /**
     * Constructs a new <code>PoolException</code> exception with
     * <code>null</code> as its detail message. The cause is not initialized.
     */
    public PoolException() {
        /* The default initialization is sufficient. */
    }
    /**
     * Constructs a new <code>PoolException</code> exception with the specified
     * detail message. The cause is not initialized.
     * @param message a <code>String</code> value which is the information
     *                about an exception. <code>null</code> value is allowed
     *                and simply means that the information about the
     *                exception is absent.
     */
    public PoolException(final String message) {
        super(message);
    }
    /**
     * Constructs a new <code>PoolException</code> exception with the specified
     * cause.
     * @param exception an <code>Exception</code> value which is the cause of
     *                  an exception. <code>null</code> value is allowed and
     *                  simply means that the cause is nonexistent or unknown.
     */
    public PoolException(final Exception exception) {
        super(exception);
    }
    /**
     * Constructs a new <code>PoolException</code> exception with the specified
     * detail message and cause. Note that the detail message associated with
     * the cause is not automatically incorporated in this exception's detail
     * message.
     * @param message a <code>String</code> value which is the information
     *                about an exception. <code>null</code> value is allowed
     *                and simply means that the information about the
     *                exception is absent.
     * @param exception an <code>Exception</code> value which is the cause of
     *                  an exception. <code>null</code> value is allowed and
     *                  simply means that the cause is nonexistent or unknown.
     */
    public PoolException(final String message,
            final Exception exception) {
        super(message, exception);
    }
}
