/*
 * ConnectionPool.java
 * A connection pool for managing connection objects that are used to interact
 * with a database.
 */
package by.training.task05.pool;


import static by.training.task05.reader.ApplicationSettingsReader
        .DATABASE_DRIVER_CLASS_NAME_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .DATABASE_PASSWORD_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .DATABASE_URL_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .DATABASE_USER_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .INITIAL_CONNECTON_NUMBER_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .MAXIMUM_CONNECTION_NUMBER_KEY;
import static by.training.task05.reader.ApplicationSettingsReader
        .WAITING_CONDITION_KEY;

import by.training.task05.reader.ApplicationSettingsReader;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * A connection pool for managing connection objects that are used to interact
 * with a database.
 * @author Yauhen Sazonau
 * @version 1.0, 06/20/19
 * @since 1.0
 */
public final class ConnectionPool implements Runnable {
    /**
     * The single instance of this class. <code>null</code> value is
     * prohibited.
     */
    private static ConnectionPool instance;
    /**
     * A logger for reporting errors. <code>null</code> value is prohibited.
     */
    private static final Logger LOGGER
            = LogManager.getLogger("by.training.task05");
    /**
     * A lock. Can't be a <code>null</code> value.
     */
    private static ReentrantLock lock = new ReentrantLock();
    /**
     * A condition. Can't be a <code>null</code> value.
     */
    private Condition condition;
    /**
     * The class name of a driver used to connect to a database. Can't be a
     * <code>null</code> value.
     */
    private String driverClassName;
    /**
     * The location of a used database. Can't be a <code>null</code> value.
     */
    private String url;
    /**
     * The user name on behalf of which a database is used. Can't be a
     * <code>null</code> value.
     */
    private String username;
    /**
     * The user password on behalf of which a database is used. Can't be a
     * <code>null</code> value.
     */
    private String password;
    /**
     * The maximum number of connections that can be instantiated in the
     * connection pool. It must be positive.
     */
    private int maxNumber;
    /**
     * The condition for instantiating a new connection. When all available
     * connections have been handed out, the number of all instantiated
     * connections is less than the maximum number of connections, there is a
     * thread that needs a connection and there is no other thread that has
     * started this procedure and it hasn't been finished yet, then it is
     * possible to create a new one connection. If <code>true</code>, there is
     * a thread which started a procedure.
     */
    private boolean connectionPending = false;
    /**
     * The condition which determines what to do in a situation when all
     * available connections have been handed out and the maximum number of
     * connections have been instantiated. The choice is to throw an
     * exception or wait for another thread until it releases a connection.
     * <code>true</code> means to wait.
     */
    private boolean waitingCondition;
    /**
     * All available connections which can be handed out for use. Can't be a
     * <code>null</code> value.
     */
    private Collection<Connection> availableConnections;
    /**
     * All connections which have been handed out for use. Can't be a
     * <code>null</code> value.
     */
    private Collection<Connection> issuedConnections;
    /**
     * Creates a connection pool. You have no ability to create an instance of
     * this class using this constructor. You should use the
     * <code>getInstance</code> method.
     * @param driverName a <code>String</code> value which is the class name of
     *                   a driver used to connect to a database
     * @param databaseURL a <code>String</code> value which is the location of
     *                    a database
     * @param login a <code>String</code> value which is the user name on
     *              behalf of which a database is used
     * @param usedPassword a <code>String</code> value which is the user
     *                     password
     * @param initialNumber an <code>int</code> value which is the initial
     *                      number of connections which will be within the
     *                      newly created connection pool
     * @param maximumNumber an <code>int</code> value which is the maximum
     *                      number of connections that can be instantiated in
     *                      the connection pool
     * @param usedCondition a <code>boolean</code> value which is the condition
     *                      which determines what to do in a situation when all
     *                      available connections have been handed out and the
     *                      maximum number of connections have been
     *                      instantiated. The choice is to throw an exception
     *                      or wait for another thread until it releases a
     *                      connection. <code>true</code> means to wait.
     * @throws PoolException if there are problems with creation of a connection
     *                       pool
     */
    private ConnectionPool(final String driverName, final String databaseURL,
            final String login, final String usedPassword,
            final int initialNumber, final int maximumNumber,
            final boolean usedCondition) throws PoolException {
        this.driverClassName = driverName;
        this.url = databaseURL;
        this.username = login;
        this.password = usedPassword;
        this.maxNumber = maximumNumber;
        this.waitingCondition = usedCondition;
        this.condition = lock.newCondition();
        /*
         * Instantiating lists for our connections. Note that no one
         * connection was handed out, so the second list is empty.
         */
        availableConnections = new LinkedList<>();
        issuedConnections = new LinkedList<>();
        /* Populating the availableConnections collection. */
        for (int i = 0; i < initialNumber; ++i) {
            availableConnections.add(createConnection());
        }
    }
    /**
     * Retrieves a connection to a database from the connection pool.
     * @return a connection to a database from the connection pool
     * @throws PoolException if there are problems with retrieving a
     *                       connection to a database
     */
    public Connection getConnection() throws PoolException {
        Connection connection;
        Iterator<Connection> iterator;
        int lockCount;
        int totalConnections;
        /* The main part of the method. */
        lock.lock();
        if (!(availableConnections.isEmpty())) {
            iterator = availableConnections.iterator();
            connection = iterator.next();
            iterator.remove();
            try {
                if ((connection.isClosed())) {
                    return getConnection();
                } else {
                    issuedConnections.add(connection);
                    lockCount = lock.getHoldCount();
                    for (int i = 0; i < lockCount; ++i) {
                        lock.unlock();
                    }
                    return connection;
                }
            } catch (SQLException e) {
                throw new PoolException("Can't retrieve a connection.", e);
            }
        } else {
            totalConnections = countTotalConnections();
            if ((totalConnections < maxNumber) && (!connectionPending)) {
                makeAdditionalConnection();
            } else if ((totalConnections == maxNumber)
                       && (!waitingCondition)) {
                throw new PoolException("Connection limit was reached and "
                        + "there is no one available connection!");
            }
            condition.awaitUninterruptibly();
            return getConnection();
        }
    }
    /**
     * Returns a connection to the connection pool. Invocation of this
     * method is vital after finishing up your work with a database!
     * @param connection a <code>Connection</code> value which is a connection
     *                   to be returned
     * @throws PoolException if the argument is a <code>null</code> value
     */
    public void returnConnection(final Connection connection)
            throws PoolException {
        if (connection == null) {
            throw new PoolException("The 'connection' argument is a 'null' "
                    + "value.");
        }
        /* The main part of the method. */
        lock.lock();
        issuedConnections.remove(connection);
        availableConnections.add(connection);
        condition.signalAll();
        lock.unlock();
    }
    /**
     * Instantiates a new connection to a database. This method mustn't be used
     * directly from the connection pool, as the connection pool can become in
     * an inconsistent state. It is used indirectly.
     */
    public void run() {
        Connection connection;
        /* The main part of the method. */
        lock.lock();
        try {
            connection = createConnection();
            availableConnections.add(connection);
        } catch (PoolException e) {
            /*
             * If creating a connection by a separate thread wasn't
             * successful, report it.
             */
            LOGGER.error("A problem occured during creating a "
                    + "connection by a separate thread.", e);
        }
        connectionPending = false;
        condition.signalAll();
        lock.unlock();
    }
    /**
     * Closes all instantiated connections in the connection pool. Note that
     * closing all instantiated connections also may close those connections
     * which are currently in use. It should be used with care!
     * @throws PoolException if there are problems with closing connections
     */
    public void closeAllConnections() throws PoolException {
        lock.lock();
        closeConnections(availableConnections);
        availableConnections.clear();
        closeConnections(issuedConnections);
        issuedConnections.clear();
        lock.unlock();
    }
    /**
     * Retrieves current state information about the connection pool.
     * @return current state information about the connection pool. Can't be a
     *         <code>null</code> value.
     */
    public String toString() {
        return ("ConnectionPool: available = "
                + availableConnections.size()
                + ", issued = " + issuedConnections.size()
                + ", maximum = " + maxNumber);
    }
    /**
     * Creates an instance of the connection pool if it is needed or returns
     * an existing. You must invoke this method because of no having an ability
     * to invoke the constructor.
     * @return a newly created instance of the <code>ConnectionPool</code>
     *         class or an existing if it has been created before. Can't be a
     *         <code>null</code> value.
     * @throws PoolException if there problems with creating an instance of the
     *                       connection pool
     */
    public static ConnectionPool getInstance() throws PoolException {
        ApplicationSettingsReader reader
                = ApplicationSettingsReader.getInstance();
        boolean usedWaitingCondition;
        int initialCount;
        int maximumCount;
        /* The main part of the method. */
        lock.lock();
        if (instance == null) {
            initialCount = Integer.parseInt(
                    reader.getProperty(INITIAL_CONNECTON_NUMBER_KEY));
            maximumCount = Integer.parseInt(
                    reader.getProperty(MAXIMUM_CONNECTION_NUMBER_KEY));
            usedWaitingCondition = Boolean.parseBoolean(
                    reader.getProperty(WAITING_CONDITION_KEY));
            instance = new ConnectionPool(
                    reader.getProperty(DATABASE_DRIVER_CLASS_NAME_KEY),
                    reader.getProperty(DATABASE_URL_KEY),
                    reader.getProperty(DATABASE_USER_KEY),
                    reader.getProperty(DATABASE_PASSWORD_KEY),
                    initialCount,
                    maximumCount,
                    usedWaitingCondition);
        }
        lock.unlock();
        return instance;
    }
    /**
     * Creates a connection object.
     * @return a connection object
     * @throws PoolException if there are problems with creation of a
     *                       connection object
     */
    private Connection createConnection() throws PoolException {
        Connection connection;
        /* Load and register a database driver. */
        try {
            Class.forName(driverClassName);
            /* Establish network connection to a particular database. */
            connection = DriverManager.getConnection(url, username, password);
        } catch (ClassNotFoundException e) {
            throw new PoolException("Can't find the class for a driver"
                    + driverClassName + ".", e);
        } catch (SQLException e) {
            throw new PoolException("Can't create a connection.", e);
        }
        if (connection == null) {
            throw new PoolException("The wrowng kind of driver is used: "
                    + driverClassName + ".");
        }
        return connection;
    }
    /**
     * Instantiates a new connection to a database using a separate thread.
     */
    private void makeAdditionalConnection() {
        Thread thread = new Thread(this);
        /* The main part of the method. */
        connectionPending = true;
        thread.start();
    }
    /**
     * Retrieves the number of instantiated connections in the connection pool.
     * @return the number of instantiated connections in the connection
     *         pool.
     */
    private int countTotalConnections() {
        return (availableConnections.size() + issuedConnections.size());
    }
    /**
     * Closes connections that there are in some collection.
     * @param connections a <code>Collection</code> value which is a collection
     *                    of connections to be closed
     * @throws PoolException if there are problems with closing connections
     */
    private void closeConnections(final Collection<Connection> connections)
            throws PoolException {
        Connection connection;
        Iterator<Connection> iterator;
        /* The main part of the method. */
        if (connections == null) {
            throw new PoolException("The 'connections' argument "
                    + "is a 'null' value.");
        }
        iterator = connections.iterator();
        while (iterator.hasNext()) {
            connection = iterator.next();
            try {
                connection.close();
            } catch (SQLException e) {
                throw new PoolException("There are problems with closing "
                        + "a connection.", e);
            }
        }
    }
}
