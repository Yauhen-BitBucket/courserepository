/**
 * Provides a connection pool. A connection pool is a quite important thing
 * when you use data from a database. All you need to interact with a database
 * is a connection object, you don't care about creating it, you simply ask for
 * a connection from a connection pool and after finishing up your work return
 * the connection back. All other intermediate work is fulfilled by the pool.
 * @author Yauhen Sazonau
 * @since 1.0
 */
package by.training.task05.pool;
