/**
 * Contains a TestNG class for verifying functionality of the CompositeClient
 * class.
 * @author Yauhen Sazonau
 * @since 1.0
 */
package test.task02.sorting;
