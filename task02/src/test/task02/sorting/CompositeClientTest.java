/*
 * CompositeClientTest.java
 * A TestNG class used to test functionality of the CompositeClient class.
 */
package test.task02.sorting;


import static by.training.task02.constant.ArrayIndexes.ARRAY_INDEX_0;
import static by.training.task02.constant.ArrayIndexes.ARRAY_INDEX_1;
import static by.training.task02.constant.ArrayIndexes.ARRAY_INDEX_2;

import static by.training.task02.constant.FilePaths.FILE_PATH_1;
import static by.training.task02.constant.FilePaths.FILE_PATH_2;
import static by.training.task02.constant.FilePaths.FILE_PATH_3;

import static by.training.task02.constant.StickingStrings.NEW_LINE_STRING;

import static org.testng.Assert.assertEquals;

import by.training.task02.client.CompositeClient;

import by.training.task02.component.Composite;

import by.training.task02.parser.TextParser;

import by.training.task02.reader.DataReader;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


/**
 * A TestNG class used to test functionality of the
 * <code>CompositeClient</code> class.
 * @author Yauhen Sazonau
 * @version 1.0, 05/19/19
 * @since 1.0
 */
public class CompositeClientTest {
    /**
     * A number of files used to test functionality of the
     * <code>CompositeClient</code> class. The identifier holds the value of
     * "4".
     */
    private static final int FILE_COUNT = 4;
    /**
     * An array of file names used to test functionality of the
     * <code>CompositeClient</code> class. Can't be a <code>null</code> value.
     */
    private static final String[] FILE_PATHS = new String[] {
                FILE_PATH_1, FILE_PATH_2, FILE_PATH_3
            };
    /**
     * An array of texts used to test functionality of the
     * <code>CompositeClient</code> class. Can't be a <code>null</code> value.
     */
    private static final String[] INPUT_TEXTS = new String[FILE_COUNT];
    /**
     * An array of texts restored after parsing. Can't be a <code>null</code>
     * value.
     */
    private static final String[] RESTORED_TEXTS = new String[] {
            "\tIt has survived not only five centuries, but also the leap "
            + "into electronic" + NEW_LINE_STRING + "typesetting, "
            + "remaining essentially unchanged. It was popularised in the "
            + "with the" + NEW_LINE_STRING + "release of Letraset sheets "
            + "containing Lorem Ipsum passages, and more recently with"
            + NEW_LINE_STRING + "desktop publishing software like Aldus "
            + "PageMaker including versions of Lorem Ipsum."
            + NEW_LINE_STRING + NEW_LINE_STRING + "\tIt is a long "
            + "established fact that a reader will be distracted by the "
            + "readable" + NEW_LINE_STRING + "content of a page when "
            + "looking at its layout. The point of using Ipsum is that it "
            + "has a" + NEW_LINE_STRING + "more-or-less normal "
            + "distribution of letters, as opposed to using 'Content "
            + "here, content" + NEW_LINE_STRING + "here', making it look "
            + "like readable English." + NEW_LINE_STRING + NEW_LINE_STRING
            + "\tIt is a established fact that a reader will be of a page "
            + "when looking at its" + NEW_LINE_STRING + "layout."
            + NEW_LINE_STRING + NEW_LINE_STRING + "\tBye.",
            "\tHello, dear world!",
            "\tПринесли сифон. Рыбников выпил стакан большими жадными "
            + "глотками. Даже руки у" + NEW_LINE_STRING + "него задрожали от "
            + "жадности. И тотчас же налил себе другой стакан. Сразу было"
            + NEW_LINE_STRING + "видно, что его уже долго мучила жажда."
            + NEW_LINE_STRING + NEW_LINE_STRING + "\t\"Притворяется, - опять "
            + "подумал Щавинский. - Что за диковинный человек! Он"
            + NEW_LINE_STRING + "недоволен, утомлен, но ничуть не пьян\"."
            + NEW_LINE_STRING + NEW_LINE_STRING + "\t- Жара, черт ее побери, "
            + "- сказал Рыбников хрипло. - Однако я, господа,"
            + NEW_LINE_STRING + "кажется, мешаю вам заниматься."
            + NEW_LINE_STRING + NEW_LINE_STRING + "\t- Нет, ничего. Мы "
            + "привыкли, - пробурчал Ряжкин."
        };
    /**
     * An array of texts with sorted paragraphs. Can't be a <code>null</code>
     * value.
     */
    private static final String[] TEXTS_WITH_SORTED_PARAGRAPHS = new String[] {
            "\tIt is a established fact that a reader will be of a page "
            + "when "
            + "looking at its" + NEW_LINE_STRING + "l"
            + "ayout."
            + NEW_LINE_STRING + NEW_LINE_STRING + "\t"
            + "Bye." + NEW_LINE_STRING
            + NEW_LINE_STRING + "\tIt has survived not only five centuries, "
            + "but also the leap into electronic" + NEW_LINE_STRING
            + "typesetting, remaining essentially unchanged. It was "
            + "popularised in the with the" + NEW_LINE_STRING + "release of "
            + "Letraset sheets containing Lorem Ipsum passages, and more "
            + "recently with" + NEW_LINE_STRING + "desktop publishing "
            + "software like Aldus PageMaker including versions of Lorem "
            + "Ipsum." + NEW_LINE_STRING + NEW_LINE_STRING + "\tIt is a long "
            + "established fact that a reader will be distracted by the "
            + "r"
            + "eadable" + NEW_LINE_STRING + "content of a page when looking "
            + "at its layout. The point of using Ipsum is that it has a"
            + NEW_LINE_STRING + "more-or-less normal distribution of letters, "
            + "as opposed to using 'Content here, content" + NEW_LINE_STRING
            + "here', making it look like readable English.",
            "\t"
            + "Hello, dear world!",
            "\t- Жара, черт ее побери, - сказал Рыбников хрипло. - Однако я, "
            + "господа," + NEW_LINE_STRING + "кажется, мешаю вам заниматься."
            + NEW_LINE_STRING + NEW_LINE_STRING + "\t- Нет, ничего. Мы "
            + "привыкли, - пробурчал Ряжкин." + NEW_LINE_STRING
            + NEW_LINE_STRING + "\t\"Притворяется, - опять подумал Щавинский. "
            + "- Что за диковинный человек! Он" + NEW_LINE_STRING
            + "недоволен, "
            + "утомлен, но ничуть не пьян\"." + NEW_LINE_STRING
            + NEW_LINE_STRING + "\tПринесли сифон. Рыбников выпил стакан "
            + "большими жадными глотками. Даже руки у" + NEW_LINE_STRING
            + "него задрожали от жадности. И тотчас же налил себе другой "
            + "стакан. Сразу было" + NEW_LINE_STRING + "видно, что его уже "
            + "долго мучила жажда."
    };
    /**
     * An array of texts with sorted sentences. Can't be a <code>null</code>
     * value.
     */
    private static final String[] TEXTS_WITH_SORTED_SENTENCES = new String[] {
            "\tIt was popularised in the with the" + NEW_LINE_STRING
            + "release of Letraset sheets containing Lorem Ipsum passages, and"
            + " more recently with" + NEW_LINE_STRING + "desktop publishing "
            + "software like Aldus PageMaker including versions of Lorem "
            + "Ipsum. It has survived not only five centuries, but also the "
            + "leap into electronic" + NEW_LINE_STRING + "typesetting, "
            + "remaining essentially unchanged." + NEW_LINE_STRING
            + NEW_LINE_STRING + "\tThe point of using Ipsum is that it has a"
            + NEW_LINE_STRING + "more-or-less normal distribution of letters, "
            + "as opposed to using 'Content here, content" + NEW_LINE_STRING
            + "here', making it look like readab"
            + "le English. It is a long "
            + "established fact that a reader will be distracted "
            + "by the "
            + "readable" + NEW_LINE_STRING + "content of a page when looking "
            + "at its lay"
            + "out." + NEW_LINE_STRING + NEW_LINE_STRING + "\tIt is "
            + "a established fact that a reader will be of a page when "
            + "looking at its" + NEW_LINE_STRING + "layout." + NEW_LINE_STRING
            + NEW_LINE_STRING + "\tB"
            + "ye.",
            "\tHello, "
            + "dear world!",
            "\tРыбников выпил стакан большими жадными глотками. Принесли "
            + "сифон. Даже руки у" + NEW_LINE_STRING + "него задрожали от "
            + "жадности. Сразу было" + NEW_LINE_STRING + "видно, что его уже "
            + "долго мучила жажда. И тотчас же налил себе другой стакан."
            + NEW_LINE_STRING + NEW_LINE_STRING + "\t\"Притворяется, - опять "
            + "подумал Щавинский. - Что за диковинный человек! Он"
            + NEW_LINE_STRING + "недоволен, утомлен, "
            + "но ничуть не пьян\"."
            + NEW_LINE_STRING + NEW_LINE_STRING + "\t- Однако я, господа,"
            + NEW_LINE_STRING + "кажется, мешаю вам заниматься. - Жара, черт "
            + "ее побери, - сказал Рыбников хрипло." + NEW_LINE_STRING
            + NEW_LINE_STRING + "\tМы привыкли, - пробурчал Ряжкин. - Нет, "
            + "ничего."

    };
    /**
     * An array of texts with sorted words. Can't be a <code>null</code>
     * value.
     */
    private static final String[] TEXTS_WITH_SORTED_WORDS = new String[] {
            "\tIt has not but the only five also leap into survived centuries, "
            + "remaining unchanged. electronic" + NEW_LINE_STRING
            + "typesetting, essentially It in of of was the the"
            + NEW_LINE_STRING + "and with more with" + NEW_LINE_STRING
            + "like Lorem Ipsum Aldus Lorem Ipsum. sheets release desktop "
            + "Letraset passages, recently software versions PageMaker "
            + "including containing publishing popularised" + NEW_LINE_STRING
            + NEW_LINE_STRING + "\ta a a It is be by of at the its long fact"
            + " that will page when reader layout. content looking readable"
            + NEW_LINE_STRING + "distracted established a" + NEW_LINE_STRING
            + "of is it of as to it The has that here, look like point using "
            + "Ipsum using here', normal making letters, opposed content"
            + NEW_LINE_STRING + "English. 'Content readable more-or-less "
            + "distribution" + NEW_LINE_STRING + NEW_LINE_STRING + "\ta a a "
            + "It is be of at its" + NEW_LINE_STRING + "fact that will page "
            + "when reader layout. looking established" + NEW_LINE_STRING
            + NEW_LINE_STRING + "\tBy"
            + "e.",
            "\tdear Hello, "
            + "world!",
            "\tсифон. Принесли выпил стакан жадными Рыбников большими "
            + "глотками. у" + NEW_LINE_STRING + "от Даже руки него жадности. "
            + "задрожали И же себе налил тотчас другой стакан. что его уже "
            + "было" + NEW_LINE_STRING + "Сразу видно, долго жажда. мучила"
            + NEW_LINE_STRING + NEW_LINE_STRING + "\t- опять подумал "
            + "Щавинский. \"Притворяется, - за Что человек! диковинный Он"
            + NEW_LINE_STRING + "но не пьян\". ничуть утомлен, недоволен,"
            + NEW_LINE_STRING + NEW_LINE_STRING + "\t- - ее Жара, черт "
            + "побери, сказал хрипло. Рыбников - я, вам мешаю Однако господа,"
            + NEW_LINE_STRING + "кажется, заниматься." + NEW_LINE_STRING
            + NEW_LINE_STRING + "\t- Нет, ничего. - Мы Ряжкин. привыкли, "
            + "пробурчал"
    };
    /**
     * Creates data for text restoring testing.
     * @return data for text restoring testing
     */
    @DataProvider(name = "restoredTextProvider")
    public Object[][] provideRestoredTexts() {
        return (new Object[][] {
                    {new TextParser().parseString(INPUT_TEXTS[ARRAY_INDEX_0]),
                     RESTORED_TEXTS[ARRAY_INDEX_0]},
                    {new TextParser().parseString(INPUT_TEXTS[ARRAY_INDEX_1]),
                     RESTORED_TEXTS[ARRAY_INDEX_1]},
                    {new TextParser().parseString(INPUT_TEXTS[ARRAY_INDEX_2]),
                     RESTORED_TEXTS[ARRAY_INDEX_2]}
                });
    }
    /**
     * Creates data for paragraph sorting testing.
     * @return data for paragraph sorting testing
     */
    @DataProvider(name = "sortedParagraphProvider")
    public Object[][] provideSortedParagraphs() {
        return (new Object[][] {
                    {new TextParser().parseString(INPUT_TEXTS[ARRAY_INDEX_0]),
                     TEXTS_WITH_SORTED_PARAGRAPHS[ARRAY_INDEX_0]},
                    {new TextParser().parseString(INPUT_TEXTS[ARRAY_INDEX_1]),
                     TEXTS_WITH_SORTED_PARAGRAPHS[ARRAY_INDEX_1]},
                    {new TextParser().parseString(INPUT_TEXTS[ARRAY_INDEX_2]),
                         TEXTS_WITH_SORTED_PARAGRAPHS[ARRAY_INDEX_2]}
                });
    }
    /**
     * Creates data for sentence sorting testing.
     * @return data for sentence sorting testing
     */
    @DataProvider(name = "sortedSentenceProvider")
    public Object[][] provideSortedSentences() {
        return (new Object[][] {
                    {new TextParser().parseString(INPUT_TEXTS[ARRAY_INDEX_0]),
                     TEXTS_WITH_SORTED_SENTENCES[ARRAY_INDEX_0], 'y'},
                    {new TextParser().parseString(INPUT_TEXTS[ARRAY_INDEX_1]),
                     TEXTS_WITH_SORTED_SENTENCES[ARRAY_INDEX_1], 'w'},
                    {new TextParser().parseString(INPUT_TEXTS[ARRAY_INDEX_2]),
                         TEXTS_WITH_SORTED_SENTENCES[ARRAY_INDEX_2], 'е'}
                });
    }
    /**
     * Creates data for word sorting testing.
     * @return data for word sorting testing
     */
    @DataProvider(name = "sortedWordProvider")
    public Object[][] provideSortedWords() {
        return (new Object[][] {
                    {new TextParser().parseString(INPUT_TEXTS[ARRAY_INDEX_0]),
                     TEXTS_WITH_SORTED_WORDS[ARRAY_INDEX_0]},
                    {new TextParser().parseString(INPUT_TEXTS[ARRAY_INDEX_1]),
                     TEXTS_WITH_SORTED_WORDS[ARRAY_INDEX_1]},
                    {new TextParser().parseString(INPUT_TEXTS[ARRAY_INDEX_2]),
                         TEXTS_WITH_SORTED_WORDS[ARRAY_INDEX_2]}
                });
    }
    /**
     * Reads the texts of input files and stores them in an internal variable.
     */
    @BeforeTest
    public static void configure() {
        DataReader reader = DataReader.getInstance();
        /* The main part of the method. */
        INPUT_TEXTS[ARRAY_INDEX_0]
                = reader.readData(FILE_PATHS[ARRAY_INDEX_0]);
        INPUT_TEXTS[ARRAY_INDEX_1]
                = reader.readData(FILE_PATHS[ARRAY_INDEX_1]);
        INPUT_TEXTS[ARRAY_INDEX_2]
                = reader.readData(FILE_PATHS[ARRAY_INDEX_2]);
    }
    /**
     * Verifies correctness of restoring a text after parsing.
     * @param composite the <code>Composite</code> value which is a runtime
     *                  representation of a text
     * @param expected the <code>String</code> value which is a text restored
     *                 after parsing
     */
    @Test(description = "Verifies that a text is correctly resrtored after"
            + " parsing.", dataProvider = "restoredTextProvider")
    public void testTextRestoring(final Composite composite,
            final String expected) {
        String actual = composite.toString();
        assertEquals(actual, expected);
    }
    /**
     * Verifies paragraph sorting.
     * @param composite the <code>Composite</code> value which is a runtime
     *                  representation of a text
     * @param expected the <code>String</code> value which is a text with
     *                 sorted paragraphs
     */
    @Test(description = "Verifies paragraph sorting.",
            dataProvider = "sortedParagraphProvider")
    public void testParagraphSorting(final Composite composite,
            final String expected) {
        CompositeClient client = new CompositeClient(composite);
        String actual;
        client.sortParagraphs();
        actual = client.retrieveStringRepresentation();
        assertEquals(actual, expected);
    }
    /**
     * Verifies sentence sorting.
     * @param composite the <code>Composite</code> value which is a runtime
     *                  representation of a text
     * @param expected the <code>String</code> value which is a text with
     *                 sorted sentences
     * @param character the <code>char</code> value which is a character
     *                  according to which sorting is performed
     */
    @Test(description = "Verifies sentence sorting.",
            dataProvider = "sortedSentenceProvider")
    public void testSentencehSorting(final Composite composite,
            final String expected, final char character) {
        CompositeClient client = new CompositeClient(composite);
        String actual;
        client.sortSentences(character);
        actual = client.retrieveStringRepresentation();
        assertEquals(actual, expected);
    }
    /**
     * Verifies word sorting.
     * @param composite the <code>Composite</code> value which is a runtime
     *                  representation of a text
     * @param expected the <code>String</code> value which is a text with
     *                 sorted words
     */
    @Test(description = "Verifies word sorting.",
            dataProvider = "sortedWordProvider")
    public void testWordSorting(final Composite composite,
            final String expected) {
        CompositeClient client = new CompositeClient(composite);
        String actual;
        client.sortWords();
        actual = client.retrieveStringRepresentation();
        assertEquals(actual, expected);
    }
}
