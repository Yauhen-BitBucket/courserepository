/**
 * Contains parsers for various input information types. There are the
 * following parsers: a text parser, a paragraph parser, a sentence parser, a
 * lexeme parser, a word parser. They are used to retrieve a corresponding
 * composite component.
 * @author Yauhen Sazonau
 * @since 1.0
 */
package by.training.task02.parser;
