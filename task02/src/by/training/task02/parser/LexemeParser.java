/*
 * LexemeParser.java
 * A lexeme parser. It's used to retrieve a lexeme composite component.
 */
package by.training.task02.parser;


import static by.training.task02.constant.ExpressionStrings
        .PUNCTUATION_EXPRESSION;
import static by.training.task02.constant.ExpressionStrings.WORD_EXPRESSION;

import by.training.task02.component.Composite;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * A lexeme parser. It's used to retrieve a lexeme composite component.
 * @author Yauhen Sazonau
 * @version 1.0, 05/16/19
 * @since 1.0
 */
public class LexemeParser extends GeneralParser {
    /**
     * Constructs an instance of this class.
     */
    public LexemeParser() {
        compositeType = Composite.CompositeType.LEXEME;
        nextParser = new WordParser();
    }
    /**
     * Retrieves a lexeme composite component.
     * @param input the <code>String</code> value from which to retrieve a
     *              lexeme composite component
     * @return a lexeme composite component
     */
    @Override
    public Composite parseString(final String input) {
        Composite composite = new Composite(compositeType);
        Pattern punctuationPattern = Pattern.compile(PUNCTUATION_EXPRESSION);
        Pattern wordPattern = Pattern.compile(WORD_EXPRESSION);
        Matcher punctuationMatcher = punctuationPattern.matcher(input);
        Matcher wordMatcher = wordPattern.matcher(input);
        /* The main part of the method. */
        wordMatcher.find();
        composite.add(nextParser.parseString(wordMatcher.group()));
        if (punctuationMatcher.find()) {
            composite.add(nextParser.parseString(punctuationMatcher.group()));
        }
        return composite;
    }
}
