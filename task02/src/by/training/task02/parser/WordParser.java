/*
 * WordParser.java
 * A word parser. It's used to retrieve a word composite component.
 */
package by.training.task02.parser;


import static by.training.task02.constant.ExpressionStrings.SYMBOL_EXPRESSION;

import by.training.task02.component.Composite;
import by.training.task02.component.Symbol;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * A word parser. It's used to retrieve a word composite component.
 * @author Yauhen Sazonau
 * @version 1.0, 05/16/19
 * @since 1.0
 */
public class WordParser extends GeneralParser {
    /**
     * Constructs an instance of this class.
     */
    public WordParser() {
        compositeType = Composite.CompositeType.WORD;
    }
    /**
     * Retrieves a word composite component.
     * @param input the <code>String</code> value from which to retrieve a word
     *              composite component
     * @return a word composite component
     */
    @Override
    public Composite parseString(final String input) {
        Composite composite = new Composite(compositeType);
        Pattern pattern = Pattern.compile(SYMBOL_EXPRESSION);
        Matcher matcher = pattern.matcher(input);
        String currentCharAsString;
        /* The main part of the method. */
        while (matcher.find()) {
            currentCharAsString = matcher.group();
            composite.add(new Symbol(currentCharAsString.charAt(0)));
        }
        return composite;
    }
}
