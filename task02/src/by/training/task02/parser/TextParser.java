/*
 * TextParser.java
 * A text parser. It's used to retrieve a text composite component.
 */
package by.training.task02.parser;


import static by.training.task02.constant.ExpressionStrings
        .PARAGRAPH_TERMINATOR_STRING;

import by.training.task02.component.Composite;


/**
 * A text parser. It's used to retrieve a text composite component.
 * @author Yauhen Sazonau
 * @version 1.0, 05/16/19
 * @since 1.0
 */
public class TextParser extends GeneralParser {
    /**
     * Constructs an instance of this class.
     */
    public TextParser() {
        compositeType = Composite.CompositeType.TEXT;
        nextParser = new ParagraphParser();
        terminator = PARAGRAPH_TERMINATOR_STRING;
    }
}
