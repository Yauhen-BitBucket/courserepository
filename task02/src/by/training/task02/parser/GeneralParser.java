/*
 * GeneralParser.java
 * A general parser. It's used to retrieve a composite component.
 */
package by.training.task02.parser;


import static by.training.task02.constant.ExpressionStrings
        .SPACE_MATCHING_STRING;

import static by.training.task02.constant.StickingStrings.EMPTY_STRING;
import static by.training.task02.constant.StickingStrings.NEW_LINE_STRING;

import by.training.task02.component.Composite;

import java.util.LinkedList;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;


/**
 * A general parser. It's used to retrieve a composite component.
 * @author Yauhen Sazonau
 * @version 1.0, 05/16/19
 * @since 1.0
 */
public abstract class GeneralParser {
    /**
     * A logger for this application. <code>null</code> value is prohibited.
     */
    private static final Logger LOGGER
            = LogManager.getLogger(GeneralParser.class);
    /**
     * A type of a composite component which is retrieved by this parser. Must
     * be specified by a subclass. <code>null</code> value is prohibited.
     */
    protected Composite.CompositeType compositeType;
    /**
     * The next parser to be invoked. Should be specified by a subclass.
     * <code>null</code> value is prohibited.
     */
    protected GeneralParser nextParser;
    /**
     * A terminator string used to split a string to be parsed. Should be
     * specified by a subclass. <code>null</code> value is prohibited.
     */
    protected String terminator;
    /**
     * Constructs an instance of this class.
     */
    public GeneralParser() {
        /* The default initialization is sufficient. */
    }
    /**
     * Retrieves a composite component of a particular type. This type is
     * specified by subclasses.
     * @param input the <code>String</code> value from which to retrieve a
     *              composite component
     * @return a composite component of a particular type. This type is
     *         specified by subclasses.
     */
    public Composite parseString(final String input) {
        Composite composie = new Composite(compositeType);
        Composite retrivedComposite;
        LinkedList<String> parts = splitSequence(input, terminator);
        /* The main part of the method. */
        for (String part : parts) {
            retrivedComposite = nextParser.parseString(part);
            composie.add(retrivedComposite);
        }
        LOGGER.info("A " + compositeType.name().toLowerCase() + " was parsed:"
                    + NEW_LINE_STRING + composie.toString());
        return composie;
    }
    /**
     * Splits the specified string using the specified terminator string.
     * @param sequence the <code>String</code> value which is a sequence to be
     *                 split
     * @param splitter the <code>String</code> value which is a terminator
     * @return a list of items after splitting
     */
    private LinkedList<String> splitSequence(final String sequence,
            final String splitter) {
        int previousEndIndex = 0;
        int interationNumber = 1;
        Pattern pattern = Pattern.compile(splitter);
        LinkedList<String> parts = new LinkedList<>();
        Pattern spacePattern = Pattern.compile(SPACE_MATCHING_STRING);
        Matcher matcher = pattern.matcher(sequence);
        Matcher spaceMatcher;
        String sentenceTerminator = EMPTY_STRING;
        StringBuilder builder = new StringBuilder();
        /* The main part of the method. */
        while (matcher.find()) {
            /*
             * The dot sign is appended because it's excluded during matching
             * a terminator string.
             */
            if (Composite.CompositeType.PARAGRAPH.equals(compositeType)) {
                sentenceTerminator = matcher.group(1);
            }
            builder.append(sequence.substring(previousEndIndex,
                    matcher.start()));
            builder.append(sentenceTerminator);
            if ((Composite.CompositeType.TEXT.equals(compositeType))
                    && (interationNumber == 1)) {
                spaceMatcher = spacePattern.matcher(builder.toString());
                if (spaceMatcher.find()) {
                    builder.delete(spaceMatcher.start(), spaceMatcher.end());
                }
            }
            if ((Composite.CompositeType.SENTENCE.equals(compositeType))
                    && (NEW_LINE_STRING.equals(matcher.group()))) {
                builder.append(NEW_LINE_STRING);
            }
            parts.add(builder.toString());
            previousEndIndex = matcher.end();
            interationNumber++;
            builder.delete(0, builder.length());
        }
        parts.add(sequence.substring(previousEndIndex, sequence.length()));
        return parts;
    }
}
