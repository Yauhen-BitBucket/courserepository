/*
 * SentenceParser.java
 * A sentence parser. It's used to retrieve a sentence composite component.
 */
package by.training.task02.parser;


import static by.training.task02.constant.ExpressionStrings
        .LEXEME_TERMINATOR_STRING;

import by.training.task02.component.Composite;


/**
 * A sentence parser. It's used to retrieve a sentence composite component.
 * @author Yauhen Sazonau
 * @version 1.0, 05/16/19
 * @since 1.0
 */
public class SentenceParser extends GeneralParser {
    /**
     * Constructs an instance of this class.
     */
    public SentenceParser() {
        compositeType = Composite.CompositeType.SENTENCE;
        nextParser = new LexemeParser();
        terminator = LEXEME_TERMINATOR_STRING;
    }
}
