/*
 * ParagraphParser.java
 * A paragraph parser. It's used to retrieve a paragraph composite component.
 */
package by.training.task02.parser;


import static by.training.task02.constant.ExpressionStrings
        .SENTENCE_TERMINATOR_STRING;

import by.training.task02.component.Composite;


/**
 * A paragraph parser. It's used to retrieve a paragraph composite component.
 * @author Yauhen Sazonau
 * @version 1.0, 05/16/19
 * @since 1.0
 */
public class ParagraphParser extends GeneralParser {
    /**
     * Constructs an instance of this class.
     */
    public ParagraphParser() {
        compositeType = Composite.CompositeType.PARAGRAPH;
        nextParser = new SentenceParser();
        terminator = SENTENCE_TERMINATOR_STRING;
    }
}
