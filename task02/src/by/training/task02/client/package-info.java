/**
 * Contains a class which is a composite component client. It is used to
 * fulfill sorting operations on a composite component.
 * @author Yauhen Sazonau
 * @since 1.0
 */
package by.training.task02.client;
