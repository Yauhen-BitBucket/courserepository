/*
 * CompositeClient.java
 * A class which is a composite component client. It is used to fulfill sorting
 * operations on a composite component.
 */
package by.training.task02.client;


import static by.training.task02.constant.StickingStrings.NEW_LINE_STRING;

import by.training.task02.component.Component;
import by.training.task02.component.Composite;
import by.training.task02.component.Composite.CompositeType;
import by.training.task02.component.Symbol;

import by.training.task02.exception.InformationHandlingException;

import java.util.Comparator;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;


/**
 * A class which is a composite component client. It is used to fulfill sorting
 * operations on a composite component.
 * @author Yauhen Sazonau
 * @version 1.0, 05/17/19
 * @since 1.0
 */
public class CompositeClient {
    /**
     * A logger for this application. <code>null</code> value is prohibited.
     */
    private static final Logger LOGGER
            = LogManager.getLogger(CompositeClient.class);
    /**
     * A composite component used by this client. <code>null</code> value is
     * prohibited.
     */
    private Composite composite;
    /**
     * A comparator used to sort lexemes. <code>null</code> value is
     * prohibited.
     */
    private Comparator<Component> lexemeComparator
            = Comparator.comparingInt(element -> {
                    Composite lexeme = (Composite) element;
                    Composite word = (Composite) lexeme.retrieveChild(0);
                    return (word.getChildrenCount());
                });
    /**
     * A comparator used to sort paragraphs. <code>null</code> value is
     * prohibited.
     */
    private Comparator<Component> paragraphComparator
            = Comparator.comparingInt(element -> {
                    Composite paragraph = (Composite) element;
                    return (paragraph.getChildrenCount());
                });
    /**
     * Constructs an instance of this class using the specified parameter.
     * @param textComposite the <code>Composite</code> value which is a
     *                      composite component of the
     *                      <code>Composite.CompositeType.TEXT</code> type
     * @throws InformationHandlingException
     */
    public CompositeClient(final Composite textComposite) {
        Composite.CompositeType type = textComposite.geCompositeType();
        /* The main part of the method. */
        if (!(type.equals(Composite.CompositeType.TEXT))) {
            throw new InformationHandlingException("The type of a composite "
                    + "component should be 'Composite.CompositeType.TEXT'. "
                    + "The value is '" + type + "'.");
        }
        composite = textComposite;
    }
    /**
     * Retrieves a string representation of an underlying composite component.
     * @return a string representation of an underlying composite component
     */
    public String retrieveStringRepresentation() {
        return composite.toString();
    }
    /**
     * Sorts paragraphs of the underlying composite component by the number of
     * sentences.
     */
    public void sortParagraphs() {
        composite.sort(paragraphComparator, CompositeType.TEXT);
        LOGGER.info("Paragraphs were sorted:" + NEW_LINE_STRING
                + retrieveStringRepresentation());
    }
    /**
     * Sorts sentences of the underlying composite component by the count of
     * occurrence of the specified character.
     * @param character the <code>char</code> value which is a character to be
     *                  counted
     */
    public void sortSentences(final char character) {
        /**
         * A class used as a data holder.
         * @author Yauhen Sazonau
         * @version 1.0, 05/19/19
         * @since 1.0
         */
        class CountHolder {
            /**
             * It's used to keep the count of occurrence of the specified
             * character in a sentence.
             */
            private int count;
        }
        /**
         * Contains all logic of calculation of the count of occurrence of the
         * specified character in a sentence.
         * @author Yauhen Sazonau
         * @version 1.0, 05/19/19
         * @since 1.0
         */
        class CountCalculator {
            /**
             * Calculates the count of occurrence of the specified character
             * in a sentence.
             * @param component the <code>Component</code> value which is a
             *                  sentence
             * @param holder the <code>CountHolder</code> value which is an
             *               object that keeps the count of occurrence of the
             *               specified character in a sentence
             */
            public void calculateCount(final Component component,
                    final CountHolder holder) {
                int length;
                Composite innerComposite;
                Symbol symbol;
                /* The main part of the method. */
                if (component instanceof Symbol) {
                    symbol = (Symbol) component;
                    if (symbol.getSymbol() == character) {
                        holder.count++;
                    }
                } else {
                    innerComposite = (Composite) component;
                    length = innerComposite.getChildrenCount();
                    for (int i = 0; i < length; i++) {
                        calculateCount(innerComposite.retrieveChild(i),
                                holder);
                    }
                }
            }
        }
        Comparator<Component> comparator = Comparator.<Component>comparingInt(
                sentence -> {
                        CountHolder countHolder = new CountHolder();
                        /* The main part of the method. */
                        new CountCalculator().calculateCount(sentence,
                                countHolder);
                        return (countHolder.count);
        });
        /* The main part of the method. */
        composite.sort(comparator, CompositeType.PARAGRAPH);
        LOGGER.info("Sentences were sorted using the '" + character
                + "' symbol:" + NEW_LINE_STRING
                + retrieveStringRepresentation());
    }
    /**
     * Sorts words of sentences of the underlying composite component by the
     * length of a word.
     */
    public void sortWords() {
        composite.sort(lexemeComparator, CompositeType.SENTENCE);
        LOGGER.info("Words were sorted:" + NEW_LINE_STRING
                + retrieveStringRepresentation());
    }
}
