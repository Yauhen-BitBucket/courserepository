/*
 * ArrayIndexes.java
 * A class for storing array indexes.
 */
package by.training.task02.constant;


/**
 * A class for storing array indexes.
 * @author Yauhen Sazonau
 * @version 1.0, 05/19/19
 * @since 1.0
 */
public final class ArrayIndexes {
    /**
     * An array index. The identifier holds the value of "0".
     */
    public static final byte ARRAY_INDEX_0 = 0;
    /**
     * An array index. The identifier holds the value of "1".
     */
    public static final byte ARRAY_INDEX_1 = 1;
    /**
     * An array index. The identifier holds the value of "2".
     */
    public static final byte ARRAY_INDEX_2 = 2;
    /**
     * An array index. The identifier holds the value of "3".
     */
    public static final byte ARRAY_INDEX_3 = 3;
    /**
     * Constructs an instance of this class.
     */
    private ArrayIndexes() {
        /* The default initialization is sufficient. */
    }
}
