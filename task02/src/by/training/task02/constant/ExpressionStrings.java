/*
 * ExpressionStrings.java
 * A class for storing expression strings.
 */
package by.training.task02.constant;


/**
 * A class for storing expression strings.
 * @author Yauhen Sazonau
 * @version 1.0, 05/16/19
 * @since 1.0
 */
public final class ExpressionStrings {
    /**
     * A string used to break a sentence into lexemes. The identifier holds the
     * value of a concatenation of the following strings: "(?:(?: |\t)+)|"
     * and a new line string.
     */
    public static final String LEXEME_TERMINATOR_STRING = "(?:(?: |\t)+)|"
            + String.format("%n");
    /**
     * A string used to break a paragraph into sentences. The identifier holds
     * the value of "(?:[.!?]|(?:\\.\\.\\.))(?: |\t)+".
     */
    public static final String SENTENCE_TERMINATOR_STRING
            = "([.!?]|(?:\\.\\.\\.))(?: |\t)+";
    /**
     * A string used to break a text into paragraphs. The identifier holds
     * the value of a concatenation of the strings: "(?:", a new line string
     * and ")+(?: |\t)+".
     */
    public static final String PARAGRAPH_TERMINATOR_STRING = "(?:"
            + String.format("%n") + ")+(?: |\t)+";
    /**
     * A string used for matching a punctuation sequence. The identifier holds
     * the value of a concatenation of the following strings "[?!,.", a new line
     * string and "]+".
     */
    public static final String PUNCTUATION_EXPRESSION = "[?!,."
            + String.format("%n") + "]+";
    /**
     * A string used to match a white space at the beginning of the first
     * paragraph. The identifier holds the value of "(?: |\t)+".
     */
    public static final String SPACE_MATCHING_STRING = "^(?: |\t)+";
    /**
     * An expression used for matching word symbols. The identifier holds the
     * value of ".|\\s".
     */
    public static final String SYMBOL_EXPRESSION = ".|\\s";
    /**
     * A string used for matching a word. The identifier holds the value of a
     * concatenation of the following strings "[^?!,.", a new line string and
     * "]+".
     */
    public static final String WORD_EXPRESSION = "[^?!,."
            + String.format("%n") + "]+";
    /**
     * Constructs an instance of this class.
     */
    private ExpressionStrings() {
        /* The default initialization is sufficient. */
    }
}
