/**
 * Contains classes which are designed to keep constants. The following
 * classes are considered: a class for keeping sticking strings, a class for
 * keeping expression strings, a class for keeping file paths, a class for
 * keeping array indexes.
 * @author Yauhen Sazonau
 * @since 1.0
 */
package by.training.task02.constant;
