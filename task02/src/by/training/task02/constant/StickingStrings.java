/*
 * StickingStrings.java
 * A class for storing sticking strings.
 */
package by.training.task02.constant;


/**
 * A class for storing sticking strings.
 * @author Yauhen Sazonau
 * @version 1.0, 05/16/19
 * @since 1.0
 */
public final class StickingStrings {
    /**
     * A string used to stick words and symbols. The identifier holds the value
     * of "".
     */
    public static final String EMPTY_STRING = "";
    /**
     * A string used to stick lexemes. The identifier holds the value of " ".
     */
    public static final String LEXEME_STRING = " ";
    /**
     * A new line string. The identifier holds the value of a new line string.
     */
    public static final String NEW_LINE_STRING = String.format("%n");
    /**
     * A string used to stick paragraphs. The identifier holds the value of a
     * concatenation of the following strings: a new line string, a new line
     * string and "\t".
     */
    public static final String PARAGRAPH_STRING = String.format("%n%n\t");
    /**
     * A string used to stick sentences. The identifier holds the value of". ".
     */
    public static final String SENTENCE_STRING = " ";
    /**
     * A tab string. The identifier holds the value of "\t".
     */
    public static final String TAB_STRING = "\t";
    /**
     * Constructs an instance of this class.
     */
    private StickingStrings() {
        /* The default initialization is sufficient. */
    }
}
