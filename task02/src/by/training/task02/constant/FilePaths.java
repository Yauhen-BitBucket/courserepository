/*
 * FilePaths.java
 * A class for storing file paths.
 */
package by.training.task02.constant;


/**
 * A class for storing file paths.
 * @author Yauhen Sazonau
 * @version 1.0, 05/19/19
 * @since 1.0
 */
public final class FilePaths {
    /**
     * Specifies a relative path to the file which a text will be read from.
     * The identifier holds the value of "data/input1.txt".
     */
    public static final String FILE_PATH_1 = "data/input1.txt";
    /**
     * Specifies a relative path to the file which a text will be read from.
     * The identifier holds the value of "data/input2.txt".
     */
    public static final String FILE_PATH_2 = "data/input2.txt";
    /**
     * Constructs an instance of this class.
     */
    public static final String FILE_PATH_3 = "data/input3.txt";
    /**
     * Constructs an instance of this class.
     */
    private FilePaths() {
        /* The default initialization is sufficient. */
    }
}
