/*
 * Composite.java
 * A composite component. It is intended to contain children.
 */
package by.training.task02.component;


import static by.training.task02.constant.StickingStrings.EMPTY_STRING;
import static by.training.task02.constant.StickingStrings.LEXEME_STRING;
import static by.training.task02.constant.StickingStrings.NEW_LINE_STRING;
import static by.training.task02.constant.StickingStrings.PARAGRAPH_STRING;
import static by.training.task02.constant.StickingStrings.SENTENCE_STRING;
import static by.training.task02.constant.StickingStrings.TAB_STRING;

import by.training.task02.exception.InformationHandlingException;

import java.util.Comparator;
import java.util.LinkedList;


/**
 * A composite component. It is intended to contain children.
 * @author Yauhen Sazonau
 * @version 1.0, 05/16/19
 * @since 1.0
 */
public class Composite implements Component {
    /**
     * Represents a composite type. The following composite types are existent:
     * a text, a paragraph, a sentence, a lexeme, a punctuation mark, a word.
     * @author Yauhen Sazonau
     * @version 1.0, 05/16/19
     * @since 1.0
     */
    public enum CompositeType {
        /**
         * A text composite type.
         */
        TEXT,
        /**
         * A paragraph composite type.
         */
        PARAGRAPH,
        /**
         * A sentence composite type.
         */
        SENTENCE,
        /**
         * A lexeme composite type.
         */
        LEXEME,
        /**
         * A word composite type.
         */
        WORD
    }
    /**
     * A coefficient used to calculate a hash code value of an object. The
     * identifier holds the value of "31".
     */
    private static final byte HASH_CODE_COEFFICIENT = 31;
    /**
     * A type of this composite component. <code>null</code> value is
     * prohibited.
     */
    private CompositeType compositeType;
    /**
     * A list of child components. <code>null</code> value is prohibited.
     */
    private LinkedList<Component> children = new LinkedList<>();
    /**
     * Constructs a composite component of the specified type.
     * @param type the <code>CompositeType</code> value which is a type of a
     *                                            composite to be created
     * @throws InformationHandlingException if the argument is a
     *                                      <code>null</code> value
     */
    public Composite(final CompositeType type) {
        if (type == null) {
            throw new InformationHandlingException("The argument is a 'null' "
                    + "value.");
        }
        this.compositeType = type;
    }
    /**
     * Retrieves a number of children of this component.
     * @return a number of children of this component
     */
    public int getChildrenCount() {
        return (children.size());
    }
    /**
     * Retrieves a type of this composite component.
     * @return a type of this composite component
     */
    public CompositeType geCompositeType() {
        return compositeType;
    }
    /**
     * Retrieves a child at the specified position.
     * @param index the <code>int</code> value which is a position of a child
     *              to retrieve
     * @return a child at the specified position
     * @throws InformationHandlingException if the argument is less than zero
     *                                      or greater than length minus one
     */
    public Component retrieveChild(final int index) {
        if ((index < 0) || (index > getChildrenCount() - 1)) {
            throw new InformationHandlingException("The specified position is "
                    + "invalid. The position is '" + index + "'.");
        }
        return (children.get(index));
    }
    /**
     * Retrieves a hash code value for this object.
     * @return a hash code value for this object
     */
    @Override
    public int hashCode() {
        Component child;
        int length = children.size();
        int value = 0;
        /* The main part of the method. */
        for (int i = 0; i < length; i++) {
            child = children.get(i);
            value += Math.pow(HASH_CODE_COEFFICIENT, (double) (length - 1 - i))
                     * child.hashCode();
        }
        return value;
    }
    /**
     * Indicates whether some other object is "equal to" this one.
     * @param object the <code>Object</code> value which is an object with
     *               which to compare. <code>null</code> value is possible.
     * @return <code>true</code> if this object is the same as the object
     *         supplied through the <code>object</code> argument;
     *         <code>false</code> otherwise
     */
    @Override
    public boolean equals(final Object object) {
        boolean result;
        Composite composite;
        int length = children.size();
        int objectLength;
        /* The main part of the method. */
        if (this == object) {
            return true;
        } else if (object == null) {
            return false;
        }
        /* The supplied object should have the same class. */
        if (getClass() != object.getClass()) {
            return false;
        }
        composite = (Composite) object;
        objectLength = composite.getChildrenCount();
        result = length == objectLength;
        if (result) {
            for (int i = 0; i < length; i++) {
                result = retrieveChild(i).equals(
                        composite.retrieveChild(i));
                if (!result) {
                    break;
                }
            }
        }
        return result;
    }
    /**
     * Adds a direct child component to this component.
     * @param component the <code>Component</code> value which is a direct
     *                  child component to be added
     */
    @Override
    public void add(final Component component) {
       children.add(component);
    }
    /**
     * Removes a direct child component from this component.
     * @param component the <code>Component</code> value which is a direct
     *                  child component to be removed
     */
    @Override
    public void remove(final Component component) {
        children.remove(component);
    }
    /**
     * Sorts children using the specified comparator and type of an enclosing
     * composite component which contains elements to be sorted.
     * @param comparator the <code>Comparator</code> value which is a
     *                   comparator used to sort children
     * @param enclosingType the <code>CompositeType</code> value which is the
     *                      type of an enclosing composite component which
     *                      contains elements to be sorted
     */
    public void sort(final Comparator<Component> comparator,
            final CompositeType enclosingType) {
        LinkedList<Component> childrenList = this.children;
        if (enclosingType.equals(this.compositeType)) {
            childrenList.sort(comparator);
        } else {
            for (Component component : childrenList) {
                ((Composite) component).sort(comparator, enclosingType);
            }
        }
    }
    /**
     * Retrieves a string representation of this composite component.
     * @return a string representation of this composite component.
     *         <code>null</code> value is prohibited.
     */
    @Override
    public String toString() {
        boolean condition = CompositeType.TEXT.equals(compositeType);
        boolean isSentence;
        int iterationCount = 1;
        String stickingString;
        String stringComponent;
        String tab = EMPTY_STRING;
        StringBuilder builder = new StringBuilder();
        /* The main part of the method. */
        switch (compositeType) {
        case TEXT:
            stickingString = PARAGRAPH_STRING;
            break;
        case PARAGRAPH:
            stickingString = SENTENCE_STRING;
            break;
        case SENTENCE:
            stickingString = LEXEME_STRING;
            break;
        case LEXEME:
        case WORD:
            stickingString = EMPTY_STRING;
            break;
        default:
            throw new InformationHandlingException("Wrong composite type "
                    + "is specified for a component. Type - " + compositeType
                    + ".");
        }
        for (Component component : children) {
            if ((condition) && (iterationCount == 1)) {
                tab = TAB_STRING;
            }
            stringComponent = component.toString();
            isSentence = (compositeType.equals(CompositeType.SENTENCE))
                    && (stringComponent.contains(NEW_LINE_STRING));
            if (isSentence) {
                stickingString = EMPTY_STRING;
            }
            builder.append(tab + stringComponent + stickingString);
            if (isSentence) {
                stickingString = LEXEME_STRING;
            }
            if (condition) {
                tab = EMPTY_STRING;
                iterationCount++;
            }
        }
        builder.delete(builder.lastIndexOf(stickingString), builder.length());
        return (builder.toString());
    }
}
