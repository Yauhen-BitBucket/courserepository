/**
 * Contains an interface which defines a basic contact which a component must
 * comply with. Also there are two types of a component: a component which can
 * have children and a component which is indivisible.
 * @author Yauhen Sazonau
 * @since 1.0
 */
package by.training.task02.component;
