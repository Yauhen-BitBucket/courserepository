/*
 * Component.java
 * A component. It describes a particular part of a whole and can contain
 * other parts, if it isnt't an indivisible component.
 */
package by.training.task02.component;


import by.training.task02.exception.InformationHandlingException;


/**
 * A component. It describes a particular part of a whole and can contain
 * other parts, if it isnt't an indivisible component.
 * @author Yauhen Sazonau
 * @version 1.0, 05/16/19
 * @since 1.0
 */
public interface Component {
    /**
     * Adds a direct child component to this component.
     * @param component the <code>Component</code> value which is a direct
     *                  child component to be added
     * @throws InformationHandlingException if this operation isn't supported
     *                                      (for example, if this component is
     *                                      indivisible)
     */
    default void add(final Component component) {
        throw new InformationHandlingException("The method isn't suppoerted");
    }
    /**
     * Removes a direct child component from this component.
     * @param component the <code>Component</code> value which is a direct
     *                  child component to be removed
     * @throws InformationHandlingException if this operation isn't supported
     *                                      (for example, if this component is
     *                                      indivisible)
     */
    default void remove(final Component component) {
        throw new InformationHandlingException("The method isn't suppoerted");
    }
}
