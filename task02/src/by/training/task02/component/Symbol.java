/*
 * Symbol.java
 * A symbol component. It is indivisible.
 */
package by.training.task02.component;


/**
 * A symbol component. It is indivisible.
 * @author Yauhen Sazonau
 * @version 1.0, 05/16/19
 * @since 1.0
 */
public class Symbol implements Component {
    /**
     * Represents a symbol.
     */
    private char sign;
    /**
     * Constructs an instance of this class using the specified character.
     * @param character the <code>char</code> value which is a symbol to be
     *                  set
     */
    public Symbol(final char character) {
        sign = character;
    }
    /**
     * Retrieves a symbol represented by this component.
     * @return a symbol represented by this component
     */
    public char getSymbol() {
        return sign;
    }
    /**
     * Sets a symbol for this component.
     * @param character the <code>char</code> value which is a symbol to be
     *                  set
     */
    public void setSymbol(final char character) {
        this.sign = character;
    }
    /**
     * Retrieves a hash code value for this object.
     * @return a hash code value for this object
     */
    @Override
    public int hashCode() {
        return sign;
    }
    /**
     * Indicates whether some other object is "equal to" this one.
     * @param object the <code>Object</code> value that is an object with
     *               which to compare. <code>null</code> value is possible.
     * @return <code>true</code> if this object is the same as the object
     *         supplied through the <code>object</code> argument;
     *         <code>false</code> otherwise
     */
    @Override
    public boolean equals(final Object object) {
        Symbol symbolObject;
        /* The main part of the method. */
        if (this == object) {
            return true;
        } else if (object == null) {
            return false;
        }
        /* The supplied object should have the same class. */
        if (getClass() != object.getClass()) {
            return false;
        }
        symbolObject = (Symbol) object;
        return (sign == symbolObject.sign);
    }
    /**
     * Retrieves a string representation of this symbol component.
     * @return a string representation of this symbol component.
     *         <code>null</code> value is prohibited.
     */
    @Override
    public String toString() {
        return (String.valueOf(sign));
    }
}
