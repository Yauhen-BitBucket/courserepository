/*
 * DataReader.java
 * A reader. It's used to read the whole text stored in a text file.
 */
package by.training.task02.reader;


import by.training.task02.exception.InformationHandlingException;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.Reader;

import java.nio.charset.StandardCharsets;

import java.util.Optional;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;


/**
 * A reader. It's used to read the whole text stored in a text file.
 * @author Yauhen Sazonau
 * @version 1.0, 05/16/19
 * @since 1.0
 */
public final class DataReader {
    /* Implements the Singleton pattern. */
    /**
     * Represents a single instance of this class. <code>null</code> value is
     * prohibited.
     */
    private static DataReader instance;
    /**
     * A logger for this application. <code>null</code> value is prohibited.
     */
    private static final Logger LOGGER
            = LogManager.getLogger(DataReader.class);
    /**
     * Specifies an exception message. The identifier holds the value of
     * "There are problems with reading a text.".
     */
    private static final String EXCEPTION_MESSAGE
            = "There are problems with reading a text.";
    /**
     * Constructs an instance of this class. You have no ability to create an
     * instance of this class using this constructor. You should use the
     * <code>getInstance</code> method.
     * @see #getInstance()
     */
    private DataReader() {
        /* The default initialization is sufficient. */
    }
    /**
     * Retrieves an instance if this class.
     * @return a newly created instance of this class or an existing if it has
     *         been created before
     */
    public static DataReader getInstance() {
        if (instance == null) {
            instance = new DataReader();
        }
        return instance;
    }
    /**
     * Reads the whole text stored in a text file.
     * @param filePath the <code>String</code> value which is a relative path
     *                 to the file which a text will be read from
     * @return the whole text stored in a text file
     * @throws InformationHandlingException if there are problems with reading
     *                                      a text
     */
    public String readData(final String filePath) {
        BufferedReader bufferedReader;
        Optional<String> line;
        StringBuilder text = new StringBuilder();
        /* The main part of the method. */
        bufferedReader = retrieveReader(filePath);
        /* Reading data. */
       try {
           line = readNextLine(bufferedReader);
           if (!(line.isPresent())) {
               throw new InformationHandlingException("There is no data to "
                       + "read.");
           }
           while (line.isPresent()) {
               text.append(String.format("%s%n", line.get()));
               line = readNextLine(bufferedReader);
           }
       } finally {
           closeReader(bufferedReader);
       }
       text.delete(text.lastIndexOf(String.format("%n")), text.length());
       return (text.toString());
    }
    /**
     * Closes the specified reader.
     * @param reader the <code>Reader</code> value which is a reader to be
     * closed.
     */
    private void closeReader(final Reader reader) {
        try {
            reader.close();
        } catch (IOException e) {
            LOGGER.error("Problems occured while closing an underlying "
                    + "reader.");
        }
    }
    /**
     * Retrieves the next line of data using the specified buffered reader.
     * @param reader the <code>BufferedReader</code> value which is a reader
     *               used to read a line of data.
     * @return the next line of data. <code>null</code> value is prohibited.
     * @throws InformationHandlingException if there are problems with reading
     *                                      the next line
     */
    private Optional<String> readNextLine(final BufferedReader reader) {
        String line;
        /* The main part of the method. */
        try {
            line = reader.readLine();
        } catch (IOException e) {
            throw new InformationHandlingException(EXCEPTION_MESSAGE, e);
        }
        return Optional.ofNullable(line);
    }
    /**
     * Retrieves an instance of the <code>BufferedReader</code> class using
     * the specified file path.
     * @param filePath the <code>String</code> value which is the path to a
     *                 text file.
     * @return a newly created instance of the <code>BufferedReader</code>
     *         class. <code>null</code> value is prohibited.
     * @throws InformationHandlingException if there are problems with
     *                                      retrieving an instance of the
     *                                      <code>BufferedReader</code> class
     */
    private BufferedReader retrieveReader(final String filePath) {
        BufferedReader bufferedReader;
        InputStreamReader streamReader;
        /* The main part of the method. */
        try {
            streamReader
                    = new InputStreamReader(new FileInputStream(filePath),
                            StandardCharsets.UTF_8);
        } catch (FileNotFoundException e) {
            throw new InformationHandlingException(EXCEPTION_MESSAGE, e);
        }
        bufferedReader = new BufferedReader(streamReader);
        return bufferedReader;
    }
}
