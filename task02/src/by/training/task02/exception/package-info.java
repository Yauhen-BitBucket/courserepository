/**
 * Provides various exception classes.
 * @author Yauhen Sazonau
 * @since 1.0
 */
package by.training.task02.exception;
