The information handling project.

There are two types of a component: a component which can have children
(a composite component) and a component which is indivisible. A text (a
composite component) is divided into paragraphs (composite components); a
paragraph is divided into sentences (composite components); a sentence is
divided into lexemes (composite components); a lexeme is divided into a word
composite component and an optional word component(but only punctuation signs,
no word characters); a word is divided into symbol component (not composite).

There is a chain of parsers used to build a text composite component: a text
parser, a paragraph parser, a sentence parser, a lexeme parser, a word parser.

There is a client which accepts a text composite component. It is used to
perform sorting operations(sorting paragraphs by sentence count, sorting words
by length, sorting sentences by occurrence count of the specified character).

An application has the following structure (packages):

 - by.training.task02.client        for a client of a text composite component
 - by.training.task02.component     types of a component
 - by.training.task02.constant      various constants
 - by.training.task02.exception     exceptions used by the application
 - by.training.task02.parser        for parsers
 - by.training.task02.reader        for a reader for reading application data
 - test.task02.sorting              for testing sorting operations
 
 Used technologies: TestNG, Log4j2.
 
Needed libraries: log4j-api-2.11.2.jar, log4j-core-2.11.2.jar,
jcommander-1.72.jar, testng-6.9.9.jar.

|----------------------------------------------------------------------|
| Important note: in Eclipse TestNG plug-in version 6.9.10 requires an |
|                 Internet connection for test to be run (otherwise    |
|                 problems with DTD).                                  |
|----------------------------------------------------------------------|