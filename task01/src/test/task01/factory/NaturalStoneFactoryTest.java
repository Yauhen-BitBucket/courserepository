/*
 * NaturalStoneFactoryTest.java
 * A TestNG class used to test functionality of the NaturalStoneFactory class.
 */
package test.task01.factory;


import static by.training.task01.constant.CaratPrices.CARAT_PRICE_7;

import static by.training.task01.constant.PlaceCodes.PLACE_CODE_1;

import static by.training.task01.constant.TranspaerncyLevels.TRANSPARENCY_5;

import static by.training.task01.constant.Weights.WEIGHT_1;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertThrows;

import by.training.task01.entity.NaturalStone;
import by.training.task01.entity.StoneName;

import by.training.task01.exception.StoneException;

import by.training.task01.factory.NaturalStoneFactory;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


/**
 * A TestNG class used to test functionality of the NaturalStoneFactory class.
 * @author Yauhen Sazonau
 * @version 1.0, 05/13/19
 * @since 1.0
 */
public class NaturalStoneFactoryTest {
    /**
     * A natural stone. Can't be a <code>null</code> value.
     */
    private static final NaturalStone NATURAL_STONE
            = new NaturalStone(StoneName.DIAMOND, PLACE_CODE_1, CARAT_PRICE_7,
                               WEIGHT_1, TRANSPARENCY_5);
    /**
     * A factory used to create natural stones. Can't be a <code>null</code>
     * value.
     */
    private static final NaturalStoneFactory NATURAL_STONE_FACTORY
            = new NaturalStoneFactory();
    /**
     * A string used to create a natural stone. The identifier holds the value
     * of "500 0.1 10 2 DIAMOND".
     */
    private static final String PARAMETER_STRING = "500 0.1 10 2 DIAMOND";
    /**
     * Creates data used by a test method.
     * @return data used by a test method
     */
    @DataProvider(name = "incorrectDataProvider")
    public Object[][] createData() {
        return new Object[][] {{null}, {"500 0.1"}, {"500 b 30 5 DIAMOND"},
                {"500 1.1 30 5 DIAMOND"}};
    }
    /**
     * Verifies functionality of the <code>createStone</code> method of the
     * NaturalStoneFactory class when it should throw an exception.
     * @param parameters the <code>String</code> value which is a space
     *                   separated list of parameters needed to create a
     *                   natural stone. The first element represents a
     *                   carat price (it should be a <code>float</code> value
     *                   which falls into an allowed range of values for the
     *                   carat price); the second - a transparency level (it
     *                   should be a <code>float</code> value which falls into
     *                   an allowed range of values for the transparency
     *                   level); the third - a weight (it should be a
     *                   <code>byte</code> value which falls into an allowed
     *                   range of values for the weight); the fourth - an
     *                   extraction place code (it should be a
     *                   <code>short</code> value which falls into an allowed
     *                   range of values for the extraction place code); the
     *                   fifth - a name (it should be a <code>String</code>
     *                   representing one of predefined <code>enum</code>
     *                   constants for the name).
     * @see NaturalStoneFactory#createStone(String)
     */
    @Test(description = "Verifies functionality of the 'createStone' method "
                        + "of the NaturalStoneFactory class when it should "
                        + "throw an exception.",
          dataProvider = "incorrectDataProvider")
    public void testNaturalStoneCreationFailure(final String parameters) {
        assertThrows(StoneException.class,
                () -> NATURAL_STONE_FACTORY.createStone(parameters));
    }
    /**
     * Verifies functionality of the <code>createStone</code> method of the
     * NaturalStoneFactory class when it should successfully create a natural
     * stone.
     */
    @Test(description = "Verifies functionality of the 'createStone' method "
                        + "of the NaturalStoneFactory class when it should "
                        + "successfully create a natural stone.")
    public void testNaturalStoneCreationSuccess() {
        NaturalStone actual
                = NATURAL_STONE_FACTORY.createStone(PARAMETER_STRING);
        assertEquals(actual, NATURAL_STONE, "The factory successfully "
                                            + "created a natural stone.");
    }
}
