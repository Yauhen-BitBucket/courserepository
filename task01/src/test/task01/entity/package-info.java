/**
 * Contains a TestNG class for verifying functionality of the ArtificialStone
 * class.
 * @author Yauhen Sazonau
 * @since 1.0
 */
package test.task01.entity;
