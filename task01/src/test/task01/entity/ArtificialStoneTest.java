/*
 * ArtificialStoneTest.java
 * A TestNG class used to test functionality of the ArtificialStone class.
 */
package test.task01.entity;


import static org.testng.Assert.assertEquals;

import by.training.task01.entity.ArtificialStone;
import by.training.task01.entity.StoneName;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


/**
 * A TestNG class used to test functionality of the ArtificialStone class.
 * @author Yauhen Sazonau
 * @version 1.0, 05/12/19
 * @since 1.0
 */
public class ArtificialStoneTest {
    /**
     * The weight of an artificial stone. The identifier holds the value of
     * "50".
     */
    private static final byte WEIGHT = 100;
    /**
     * The price per one carat of an artificial stone. The identifier holds
     * the value of "50".
     */
    private static final float CARAT_PRICE = 50;
    /**
     * The level of transparency of an artificial stone. The identifier holds
     * the value of "0.2".
     */
    private static final float TRANSPARENCY_1 = 0.2f;
    /**
     * The level of transparency of an artificial stone. The identifier holds
     * the value of "0.25".
     */
    private static final float TRANSPARENCY_2 = 0.25f;
    /**
     * The number of days needed to produce a stone. The identifier holds
     * the value of "99".
     */
    private static final short GROWTH_PERIOD_1 = 99;
    /**
     * The number of days needed to produce a stone. The identifier holds
     * the value of "100".
     */
    private static final short GROWTH_PERIOD_2 = 100;
    /**
     * An artificial stone. Can't be a <code>null</code> value.
     */
    private static final ArtificialStone ARTIFICIAL_STONE
            = new ArtificialStone(GROWTH_PERIOD_2, StoneName.DIAMOND,
                                  CARAT_PRICE, WEIGHT, TRANSPARENCY_1);
    /**
     * Creates data (various objects) used by a test method.
     * @return data (various objects) used by a test method
     */
    @DataProvider(name = "objectProvider")
    public Object[][] createData() {
        return new Object[][] {
            {ARTIFICIAL_STONE, true, "The object is equal to itself."},
            {null, false, "The object isn't equal to a 'null' value."},
            {new Object(), false,
             "The object isn't equal to an object from a different "
             + "hierarchy."},
            {new ArtificialStone(GROWTH_PERIOD_1, StoneName.DIAMOND,
                                 CARAT_PRICE, WEIGHT, TRANSPARENCY_2),
             false,
             "The object isn't equal to an object from its hierarchy with "
             + "different field values."},
            {new ArtificialStone(GROWTH_PERIOD_2, StoneName.DIAMOND,
                                 CARAT_PRICE, WEIGHT, TRANSPARENCY_1),
             true,
             "The object is equal to an object from its hierarchy with the "
             + "same field values."}};
    }
    /**
     * Verifies functionality of the <code>equals</code> method of the
     * ArtificialStone class.
     * @param stone the <code>Object</code> value with which to
     *              compare
     * @param result <code>boolean</code> value which is the expected result
     *        of the comparison
     * @param message the <code>String</code> value which is a test message
     * @see ArtificialStone#equals(Object)
     */
    @Test(description = "Verifies functionality of the 'equals' method of the "
            + "ArtificialStone class.", dataProvider = "objectProvider")
    public void testEquals(final Object stone,
            final boolean result, final String message) {
        boolean actual = ARTIFICIAL_STONE.equals(stone);
        assertEquals(actual, result, message);
    }
}
