/**
 * Contains a TestNG class for verifying functionality of the
 * CollectionNecklace class.
 * @author Yauhen Sazonau
 * @since 1.0
 */
package test.task01.repository;
