/*
 * CollectionNecklaceTest.java
 * A TestNG class used to test functionality of the CollectionNecklace class.
 */
package test.task01.repository;


import static by.training.task01.constant.ArrayIndexes.ARRAY_INDEX_0;
import static by.training.task01.constant.ArrayIndexes.ARRAY_INDEX_1;
import static by.training.task01.constant.ArrayIndexes.ARRAY_INDEX_2;
import static by.training.task01.constant.ArrayIndexes.ARRAY_INDEX_3;
import static by.training.task01.constant.ArrayIndexes.ARRAY_INDEX_4;
import static by.training.task01.constant.ArrayIndexes.ARRAY_INDEX_5;
import static by.training.task01.constant.ArrayIndexes.ARRAY_INDEX_6;

import static by.training.task01.constant.CaratPrices.CARAT_PRICE_1;
import static by.training.task01.constant.CaratPrices.CARAT_PRICE_2;
import static by.training.task01.constant.CaratPrices.CARAT_PRICE_3;
import static by.training.task01.constant.CaratPrices.CARAT_PRICE_4;
import static by.training.task01.constant.CaratPrices.CARAT_PRICE_5;
import static by.training.task01.constant.CaratPrices.CARAT_PRICE_6;
import static by.training.task01.constant.CaratPrices.CARAT_PRICE_7;
import static by.training.task01.constant.CaratPrices.CARAT_PRICE_8;
import static by.training.task01.constant.CaratPrices.CARAT_PRICE_9;
import static by.training.task01.constant.CaratPrices.CARAT_PRICE_10;
import static by.training.task01.constant.CaratPrices.CARAT_PRICE_11;
import static by.training.task01.constant.CaratPrices.CARAT_PRICE_12;
import static by.training.task01.constant.CaratPrices.CARAT_PRICE_13;
import static by.training.task01.constant.CaratPrices.CARAT_PRICE_14;
import static by.training.task01.constant.CaratPrices.CARAT_PRICE_15;
import static by.training.task01.constant.CaratPrices.CARAT_PRICE_16;

import static by.training.task01.constant.GrowthPeriods.GROWTH_TIME_1;
import static by.training.task01.constant.GrowthPeriods.GROWTH_TIME_2;
import static by.training.task01.constant.GrowthPeriods.GROWTH_TIME_3;
import static by.training.task01.constant.GrowthPeriods.GROWTH_TIME_4;
import static by.training.task01.constant.GrowthPeriods.GROWTH_TIME_5;
import static by.training.task01.constant.GrowthPeriods.GROWTH_TIME_6;
import static by.training.task01.constant.GrowthPeriods.GROWTH_TIME_7;

import static by.training.task01.constant.PlaceCodes.PLACE_CODE_1;
import static by.training.task01.constant.PlaceCodes.PLACE_CODE_2;
import static by.training.task01.constant.PlaceCodes.PLACE_CODE_3;
import static by.training.task01.constant.PlaceCodes.PLACE_CODE_4;
import static by.training.task01.constant.PlaceCodes.PLACE_CODE_5;
import static by.training.task01.constant.PlaceCodes.PLACE_CODE_6;
import static by.training.task01.constant.PlaceCodes.PLACE_CODE_7;
import static by.training.task01.constant.PlaceCodes.PLACE_CODE_8;
import static by.training.task01.constant.PlaceCodes.PLACE_CODE_9;
import static by.training.task01.constant.PlaceCodes.PLACE_CODE_10;
import static by.training.task01.constant.PlaceCodes.PLACE_CODE_11;

import static by.training.task01.constant.TranspaerncyLevels.TRANSPARENCY_1;
import static by.training.task01.constant.TranspaerncyLevels.TRANSPARENCY_2;
import static by.training.task01.constant.TranspaerncyLevels.TRANSPARENCY_3;
import static by.training.task01.constant.TranspaerncyLevels.TRANSPARENCY_4;
import static by.training.task01.constant.TranspaerncyLevels.TRANSPARENCY_5;
import static by.training.task01.constant.TranspaerncyLevels.TRANSPARENCY_6;
import static by.training.task01.constant.TranspaerncyLevels.TRANSPARENCY_7;
import static by.training.task01.constant.TranspaerncyLevels.TRANSPARENCY_8;
import static by.training.task01.constant.TranspaerncyLevels.TRANSPARENCY_9;
import static by.training.task01.constant.TranspaerncyLevels.TRANSPARENCY_10;
import static by.training.task01.constant.TranspaerncyLevels.TRANSPARENCY_11;
import static by.training.task01.constant.TranspaerncyLevels.TRANSPARENCY_12;
import static by.training.task01.constant.TranspaerncyLevels.TRANSPARENCY_13;
import static by.training.task01.constant.TranspaerncyLevels.TRANSPARENCY_14;

import static by.training.task01.constant.Weights.WEIGHT_1;
import static by.training.task01.constant.Weights.WEIGHT_2;
import static by.training.task01.constant.Weights.WEIGHT_3;
import static by.training.task01.constant.Weights.WEIGHT_4;
import static by.training.task01.constant.Weights.WEIGHT_5;
import static by.training.task01.constant.Weights.WEIGHT_6;
import static by.training.task01.constant.Weights.WEIGHT_7;
import static by.training.task01.constant.Weights.WEIGHT_8;
import static by.training.task01.constant.Weights.WEIGHT_9;
import static by.training.task01.constant.Weights.WEIGHT_10;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import by.training.task01.entity.ArtificialStone;
import by.training.task01.entity.NaturalStone;
import by.training.task01.entity.Stone;
import by.training.task01.entity.StoneName;

import by.training.task01.reader.DataReader;

import by.training.task01.repository.CollectionNecklace;
import by.training.task01.repository.Necklace;

import by.training.task01.specification.CharSearchSpecification;
import by.training.task01.specification.IdRangeSearchSpecification;
import by.training.task01.specification.IdSearchSpecification;
import by.training.task01.specification.NameSearchSpecification;
import by.training.task01.specification.SortingByNameSpecification;
import by.training.task01.specification.SortingByTranspAndWeightSpecification;
import by.training.task01.specification.Specification;
import by.training.task01.specification.TranspSearchSpecification;

import java.util.LinkedList;
import java.util.List;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


/**
 * A TestNG class used to test functionality of the CollectionNecklace class.
 * @author Yauhen Sazonau
 * @version 1.0, 05/13/19
 * @since 1.0
 */
public class CollectionNecklaceTest {
    /**
     * A first character used to retrieve stones. The identifier holds
     * the value of "S".
     */
    private static final char FIRST_CHARACTER = 'S';
    /**
     * Expected cost of all stones in a repository. The identifier holds
     * the value of "108506".
     */
    private static final float EXPECTED_COST = 108506f;
    /**
     * Expected weight of all stones in a repository. The identifier holds
     * the value of "589".
     */
    private static final float EXPECTED_WEIGHT = 589f;
    /**
     * A maximum transparency level used to retrieve stones. The identifier
     * holds the value of "0.31".
     */
    private static final float MAX_TRANSPARENCY = 0.31f;
    /**
     * A minimum transparency level used to retrieve stones. The identifier
     * holds the value of "0.31".
     */
    private static final float MIN_TRANSPARENCY = 0.31f;
    /**
     * A number of specifications used to test <code>query</code>. The
     * identifier holds the value of "7".
     */
    private static final int SPECIFICATION_COUNT = 7;
    /**
     * An identification number used to retrieve a stone. The identifier holds
     * the value of "13".
     */
    private static final long ID = 13;
    /**
     * A maximum identification number used to retrieve stones. The
     * identifier holds the value of "19".
     */
    private static final long MAX_ID = 19;
    /**
     * A minimum identification number used to retrieve stones. The
     * identifier holds the value of "19".
     */
    private static final long MIN_ID = 19;
    /**
     * A repository of stones. Can't be a <code>null</code> value.
     */
    private static final Necklace REPOSITORY
            = CollectionNecklace.getInstance();
    /**
     * An array of lists used to test various specifications. Can't be a
     * <code>null</code> value.
     */
    private static final Object[] SPEC_LISTS
            = new Object[SPECIFICATION_COUNT];
    /**
     * An array of specifications used to retrieve stones. Can't be a
     * <code>null</code> value.
     */
    private static final Specification[] SPEC_LIST
            = new Specification[SPECIFICATION_COUNT];
    /**
     * A stone name used to retrieve stones. The identifier holds the value of
     * "EMERALD".
     */
    private static final String NAME = "EMERALD";
    /**
     * Initializes specifications needed to perform queries and various
     * expected lists used to verify queries.
     */
    @BeforeTest(description = "Initializes specifications needed to perform "
                               + "queries and various expected lists used to "
                               + "verify queries.")
    public void configure() {
        initializeIdList();
        initializeNameList();
        initializeCharList();
        initializeIdRangeList();
        initializeTranspList();
        initializeSortedByNameList();
        initializeSortedByTranspAndWeightList();
        initializeSpecifications();
    }
    /**
     * Creates data (a request specification, an expected list of stones) used
     * by a test method.
     * @return data (a request specification, an expected list of stones) used
     *         by a test method
     */
    @DataProvider(name = "dataProvider")
    public Object[][] createData() {
        return new Object[][] {
            {SPEC_LIST[ARRAY_INDEX_0], SPEC_LISTS[ARRAY_INDEX_0],
             "A stone with a particular identification number was found."},
            {SPEC_LIST[ARRAY_INDEX_1], SPEC_LISTS[ARRAY_INDEX_1],
             "Stones with a particular name were found."},
            {SPEC_LIST[ARRAY_INDEX_2], SPEC_LISTS[ARRAY_INDEX_2],
             "Stones with a name which starts with a particular character "
             + "were found."},
            {SPEC_LIST[ARRAY_INDEX_3], SPEC_LISTS[ARRAY_INDEX_3],
             "Stones with a identification number which falls into a "
             + "particular range were found."},
            {SPEC_LIST[ARRAY_INDEX_4], SPEC_LISTS[ARRAY_INDEX_4],
             "Stones with a transparency level which falls into a particular "
             + "range were found."},
            {SPEC_LIST[ARRAY_INDEX_5], SPEC_LISTS[ARRAY_INDEX_5],
             "Stones sorted by name."},
            {SPEC_LIST[ARRAY_INDEX_6], SPEC_LISTS[ARRAY_INDEX_6],
             "Stones sorted by transparency level and weight."}};
    }
    /**
     * Verifies functionality of the <code>query</code> method of the
     * CollectionNecklace class.
     * @param spec the <code>Specification</code> value which is used to
     *             retrieve stones
     * @param expected the <code>List</code> value which is an expected list
     *                 to retrieve after querying using the specified
     *                 specification
     * @param message the <code>String</code> value which is a test message
     * @see CollectionNecklace#query(Specification)
     */
    @Test(description = "Verifies functionality of the 'query' method of the "
                        + "CollectionNecklace class.",
          dataProvider = "dataProvider",
          dependsOnMethods = {"testDataReading"})
    public void testQuery(final Specification spec,
            final List<Stone> expected, final String message) {
        List<Stone> actual = REPOSITORY.query(spec);
        assertEquals(actual, expected, message);
    }
    /**
     * Verifies functionality of the <code>getCost</code> method of the
     * CollectionNecklace class.
     * @see CollectionNecklace#getCost()
     */
    @Test(description = "Verifies functionality of the 'getCost' method of "
                        + "the CollectionNecklace class.",
          dependsOnMethods = {"testDataReading"})
    public void testCostCalculation() {
       float actual = REPOSITORY.getCost();
       assertEquals(actual,
               EXPECTED_COST, "The cost of all stones equals to "
                              + "an expected value.");
    }
    /**
     * Verifies functionality of the <code>getWeight</code> method of the
     * CollectionNecklace class.
     * @see CollectionNecklace#getWeight()
     */
    @Test(description = "Verifies functionality of the 'getWeight' method of "
                        + "the CollectionNecklace class.",
          dependsOnMethods = {"testDataReading"})
    public void testWeightCalculation() {
        float actual = REPOSITORY.getWeight();
        assertEquals(actual,
                EXPECTED_WEIGHT, "The weight of all stones equals to "
                                 + "an expected value.");
    }
    /**
     * Verifies that information about stones stored in a text file was
     * correctly read and that stones were created in a repository
     * based on that information.
     */
    @Test(description = "Verifies that information about stones stored in a "
                        + "text file was correctly read and that stones were "
                        + "created in a repository based on that information.")
    public void testDataReading() {
        boolean condition =  new DataReader().readData();
        assertTrue(condition, "Data was correctly read.");
    }
    /**
     * Initializes a list which will keep a stone with a particular
     * identification number.
     */
    private static void initializeIdList() {
        List<Stone> list = new LinkedList<>();
        NaturalStone stone
                = new NaturalStone(StoneName.PEARL, PLACE_CODE_3,
                                   CARAT_PRICE_1, WEIGHT_1, TRANSPARENCY_1);
        /* The main part of the method. */
        stone.setStoneId(ID);
        list.add(stone);
        SPEC_LISTS[ARRAY_INDEX_0] = list;
    }
    /**
     * Initializes a list which will keep stones with a particular name.
     */
    private static void initializeNameList() {
        ArtificialStone artificialStone
                = new ArtificialStone(GROWTH_TIME_1, StoneName.valueOf(NAME),
                                      CARAT_PRICE_2, WEIGHT_2,
                                      TRANSPARENCY_2);
        List<Stone> list = new LinkedList<>();
        NaturalStone naturalStone
                = new NaturalStone(StoneName.valueOf(NAME), PLACE_CODE_4,
                                   CARAT_PRICE_3, WEIGHT_1, TRANSPARENCY_3);
        /* The main part of the method. */
        list.add(naturalStone);
        list.add(artificialStone);
        SPEC_LISTS[ARRAY_INDEX_1] = list;
    }
    /**
     * Initializes a list which will keep stones with a name that starts with
     * a particular character.
     */
    private static void initializeCharList() {
        ArtificialStone artificialStone1
                = new ArtificialStone(GROWTH_TIME_2, StoneName.SAPPHIRE,
                                      CARAT_PRICE_4, WEIGHT_3, TRANSPARENCY_4);
        ArtificialStone artificialStone2
                = new ArtificialStone(GROWTH_TIME_3, StoneName.SAPPHIRE,
                                      CARAT_PRICE_3, WEIGHT_4, TRANSPARENCY_5);
        List<Stone> list = new LinkedList<>();
        /* The main part of the method. */
        list.add(artificialStone1);
        list.add(artificialStone2);
        SPEC_LISTS[ARRAY_INDEX_2] = list;
    }
    /**
     * Initializes a list which will keep stones with a identification number
     * that falls into a particular range.
     */
    private static void initializeIdRangeList() {
        NaturalStone naturalStone
                = new NaturalStone(StoneName.AMBER, PLACE_CODE_2,
                                   CARAT_PRICE_5, WEIGHT_5, TRANSPARENCY_4);
        List<Stone> list = new LinkedList<>();
        /* The main part of the method. */
        list.add(naturalStone);
        SPEC_LISTS[ARRAY_INDEX_3] = list;
    }
    /**
     * Initializes a list which will keep stones with a transparency level
     * that falls into a particular range.
     */
    private static void initializeTranspList() {
        NaturalStone naturalStone
                = new NaturalStone(StoneName.DIAMOND, PLACE_CODE_1,
                                   CARAT_PRICE_6, WEIGHT_6, TRANSPARENCY_6);
        List<Stone> list = new LinkedList<>();
        /* The main part of the method. */
        list.add(naturalStone);
        SPEC_LISTS[ARRAY_INDEX_4] = list;
    }
    /**
     * Initializes a list which will keep stones sorted by name.
     */
    private static void initializeSortedByNameList() {
        List<Stone> list = new LinkedList<>();
        /* The main part of the method. */
        list.add(new ArtificialStone(GROWTH_TIME_4, StoneName.AMBER,
                                     CARAT_PRICE_8, WEIGHT_4,
                                     TRANSPARENCY_7));
        list.add(new NaturalStone(StoneName.AMBER, PLACE_CODE_10,
                                  CARAT_PRICE_15, WEIGHT_8, TRANSPARENCY_13));
        list.add(new NaturalStone(StoneName.AMBER, PLACE_CODE_2,
                                  CARAT_PRICE_5, WEIGHT_5, TRANSPARENCY_4));
        list.add(new NaturalStone(StoneName.DIAMOND, PLACE_CODE_5,
                                  CARAT_PRICE_7, WEIGHT_7, TRANSPARENCY_5));
        list.add(new NaturalStone(StoneName.DIAMOND, PLACE_CODE_7,
                                  CARAT_PRICE_11, WEIGHT_8, TRANSPARENCY_5));
        list.add(new NaturalStone(StoneName.DIAMOND, PLACE_CODE_8,
                                  CARAT_PRICE_12, WEIGHT_9, TRANSPARENCY_10));
        list.add(new NaturalStone(StoneName.DIAMOND, PLACE_CODE_9,
                                  CARAT_PRICE_1, WEIGHT_1, TRANSPARENCY_3));
        list.add(new NaturalStone(StoneName.DIAMOND, PLACE_CODE_1,
                                  CARAT_PRICE_6, WEIGHT_6, TRANSPARENCY_6));
        list.add(new NaturalStone(StoneName.EMERALD, PLACE_CODE_4,
                                  CARAT_PRICE_3, WEIGHT_1, TRANSPARENCY_3));
        list.add(new ArtificialStone(GROWTH_TIME_1, StoneName.EMERALD,
                                     CARAT_PRICE_2, WEIGHT_2, TRANSPARENCY_2));
        list.add(new NaturalStone(StoneName.PEARL, PLACE_CODE_6,
                                  CARAT_PRICE_9, WEIGHT_2, TRANSPARENCY_8));
        list.add(new NaturalStone(StoneName.PEARL, PLACE_CODE_3,
                                  CARAT_PRICE_1, WEIGHT_1, TRANSPARENCY_1));
        list.add(new NaturalStone(StoneName.PEARL, PLACE_CODE_3,
                                  CARAT_PRICE_1, WEIGHT_1, TRANSPARENCY_1));
        list.add(new ArtificialStone(GROWTH_TIME_5, StoneName.RUBY,
                                     CARAT_PRICE_10, WEIGHT_7,
                                     TRANSPARENCY_9));
        list.add(new ArtificialStone(GROWTH_TIME_6, StoneName.RUBY,
                                     CARAT_PRICE_13, WEIGHT_10,
                                     TRANSPARENCY_11));
        list.add(new ArtificialStone(GROWTH_TIME_7, StoneName.RUBY,
                                     CARAT_PRICE_14, WEIGHT_3,
                                     TRANSPARENCY_12));
        list.add(new NaturalStone(StoneName.RUBY, PLACE_CODE_11,
                                  CARAT_PRICE_16, WEIGHT_6, TRANSPARENCY_14));
        list.add(new ArtificialStone(GROWTH_TIME_2, StoneName.SAPPHIRE,
                                     CARAT_PRICE_4, WEIGHT_3,
                                     TRANSPARENCY_4));
        list.add(new ArtificialStone(GROWTH_TIME_3, StoneName.SAPPHIRE,
                                     CARAT_PRICE_3, WEIGHT_4,
                                     TRANSPARENCY_5));
        SPEC_LISTS[ARRAY_INDEX_5] = list;
    }
    /**
     * Initializes a list which will keep stones sorted by transparency level
     * and weight.
     */
    private static void initializeSortedByTranspAndWeightList() {
        List<Stone> list = new LinkedList<>();
        /* The main part of the method. */
        list.add(new NaturalStone(StoneName.RUBY, PLACE_CODE_11,
                                  CARAT_PRICE_16, WEIGHT_6, TRANSPARENCY_14));
        list.add(new NaturalStone(StoneName.DIAMOND, PLACE_CODE_5,
                                  CARAT_PRICE_7, WEIGHT_7, TRANSPARENCY_5));
        list.add(new NaturalStone(StoneName.DIAMOND, PLACE_CODE_7,
                                  CARAT_PRICE_11, WEIGHT_8, TRANSPARENCY_5));
        list.add(new ArtificialStone(GROWTH_TIME_3, StoneName.SAPPHIRE,
                                     CARAT_PRICE_3, WEIGHT_4,
                                     TRANSPARENCY_5));
        list.add(new ArtificialStone(GROWTH_TIME_7, StoneName.RUBY,
                                     CARAT_PRICE_14, WEIGHT_3,
                                     TRANSPARENCY_12));
        list.add(new NaturalStone(StoneName.DIAMOND, PLACE_CODE_8,
                                  CARAT_PRICE_12, WEIGHT_9, TRANSPARENCY_10));
        list.add(new NaturalStone(StoneName.EMERALD, PLACE_CODE_4,
                                  CARAT_PRICE_3, WEIGHT_1, TRANSPARENCY_3));
        list.add(new NaturalStone(StoneName.DIAMOND, PLACE_CODE_9,
                                  CARAT_PRICE_1, WEIGHT_1, TRANSPARENCY_3));
        list.add(new NaturalStone(StoneName.DIAMOND, PLACE_CODE_1,
                                  CARAT_PRICE_6, WEIGHT_6, TRANSPARENCY_6));
        list.add(new ArtificialStone(GROWTH_TIME_6, StoneName.RUBY,
                                     CARAT_PRICE_13, WEIGHT_10,
                                     TRANSPARENCY_11));
        list.add(new NaturalStone(StoneName.AMBER, PLACE_CODE_2,
                                  CARAT_PRICE_5, WEIGHT_5, TRANSPARENCY_4));
        list.add(new ArtificialStone(GROWTH_TIME_2, StoneName.SAPPHIRE,
                                     CARAT_PRICE_4, WEIGHT_3,
                                     TRANSPARENCY_4));
        list.add(new ArtificialStone(GROWTH_TIME_5, StoneName.RUBY,
                                     CARAT_PRICE_10, WEIGHT_7,
                                     TRANSPARENCY_9));
        list.add(new ArtificialStone(GROWTH_TIME_1, StoneName.EMERALD,
                                     CARAT_PRICE_2, WEIGHT_2, TRANSPARENCY_2));
        list.add(new NaturalStone(StoneName.AMBER, PLACE_CODE_10,
                                  CARAT_PRICE_15, WEIGHT_8, TRANSPARENCY_13));
        list.add(new ArtificialStone(GROWTH_TIME_4, StoneName.AMBER,
                                     CARAT_PRICE_8, WEIGHT_4,
                                     TRANSPARENCY_7));
        list.add(new NaturalStone(StoneName.PEARL, PLACE_CODE_6,
                                  CARAT_PRICE_9, WEIGHT_2, TRANSPARENCY_8));
        list.add(new NaturalStone(StoneName.PEARL, PLACE_CODE_3,
                                  CARAT_PRICE_1, WEIGHT_1, TRANSPARENCY_1));
        list.add(new NaturalStone(StoneName.PEARL, PLACE_CODE_3,
                                  CARAT_PRICE_1, WEIGHT_1, TRANSPARENCY_1));
        SPEC_LISTS[ARRAY_INDEX_6] = list;
    }
    /**
     * Initializes an array of specification.
     */
    private static void initializeSpecifications() {
        SPEC_LIST[ARRAY_INDEX_0] = new IdSearchSpecification(ID);
        SPEC_LIST[ARRAY_INDEX_1] = new NameSearchSpecification(NAME);
        SPEC_LIST[ARRAY_INDEX_2]
                = new CharSearchSpecification(FIRST_CHARACTER);
        SPEC_LIST[ARRAY_INDEX_3]
                = new IdRangeSearchSpecification(MIN_ID, MAX_ID);
        SPEC_LIST[ARRAY_INDEX_4]
                = new TranspSearchSpecification(MIN_TRANSPARENCY,
                                                MAX_TRANSPARENCY);
        SPEC_LIST[ARRAY_INDEX_5] = new SortingByNameSpecification();
        SPEC_LIST[ARRAY_INDEX_6]
                = new SortingByTranspAndWeightSpecification();
    }
}
