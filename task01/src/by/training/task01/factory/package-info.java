/**
 * Contains factories for creating stones which belong to the stone hierarchy.
 * @author Yauhen Sazonau
 * @since 1.0
 */
package by.training.task01.factory;
