/*
 * NaturalStoneFactory.java
 * This factory is used to create natural stones.
 */
package by.training.task01.factory;


import static by.training.task01.constant.ArrayIndexes.ARRAY_INDEX_0;
import static by.training.task01.constant.ArrayIndexes.ARRAY_INDEX_1;
import static by.training.task01.constant.ArrayIndexes.ARRAY_INDEX_2;
import static by.training.task01.constant.ArrayIndexes.ARRAY_INDEX_3;
import static by.training.task01.constant.ArrayIndexes.ARRAY_INDEX_4;

import static by.training.task01.validator.FieldValidator.validatePlaceCode;
import static by.training.task01.validator.FieldValidator.validateTriplet;

import by.training.task01.entity.NaturalStone;
import by.training.task01.entity.StoneName;

import by.training.task01.exception.StoneException;


/**
 * This factory is used to create natural stones.
 * @author Yauhen Sazonau
 * @version 1.0, 05/03/19
 * @since 1.0
 */
public class NaturalStoneFactory extends StoneFactory {
    /**
     * Represents the number of parameters needed to create a natural stone.
     * The identifier holds the value of "5".
     */
    private static final byte PARAMETER_QUANTITY = 5;
    /**
     * Represents a part of a string which will be incorporated into an
     * exception message. The identifier holds the value of
     * "A natural stone can't be created ".
     */
    private static final String EXCEPTION_STRING
            = "A natural stone can't be created ";
    /**
     * Creates an instance of this class.
     */
    public NaturalStoneFactory() {
        /* The default initialization is sufficient. */
    }
    /**
     * Creates a natural stone using specified parameters.
     * @param parameters the <code>String</code> value which is a space
     *                   separated list of parameters needed to create a
     *                   natural stone. The first element represents a
     *                   carat price (it should be a <code>float</code> value
     *                   which falls into an allowed range of values for the
     *                   carat price); the second - a transparency level (it
     *                   should be a <code>float</code> value which falls into
     *                   an allowed range of values for the transparency
     *                   level); the third - a weight (it should be a
     *                   <code>byte</code> value which falls into an allowed
     *                   range of values for the weight); the fourth - an
     *                   extraction place code (it should be a
     *                   <code>short</code> value which falls into an allowed
     *                   range of values for the extraction place code); the
     *                   fifth - a name (it should be a <code>String</code>
     *                   representing one of predefined <code>enum</code>
     *                   constants for the name).
     * @return a newly created natural stone. <code>null</code> value is
     *         prohibited.
     * @throws StoneException if a natural stone can't be created due to
     *                        invalid arguments or if the argument is a
     *                        <code>null</code> value
     * @see by.training.task01.entity.StoneName
     */
    public NaturalStone createStone(final String parameters) {
        byte weight;
        float caratPrice;
        float transparency;
        Object[] params;
        short placeCode;
        StoneName name;
        if (parameters == null) {
            throw new StoneException("The argument is a null value.");
        }
        /* The main part of the method. */
        params = parseParameters(parameters);
        caratPrice = (Float) params[ARRAY_INDEX_0];
        transparency = (Float) params[ARRAY_INDEX_1];
        weight = (Byte) params[ARRAY_INDEX_2];
        placeCode = (Short) params[ARRAY_INDEX_3];
        name = (StoneName) params[ARRAY_INDEX_4];
        if ((!validateTriplet(caratPrice, transparency, weight))
                || (!validatePlaceCode(placeCode))) {
            throw new StoneException(EXCEPTION_STRING
                    + "beacause parameters don't comply with boundary "
                    + "constraints");
            }
        return (new NaturalStone(name, placeCode, caratPrice, weight,
                                 transparency));
    }
    /**
     * Retrieves parameters needed to create a natural stone from the
     * specified argument as an array of corresponding objects.
     * @param parameters the <code>String</code> value which is a space
     *                   separated list of parameters needed to create a
     *                   natural stone. The first element represents a
     *                   carat price (it should be a <code>float</code> value
     *                   which falls into an allowed range of values for the
     *                   carat price); the second - a transparency level (it
     *                   should be a <code>float</code> value which falls into
     *                   an allowed range of values for the transparency
     *                   level); the third - a weight (it should be a
     *                   <code>byte</code> value which falls into an allowed
     *                   range of values for the weight); the fourth - an
     *                   extraction place code (it should be a
     *                   <code>short</code> value which falls into an allowed
     *                   range of values for the extraction place code); the
     *                   fifth - a name (it should be a <code>String</code>
     *                   representing one of predefined <code>enum</code>
     *                   constants for the name).
     * @return an array of objects which represents parameters
     * @throws StoneException if an array of arguments cannot be created
     */
    private Object[] parseParameters(final String parameters) {
        /*
         * The implementation of this method is identical to the one for the
         * ArtificialStoneFactory. It's just a coincidence that artificial and
         * natural stones are created using the same number of parameters and
         * that types of these parameters are identical. So the implementation
         * is given */
        Object[] result;
        String[] params = parameters.split(" ");
        /* The main part of the method. */
        if (params.length != PARAMETER_QUANTITY) {
            throw new StoneException("Incorrect quantity of parameters.");
        }
        result = new Object[PARAMETER_QUANTITY];
        try {
            result[ARRAY_INDEX_0] = Float.parseFloat(params[ARRAY_INDEX_0]);
            result[ARRAY_INDEX_1] = Float.parseFloat(params[ARRAY_INDEX_1]);
            result[ARRAY_INDEX_2] = Byte.parseByte(params[ARRAY_INDEX_2]);
            result[ARRAY_INDEX_3] = Short.parseShort(params[ARRAY_INDEX_3]);
            result[ARRAY_INDEX_4] = StoneName.valueOf(params[ARRAY_INDEX_4]);
        } catch (IllegalArgumentException e) {
            throw new StoneException("Parameters are invalid", e);
        }
        return result;
    }
}
