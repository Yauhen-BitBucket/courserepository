/*
 * StoneFactory.java
 * This class describes a general contract which a stone factory should
 * correspond to. It plays role of a creator of a stone: an object that
 * belongs to the stone hierarchy.
 */
package by.training.task01.factory;


import by.training.task01.entity.Stone;

import by.training.task01.exception.StoneException;


/**
 * This class describes a general contract which a stone factory should
 * correspond to. It plays role of a creator of a stone: an object that
 * belongs to the stone hierarchy.
 * @author Yauhen Sazonau
 * @version 1.0, 05/02/19
 * @since 1.0
 */
public abstract class StoneFactory {
    /**
     * Creates an instance of this class.
     */
    public StoneFactory() {
        /* The default initialization is sufficient. */
    }
    /**
     * Creates a stone using specified parameters.
     * @param parameters the <code>String</code> value which is a space
     *                   separated list of parameters needed to create a stone
     * @return a newly created stone. <code>null</code> value is prohibited.
     */
    public abstract Stone createStone(String parameters);
    /**
     * Creates a stone factory depending on the specified stone type.
     * @param stoneType the <code>String</code> value which is a stone type.
     *                  Accepted values are "artificial", "natural".
     *                  <code>null</code> value is prohibited.
     * @return a stone factory depending on the specified stone type
     * @throws StoneException if the specified stone type is not supported or
     *                        is a <code>null</code> value
     */
    public static StoneFactory getStoneFactory(final String stoneType) {
        boolean isArtificial = "artificial".equals(stoneType);
        boolean isNatural = "natural".equals(stoneType);
        StoneFactory factory;
        /* The main part of the method. */
        if (stoneType == null) {
            throw new StoneException("The type of a stone mustn't be a null "
                    + "value.");
        }
        if (!(isArtificial || isNatural)) {
            throw new StoneException("The specified stone type is not "
                    + "supported. The supplied value is '" + stoneType
                    + "'.");
        }
        if (isArtificial) {
            factory = new ArtificialStoneFactory();
        } else {
            factory = new NaturalStoneFactory();
        }
        return factory;
    }
}
