/*
 * NameSearchSpecification.java
 * Represents a search criterion with which to retrieve stones, when they are
 * stored in memory. Retrieved stones have a name represented by this
 * criterion.
 */
package by.training.task01.specification;


import by.training.task01.entity.Stone;
import by.training.task01.entity.StoneName;

import by.training.task01.exception.StoneException;

import java.util.Optional;


/**
 * Represents a search criterion with which to retrieve stones, when they are
 * stored in memory. Retrieved stones have a name represented by this
 * criterion.
 * @author Yauhen Sazonau
 * @version 1.0, 05/06/19
 * @since 1.0
 */
public class NameSearchSpecification implements SearchSpecification<Stone> {
    /**
     * The name of a stone used to search for stones. <code>null</code> value
     * is prohibited.
     */
    private Optional<StoneName> stoneName;
    /**
     * A user supplied name of a stone used to search for stones. Comparison
     * is fulfilled in case-insensitive manner.<code>null</code> value is
     * prohibited.
     */
    private String name;
    /**
     * Constructs an instance if this class with the specified argument.
     * @param suppliedName the <code>String</code> value which is a name used
     *                     to search for stones. Comparison is fulfilled in
     *                     case-insensitive manner.
     * @throws StoneException if the argument is a <code>null</code> value
     */
    public NameSearchSpecification(final String suppliedName) {
        calculateStoneName(suppliedName);
    }
    /**
     * Retrieves a user supplied name of a stone used to search for stones.
     * @return a user supplied name of a stone used to search for stones
     */
    public String getName() {
        return name;
    }
    /**
     * Sets the user supplied name of a stone used to search for stones.
     * @param suppliedName the <code>String</code> value which is a name used
     *                     to search for stones. Comparison is fulfilled in
     *                     case-insensitive manner.
     * @throws StoneException if the argument is a <code>null</code> value
     */
    public void setName(final String suppliedName) {
        calculateStoneName(suppliedName);
    }
    /**
     * Sets the value of the "stoneName" field depending on the user supplied
     * name of a stone used to search for stones.
     * @param suppliedName the <code>String</code> value which is a name used
     *                     to search for stones. Comparison is fulfilled in
     *                     case-insensitive manner.
     * @throws StoneException if the argument is a <code>null</code> value
     */
    private void calculateStoneName(final String suppliedName) {
        boolean isSet = false;
        String upperCased;
        /* The main part of the method. */
        if (suppliedName == null) {
            throw new StoneException("The 'suppliedName' argument is null.");
        }
        name = suppliedName;
        for (StoneName tempName : StoneName.values()) {
            upperCased = suppliedName.toUpperCase();
            if (upperCased.equals(tempName.name())) {
                stoneName = Optional.of(tempName);
                isSet = true;
                break;
            }
        }
        if (!isSet) {
            stoneName = Optional.<StoneName>empty();
        }
    }
    /**
     * Determines whether the specified stone has the name represented by this
     * criterion. Comparison is fulfilled in case-insensitive manner.
     * @param stone the <code>Stone</code> value which is a stone to be
     *              verified
     * @throws StoneException if the argument is a <code>null</code> value
     */
    @Override
    public boolean isRetrieved(final Stone stone) {
        StoneName storedName;
        /* The main part of the method. */
        if (stone == null) {
            throw new StoneException("The 'stone' argument is null.");
        }
        if (!(stoneName.isPresent())) {
            return false;
        }
        storedName = stoneName.get();
        return (storedName.equals(stone.getStoneName()));
    }
}
