/*
 * SortingByNameSpecification.java
 * Represents a criterion with which to sort retrieved elements (all
 * elements are retrieved, but the order matters), when they are stored in
 * memory. All elements will be sorted by name.
 */
package by.training.task01.specification;


import java.util.Comparator;

import by.training.task01.entity.Stone;
import by.training.task01.entity.StoneName;


/**
 * Represents a criterion with which to sort retrieved elements (all
 * elements are retrieved, but the order matters), when they are stored in
 * memory. All elements will be sorted by name.
 * @author Yauhen Sazonau
 * @version 1.0, 05/14/19
 * @since 1.0
 */
public class SortingByNameSpecification
        implements SortingSpecification<Stone> {
    /**
     * Retrieves a <code>Comparator</code> instance used to sort retrieved
     * stones by name (all elements are retrieved, but the order matters).
     * @return a <code>Comparator</code> instance used to sort retrieved
     *         stones by name
     * @see Comparator
     */
    @Override
    public Comparator<Stone> retrieveComparator() {
        return (Comparator.<Stone, String>comparing(stone -> {
                StoneName stoneName = stone.getStoneName();
                return stoneName.name(); }));
    }
}
