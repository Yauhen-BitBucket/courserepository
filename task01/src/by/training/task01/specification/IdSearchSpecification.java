/*
 * IdSearchSpecification.java
 * Represents a search criterion with which to retrieve stones, when they are
 * stored in memory. Retrieved stones have an identification number which
 * equals the number represented by this criterion.
 */
package by.training.task01.specification;


import by.training.task01.entity.Stone;

import by.training.task01.exception.StoneException;


/**
 * Represents a search criterion with which to retrieve stones, when they are
 * stored in memory. Retrieved stones have an identification number which
 * equals the number represented by this criterion.
 * @author Yauhen Sazonau
 * @version 1.0, 05/04/19
 * @since 1.0
 */
public class IdSearchSpecification implements SearchSpecification<Stone> {
    /**
     * Represents the minimum value the stone identification number can have.
     * The identifier holds the value of "1".
     */
    public static final long STONE_ID_MIN_VALUE = 1;
    /**
     * The identification number of a stone to be searched for. It must be
     * positive.
     */
    private long stoneId;
    /**
     * Constructs an instance if this class with the specified argument.
     * @param id the <code>long</code> value which is the identification
     *           number of a stone to be searched for. It must be positive.
     * @throws StoneException if the argument isn't positive
     */
    public IdSearchSpecification(final long id) {
        if (id < STONE_ID_MIN_VALUE) {
            throw new StoneException("The 'stoneId' argument must be "
                    + "positive.");
        }
        this.stoneId = id;
    }
    /**
     * Retrieves the identification number of a stone to be searched for.
     * @return the identification number of a stone to be searched for
     */
    public long getStoneId() {
        return stoneId;
    }
    /**
     * Sets the identification number of a stone to be searched for.
     * @param id the <code>long</code> value which is the identification
     *           number of a stone to be set
     * @throws StoneException if the argument isn't positive
     */
    public void setStoneId(final long id) {
        if (id < STONE_ID_MIN_VALUE) {
            throw new StoneException("The 'stoneId' argument must be "
                    + "positive.");
        }
        this.stoneId = id;
    }
    /**
     * Determines whether the specified stone has the identification number
     * represented by this criterion.
     * @param stone the <code>Stone</code> value which is a stone to be
     *              verified
     * @return <code>true</code> if the specified stone has the identification
     *         number represented by this criterion, <code>false</code>
     *         otherwise
     * @throws StoneException if the argument is a <code>null</code> value
     */
    @Override
    public boolean isRetrieved(final Stone stone) {
        if (stone == null) {
            throw new StoneException("The 'stone' argument is null.");
        }
        return (stoneId == stone.getStoneId());
    }
}
