/*
 * Specification.java
 * Represents a criterion with which to retrieve stones.
 */
package by.training.task01.specification;


/**
 * Represents a criterion with which to retrieve stones.
 * @author Yauhen Sazonau
 * @version 1.0, 05/04/19
 * @since 1.0
 */
public interface Specification {
    /* It's intended for extension. */
}
