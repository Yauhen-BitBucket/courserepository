/*
 * IdRangeSearchSpecification.java
 * Represents a search criterion with which to retrieve stones, when they are
 * stored in memory. Retrieved stones have an identification number which
 * falls into the range represented by this criterion.
 */
package by.training.task01.specification;


import by.training.task01.entity.Stone;

import by.training.task01.exception.StoneException;


/**
 * Represents a search criterion with which to retrieve stones, when they are
 * stored in memory. Retrieved stones have an identification number which
 * falls into the range represented by this criterion.
 * @author Yauhen Sazonau
 * @version 1.0, 05/06/19
 * @since 1.0
 */
public class IdRangeSearchSpecification
        implements SearchSpecification<Stone> {
    /**
     * The maximum identification number used to search for stones. It must be
     * positive.
     */
    private long maximumId;
    /**
     * The minimum identification number used to search for stones. It must be
     * positive.
     */
    private long minimumId;
    /**
     * Constructs an instance if this class with the specified arguments.
     * @param min the <code>long</code> value which is a minimum
     *            identification number used to search for stones. It must be
     *            positive.
     * @param max the <code>long</code> value which is a maximum
     *            identification number used to search for stones. It must be
     *            positive.
     * @throws StoneException if arguments are not positive or the
     *                        <code>min</code> argument is greater than the
     *                        <code>max</code> argument
     */
    public IdRangeSearchSpecification(final long min, final long max) {
        setRange(min, max);
    }
    /**
     * Retrieves the maximum identification number used to search for stones.
     * @return the maximum identification number used to search for stones
     */
    public long getMaximumId() {
        return maximumId;
    }
    /**
     * Retrieves the minimum identification number used to search for stones.
     * @return the minimum identification number used to search for stones
     */
    public long getMinimumId() {
        return minimumId;
    }
    /**
     * Sets the range used to retrieve stones.
     * @param min the <code>long</code> value which is a minimum
     *            identification number used to search for stones. It must be
     *            positive.
     * @param max the <code>long</code> value which is a maximum
     *            identification number used to search for stones. It must be
     *            positive.
     * @throws StoneException if arguments are not positive or the
     *                        <code>min</code> argument is greater than the
     *                        <code>max</code> argument
     */
    public void setRange(final long min, final long max) {
        if ((min < IdSearchSpecification.STONE_ID_MIN_VALUE)
                || (max < IdSearchSpecification.STONE_ID_MIN_VALUE)
                || (min > max)) {
            throw new StoneException("Arguments must be positive and the "
                    + "'min' argument mustn't be greater than the 'max' "
                    + "argument. 'min' = " + min + ", 'max' = " + max + ".");
        }
        minimumId = min;
        maximumId = max;
    }
    /**
     * Determines whether the specified stone has an identification number
     * which falls into the range represented by this criterion.
     * @param stone the <code>Stone</code> value which is a stone to be
     *              verified
     * @throws StoneException if the argument is a <code>null</code> value
     */
    @Override
    public boolean isRetrieved(final Stone stone) {
        long stoneId;
        /* The main part of the method. */
        if (stone == null) {
            throw new StoneException("The 'stone' argument is null.");
        }
        stoneId = stone.getStoneId();
        return (!((stoneId < minimumId) || (stoneId > maximumId)));
    }
}
