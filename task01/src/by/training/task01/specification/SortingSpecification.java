/*
 * SortingSpecification.java
 * Represents a criterion with which to sort retrieved elements (all
 * elements are retrieved, but the order matters), when they are stored in
 * memory.
 */
package by.training.task01.specification;


import java.util.Comparator;


/**
 * Represents a criterion with which to sort retrieved elements (all
 * elements are retrieved, but the order matters), when they are stored in
 * memory.
 * @author Yauhen Sazonau
 * @param <T> defines the base type of elements which are allowed to be stored
 *            in a collection
 * @version 1.0, 05/14/19
 * @since 1.0
 */
public interface SortingSpecification<T> extends Specification {
    /**
     * Retrieves a <code>Comparator</code> instance used to sort retrieved
     * elements (all elements are retrieved, but the order matters).
     * @return a <code>Comparator</code> instance used to sort retrieved
     *         elements
     * @see Comparator
     */
    Comparator<T> retrieveComparator();
}
