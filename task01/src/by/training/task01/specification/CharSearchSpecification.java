/*
 * CharSearchSpecification.java
 * Represents a search criterion with which to retrieve stones, when they are
 * stored in memory. Retrieved stones have a name which starts from the
 * character represented by this criterion.
 */
package by.training.task01.specification;


import by.training.task01.entity.Stone;
import by.training.task01.entity.StoneName;

import by.training.task01.exception.StoneException;


/**
 * Represents a search criterion with which to retrieve stones, when they are
 * stored in memory. Retrieved stones have a name which starts from the
 * character represented by this criterion.
 * @author Yauhen Sazonau
 * @version 1.0, 05/06/19
 * @since 1.0
 */
public class CharSearchSpecification implements SearchSpecification<Stone> {
    /**
     * Represents the index of the first character in a string. The identifier
     * holds the value of "0".
     */
    private static final int FIRST_CHARACTER_INDEX = 0;
    /**
     * The character used to search for stones.
     */
    private char firstCharacter;
    /**
     * Constructs an instance if this class with the specified argument.
     * @param character the <code>char</code> value used to search for stones
     */
    public CharSearchSpecification(final char character) {
       firstCharacter = character;
    }
    /**
     * Retrieves the character used to search for stones.
     * @return the character used to search for stones
     */
    public char getFirstCharacter() {
        return firstCharacter;
    }
    /**
     * Sets the character used to search for stones.
     * @param character the <code>char</code> value used to search for
     *                       stones to be set
     */
    public void setFirstCharacter(final char character) {
        this.firstCharacter = character;
    }
    /**
     * Determines whether the specified stone has a name which starts from
     * the character represented by this criterion.
     * @param stone the <code>Stone</code> value which is a stone to be
     *              verified
     * @throws StoneException if the argument is a <code>null</code> value
     */
    @Override
    public boolean isRetrieved(final Stone stone) {
        StoneName  stoneName;
        String stringName;
        /* The main part of the method. */
        if (stone == null) {
            throw new StoneException("The 'stone' argument is null.");
        }
        stoneName = stone.getStoneName();
        stringName = stoneName.name();
        return (stringName.charAt(FIRST_CHARACTER_INDEX) == firstCharacter);
    }
}
