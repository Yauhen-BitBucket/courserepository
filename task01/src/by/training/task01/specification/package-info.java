/**
 * Contains a specification interface used to fulfill search requests and its
 * various implementations. It is available to perform the following queries:
 * to search a stone with the specified identification number, to search
 * stones with the specified name, to search stones with a name which starts
 * with the specified character, to search stones with an identification
 * number which falls into the specified range, to search stones with a
 * transparency level which falls into the specified range.
 * @author Yauhen Sazonau
 * @since 1.0
 */
package by.training.task01.specification;
