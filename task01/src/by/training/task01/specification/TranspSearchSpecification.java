/*
 * TranspSearchSpecification.java
 * Represents a search criterion with which to retrieve stones, when they are
 * stored in memory. Retrieved stones have a transparency level which falls
 * into the range represented by this criterion.
 */
package by.training.task01.specification;


import by.training.task01.entity.Stone;

import by.training.task01.exception.StoneException;

import by.training.task01.validator.FieldValidator;


/**
 * Represents a search criterion with which to retrieve stones, when they are
 * stored in memory. Retrieved stones have a transparency level which falls
 * into the range represented by this criterion.
 * @author Yauhen Sazonau
 * @version 1.0, 05/06/19
 * @since 1.0
 */
public class TranspSearchSpecification
        implements SearchSpecification<Stone> {
    /**
     * The maximum transparency level used to search for stones. It mustn't be
     * less then zero and greater than one.
     */
    private float maximumTransparency;
    /**
     * The minimum transparency level used to search for stones. It mustn't be
     * less then zero and greater than one.
     */
    private float minimumTransparency;
    /**
     * Constructs an instance if this class with the specified arguments.
     * @param min the <code>float</code> value which is a minimum
     *            transparency level used to search for stones. It mustn't be
     *            less then zero and greater than one.
     * @param max the <code>float</code> value which is a maximum
     *            transparency level used to search for stones. It mustn't be
     *            less then zero and greater than one.
     * @throws StoneException if arguments are less then zero or greater than
     *                        one; if the <code>min</code> argument is greater
     *                        than the <code>max</code> argument
     */
    public TranspSearchSpecification(final float min, final float max) {
        setRange(min, max);
    }
    /**
     * Retrieves the maximum transparency level used to search for stones.
     * @return the maximum transparency level used to search for stones
     */
    public float getMaximumTransparency() {
        return maximumTransparency;
    }
    /**
     * Retrieves the minimum transparency level used to search for stones.
     * @return the minimum transparency level used to search for stones
     */
    public float getMinimumTransparency() {
        return minimumTransparency;
    }
    /**
     * Sets the range used to retrieve stones.
     * @param min the <code>float</code> value which is a minimum
     *            transparency level used to search for stones. It mustn't be
     *            less then zero and greater than one.
     * @param max the <code>float</code> value which is a maximum
     *            transparency level used to search for stones. It mustn't be
     *            less then zero and greater than one.
     * @throws StoneException if arguments are less then zero or greater than
     *                        one; if the <code>min</code> argument is greater
     *                        than the <code>max</code> argument
     */
    public void setRange(final float min, final float max) {
        if ((!FieldValidator.validateTransparency(min))
                || (!FieldValidator.validateTransparency(max))
                || (min > max)) {
            throw new StoneException("Arguments musn't be less then zero "
                    + "and greater than one and the 'min' argument mustn't "
                    + "be greater than the 'max' argument. 'min' = " + min
                    + ", 'max' = " + max + ".");
        }
        minimumTransparency = min;
        maximumTransparency = max;
    }
    /**
     * Determines whether the specified stone has a transparency level which
     * falls into the range represented by this criterion.
     * @param stone the <code>Stone</code> value which is a stone to be
     *              verified
     * @throws StoneException if the argument is a <code>null</code> value
     */
    @Override
    public boolean isRetrieved(final Stone stone) {
        float transparency;
        /* The main part of the method. */
        if (stone == null) {
            throw new StoneException("The 'stone' argument is null.");
        }
        transparency = stone.getTransparencyLevel();
        return (!((transparency < minimumTransparency)
                  || (transparency > maximumTransparency)));
    }
}
