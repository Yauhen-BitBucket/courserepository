/*
 * SearchSpecification.java
 * Represents a search criterion with which to retrieve elements, when they
 * are stored in memory.
 */
package by.training.task01.specification;


/**
 * Represents a search criterion with which to retrieve elements, when they
 * are stored in memory.
 * @author Yauhen Sazonau
 * @param <T> defines the base type of elements which are allowed to be stored
 *            in a collection
 * @version 1.0, 05/04/19
 * @since 1.0
 */
public interface SearchSpecification<T> extends Specification {
    /**
     * Determines whether the specified value complies with a criterion
     * defined by an implementor.
     * @param value the value to be verified
     * @return <code>true</code> if the specified value complies with a
     *         criterion defined by an implementor, <code>false</code>
     *         otherwise
     */
    boolean isRetrieved(T value);
}
