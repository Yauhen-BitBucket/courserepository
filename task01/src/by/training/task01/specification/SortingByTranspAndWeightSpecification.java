/*
 * SortingByTranspAndWeightSpecification.java
 * Represents a criterion with which to sort retrieved elements (all
 * elements are retrieved, but the order matters), when they are stored in
 * memory. All elements will be sorted by transparency level and weight.
 */
package by.training.task01.specification;


import java.util.Comparator;

import by.training.task01.entity.Stone;


/**
 * Represents a criterion with which to sort retrieved elements (all
 * elements are retrieved, but the order matters), when they are stored in
 * memory. All elements will be sorted by transparency level and weight.
 * @author Yauhen Sazonau
 * @version 1.0, 05/14/19
 * @since 1.0
 */
public class SortingByTranspAndWeightSpecification
        implements SortingSpecification<Stone> {
    /**
     * Retrieves a <code>Comparator</code> instance used to sort retrieved
     * stones by transparency level and weight (all elements are retrieved,
     * but the order matters).
     * @return a <code>Comparator</code> instance used to sort retrieved
     *         stones by transparency level and weight
     * @see Comparator
     */
    @Override
    public Comparator<Stone> retrieveComparator() {
        Comparator<Stone> transparencyComparator
                = Comparator.<Stone>comparingDouble(
                        Stone::getTransparencyLevel);
        return (transparencyComparator.thenComparingInt(Stone::getWeight));
    }
}
