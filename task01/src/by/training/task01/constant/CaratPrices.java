/*
 * CaratPrices.java
 * A class for storing carat prices.
 */
package by.training.task01.constant;


/**
 * A class for storing carat prices.
 * @author Yauhen Sazonau
 * @version 1.0, 05/13/19
 * @since 1.0
 */
public final class CaratPrices {
    /**
     * A carat price. The identifier holds the value of "300".
     */
    public static final float CARAT_PRICE_1 = 300f;
    /**
     * A carat price. The identifier holds the value of "32".
     */
    public static final float CARAT_PRICE_2 = 32f;
    /**
     * A carat price. The identifier holds the value of "100".
     */
    public static final float CARAT_PRICE_3 = 100f;
    /**
     * A carat price. The identifier holds the value of "40".
     */
    public static final float CARAT_PRICE_4 = 40f;
    /**
     * A carat price. The identifier holds the value of "99".
     */
    public static final float CARAT_PRICE_5 = 99f;
    /**
     * A carat price. The identifier holds the value of "280".
     */
    public static final float CARAT_PRICE_6 = 280f;
    /**
     * A carat price. The identifier holds the value of "500".
     */
    public static final float CARAT_PRICE_7 = 500f;
    /**
     * A carat price. The identifier holds the value of "10".
     */
    public static final float CARAT_PRICE_8 = 10f;
    /**
     * A carat price. The identifier holds the value of "250".
     */
    public static final float CARAT_PRICE_9 = 250f;
    /**
     * A carat price. The identifier holds the value of "50".
     */
    public static final float CARAT_PRICE_10 = 50f;
    /**
     * A carat price. The identifier holds the value of "600".
     */
    public static final float CARAT_PRICE_11 = 600f;
    /**
     * A carat price. The identifier holds the value of "450".
     */
    public static final float CARAT_PRICE_12 = 450f;
    /**
     * A carat price. The identifier holds the value of "95".
     */
    public static final float CARAT_PRICE_13 = 95f;
    /**
     * A carat price. The identifier holds the value of "130".
     */
    public static final float CARAT_PRICE_14 = 130f;
    /**
     * A carat price. The identifier holds the value of "80".
     */
    public static final float CARAT_PRICE_15 = 80f;
    /**
     * A carat price. The identifier holds the value of "1000".
     */
    public static final float CARAT_PRICE_16 = 1000f;
    /**
     * Constructs an instance of this class.
     */
    private CaratPrices() {
        /* The default initialization is sufficient. */
    }
}
