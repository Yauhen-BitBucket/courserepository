/*
 * Weights.java
 * A class for storing weights.
 */
package by.training.task01.constant;


/**
 * A class for storing weights.
 * @author Yauhen Sazonau
 * @version 1.0, 05/13/19
 * @since 1.0
 */
public final class Weights {
    /**
     * A weight. The identifier holds the value of "10".
     */
    public static final byte WEIGHT_1 = 10;
    /**
     * A weight. The identifier holds the value of "25".
     */
    public static final byte WEIGHT_2 = 25;
    /**
     * A weight. The identifier holds the value of "20".
     */
    public static final byte WEIGHT_3 = 20;
    /**
     * A weight. The identifier holds the value of "100".
     */
    public static final byte WEIGHT_4 = 100;
    /**
     * A weight. The identifier holds the value of "14".
     */
    public static final byte WEIGHT_5 = 14;
    /**
     * A weight. The identifier holds the value of "7".
     */
    public static final byte WEIGHT_6 = 7;
    /**
     * A weight. The identifier holds the value of "30".
     */
    public static final byte WEIGHT_7 = 30;
    /**
     * A weight. The identifier holds the value of "57".
     */
    public static final byte WEIGHT_8 = 57;
    /**
     * A weight. The identifier holds the value of "17".
     */
    public static final byte WEIGHT_9 = 17;
    /**
     * A weight. The identifier holds the value of "40".
     */
    public static final byte WEIGHT_10 = 40;
    /**
     * Constructs an instance of this class.
     */
    private Weights() {
        /* The default initialization is sufficient. */
    }
}
