/**
 * Contains classes which are designed to keep constants. The following
 * classes are considered: a class for keeping carat prices, a class for
 * keeping growth periods, a class for keeping extraction place codes, a class
 * for keeping transparency levels, a class for keeping weights, a class for
 * keeping array indexes.
 * @author Yauhen Sazonau
 * @since 1.0
 */
package by.training.task01.constant;
