/*
 * GrowthPeriods.java
 * A class for storing growth periods.
 */
package by.training.task01.constant;


/**
 * A class for storing growth periods.
 * @author Yauhen Sazonau
 * @version 1.0, 05/13/19
 * @since 1.0
 */
public final class GrowthPeriods {
    /**
     * Growth time. The identifier holds the value of "140".
     */
    public static final short GROWTH_TIME_1 = 140;
    /**
     * Growth time. The identifier holds the value of "200".
     */
    public static final short GROWTH_TIME_2 = 200;
    /**
     * Growth time. The identifier holds the value of "700".
     */
    public static final short GROWTH_TIME_3 = 700;
    /**
     * Growth time. The identifier holds the value of "10".
     */
    public static final short GROWTH_TIME_4 = 10;
    /**
     * Growth time. The identifier holds the value of "300".
     */
    public static final short GROWTH_TIME_5 = 300;
    /**
     * Growth time. The identifier holds the value of "380".
     */
    public static final short GROWTH_TIME_6 = 380;
    /**
     * Growth time. The identifier holds the value of "150".
     */
    public static final short GROWTH_TIME_7 = 150;
    /**
     * Constructs an instance of this class.
     */
    private GrowthPeriods() {
        /* The default initialization is sufficient. */
    }
}
