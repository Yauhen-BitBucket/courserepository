/*
 * PlaceCodes.java
 * A class for storing extraction place codes.
 */
package by.training.task01.constant;


/**
 * A class for storing extraction place codes.
 * @author Yauhen Sazonau
 * @version 1.0, 05/13/19
 * @since 1.0
 */
public final class PlaceCodes {
    /**
     * An extraction place code. The identifier holds the value of "2".
     */
    public static final short PLACE_CODE_1 = 2;
    /**
     * An extraction place code. The identifier holds the value of "11".
     */
    public static final short PLACE_CODE_2 = 11;
    /**
     * An extraction place code. The identifier holds the value of "15".
     */
    public static final short PLACE_CODE_3 = 15;
    /**
     * An extraction place code. The identifier holds the value of "100".
     */
    public static final short PLACE_CODE_4 = 100;
    /**
     * An extraction place code. The identifier holds the value of "5".
     */
    public static final short PLACE_CODE_5 = 5;
    /**
     * An extraction place code. The identifier holds the value of "87".
     */
    public static final short PLACE_CODE_6 = 87;
    /**
     * An extraction place code. The identifier holds the value of "9".
     */
    public static final short PLACE_CODE_7 = 9;
    /**
     * An extraction place code. The identifier holds the value of "6".
     */
    public static final short PLACE_CODE_8 = 6;
    /**
     * An extraction place code. The identifier holds the value of "4".
     */
    public static final short PLACE_CODE_9 = 4;
    /**
     * An extraction place code. The identifier holds the value of "93".
     */
    public static final short PLACE_CODE_10 = 93;
    /**
     * An extraction place code. The identifier holds the value of "1".
     */
    public static final short PLACE_CODE_11 = 1;
    /**
     * Constructs an instance of this class.
     */
    private PlaceCodes() {
        /* The default initialization is sufficient. */
    }
}
