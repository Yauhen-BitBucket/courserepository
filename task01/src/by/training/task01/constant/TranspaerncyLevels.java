/*
 * TranspaerncyLevels.java
 * A class for storing transparency levels.
 */
package by.training.task01.constant;


/**
 * A class for storing transparency levels.
 * @author Yauhen Sazonau
 * @version 1.0, 05/13/19
 * @since 1.0
 */
public final class TranspaerncyLevels {
    /**
     * A transparency level. The identifier holds the value of "0.95".
     */
    public static final float TRANSPARENCY_1 = 0.95f;
    /**
     * A transparency level. The identifier holds the value of "0.57".
     */
    public static final float TRANSPARENCY_2 = 0.57f;
    /**
     * A transparency level. The identifier holds the value of "0.3".
     */
    public static final float TRANSPARENCY_3 = 0.3f;
    /**
     * A transparency level. The identifier holds the value of "0.4".
     */
    public static final float TRANSPARENCY_4 = 0.4f;
    /**
     * A transparency level. The identifier holds the value of "0.1".
     */
    public static final float TRANSPARENCY_5 = 0.1f;
    /**
     * A transparency level. The identifier holds the value of "0.31".
     */
    public static final float TRANSPARENCY_6 = 0.31f;
    /**
     * A transparency level. The identifier holds the value of "0.7".
     */
    public static final float TRANSPARENCY_7 = 0.7f;
    /**
     * A transparency level. The identifier holds the value of "0.9".
     */
    public static final float TRANSPARENCY_8 = 0.9f;
    /**
     * A transparency level. The identifier holds the value of "0.5".
     */
    public static final float TRANSPARENCY_9 = 0.5f;
    /**
     * A transparency level. The identifier holds the value of "0.25".
     */
    public static final float TRANSPARENCY_10 = 0.25f;
    /**
     * A transparency level. The identifier holds the value of "0.37".
     */
    public static final float TRANSPARENCY_11 = 0.37f;
    /**
     * A transparency level. The identifier holds the value of "0.2".
     */
    public static final float TRANSPARENCY_12 = 0.2f;
    /**
     * A transparency level. The identifier holds the value of "0.65".
     */
    public static final float TRANSPARENCY_13 = 0.65f;
    /**
     * A transparency level. The identifier holds the value of "0.05".
     */
    public static final float TRANSPARENCY_14 = 0.05f;
    /**
     * Constructs an instance of this class.
     */
    private TranspaerncyLevels() {
        /* The default initialization is sufficient. */
    }
}
