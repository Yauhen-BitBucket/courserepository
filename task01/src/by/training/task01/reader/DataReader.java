/*
 * DataReader.java
 * A reader. It's used to read information about stones stored in a text file
 * and to create stones based on that information storing them in a
 * repository.
 */
package by.training.task01.reader;


import by.training.task01.entity.Stone;

import by.training.task01.exception.StoneException;

import by.training.task01.factory.StoneFactory;

import by.training.task01.repository.CollectionNecklace;
import by.training.task01.repository.Necklace;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.Reader;

import java.nio.charset.StandardCharsets;

import java.util.Optional;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;


/**
 * A reader. It's used to read information about stones stored in a text file
 * and to create stones based on that information storing them in a
 * repository.
 * @author Yauhen Sazonau
 * @version 1.0, 05/05/19
 * @since 1.0
 */
public class DataReader {
    /**
     * Specifies a number used to indicate that a character is absent in a
     * string. The identifier holds the value of "-1".
     */
    private static final byte ABSENSE_VALUE = -1;
    /**
     * Specifies a position number of the first character in a string. The
     * identifier holds the value of "0".
     */
    private static final byte FIRST_CHARACTER_POSITION = 0;
    /**
     * A logger for this application. <code>null</code> value is prohibited.
     */
    private static final Logger LOGGER
            = LogManager.getLogger(DataReader.class);
    /**
     * Specifies an empty string. The identifier holds the value of "".
     */
    private static final String EMPTY_STRING = "";
    /**
     * Specifies an exception message. The identifier holds the value of
     * "There are problems with reading data.".
     */
    private static final String EXCEPTION_MESSAGE
            = "There are problems with reading data.";
    /**
     * Specifies a relative path to the file which information will be read
     * from. The identifier holds the value of "data/stones.txt".
     */
    private static final String FILE_PATH = "data/stones.txt";
    /**
     * Constructs an instance of this class.
     */
    public DataReader() {
        /* The default initialization is sufficient. */
    }
    /**
     * Reads information about stones stored in a text file and creates stones
     * based on that information storing them in a repository.
     * @return <code>true</code> (always returns <code>true</code> if there
     *         wasn't an exception)
     * @throws StoneException if there are problems with reading information
     */
    public boolean readData() {
        BufferedReader bufferedReader;
        int lineNumber = 1;
        Necklace repository;
        Optional<Stone> stoneOptional;
        Optional<String> line;
        /* The main part of the method. */
        bufferedReader = retrieveReader(FILE_PATH);
        /* Reading data. */
       try {
           line = Optional.ofNullable(bufferedReader.readLine());
           if (!(line.isPresent())) {
               throw new StoneException("There is no data to read.");
           }
           repository = CollectionNecklace.getInstance();
           while (line.isPresent()) {
               stoneOptional = retrieveStone(line.get(), lineNumber);
               if (stoneOptional.isPresent()) {
                   repository.addStone(stoneOptional.get());
               }
               line = readNextLine(bufferedReader);
               lineNumber++;
           }
       } catch (IOException e) {
           throw new StoneException(EXCEPTION_MESSAGE);
       } finally {
           closeReader(bufferedReader);
       }
       return true;
    }
    /**
     * Closes the specified reader.
     * @param reader the <code>Reader</code> value which is a reader to be
     * closed.
     */
    private void closeReader(final Reader reader) {
        try {
            reader.close();
        } catch (IOException e) {
            LOGGER.error("Problems occured while closing an underlying "
                    + "reader.");
        }
    }
    /**
     * Retrieves the next line of data using the specified buffered reader.
     * @param reader the <code>BufferedReader</code> value which is a reader
     *               used to read a line of data.
     * @return the next line of data. <code>null</code> value is prohibited.
     */
    private Optional<String> readNextLine(final BufferedReader reader) {
        String line;
        /* The main part of the method. */
        try {
            line = reader.readLine();
        } catch (IOException e) {
            LOGGER.error(EXCEPTION_MESSAGE);
            line = null;
        }
        return Optional.ofNullable(line);
    }
    /**
     * Retrieves a parameter list used to create a stone from the specified
     * string.
     * @param line the <code>String</code> value which is a line read from a
     *             file
     * @return a parameter list used to create a stone. <code>null</code>
     *         value is prohibited.
     */
    private String parseParameterList(final String line) {
        int length = line.length();
        int startingPosition = line.indexOf(' ');
        /* The main part of the method. */
        if (startingPosition == ABSENSE_VALUE) {
            return EMPTY_STRING;
        } else {
            startingPosition++;
        }
        if (startingPosition == length) {
            return EMPTY_STRING;
        }
        return (line.substring(startingPosition, length));
    }
    /**
     * Retrieves a stone type used to create a stone factory from the
     * specified string.
     * @param line the <code>String</code> value which is a line read from a
     *             file
     * @return a stone type used to create a factory. <code>null</code> value
     *         is prohibited.
     */
    private String parseStoneType(final String line) {
        int endingPosition = line.indexOf(' ');
        /* The main part of the method. */
        if (endingPosition == ABSENSE_VALUE) {
            endingPosition = line.length();
        }
        return (line.substring(FIRST_CHARACTER_POSITION, endingPosition));
    }
    /**
     * Retrieves an instance of the <code>BufferedReader</code> class using
     * the specified file path.
     * @param filePath the <code>String</code> value which is the path to a
     *                 text file.
     * @return a newly created instance of the <code>BufferedReader</code>
     *         class. <code>null</code> value is prohibited.
     * @throws StoneException if there are problems with retrieving an
     *                        instance of the <code>BufferedReader</code>
     *                        class
     */
    private BufferedReader retrieveReader(final String filePath) {
        BufferedReader bufferedReader;
        InputStreamReader streamReader;
        /* The main part of the method. */
        try {
            streamReader
                    = new InputStreamReader(new FileInputStream(filePath),
                            StandardCharsets.UTF_8);
        } catch (FileNotFoundException e) {
            throw new StoneException(EXCEPTION_MESSAGE);
        }
        bufferedReader = new BufferedReader(streamReader);
        return bufferedReader;
    }
    /**
     * Retrieves a stone using the specified string which contains information
     * about it.
     * @param line the <code>String</code> value which is information about a
     *             stone to retrieve
     * @param lineNumber the integer value which is the line number of the
     *                   specified string
     * @return a stone. <code>null</code> value is prohibited.
     */
    private Optional<Stone> retrieveStone(final String line,
            final int lineNumber) {
        Stone stone;
        StoneFactory factory;
        String parameterList;
        String stoneType;
        /* The main part of the method. */
        stoneType = parseStoneType(line);
        parameterList = parseParameterList(line);
        try {
            factory = StoneFactory.getStoneFactory(stoneType);
            stone = factory.createStone(parameterList);
        } catch (StoneException e) {
            LOGGER.error("A stone can't be created, because a line with "
                    + "number '" + lineNumber + "' is incorrect.");
            stone = null;
        }
        return Optional.ofNullable(stone);
    }
}
