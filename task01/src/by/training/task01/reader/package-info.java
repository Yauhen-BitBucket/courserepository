/**
 * Contains a reader used to read information about stones stored in a text
 * file.
 * @author Yauhen Sazonau
 * @since 1.0
 */
package by.training.task01.reader;
