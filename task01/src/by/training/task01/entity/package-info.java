/**
 * Provides classes each of which encapsulates the information about some
 * entity. These entities are: a stone, an artificial stone, a natural stone.
 * The main purpose of this classes is that they are used as data transfer
 * objects.
 * @author Yauhen Sazonau
 * @since 1.0
 */
package by.training.task01.entity;
