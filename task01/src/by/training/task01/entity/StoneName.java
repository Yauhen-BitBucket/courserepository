/*
 * StoneName.java
 * The stone name. A stone can have one of the following names: amber,
 * diamond, emerald, pearl, ruby, sapphire.
 */
package by.training.task01.entity;


/**
 * The stone name. A stone can have one of the following names: amber,
 * diamond, emerald, pearl, ruby, sapphire.
 * @author Yauhen Sazonau
 * @version 1.0, 05/02/19
 * @see by.training.task01.entity.Stone
 * @since 1.0
 */
public enum StoneName {
    /**
     * The name of amber.
     */
    AMBER,
    /**
     * The name of a diamond.
     */
    DIAMOND,
    /**
     * The name of an emerald.
     */
    EMERALD,
    /**
     * The name of a pearl.
     */
    PEARL,
    /**
     * The name of a ruby.
     */
    RUBY,
    /**
     * The name of a sapphire.
     */
    SAPPHIRE,
}
