/*
 * NaturalStone.java
 * A natural stone. The main difference between it and a general stone is
 * that natural stones are characterized by the place of extraction.
 */
package by.training.task01.entity;


/**
 * A natural stone. The main difference between it and a general stone is
 * that natural stones are characterized by the place of extraction.
 * @author Yauhen Sazonau
 * @version 1.0, 05/02/19
 * @since 1.0
 */
public class NaturalStone extends Stone {
    /**
     * The code of a place where a stone was extracted. It must be positive.
     */
    private short extractionPlaceCode;
    /**
     * Constructs an instance of this class using the specified carat price,
     * name, transparency level, weight and code of a place of extraction.
     * @param name the <code>StoneName</code> value which is the name of
     *             a stone
     * @param placeCode the <code>short</code> value which is a stone's code
     *                  of a place of extraction
     * @param price the <code>float</code> value which is the price per one
     *              carat of a stone
     * @param stoneWeight the <code>byte</code> value which is the weight in
     *                    carats of a stone
     * @param transparency the <code>float</code> value which is the
     *                     transparency level of a stone
     */
    public NaturalStone(final StoneName name, final short placeCode,
            final float price, final byte stoneWeight,
            final float transparency) {
        super(name, price, stoneWeight, transparency);
        this.extractionPlaceCode = placeCode;
    }
    /**
     * Retrieves the code of a place where this stone was extracted.
     * @return the code of a place where this stone was extracted
     */
    public short getExtractionPlaceCode() {
        return extractionPlaceCode;
    }
    /**
     * Sets the code of a place where this stone was extracted.
     * @param placeCode the <code>short</code> value which is a code
     *                  of a place of extraction to be set
     */
    public void setExtractionPlaceCode(final short placeCode) {
        this.extractionPlaceCode = placeCode;
    }
    /**
     * Indicates whether some other object is "equal to" this one.
     * @param object the <code>Object</code> value that is an object with
     *               which to compare. <code>null</code> value is possible.
     * @return <code>true</code> if this object is the same as the object
     *         supplied through the <code>object</code> argument;
     *         <code>false</code> otherwise
     */
    @Override
    public boolean equals(final Object object) {
        NaturalStone stone;
        boolean result = true;
        /* The main part of the method. */
        if (!super.equals(object)) {
            return false;
        }
        /* The class of the supplied object should be the same or extend this
         * class.
         */
        result = object instanceof NaturalStone;
        if (result) {
            stone = (NaturalStone) object;
            result = (extractionPlaceCode == stone.extractionPlaceCode);
        }
        return result;
    }
    /**
     * Retrieves a hash code value for this object.
     * @return a hash code value for this object
     */
    @Override
    public int hashCode() {
        return ((Short.hashCode(extractionPlaceCode)
                 * (Stone.HASH_CODE_COEFFICIENT ^ Stone.POWER_COEFFICIENT_4))
                + super.hashCode());
    }
    /**
     * Returns a string representation of this object.
     * @return a string representation of the object. <code>null</code> value
     *         is prohibited.
     */
    @Override
    public String toString() {
        String string;
        /* The main part of the method. */
        string = super.toString() + "\nExtraction place code: "
                 + extractionPlaceCode;
        return string;
    }
}
