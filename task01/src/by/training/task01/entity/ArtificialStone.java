/*
 * ArtificialStone.java
 * An artificial stone. The main difference between it and a general stone is
 * that artificial stones are characterized by the time needed to produce it.
 */
package by.training.task01.entity;


/**
 * An artificial stone. The main difference between it and a general stone is
 * that artificial stones are characterized by the time needed to produce it.
 * @author Yauhen Sazonau
 * @version 1.0, 05/02/19
 * @since 1.0
 */
public class ArtificialStone extends Stone {
    /**
     * The number of days needed to produce a stone. It must be positive.
     */
    private short growthTime;
    /**
     * Constructs an instance of this class using the specified carat price,
     * name, transparency level, weight and growth time.
     * @param growthPeriod the <code>short</code> value which is the number of
     *                     days needed to produce a stone
     * @param name the <code>StoneName</code> value which is the name of a
     *             stone
     * @param price the <code>float</code> value which is the price per one
     *              carat of a stone
     * @param stoneWeight the <code>byte</code> value which is the weight in
     *                    carats of a stone
     * @param transparency the <code>float</code> value which is the
     *                     transparency level of a stone
     */
    public ArtificialStone(final short growthPeriod, final StoneName name,
            final float price, final byte stoneWeight,
            final float transparency) {
        super(name, price, stoneWeight, transparency);
        this.growthTime = growthPeriod;
    }
    /**
     * Retrieves the number of days needed to produce this stone.
     * @return the number of days needed to produce this stone
     */
    public short getGrowthTime() {
        return growthTime;
    }
    /**
     * Sets the number of days needed to produce this stone.
     * @param growthPeriod the <code>short</code> value which is the number of
     *                     days to be set
     */
    public void setGrowthTime(final short growthPeriod) {
        this.growthTime = growthPeriod;
    }
    /**
     * Indicates whether some other object is "equal to" this one.
     * @param object the <code>Object</code> value that is an object with
     *               which to compare. <code>null</code> value is possible.
     * @return <code>true</code> if this object is the same as the object
     *         supplied through the <code>object</code> argument;
     *         <code>false</code> otherwise
     */
    @Override
    public boolean equals(final Object object) {
        ArtificialStone stone;
        boolean result = true;
        /* The main part of the method. */
        if (!super.equals(object)) {
            return false;
        }
        /* The class of the supplied object should be the same or extend this
         * class.
         */
        result = object instanceof ArtificialStone;
        if (result) {
            stone = (ArtificialStone) object;
            result = (growthTime == stone.growthTime);
        }
        return result;
    }
    /**
     * Retrieves a hash code value for this object.
     * @return a hash code value for this object
     */
    @Override
    public int hashCode() {
        return ((Short.hashCode(growthTime)
                 * (Stone.HASH_CODE_COEFFICIENT ^ Stone.POWER_COEFFICIENT_4))
                + (super.hashCode()));
    }
    /**
     * Returns a string representation of this object.
     * @return a string representation of the object. <code>null</code> value
     *         is prohibited.
     */
    @Override
    public String toString() {
        String string;
        /* The main part of the method. */
        string = super.toString() + "\nGrowth time: " + growthTime;
        return string;
    }
}
