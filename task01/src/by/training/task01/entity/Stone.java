/*
 * Stone.java
 * A stone. The base class in the stone hierarchy. Contains fields for
 * storing information which is common for all stones.
 */
package by.training.task01.entity;


/**
 * A stone. The base class in the stone hierarchy. Contains fields for
 * storing information which is common for all stones.
 * @author Yauhen Sazonau
 * @version 1.0, 05/02/19
 * @since 1.0
 */
public abstract class Stone {
    /*
     * This class is abstract. It's not intended for direct instantiation.
     * More specific versions must be defined.
     */
    /**
     * A coefficient used to calculate a hash code value of an object. The
     * identifier holds the value of "31".
     */
    public static final byte HASH_CODE_COEFFICIENT = 31;
    /**
     * A power coefficient used to calculate a hash code value of an object.
     * The identifier holds the value of "4".
     */
    public static final byte POWER_COEFFICIENT_4 = 4;
    /**
     * A power coefficient used to calculate a hash code value of an object.
     * The identifier holds the value of "3".
     */
    private static final byte POWER_COEFFICIENT_3 = 3;
    /**
     * A power coefficient used to calculate a hash code value of an object.
     * The identifier holds the value of "2".
     */
    private static final byte POWER_COEFFICIENT_2 = 2;
    /**
     * The price per one carat of this stone. It must be positive.
     */
    private float caratPrice;
    /**
     * The identification number of a stone. It must be positive.
     */
    private long stoneId;
    /**
     * The name of this stone. <code>null</code> is prohibited.
     */
    private StoneName stoneName;
    /**
     * The level of transparency of this stone. It could take a value from
     * zero to one(both sides inclusively). The value of zero corresponds to
     * a completely transparent stone; the value of one - to a completely
     * opaque.
     */
    private float transparencyLevel;
    /**
     * The weight in carats of this stone. It must be positive.
     */
    private byte weight;
    /**
     * Constructs an instance of this class using the specified carat price,
     * name, transparency level and weight.
     * @param name the <code>StoneName</code> value which is the name of a
     *             stone
     * @param price the <code>float</code> value which is the price per one
     *              carat of a stone
     * @param stoneWeight the <code>byte</code> value which is the weight in
     *                    carats of a stone
     * @param transparency the <code>float</code> value which is the
     *                     transparency level of a stone
     */
    public Stone(final StoneName name, final float price,
            final byte stoneWeight, final float transparency) {
        this.caratPrice = price;
        this.stoneName = name;
        this.transparencyLevel = transparency;
        this.weight = stoneWeight;
    }
    /**
     * Retrieves the price per one carat of this stone.
     * @return the price per one carat of this stone
     */
    public float getCaratPrice() {
        return caratPrice;
    }
    /**
     * Sets the price per one carat of this stone.
     * @param price the <code>float</code> value which is the price per one
     *              carat to be set
     */
    public void setCaratPrice(final float price) {
        this.caratPrice = price;
    }
    /**
     * Retrieves the identification number of this stone.
     * @return the identification number of this stone
     */
    public long getStoneId() {
        return stoneId;
    }
    /**
     * Sets the identification number of this stone.
     * @param id the <code>long</code> value which is an identification number
     *           to be set
     */
    public void setStoneId(final long id) {
        this.stoneId = id;
    }
    /**
     * Retrieves the name of this stone.
     * @return the name of this stone
     */
    public StoneName getStoneName() {
        return stoneName;
    }
    /**
     * Sets the name of this stone.
     * @param name the <code>StoneName</code> value which is a name to be set
     */
    public void setStoneName(final StoneName name) {
        this.stoneName = name;
    }
    /**
     * Retrieves the level of transparency of this stone.
     * @return the level of transparency of this stone
     */
    public float getTransparencyLevel() {
        return transparencyLevel;
    }
    /**
     * Sets the level of transparency of this stone.
     * @param transparency the <code>float</code> value which is the
     *                     transparency level to be set
     */
    public void setTransparencyLevel(final float transparency) {
        this.transparencyLevel = transparency;
    }
    /**
     * Retrieves the weight in carats of this stone.
     * @return the weight in carats of this stone
     */
    public byte getWeight() {
        return weight;
    }
    /**
     * Sets the weight in carats of this stone.
     * @param stoneWeight the <code>byte</code> value which is the weight in
     *                    carats to be set
     */
    public void setWeight(final byte stoneWeight) {
        this.weight = stoneWeight;
    }
    /**
     * Indicates whether some other object is "equal to" this one.
     * @param object the <code>Object</code> value that is an object with
     *               which to compare. <code>null</code> value is possible.
     * @return <code>true</code> if this object is the same as the object
     *         supplied through the <code>object</code> argument;
     *         <code>false</code> otherwise
     */
    @Override
    public boolean equals(final Object object) {
        boolean result = true;
        Stone stone;
        /* The main part of the method. */
        if (this == object) {
            return true;
        } else if (object == null) {
            return false;
        }
        /* The supplied object should belong to the stone hierarchy. */
        result = object instanceof Stone;
        if (result) {
            stone = (Stone) object;
            result = (Double.compare(caratPrice, stone.caratPrice) == 0)
                     && (stoneName == stone.stoneName)
                     && (Double.compare(transparencyLevel,
                             stone.transparencyLevel) == 0)
                     && (weight == stone.weight);
        }
        return result;
    }
    /**
     * Retrieves a hash code value for this object.
     * @return a hash code value for this object
     */
    @Override
    public int hashCode() {
        return ((Float.hashCode(caratPrice)
                 * (HASH_CODE_COEFFICIENT ^ POWER_COEFFICIENT_3))
                + (stoneName.hashCode()
                   * (HASH_CODE_COEFFICIENT ^ POWER_COEFFICIENT_2))
                + (Float.hashCode(transparencyLevel) * HASH_CODE_COEFFICIENT)
                + (Byte.hashCode(weight)));
    }
    /**
     * Returns a string representation of this object.
     * @return a string representation of the object. <code>null</code> value
     *         is prohibited.
     */
    @Override
    public String toString() {
        String string;
        /* The main part of the method. */
        string = "Stone properties:" + "\nPrice per carat: " + caratPrice
                 + "\nStone name: " + stoneName + "\nTransparency level: "
                 + transparencyLevel + "\nWeight: " + weight;
        return string;
    }
}
