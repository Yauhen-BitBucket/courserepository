/*
 * FieldValidator.java
 * Represents a validator which contains methods to verify whether a field
 * value is correct.
 */
package by.training.task01.validator;


/**
 * Represents a validator which contains methods to verify whether a field
 * value is correct.
 * @author Yauhen Sazonau
 * @version 1.0, 05/05/19
 * @since 1.0
 */
public final class FieldValidator {
    /* Contains only static methods. */
    /**
     * Represents the maximum value the growth time can have. The identifier
     * holds the value of "1100".
     */
    public static final short GROWTH_TIME_MAX_VALUE = 1100;
    /**
     * Represents the minimum value the growth time can have. The identifier
     * holds the value of "1".
     */
    public static final short GROWTH_TIME_MIN_VALUE = 1;
    /**
     * Represents the maximum value the extraction place code can have. The
     * identifier holds the value of "100".
     */
    public static final short PLACE_CODE_MAX_VALUE = 100;
    /**
     * Represents the minimum value the extraction place code can have. The
     * identifier holds the value of "1".
     */
    public static final short PLACE_CODE_MIN_VALUE = 1;
    /**
     * Represents the maximum value the carat price can have. The identifier
     * holds the value of "10000".
     */
    public static final float PRICE_MAX_VALUE = 10000;
    /**
     * Represents the minimum value the carat price can have. The identifier
     * holds the value of "1".
     */
    public static final float PRICE_MIN_VALUE = 1;
    /**
     * Represents the maximum value the transparency level can have. The
     * identifier holds the value of "1".
     */
    public static final float TRANSPARENCY_MAX_VALUE = 1;
    /**
     * Represents the minimum value the transparency level can have. The
     * identifier holds the value of "0".
     */
    public static final float TRANSPARENCY_MIN_VALUE = 0;
    /**
     * Represents the maximum value the weight can have. The identifier holds
     * the value of "127".
     */
    public static final byte WEIGHT_MAX_VALUE = 127;
    /**
     * Represents the minimum value the weight can have. The identifier holds
     * the value of "1".
     */
    public static final byte WEIGHT_MIN_VALUE = 1;
    /**
     * Constructs an instance of this class. It shouldn't be called.
     */
    private FieldValidator() {
        /* It shouldn't be implemented. */
    }
    /**
     * Verifies whether the specified carat price is correct.
     * @param caratPrice the <code>float</code> value which is the price per
     *                   one carat of a stone. It should be a value which
     *                   falls into an allowed range of values for the carat
     *                   price.
     * @return <code>true</code> if the specified carat price is correct;
     *         <code>false</code>
     */
    public static boolean validateCaratPrice(final float caratPrice) {
        return (!((caratPrice < PRICE_MIN_VALUE)
                  || (caratPrice > PRICE_MAX_VALUE)));
    }
    /**
     * Verifies whether the specified growth time is correct.
     * @param growthTime the <code>short</code> value which is the number of
     *                   days needed to produce a stone. It should be a value
     *                   which falls into an allowed range of values for the
     *                   growth time.
     * @return <code>true</code> if the specified growth time is correct;
     *         <code>false</code>
     */
    public static boolean validateGrowthTime(final short growthTime) {
        return (!((growthTime < GROWTH_TIME_MIN_VALUE)
                  || (growthTime > GROWTH_TIME_MAX_VALUE)));
    }
    /**
     * Verifies whether the specified extraction place code is correct.
     * @param placeCode the <code>short</code> value which is a stone's code
     *                  of a place of extraction. It should be a value which
     *                  falls into an allowed range of values for the
     *                  extraction place code.
     * @return <code>true</code> if the specified extraction place code is
     *         correct; <code>false</code>
     */
    public static boolean validatePlaceCode(final short placeCode) {
        return (!((placeCode < PLACE_CODE_MIN_VALUE)
                  || (placeCode > PLACE_CODE_MAX_VALUE)));
    }
    /**
     * Verifies whether the specified transparency level is correct.
     * @param transparency the <code>float</code> value which is the
     *                     transparency level of a stone. It should be a value
     *                     which falls into an allowed range of values for the
     *                     transparency level.
     * @return <code>true</code> if the specified transparency level is
     *         correct; <code>false</code>
     */
    public static boolean validateTransparency(final float transparency) {
        return (!((transparency < TRANSPARENCY_MIN_VALUE)
                  || (transparency > TRANSPARENCY_MAX_VALUE)));
    }
    /**
     * Verifies whether the specified carat price, transparency level, weight
     * are correct.
     * @param caratPrice the <code>float</code> value which is the price per
     *                   one carat of a stone. It should be a value which
     *                   falls into an allowed range of values for the carat
     *                   price.
     * @param transparency the <code>float</code> value which is the
     *                     transparency level of a stone. It should be a value
     *                     which falls into an allowed range of values for the
     *                     transparency level.
     * @param weight the <code>byte</code> value which is the weight in carats
     *               of a stone. It should be a value which falls into an
     *               allowed range of values for the weight.
     * @return <code>true</code> if the specified carat price, transparency
     *         level, weight are correct; <code>false</code>
     */
    public static boolean validateTriplet(final float caratPrice,
            final float transparency, final byte weight) {
        return ((validateCaratPrice(caratPrice))
                && (validateTransparency(transparency))
                && (validateWeight(weight)));
    }
    /**
     * Verifies whether the specified weight is correct.
     * @param weight the <code>byte</code> value which is the weight in carats
     *               of a stone. It should be a value which falls into an
     *               allowed range of values for the weight.
     * @return <code>true</code> if the specified weight is correct;
     *         <code>false</code>
     */
    public static boolean validateWeight(final byte weight) {
        return (!((weight < WEIGHT_MIN_VALUE)
                  || (weight > WEIGHT_MAX_VALUE)));
    }
}
