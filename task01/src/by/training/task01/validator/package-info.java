/**
 * Contains validator classes used to verify data.
 * @author Yauhen Sazonau
 * @since 1.0
 */
package by.training.task01.validator;
