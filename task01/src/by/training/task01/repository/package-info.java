/**
 * Contains a repository interface which defines the basic contract for a
 * storage of stones and some concrete implementations.
 * @author Yauhen Sazonau
 * @since 1.0
 */
package by.training.task01.repository;
