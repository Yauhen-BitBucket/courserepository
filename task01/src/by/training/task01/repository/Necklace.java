/*
 * Necklace.java
 * A necklace. Defines the basic contract for a storage of stones. This
 * abstraction complies with the Repository pattern.
 */
package by.training.task01.repository;


import by.training.task01.entity.Stone;

import by.training.task01.specification.Specification;

import java.util.List;


/**
 * A necklace. Defines the basic contract for a storage of stones. This
 * abstraction complies with the Repository pattern.
 * @author Yauhen Sazonau
 * @version 1.0, 05/04/19
 * @since 1.0
 */
public interface Necklace {
    /**
     * Adds the specified stone to this necklace (storage).
     * @param stone the <code>Stone</code> value which is a stone to be added
     * @return <code>true</code> if the specified stone was added;
     *         <code>false</code> otherwise
     */
    boolean addStone(Stone stone);
    /**
     * Calculates the cost of this necklace.
     * @return the cost of this necklace
     */
    float getCost();
    /**
     * Calculates the weight of this necklace.
     * @return the weight of this necklace
     */
    int getWeight();
    /**
     * Retrieves stones which comply with the specified criterion.
     * @param specification the <code>Specification</code> value that is a
     *                      criterion with which to retrieve stones
     * @return a set of stones which comply with the specified criterion. If
     *         there are none, an empty set is returned. <code>null</code>
     *         value is prohibited.
     */
    List<Stone> query(Specification specification);
    /**
     * Removes the specified stone from this necklace (storage).
     * @param stone the <code>Stone</code> value which is a stone to be
     *              removed
     * @return <code>true</code> if the specified stone was removed;
     *         <code>false</code> otherwise
     */
    boolean removeStone(Stone stone);
}
