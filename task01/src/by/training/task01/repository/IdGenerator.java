/*
 * IdGenerator.java
 * A generator used to retrieve unique identification numbers.
 */
package by.training.task01.repository;


import by.training.task01.exception.StoneException;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;


/**
 * A generator used to retrieve unique identification numbers.
 * @author Yauhen Sazonau
 * @version 1.0, 05/12/19
 * @since 1.0
 */
public final class IdGenerator {
    /* Implements the Singleton pattern. */
    /**
     * Represents the minimum value the identification number can have. The
     * identifier holds the value of "1".
     */
    public static final long MIN_ID = 1;
    /**
     * Represents a single instance of this class. <code>null</code> value is
     * prohibited.
     */
    private static IdGenerator instance;
    /**
     * Represents the next identification number to be retrieved.
     */
    private long nextId = MIN_ID;
    /**
     * A collection to store identification numbers which have been made free.
     */
    private Set<Long> unusedIds = new LinkedHashSet<>();
    /**
     * Constructs an instance of this class. You have no ability to create an
     * instance of this class using this constructor. You should use the
     * <code>getInstance</code> method.
     * @see #getInstance()
     */
    private IdGenerator() {
        /* The default initialization is sufficient. */
    }
    /**
     * Retrieves an instance if this class.
     * @return a newly created instance of this class or an existing if it has
     *         been created before
     */
    public static IdGenerator getInstance() {
        if (instance == null) {
            instance = new IdGenerator();
        }
        return instance;
    }
    /**
     * Retrieves the next identification number.
     * @return the next identification number. The returned id is newly
     *         generated or retrieved from pool of free identification
     *         numbers.
     */
    public long retrieveNextId() {
        Iterator<Long> iterator;
        long id;
        if (unusedIds.isEmpty()) {
            id = nextId++;
        } else {
            iterator = unusedIds.iterator();
            id = iterator.next();
            unusedIds.remove(id);
        }
        return id;
    }
    /**
     * Returns the specified identification number to an underlying pool.
     * @param id the <code>long</code> value which is an identification number
     *           to be returned. It mustn't be less than the minimum
     *           identification number value.
     * @return <code>true</code> if the specified identification number was
     *         returned to an underlying pool; <code>false</code> otherwise
     *         (you cannot return a value which hasn't been yet retrieved)
     * @throws StoneException if the argument is less than the minimum
     *                        identification number value
     */
    public boolean returnId(final long id) {
        if (id < MIN_ID) {
            throw new StoneException("The argument musn't be less that the "
                    + "minimum identification number value");
        }
        if (id >= nextId) {
            return false;
        }
        return (unusedIds.add(id));
    }
}
