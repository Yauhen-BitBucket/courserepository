/*
 * CollectionNecklace.java
 * Represents a necklace using a collection as its underlying implementation.
 */
package by.training.task01.repository;


import by.training.task01.entity.Stone;

import by.training.task01.exception.StoneException;

import by.training.task01.specification.Specification;
import by.training.task01.specification.SearchSpecification;
import by.training.task01.specification.SortingSpecification;

import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;


/**
 * Represents a necklace using a collection as its underlying implementation.
 * @author Yauhen Sazonau
 * @version 1.0, 05/06/19
 * @since 1.0
 */
public final class CollectionNecklace implements Necklace {
    /* Implements the Singleton pattern. */
    /**
     * Represents a single instance of this class. <code>null</code> value is
     * prohibited.
     */
    private static CollectionNecklace instance;
    /**
     * A logger for this application. <code>null</code> value is prohibited.
     */
    private static final Logger LOGGER
            = LogManager.getLogger(CollectionNecklace.class);
    /**
     * An underlying collection to store stones which make up this necklace.
     */
    private LinkedList<Stone> collection = new LinkedList<>();
    /**
     * Constructs an instance of this class. You have no ability to create an
     * instance of this class using this constructor. You should use the
     * <code>getInstance</code> method.
     * @see #getInstance()
     */
    private CollectionNecklace() {
        /* The default initialization is sufficient. */
    }
    /**
     * Retrieves an instance if this class.
     * @return a newly created instance of this class or an existing if it has
     *         been created before
     */
    public static CollectionNecklace getInstance() {
        if (instance == null) {
            instance = new CollectionNecklace();
        }
        return instance;
    }
    /**
     * Adds the specified stone to this necklace (storage).
     * @param stone the <code>Stone</code> value which is a stone to be added
     * @return <code>true</code> if the specified stone was added;
     *         <code>false</code> otherwise
     * @throws StoneException if the argument is a <code>null</code> value
     */
    public boolean addStone(final Stone stone) {
        boolean result;
        IdGenerator idGenerator;
        /* The main part of the method. */
        if (stone == null) {
            throw new StoneException("The 'stone' argument is null.");
        }
        idGenerator = IdGenerator.getInstance();
        stone.setStoneId(idGenerator.retrieveNextId());
        result = collection.add(stone);
        if (result) {
            LOGGER.info("A stone with the name '" + stone.getStoneName()
                    + "' was added to a necklace.");
        } else {
            idGenerator.returnId(stone.getStoneId());
        }
        return result;
    }
    /**
     * Calculates the cost of this necklace.
     * @return the cost of this necklace
     */
    public float getCost() {
        byte weight;
        float caratPrice;
        float cost = 0;
        Iterator<Stone> iterator = collection.iterator();
        Stone stone;
        /* The main part of the method. */
        while (iterator.hasNext()) {
            stone = iterator.next();
            caratPrice = stone.getCaratPrice();
            weight = stone.getWeight();
            cost += caratPrice * weight;
        }
        return cost;
    }
    /**
     * Calculates the weight of this necklace.
     * @return the weight of this necklace
     */
    public int getWeight() {
        int weight = 0;
        Iterator<Stone> iterator = collection.iterator();
        Stone stone;
        /* The main part of the method. */
        while (iterator.hasNext()) {
            stone = iterator.next();
            weight += stone.getWeight();
        }
        return weight;
    }
    /**
     * Retrieves stones which comply with the specified criterion.
     * @param specification the <code>Specification</code> value that is a
     *                      criterion with which to retrieve stones. The
     *                      expected type of the specification is
     *                      <code>SearchSpecification</code> or
     *                      <code>SortingSpecification</code>.
     * @return a set of stones which comply with the specified criterion. If
     *         there are none, an empty set is returned. <code>null</code>
     *         value is prohibited.
     * @throws StoneException if the wrong type of the specification is given
     *                        or the argument is a <code>null</code> value
     * @see SearchSpecification
     * @see SortingSpecification
     */
    @SuppressWarnings("unchecked")
    public List<Stone> query(final Specification specification) {
        Comparator<Stone> comparator;
        SearchSpecification<Stone> searchSpec;
        SortingSpecification<Stone> sortingSpec;
        Iterator<Stone> iterator = collection.iterator();
        LinkedList<Stone> stones = new LinkedList<>();
        Stone stone;
        /* The main part of the method. */
        if (specification == null) {
            throw new StoneException("The 'specification' argument is null.");
        }
        if (specification instanceof SearchSpecification<?>) {
            searchSpec = (SearchSpecification<Stone>) specification;
            while (iterator.hasNext()) {
                stone = iterator.next();
                if (searchSpec.isRetrieved(stone)) {
                    stones.add(stone);
                }
            }
        } else if (specification instanceof SortingSpecification<?>) {
            stones.addAll(collection);
            sortingSpec = (SortingSpecification<Stone>) specification;
            comparator = sortingSpec.retrieveComparator();
            stones.sort(comparator);
        } else {
            throw new StoneException("The wrong type of the specification is "
                    + "given. The expected type is "
                    + "'SearchSpecification' or 'SortingSpecification'.");
        }
        return stones;
    }
    /**
     * Removes the specified stone from this necklace (storage). The removed
     * stone is the first stone which "equals" to the specified stone.
     * @param stone the <code>Stone</code> value which is a stone to be
     *              removed
     * @return <code>true</code> if the specified stone was removed;
     *         <code>false</code> otherwise
     * @throws StoneException if the argument is a <code>null</code> value
     */
    public boolean removeStone(final Stone stone) {
        boolean result;
        IdGenerator idGenerator;
        /* The main part of the method. */
        if (stone == null) {
            throw new StoneException("The 'stone' argument is null.");
        }
        result = collection.remove(stone);
        if (result) {
            LOGGER.info("A stone with the name '" + stone.getStoneName()
                    + "' was removed from a necklace.");
            idGenerator = IdGenerator.getInstance();
            idGenerator.returnId(stone.getStoneId());
        }
        return result;
    }
}
