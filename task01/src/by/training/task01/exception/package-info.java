/**
 * Provides various exception classes.
 * @author Yauhen Sazonau
 * @since 1.0
 */
package by.training.task01.exception;
