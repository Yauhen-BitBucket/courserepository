The necklace project. The hierarchy of stones was defined (it consists of a
general stone and two descendants - an artificial stone and a natural stone). A
repository (necklace) of stones is defined. It's possible to calculate the
total weight and the total cost of the necklace. The necklace can be queried to
retrieve stones based on the specified criterion. An application has the
following structure:

    - constant      classes with constants
	- entity: 		entities of data domain
	- exception: 		exceptions used by the application
	- factory:		factories to create entities
	- reader:		reader for reading application data
	- repository:		storage for entities
	- specification:	criteria for querying the repository
	- validator:		validators of various data

The following design patterns was used: the Singleton pattern (for the
repository,for the id generator), the Factory Method pattern (for creating
entities), the Repository
pattern (for the repository).

Used technologies: TestNG, Log4j2.

Needed libraries: log4j-api-2.11.2.jar, log4j-core-2.11.2.jar,
jcommander-1.72.jar, testng-6.9.9.jar.

|----------------------------------------------------------------------|
| Important note: in Eclipse TestNG plug-in version 6.9.10 requires an |
|                 Internet connection for test to be run (otherwise    |
|                 problems with DTD).                                  |
|----------------------------------------------------------------------|